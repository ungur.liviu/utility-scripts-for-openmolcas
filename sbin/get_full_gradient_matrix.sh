#!/bin/bash

get_full_gradient_matrix() {

local ms=$1
local nroots=$2
local natoms=$3

declare -a Xmatrix=()
declare -a Ymatrix=()
declare -a Zmatrix=()
declare -a LABEL=( )
declare -a X=( )
declare -a Y=( )
declare -a Z=( )

local nrows=$nroots
local ncolumns=$nroots
local atom=0;

local READFILE=$CurrDir/"$Project"_alaska_ms"$ms"_init_full.out
local WRITEFILE=$CurrDir/"$Project"_alaska_ms"$ms"_gradient_and_nac.out

if [ ! -f $READFILE ]; then
   echo get_full_gradient_matrix::  FILE=$READFILE does NOT exist.
   exit 9
fi
if [ -f $WRITEFILE ]; then rm -rf $WRITEFILE; fi

# read the ALASKA_ININTIAL_GREP file
local index_tri=0
while read -a line; do
    COLS=${#line[@]}
    if [ ! -z "${line[0]}" ] ; then
        LABEL[$index_tri]=${line[0]}
            X[$index_tri]=${line[1]}
            Y[$index_tri]=${line[2]}
            Z[$index_tri]=${line[3]}
        echo $index_tri   ${LABEL[$index_tri]}  ${X[$index_tri]}  ${Y[$index_tri]}  ${Z[$index_tri]} >> $CurrDir/"$Project"_alaska_ms"$ms"_init_full.out2
        ((index_tri++))
    fi
done < $READFILE


echo get_full_gradient_matrix:: Number of atoms    $natoms #>> $WRITEFILE
echo get_full_gradient_matrix:: Number of roots    $nroots #>> $WRITEFILE
echo get_full_gradient_matrix:: Spin multiplicity  $ms     #>> $WRITEFILE
echo get_full_gradient_matrix:: Index              $index  #>> $WRITEFILE
echo get_full_gradient_matrix:: roots_in_ALASKA[@] ${roots_in_ALASKA[@]}


for ((atom=0;atom<$natoms;atom++)); do

    xindex=$atom;
    echo "${LABEL[$xindex]} X direction" >> $WRITEFILE
    for r1 in ${roots_in_ALASKA[@]}; do
        for r2 in ${roots_in_ALASKA[@]}; do
            if [[ $r1 -le $r2 ]] ; then
                Xmatrix[$r1,$r2]=${X[$xindex]}
                echo "${LABEL[$xindex]}   1   $r1   $r2    ${Xmatrix[$r1,$r2]}   "                   >> $WRITEFILE
                xindex=`expr $xindex + $natoms`
            fi
        done
    done
    echo "------------------------------" >> $WRITEFILE

    yindex=$atom
    echo  ${LABEL[$yindex]} Y direction                              >> $WRITEFILE
    for r1 in ${roots_in_ALASKA[@]}; do
        for r2 in ${roots_in_ALASKA[@]}; do
            if [[ $r1 -le $r2 ]] ; then
                Ymatrix[$r1,$r2]=${Y[$yindex]}
                echo "${LABEL[$yindex]}   2   $r1    $r2    ${Ymatrix[$r1,$r2]}   "                   >> $WRITEFILE
                yindex=`expr $yindex + $natoms`
            fi
        done
    done
    echo "------------------------------" >> $WRITEFILE

    zindex=$atom
    echo  ${LABEL[$zindex]} Z direction                              >> $WRITEFILE
    for r1 in ${roots_in_ALASKA[@]}; do
        for r2 in ${roots_in_ALASKA[@]}; do
            if [[ $r1 -le $r2 ]] ; then
                Zmatrix[$r1,$r2]=${Z[$zindex]}
                echo "${LABEL[$zindex]}   3   $r1    $r2    ${Zmatrix[$r1,$r2]}   "                   >> $WRITEFILE
                zindex=`expr $zindex + $natoms`
            fi
        done
    done
    echo "------------------------------" >> $WRITEFILE

done
}

