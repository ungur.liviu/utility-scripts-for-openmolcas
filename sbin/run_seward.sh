#!/bin/bash

#------------------------------------------
run_seward(){
   local Complex=$1
   local bas1=$2
   local bas2=$3
   local me=$( echo "$4" | tr '[:lower:]' '[:upper:]' ) # make uppercase
   local r=$5

   echo "run_seward::  Complex="$Complex
   echo "run_seward::   bas1  ="$bas1
   echo "run_seward::   bas2  ="$bas2
   echo "run_seward::   me    ="$me
   echo "run_seward::   r     ="$r

   if [ -z $me ]; then
      echo "run_seward::   me= string is empty. Exit"
      exit 99
   fi

   export WorkDir=$RootDir

#  check_previous_runs
   old_run=0
   if [ -f $CurrDir/"$Project"_seward.out ]; then
      old_run=`grep 'Stop Module:' $CurrDir/"$Project"_seward.out | grep "seward" | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
   fi

  if [ ! -f $CurrDir/"$Project"_seward.out ] || [ "$old_run" -ne 1 ] || [ ! "$(ls -A $WorkDir)" ] ; then
     if [ -f $CurrDir/$Complex.xyz ]; then
        echo File $CurrDir/$Complex.xyz was found. Proceed to process it.
        dos2unix $CurrDir/$Complex.xyz

        # get the number of atoms from the input XYZ file
        natoms=$(head -n 1 $Complex.xyz)
        echo "run_seward::   natoms="$natoms

        tail -n +3 $CurrDir/$Complex.xyz > $WorkDir/$Complex.coords.tmp12
        cat $WorkDir/$Complex.coords.tmp12

        tail -n +3 $CurrDir/$Complex.xyz | grep -v '^$' > $WorkDir/$Complex.coords.tmp
     else
        echo File $CurrDir/$Complex.xyz was NOT found. ERROR. Stop Now.
        exit 9
     fi

      cat  $WorkDir/$Complex.coords.tmp


      declare -a LABEL=( )
      declare -a X=( )
      declare -a Y=( )
      declare -a Z=( )

      echo "Coords read from the XYZ file"
      index=0;
      while read -a line; do
          echo 'line:' ${line[0]}  ${line[1]}  ${line[2]}  ${line[3]}
          COLS=${#line[@]}
          echo COLS=$COLS
          echo index=$index
          #LABEL[$index]=${line[0]}
          LABEL[$index]=$( echo "${line[0]}" | tr '[:lower:]' '[:upper:]' ) # make uppercase
          X[$index]=${line[1]}
          Y[$index]=${line[2]}
          Z[$index]=${line[3]}
          echo 'final:' ${LABEL[$index]} ${X[$index]} ${Y[$index]} ${Z[$index]}
          ((index++))
      done < $WorkDir/$Complex.coords.tmp
      copy $WorkDir/$Complex.coords.tmp $CurrDir/$Complex.coords.tmp

      for ((i=0;i<$natoms;i++)); do
          label=${LABEL[$i]}
          echo label=$label
          if [ "${LABEL[$i]}" == "$me" ]; then
             x=${X[$i]}
             y=${Y[$i]}
             z=${Z[$i]}
          fi
      done
      echo X_me=$x  Y_me=$y  Z_me=$z
      X_me_bohr=$(bc -l <<<"$x / 0.529177210903")
      Y_me_bohr=$(bc -l <<<"$y / 0.529177210903")
      Z_me_bohr=$(bc -l <<<"$z / 0.529177210903")


      echo "&SEWARD"                                                                     >  $CurrDir/"$Project"_seward.inp
      echo "Title"                                                                      >>  $CurrDir/"$Project"_seward.inp
      echo $Complex                                                                     >>  $CurrDir/"$Project"_seward.inp
      echo "ANGM"                                                                       >>  $CurrDir/"$Project"_seward.inp
      echo $X_me_bohr  $Y_me_bohr  $Z_me_bohr                                           >>  $CurrDir/"$Project"_seward.inp
      echo "AMFI"                                                                       >>  $CurrDir/"$Project"_seward.inp
      echo ""                                                                           >>  $CurrDir/"$Project"_seward.inp
      echo "DOAN"                                                                       >>  $CurrDir/"$Project"_seward.inp
      echo "RICD"                                                                       >>  $CurrDir/"$Project"_seward.inp
      echo "SDIP"                                                                       >>  $CurrDir/"$Project"_seward.inp
      echo ""                                                                           >>  $CurrDir/"$Project"_seward.inp
      echo "CDTH"                                                                       >>  $CurrDir/"$Project"_seward.inp
      echo "1.0d-7"                                                                     >>  $CurrDir/"$Project"_seward.inp
      echo ""                                                                           >>  $CurrDir/"$Project"_seward.inp
      echo "VERBOSE"                                                                    >>  $CurrDir/"$Project"_seward.inp
      echo ""                                                                           >>  $CurrDir/"$Project"_seward.inp

      if [ ${#BASIS_LBL[@]} -eq 0 ]; then
          # ------------------------------------------
          # the array ${BASIS_LBL[@]} is not defined, proceed the ususal approach
          for ((atom=0;atom<$natoms;atom++)); do
              index=$(bc -l <<<"$atom + 1")
              xdiff=$(bc -l <<<"${X[$atom]} - $x")
              ydiff=$(bc -l <<<"${Y[$atom]} - $y")
              zdiff=$(bc -l <<<"${Z[$atom]} - $z")
              distance=$(bc -l <<< "sqrt($xdiff*$xdiff + $ydiff*$ydiff + $zdiff*$zdiff)")

              if [[ 1 -eq "$( echo "${distance} < ${r}" | bc -l )" ]]; then
                    echo "Basis Set"                                                        >> $CurrDir/"$Project"_seward.inp
                    echo ${LABEL[$atom]}".ANO-RCC-"$bas1                                    >> $CurrDir/"$Project"_seward.inp
                    echo ${LABEL[$atom]}$index ${X[$atom]} ${Y[$atom]} ${Z[$atom]} Angstrom >> $CurrDir/"$Project"_seward.inp
                    echo "End of Basis Set"                                                 >> $CurrDir/"$Project"_seward.inp
                    echo ""                                                                 >> $CurrDir/"$Project"_seward.inp
              else
                    echo "Basis Set"                                                        >> $CurrDir/"$Project"_seward.inp
                    echo ${LABEL[$atom]}".ANO-RCC-"$bas2                                    >> $CurrDir/"$Project"_seward.inp
                    echo ${LABEL[$atom]}$index ${X[$atom]} ${Y[$atom]} ${Z[$atom]} Angstrom >> $CurrDir/"$Project"_seward.inp
                    echo "End of Basis Set"                                                 >> $CurrDir/"$Project"_seward.inp
                    echo ""                                                                 >> $CurrDir/"$Project"_seward.inp
              fi
          done
      else
          # ------------------------------------------
          # the array ${BASIS_LBL[@]} is defined. No need to check distances again
          for ((atom=0;atom<$natoms;atom++)); do
              index=$(bc -l <<<"$atom + 1")
              echo "Basis Set"                                                        >> $CurrDir/"$Project"_seward.inp
              echo ${LABEL[$atom]}".ANO-RCC-"${BASIS_LBL[$atom]}                      >> $CurrDir/"$Project"_seward.inp
              echo ${LABEL[$atom]}$index ${X[$atom]} ${Y[$atom]} ${Z[$atom]} Angstrom >> $CurrDir/"$Project"_seward.inp
              echo "End of Basis Set"                                                 >> $CurrDir/"$Project"_seward.inp
              echo ""                                                                 >> $CurrDir/"$Project"_seward.inp
          done
      fi

      echo "End of Input"                                                               >> $CurrDir/"$Project"_seward.inp
      #------------------------------------------------------------------------------------------------------------------
      # evaluate total nuclear charge of the system
      #

      echo loop over atoms to compute total nuclear charge
      total_Z=0
      for ((atom=0;atom<$natoms;atom++)); do
          index=$(bc -l <<<"$atom + 1")
          echo '${LABEL[$atom]}=' ${LABEL[$atom]}
          # find the total charge of this particular atom:
          charge_atom=0
          if [ "${LABEL[$atom]}" == "H"  ]; then charge_atom=1    ; fi ;
          if [ "${LABEL[$atom]}" == "HE" ]; then charge_atom=2    ; fi ;
          #-----------------------------------------------
          if [ "${LABEL[$atom]}" == "LI" ]; then charge_atom=3    ; fi ;
          if [ "${LABEL[$atom]}" == "BE" ]; then charge_atom=4    ; fi ;
          if [ "${LABEL[$atom]}" == "B"  ]; then charge_atom=5    ; fi ;
          if [ "${LABEL[$atom]}" == "C"  ]; then charge_atom=6    ; fi ;
          if [ "${LABEL[$atom]}" == "N"  ]; then charge_atom=7    ; fi ;
          if [ "${LABEL[$atom]}" == "O"  ]; then charge_atom=8    ; fi ;
          if [ "${LABEL[$atom]}" == "F"  ]; then charge_atom=9    ; fi ;
          if [ "${LABEL[$atom]}" == "NE" ]; then charge_atom=10   ; fi ;
          #-----------------------------------------------
          if [ "${LABEL[$atom]}" == "NA" ]; then charge_atom=11   ; fi ;
          if [ "${LABEL[$atom]}" == "MG" ]; then charge_atom=12   ; fi ;
          if [ "${LABEL[$atom]}" == "AL" ]; then charge_atom=13   ; fi ;
          if [ "${LABEL[$atom]}" == "SI" ]; then charge_atom=14   ; fi ;
          if [ "${LABEL[$atom]}" == "P"  ]; then charge_atom=15   ; fi ;
          if [ "${LABEL[$atom]}" == "S"  ]; then charge_atom=16   ; fi ;
          if [ "${LABEL[$atom]}" == "CL" ]; then charge_atom=17   ; fi ;
          if [ "${LABEL[$atom]}" == "AR" ]; then charge_atom=18   ; fi ;
          #-----------------------------------------------
          if [ "${LABEL[$atom]}" == "K"  ]; then charge_atom=19   ; fi ;
          if [ "${LABEL[$atom]}" == "CA" ]; then charge_atom=20   ; fi ;
          if [ "${LABEL[$atom]}" == "SC" ]; then charge_atom=21   ; fi ;
          if [ "${LABEL[$atom]}" == "TI" ]; then charge_atom=22   ; fi ;
          if [ "${LABEL[$atom]}" == "V"  ]; then charge_atom=23   ; fi ;
          if [ "${LABEL[$atom]}" == "CR" ]; then charge_atom=24   ; fi ;
          if [ "${LABEL[$atom]}" == "MN" ]; then charge_atom=25   ; fi ;
          if [ "${LABEL[$atom]}" == "FE" ]; then charge_atom=26   ; fi ;
          if [ "${LABEL[$atom]}" == "CO" ]; then charge_atom=27   ; fi ;
          if [ "${LABEL[$atom]}" == "NI" ]; then charge_atom=28   ; fi ;
          if [ "${LABEL[$atom]}" == "CU" ]; then charge_atom=29   ; fi ;
          if [ "${LABEL[$atom]}" == "ZN" ]; then charge_atom=30   ; fi ;
          if [ "${LABEL[$atom]}" == "GA" ]; then charge_atom=31   ; fi ;
          if [ "${LABEL[$atom]}" == "GE" ]; then charge_atom=32   ; fi ;
          if [ "${LABEL[$atom]}" == "AS" ]; then charge_atom=33   ; fi ;
          if [ "${LABEL[$atom]}" == "SE" ]; then charge_atom=34   ; fi ;
          if [ "${LABEL[$atom]}" == "BR" ]; then charge_atom=35   ; fi ;
          if [ "${LABEL[$atom]}" == "KR" ]; then charge_atom=36   ; fi ;
          #-----------------------------------------------
          if [ "${LABEL[$atom]}" == "RB" ]; then charge_atom=37   ; fi ;
          if [ "${LABEL[$atom]}" == "SR" ]; then charge_atom=38   ; fi ;
          if [ "${LABEL[$atom]}" == "Y"  ]; then charge_atom=39   ; fi ;
          if [ "${LABEL[$atom]}" == "ZR" ]; then charge_atom=40   ; fi ;
          if [ "${LABEL[$atom]}" == "NB" ]; then charge_atom=41   ; fi ;
          if [ "${LABEL[$atom]}" == "MO" ]; then charge_atom=42   ; fi ;
          if [ "${LABEL[$atom]}" == "TC" ]; then charge_atom=43   ; fi ;
          if [ "${LABEL[$atom]}" == "RU" ]; then charge_atom=44   ; fi ;
          if [ "${LABEL[$atom]}" == "RH" ]; then charge_atom=45   ; fi ;
          if [ "${LABEL[$atom]}" == "PD" ]; then charge_atom=46   ; fi ;
          if [ "${LABEL[$atom]}" == "AG" ]; then charge_atom=47   ; fi ;
          if [ "${LABEL[$atom]}" == "CD" ]; then charge_atom=48   ; fi ;
          if [ "${LABEL[$atom]}" == "IN" ]; then charge_atom=49   ; fi ;
          if [ "${LABEL[$atom]}" == "SN" ]; then charge_atom=50   ; fi ;
          if [ "${LABEL[$atom]}" == "SB" ]; then charge_atom=51   ; fi ;
          if [ "${LABEL[$atom]}" == "TE" ]; then charge_atom=52   ; fi ;
          if [ "${LABEL[$atom]}" == "I"  ]; then charge_atom=53   ; fi ;
          if [ "${LABEL[$atom]}" == "XE" ]; then charge_atom=54   ; fi ;
          #-----------------------------------------------
          if [ "${LABEL[$atom]}" == "CS" ]; then charge_atom=55   ; fi ;
          if [ "${LABEL[$atom]}" == "BA" ]; then charge_atom=56   ; fi ;
          if [ "${LABEL[$atom]}" == "LA" ]; then charge_atom=57   ; fi ;
          if [ "${LABEL[$atom]}" == "CE" ]; then charge_atom=58   ; fi ;
          if [ "${LABEL[$atom]}" == "PR" ]; then charge_atom=59   ; fi ;
          if [ "${LABEL[$atom]}" == "ND" ]; then charge_atom=60   ; fi ;
          if [ "${LABEL[$atom]}" == "PM" ]; then charge_atom=61   ; fi ;
          if [ "${LABEL[$atom]}" == "SM" ]; then charge_atom=62   ; fi ;
          if [ "${LABEL[$atom]}" == "EU" ]; then charge_atom=63   ; fi ;
          if [ "${LABEL[$atom]}" == "GD" ]; then charge_atom=64   ; fi ;
          if [ "${LABEL[$atom]}" == "TB" ]; then charge_atom=65   ; fi ;
          if [ "${LABEL[$atom]}" == "DY" ]; then charge_atom=66   ; fi ;
          if [ "${LABEL[$atom]}" == "HO" ]; then charge_atom=67   ; fi ;
          if [ "${LABEL[$atom]}" == "ER" ]; then charge_atom=68   ; fi ;
          if [ "${LABEL[$atom]}" == "TM" ]; then charge_atom=69   ; fi ;
          if [ "${LABEL[$atom]}" == "YB" ]; then charge_atom=70   ; fi ;
          if [ "${LABEL[$atom]}" == "LU" ]; then charge_atom=71   ; fi ;
          if [ "${LABEL[$atom]}" == "HF" ]; then charge_atom=72   ; fi ;
          if [ "${LABEL[$atom]}" == "TA" ]; then charge_atom=73   ; fi ;
          if [ "${LABEL[$atom]}" == "W"  ]; then charge_atom=74   ; fi ;
          if [ "${LABEL[$atom]}" == "RE" ]; then charge_atom=75   ; fi ;
          if [ "${LABEL[$atom]}" == "OS" ]; then charge_atom=76   ; fi ;
          if [ "${LABEL[$atom]}" == "IR" ]; then charge_atom=77   ; fi ;
          if [ "${LABEL[$atom]}" == "PT" ]; then charge_atom=78   ; fi ;
          if [ "${LABEL[$atom]}" == "AU" ]; then charge_atom=79   ; fi ;
          if [ "${LABEL[$atom]}" == "HG" ]; then charge_atom=80   ; fi ;
          if [ "${LABEL[$atom]}" == "TL" ]; then charge_atom=81   ; fi ;
          if [ "${LABEL[$atom]}" == "PB" ]; then charge_atom=82   ; fi ;
          if [ "${LABEL[$atom]}" == "BI" ]; then charge_atom=83   ; fi ;
          if [ "${LABEL[$atom]}" == "PO" ]; then charge_atom=84   ; fi ;
          if [ "${LABEL[$atom]}" == "AT" ]; then charge_atom=85   ; fi ;
          if [ "${LABEL[$atom]}" == "RN" ]; then charge_atom=86   ; fi ;
          #-----------------------------------------------
          if [ "${LABEL[$atom]}" == "FR" ]; then charge_atom=87   ; fi ;
          if [ "${LABEL[$atom]}" == "RA" ]; then charge_atom=88   ; fi ;
          if [ "${LABEL[$atom]}" == "AC" ]; then charge_atom=89   ; fi ;
          if [ "${LABEL[$atom]}" == "TH" ]; then charge_atom=90   ; fi ;
          if [ "${LABEL[$atom]}" == "PA" ]; then charge_atom=91   ; fi ;
          if [ "${LABEL[$atom]}" == "U"  ]; then charge_atom=92   ; fi ;
          if [ "${LABEL[$atom]}" == "NP" ]; then charge_atom=93   ; fi ;
          if [ "${LABEL[$atom]}" == "PU" ]; then charge_atom=94   ; fi ;
          if [ "${LABEL[$atom]}" == "AM" ]; then charge_atom=95   ; fi ;
          if [ "${LABEL[$atom]}" == "CM" ]; then charge_atom=96   ; fi ;
          if [ "${LABEL[$atom]}" == "BK" ]; then charge_atom=97   ; fi ;
          if [ "${LABEL[$atom]}" == "CF" ]; then charge_atom=98   ; fi ;
          if [ "${LABEL[$atom]}" == "ES" ]; then charge_atom=99   ; fi ;
          if [ "${LABEL[$atom]}" == "FM" ]; then charge_atom=100  ; fi ;
          if [ "${LABEL[$atom]}" == "MD" ]; then charge_atom=101  ; fi ;
          if [ "${LABEL[$atom]}" == "NO" ]; then charge_atom=102  ; fi ;
          if [ "${LABEL[$atom]}" == "LR" ]; then charge_atom=103  ; fi ;
          #-----------------------------------------------
          echo '${LABEL[$atom]}=' ${LABEL[$atom]} ' charge_atom=' $charge_atom
          total_Z=$(bc -l <<<"$total_Z + $charge_atom")
      done
      echo 'run_seward:  total nuclear charge of the entire molecule is  =' $total_Z
      total_Nel=$(bc -l <<<"$total_Z - $total_charge")
      echo 'run_seward:  total number of electrons of the entire molecule=' $total_Nel

#      #------------------------------------------------------------------------------------------------------------------



      if [ "$cluster" == "NSCC" ] ; then
          delete $CurrDir/"$Project"_seward.sh
          echo "#!/bin/sh"                                                            > $CurrDir/"$Project"_seward.sh
          echo "#PBS -q $queue"                                                      >> $CurrDir/"$Project"_seward.sh
          if [ ! -z "$ProjectID" ] ; then
            echo "#PBS -P $ProjectID"                                                >> $CurrDir/"$Project"_seward.sh
          else
            echo "#PBS -P Personal"                                                  >> $CurrDir/"$Project"_seward.sh
          fi
          echo "#PBS -l walltime=$walltime:00:00"                                    >> $CurrDir/"$Project"_seward.sh
          echo "#PBS -l select=$no_of_cores:ncpus=$no_of_ncpus:mpiprocs=$no_of_mpiprocs:mem=$memory" \
                                                                                     >> $CurrDir/"$Project"_seward.sh
          echo "#PBS -j oe"                                                          >> $CurrDir/"$Project"_seward.sh
          echo "#PBS -N "$Project"_seward"                                           >> $CurrDir/"$Project"_seward.sh
          echo                                                                       >> $CurrDir/"$Project"_seward.sh
          echo module load python/3.5.1                                              >> $CurrDir/"$Project"_seward.sh
          echo module load hdf5/1.8.16/xe_2016/serial                                >> $CurrDir/"$Project"_seward.sh
          echo                                                                       >> $CurrDir/"$Project"_seward.sh
          echo                                                                       >> $CurrDir/"$Project"_seward.sh
          echo export CurrDir=$CurrDir                                               >> $CurrDir/"$Project"_seward.sh
          echo export Project=$Project                                               >> $CurrDir/"$Project"_seward.sh
          echo export FileDir=$FileDir                                               >> $CurrDir/"$Project"_seward.sh
          echo export WorkDir=$WorkDir                                               >> $CurrDir/"$Project"_seward.sh
          echo export MOLCAS=$MOLCAS                                                 >> $CurrDir/"$Project"_seward.sh
          echo export MOLCAS_MEM=$MOLCAS_MEM                                         >> $CurrDir/"$Project"_seward.sh
          echo                                                                       >> $CurrDir/"$Project"_seward.sh
          echo "if [ ! -d $WorkDir ]; then mkdir -p $WorkDir ; fi"                   >> $CurrDir/"$Project"_seward.sh
          echo cd $WorkDir                                                           >> $CurrDir/"$Project"_seward.sh
          echo                                                                       >> $CurrDir/"$Project"_seward.sh
          echo pymolcas $CurrDir/"$Project"_seward'.inp  > \'                        >> $CurrDir/"$Project"_seward.sh
          echo          $CurrDir/"$Project"_seward'.out 2> \'                        >> $CurrDir/"$Project"_seward.sh
          echo          $CurrDir/"$Project"_seward.err                               >> $CurrDir/"$Project"_seward.sh
          echo                                                                       >> $CurrDir/"$Project"_seward.sh
          echo exit $?                                                               >> $CurrDir/"$Project"_seward.sh
      else
          cd $WorkDir
          pymolcas $CurrDir/"$Project"_seward.inp  > \
                   $CurrDir/"$Project"_seward.out 2> \
                   $CurrDir/"$Project"_seward.err
          if [ $? -eq 0 ]; then
#              delete  $CurrDir/"$Project"_seward.inp
#              delete  $CurrDir/"$Project"_seward.xml
              return 0
          else
              echo SEWARD did not finish well. Please check.
              echo $WorkDir is not deleted, for future re-use.
              return $?
          fi
      fi
  else
      echo The file "$Project"_seward.out is present in the "$CurrDir". Nothing to do.
      echo If not satisfied, remove the following files or folders and resubmit
      echo "#----->" $CurrDir/"$Project"_seward.out
      echo "#----->" $WorkDir
  fi
  delete $WorkDir/$Complex.coords.tmp
}


