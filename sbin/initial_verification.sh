initial_verification() {
  declare -a spinMS_local=("${!1}")
  declare -a rootsCASSCF_local=("${!2}")
  declare -a rootsCASPT2_local=("${!3}")
  declare -a rootsRASSI_local=("${!4}")
  declare -a groundL_local=("${!5}")

  if [ "${#spinMS_local[*]}" -ne "${#rootsCASSCF_local[*]}" ]; then
      echo ""spinMS" and "rootsCASSCF" should have the same dimension. Please verify!"
      echo "Dimension of "spinMS[]      =" ${#spinMS_local[*]}"
      echo "Dimension of "rootsCASSCF[] =" ${#rootsCASSCF_local[*]}"
      exit 110
  fi
  if [ "${#spinMS_local[*]}" -ne "${#rootsCASPT2_local[*]}" ]; then
      echo ""spinMS" and "rootsCASPT2" should have the same dimension. Please verify!"
      echo "Dimension of "spinMS[]      =" ${#spinMS_local[*]}"
      echo "Dimension of "rootsCASPT2[] =" ${#rootsCASPT2_local[*]}"
      exit 111
  fi
  if [ "${#spinMS_local[*]}" -ne "${#rootsRASSI_local[*]}" ]; then
      echo ""spinMS" and "rootsRASSI" should have the same dimension. Please verify!"
      echo "Dimension of "spinMS[]      =" ${#spinMS_local[*]}"
      echo "Dimension of "rootsRASSI[]  =" ${#rootsRASSI_local[*]}"
      exit 112
  fi
  if [ "${#rootsCASSCF_local[*]}" -ne "${#rootsCASPT2_local[*]}" ]; then
      echo ""rootsCASSCF" and "rootsCASPT2" should have the same dimension. Please verify!"
      echo "Dimension of "rootsCASSCF[] =" ${#rootsCASSCF_local[*]}"
      echo "Dimension of "rootsCASPT2[] =" ${#rootsCASPT2_local[*]}"
      exit 113
  fi
  if [ "${#rootsCASSCF_local[*]}" -ne "${#rootsRASSI_local[*]}" ]; then
      echo ""rootsCASSCF" and "rootsRASSI" should have the same dimension. Please verify!"
      echo "Dimension of "rootsCASSCF[] =" ${#rootsCASSCF_local[*]}"
      echo "Dimension of "rootsRASSI[]  =" ${#rootsRASSI_local[*]}"
      exit 114
  fi
  if [ "${#rootsCASPT2_local[*]}" -ne "${#rootsRASSI_local[*]}" ]; then
      echo ""rootsCASPT2" and "rootsRASSI" should have the same dimension. Please verify!"
      echo "Dimension of "rootsCASPT2[] =" ${#rootsCASPT2_local[*]}"
      echo "Dimension of "rootsRASSI[]  =" ${#rootsRASSI_local[*]}"
      exit 115
  fi

  local A=0
  for i in ${groundL_local[*]}; do
     let A=$A+$i
  done
  if [[ $A -ne ${rootsCASSCF_local[0]} ]]; then
     echo "number of states in "groundL[]" is not consistent with the first value in "rootsCASSCF[]"!"
     exit 116
  fi
  if [[ $A -ne ${rootsCASPT2_local[0]} ]]; then
     echo "number of states in "groundL[]" is not consistent with the first value in "rootsCASPT2[]"!"
     exit 117
  fi
  if [[ $A -ne ${rootsRASSI_local[0]} ]]; then
     echo "number of states in "groundL[]" is not consistent with the first value in "rootsRASSI[]"!"
     exit 118
  fi
}


