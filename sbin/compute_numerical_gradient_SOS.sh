#!/bin/bash

function compute_numerical_gradient_SOS() {

    echo compute_numerical_gradient_SOS::      CurrDir=$CurrDir
    echo compute_numerical_gradient_SOS::      FileFir=$FileDir
    echo compute_numerical_gradient_SOS::      Complex=$Complex
    echo compute_numerical_gradient_SOS::       LABELS=${LABEL[@]}
    echo compute_numerical_gradient_SOS::         bas1=$bas1
    echo compute_numerical_gradient_SOS::         bas2=$bas2
    echo compute_numerical_gradient_SOS:: active_space=$active_space
    echo compute_numerical_gradient_SOS::      npoints=$npoints

    cd $CurrDir

#----------------------------------------------------------------------------
if   [[ $npoints -eq 2 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for iaxis in X Y Z; do
    for idist in $(seq 1 $npoints ); do
      grep :: $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.out | grep ' SO-RASSI State' | colrm 1 45 >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
    done
  done

  unset ESOS_X_1 ESOS_X_2
  unset ESOS_Y_1 ESOS_Y_2
  unset ESOS_Z_1 ESOS_Z_2

  declare -a ESOS_X_1=( ) ; declare -a ESOS_Y_1=( ) ; declare -a ESOS_Z_1=( )
  declare -a ESOS_X_2=( ) ; declare -a ESOS_Y_2=( ) ; declare -a ESOS_Z_2=( )

  index=0;
  while read -a line; do
    ESOS_X_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_1.txt
  index=0;
  while read -a line; do
    ESOS_X_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_2.txt
  index=0;
  while read -a line; do
    ESOS_Y_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_1.txt
  index=0;
  while read -a line; do
    ESOS_Y_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_2.txt
  index=0;
  while read -a line; do
    ESOS_Z_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_1.txt
  index=0;
  while read -a line; do
    ESOS_Z_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_2.txt
  SOSroots=$index

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "SPIN-ORBIT GRADIENT COMPUTED BY 2-POINT ATOM DISTORTION"   > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "FORMULA USED:                           "                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "           -ENERGY(1) + ENERGY(2) "                       >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "gradient = ---------------------- "                       >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "                   2*delta        "                       >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo " where:                              "                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         delta     = $dist Bohr      "                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(1) = ENERGY( -delta )"                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(2) = ENERGY( +delta )"                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo " "                                                        >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "SPIN-ORBIT STATES"                                        >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "  Atom  | root |      SOS gradient-X     SOS gradient-Y     SOS gradient-Z   |       SOS gradient-X         SOS gradient-Y         SOS gradient-Z   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "Lbl. Nr.|      |         Ha/Bohr            Ha/Bohr           Ha/Bohr        |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
  fi

  iroot=0;
  for i in $(seq 1 $SOSroots ); do
    Xau=`echo "( - ${ESOS_X_1[$iroot]} + ${ESOS_X_2[$iroot]} ) / ( 2* $dist ) " | bc -l`
    Yau=`echo "( - ${ESOS_Y_1[$iroot]} + ${ESOS_Y_2[$iroot]} ) / ( 2* $dist ) " | bc -l`
    Zau=`echo "( - ${ESOS_Z_1[$iroot]} + ${ESOS_Z_2[$iroot]} ) / ( 2* $dist ) " | bc -l`
    Xcm=`echo "$Xau * 219474.7 / 0.529177210903" | bc -l`
    Ycm=`echo "$Yau * 219474.7 / 0.529177210903" | bc -l`
    Zcm=`echo "$Zau * 219474.7 / 0.529177210903" | bc -l`
    printf "%2s%4d %s %4d %s    %18.14f %18.14f %18.14f %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |" "$i" "|"  $Xau $Yau $Zau "|" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    ((iroot++))
  done
  echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt

  # delete the unnecessary large files. They might clutter the disc space
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
    done
  done
  let indxATOM=$indxATOM+1 ;
#end  of the main loop over atoms
done





#----------------------------------------------------------------------------
elif   [[ $npoints -eq 4 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      grep :: $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.out | grep ' SO-RASSI State' | colrm 1 45 >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
    done
  done

  unset ESOS_X_1 ESOS_X_2 ESOS_X_3 ESOS_X_4
  unset ESOS_Y_1 ESOS_Y_2 ESOS_Y_3 ESOS_Y_4
  unset ESOS_Z_1 ESOS_Z_2 ESOS_Z_3 ESOS_Z_4

  declare -a ESOS_X_1=( ) ; declare -a ESOS_Y_1=( ) ; declare -a ESOS_Z_1=( )
  declare -a ESOS_X_2=( ) ; declare -a ESOS_Y_2=( ) ; declare -a ESOS_Z_2=( )
  declare -a ESOS_X_3=( ) ; declare -a ESOS_Y_3=( ) ; declare -a ESOS_Z_3=( )
  declare -a ESOS_X_4=( ) ; declare -a ESOS_Y_4=( ) ; declare -a ESOS_Z_4=( )

  index=0;
  while read -a line; do
    ESOS_X_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_1.txt
  index=0;
  while read -a line; do
    ESOS_X_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_2.txt
  index=0;
  while read -a line; do
    ESOS_X_3[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_3.txt
  index=0;
  while read -a line; do
    ESOS_X_4[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_4.txt
  index=0;
  while read -a line; do
    ESOS_Y_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_1.txt
  index=0;
  while read -a line; do
    ESOS_Y_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_2.txt
  index=0;
  while read -a line; do
    ESOS_Y_3[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_3.txt
  index=0;
  while read -a line; do
    ESOS_Y_4[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_4.txt
  index=0;
  while read -a line; do
    ESOS_Z_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_1.txt
  index=0;
  while read -a line; do
    ESOS_Z_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_2.txt
  index=0;
  while read -a line; do
    ESOS_Z_3[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_3.txt
  index=0;
  while read -a line; do
    ESOS_Z_4[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_4.txt
  SOSroots=$index

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "SPIN-ORBIT GRADIENT COMPUTED BY 4-POINT ATOM DISTORTION"    > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "FORMULA USED:                            "                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "           ENERGY(1) -8*ENERGY(2) +8*ENERGY(3) -ENERGY(4)" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "gradient = ----------------------------------------------" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "                             12*delta"                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo " where:                                  "                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         delta     = $dist Bohr "                          >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(1) = ENERGY( -2*delta )  "                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(2) = ENERGY( -  delta )  "                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(3) = ENERGY( +  delta )  "                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(4) = ENERGY( +2*delta )  "                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo " "                                                         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "SPIN-ORBIT STATES"                                         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "  Atom  | root |      SOS gradient-X     SOS gradient-Y     SOS gradient-Z   |       SOS gradient-X         SOS gradient-Y         SOS gradient-Z   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "Lbl. Nr.|      |         Ha/Bohr            Ha/Bohr           Ha/Bohr        |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
  fi

  iroot=0;
  for i in $(seq 1 $SOSroots ); do
    Xau=`echo "( ${ESOS_X_1[$iroot]} -8* ${ESOS_X_2[$iroot]} +8* ${ESOS_X_3[$iroot]} - ${ESOS_X_4[$iroot]} ) / ( 12* $dist ) " | bc -l`
    Yau=`echo "( ${ESOS_Y_1[$iroot]} -8* ${ESOS_Y_2[$iroot]} +8* ${ESOS_Y_3[$iroot]} - ${ESOS_Y_4[$iroot]} ) / ( 12* $dist ) " | bc -l`
    Zau=`echo "( ${ESOS_Z_1[$iroot]} -8* ${ESOS_Z_2[$iroot]} +8* ${ESOS_Z_3[$iroot]} - ${ESOS_Z_4[$iroot]} ) / ( 12* $dist ) " | bc -l`
    Xcm=`echo "$Xau * 219474.7 / 0.529177210903" | bc -l`
    Ycm=`echo "$Yau * 219474.7 / 0.529177210903" | bc -l`
    Zcm=`echo "$Zau * 219474.7 / 0.529177210903" | bc -l`
    printf "%2s%4d %s %4d %s    %18.14f %18.14f %18.14f %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |" "$i" "|"  $Xau $Yau $Zau "|" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    ((iroot++))
  done
  echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt

  # delete the unnecessary large files. They might clutter the disc space
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
    done
  done
  let indxATOM=$indxATOM+1 ;
#end of the main loop over atoms
done



#----------------------------------------------------------------------------
elif   [[ $npoints -eq 6 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      grep :: $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.out | grep ' SO-RASSI State' | colrm 1 45 >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
    done
  done

  unset ESOS_X_1 ESOS_X_2 ESOS_X_3 ESOS_X_4 ESOS_X_5 ESOS_X_6
  unset ESOS_Y_1 ESOS_Y_2 ESOS_Y_3 ESOS_Y_4 ESOS_Y_5 ESOS_Y_6
  unset ESOS_Z_1 ESOS_Z_2 ESOS_Z_3 ESOS_Z_4 ESOS_Z_5 ESOS_Z_6

  declare -a ESOS_X_1=( ) ; declare -a ESOS_Y_1=( ) ; declare -a ESOS_Z_1=( )
  declare -a ESOS_X_2=( ) ; declare -a ESOS_Y_2=( ) ; declare -a ESOS_Z_2=( )
  declare -a ESOS_X_3=( ) ; declare -a ESOS_Y_3=( ) ; declare -a ESOS_Z_3=( )
  declare -a ESOS_X_4=( ) ; declare -a ESOS_Y_4=( ) ; declare -a ESOS_Z_4=( )
  declare -a ESOS_X_5=( ) ; declare -a ESOS_Y_5=( ) ; declare -a ESOS_Z_5=( )
  declare -a ESOS_X_6=( ) ; declare -a ESOS_Y_6=( ) ; declare -a ESOS_Z_6=( )

  index=0;
  while read -a line; do
    ESOS_X_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_1.txt
  index=0;
  while read -a line; do
    ESOS_X_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_2.txt
  index=0;
  while read -a line; do
    ESOS_X_3[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_3.txt
  index=0;
  while read -a line; do
    ESOS_X_4[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_4.txt
  index=0;
  while read -a line; do
    ESOS_X_5[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_5.txt
  index=0;
  while read -a line; do
    ESOS_X_6[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_6.txt
  index=0;
  while read -a line; do
    ESOS_Y_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_1.txt
  index=0;
  while read -a line; do
    ESOS_Y_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_2.txt
  index=0;
  while read -a line; do
    ESOS_Y_3[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_3.txt
  index=0;
  while read -a line; do
    ESOS_Y_4[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_4.txt
  index=0;
  while read -a line; do
    ESOS_Y_5[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_5.txt
  index=0;
  while read -a line; do
    ESOS_Y_6[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_6.txt
  index=0;
  while read -a line; do
    ESOS_Z_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_1.txt
  index=0;
  while read -a line; do
    ESOS_Z_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_2.txt
  index=0;
  while read -a line; do
    ESOS_Z_3[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_3.txt
  index=0;
  while read -a line; do
    ESOS_Z_4[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_4.txt
  index=0;
  while read -a line; do
    ESOS_Z_5[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_5.txt
  index=0;
  while read -a line; do
    ESOS_Z_6[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_6.txt
  SOSroots=$index

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "SPIN-ORBIT GRADIENT COMPUTED BY 6-POINT ATOM DISTORTION"                                 > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "FORMULA USED:                                          "                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "           -ENERGY(1) +9*ENERGY(2) -45*ENERGY(3) +45*ENERGY(4) -9*ENERGY(5) +ENERGY(6)" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "gradient = ---------------------------------------------------------------------------" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "                                              60*delta                                " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo " where:                                  "                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         delta     = $dist Bohr          "                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(1) = ENERGY( -3*delta )  "                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(2) = ENERGY( -2*delta )  "                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(3) = ENERGY( -  delta )  "                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(4) = ENERGY( +  delta )  "                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(5) = ENERGY( +2*delta )  "                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(6) = ENERGY( +3*delta )  "                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo " "                                                                                      >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "SPIN-ORBIT STATES"                                                                      >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "  Atom  | root |      SOS gradient-X     SOS gradient-Y     SOS gradient-Z   |       SOS gradient-X         SOS gradient-Y         SOS gradient-Z   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "Lbl. Nr.|      |         Ha/Bohr            Ha/Bohr           Ha/Bohr        |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
  fi

  iroot=0;
  for i in $(seq 1 $SOSroots ); do
    Xau=`echo "( - ${ESOS_X_1[$iroot]} +9* ${ESOS_X_2[$iroot]} -45* ${ESOS_X_3[$iroot]} +45* ${ESOS_X_4[$iroot]} -9* ${ESOS_X_5[$iroot]} + ${ESOS_X_6[$iroot]} ) / ( 60* $dist ) " | bc -l`
    Yau=`echo "( - ${ESOS_Y_1[$iroot]} +9* ${ESOS_Y_2[$iroot]} -45* ${ESOS_Y_3[$iroot]} +45* ${ESOS_Y_4[$iroot]} -9* ${ESOS_Y_5[$iroot]} + ${ESOS_Y_6[$iroot]} ) / ( 60* $dist ) " | bc -l`
    Zau=`echo "( - ${ESOS_Z_1[$iroot]} +9* ${ESOS_Z_2[$iroot]} -45* ${ESOS_Z_3[$iroot]} +45* ${ESOS_Z_4[$iroot]} -9* ${ESOS_Z_5[$iroot]} + ${ESOS_Z_6[$iroot]} ) / ( 60* $dist ) " | bc -l`
    Xcm=`echo "$Xau * 219474.7 / 0.529177210903" | bc -l`
    Ycm=`echo "$Yau * 219474.7 / 0.529177210903" | bc -l`
    Zcm=`echo "$Zau * 219474.7 / 0.529177210903" | bc -l`
    printf "%2s%4d %s %4d %s    %18.14f %18.14f %18.14f %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |" "$i" "|"  $Xau $Yau $Zau "|" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    ((iroot++))
  done
  echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt

  # delete the unnecessary large files. They might clutter the disc space
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
    done
  done
  let indxATOM=$indxATOM+1 ;
  #end  of the main loop over atoms
done



#----------------------------------------------------------------------------
elif   [[ $npoints -eq 8 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      grep :: $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.out | grep ' SO-RASSI State' | colrm 1 45 >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
    done
  done

  unset ESOS_X_1 ESOS_X_2 ESOS_X_3 ESOS_X_4 ESOS_X_5 ESOS_X_6 ESOS_X_7 ESOS_X_8
  unset ESOS_Y_1 ESOS_Y_2 ESOS_Y_3 ESOS_Y_4 ESOS_Y_5 ESOS_Y_6 ESOS_Y_7 ESOS_Y_8
  unset ESOS_Z_1 ESOS_Z_2 ESOS_Z_3 ESOS_Z_4 ESOS_Z_5 ESOS_Z_6 ESOS_Z_7 ESOS_Z_8

  declare -a ESOS_X_1=( ) ; declare -a ESOS_Y_1=( ) ; declare -a ESOS_Z_1=( )
  declare -a ESOS_X_2=( ) ; declare -a ESOS_Y_2=( ) ; declare -a ESOS_Z_2=( )
  declare -a ESOS_X_3=( ) ; declare -a ESOS_Y_3=( ) ; declare -a ESOS_Z_3=( )
  declare -a ESOS_X_4=( ) ; declare -a ESOS_Y_4=( ) ; declare -a ESOS_Z_4=( )
  declare -a ESOS_X_5=( ) ; declare -a ESOS_Y_5=( ) ; declare -a ESOS_Z_5=( )
  declare -a ESOS_X_6=( ) ; declare -a ESOS_Y_6=( ) ; declare -a ESOS_Z_6=( )
  declare -a ESOS_X_7=( ) ; declare -a ESOS_Y_7=( ) ; declare -a ESOS_Z_7=( )
  declare -a ESOS_X_8=( ) ; declare -a ESOS_Y_8=( ) ; declare -a ESOS_Z_8=( )

  index=0;
  while read -a line; do
    ESOS_X_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_1.txt
  index=0;
  while read -a line; do
    ESOS_X_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_2.txt
  index=0;
  while read -a line; do
    ESOS_X_3[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_3.txt
  index=0;
  while read -a line; do
    ESOS_X_4[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_4.txt
  index=0;
  while read -a line; do
    ESOS_X_5[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_5.txt
  index=0;
  while read -a line; do
    ESOS_X_6[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_6.txt
  index=0;
  while read -a line; do
    ESOS_X_7[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_7.txt
  index=0;
  while read -a line; do
    ESOS_X_8[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_X_8.txt
  index=0;
  while read -a line; do
    ESOS_Y_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_1.txt
  index=0;
  while read -a line; do
    ESOS_Y_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_2.txt
  index=0;
  while read -a line; do
    ESOS_Y_3[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_3.txt
  index=0;
  while read -a line; do
    ESOS_Y_4[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_4.txt
  index=0;
  while read -a line; do
    ESOS_Y_5[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_5.txt
  index=0;
  while read -a line; do
    ESOS_Y_6[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_6.txt
  index=0;
  while read -a line; do
    ESOS_Y_7[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_7.txt
  index=0;
  while read -a line; do
    ESOS_Y_8[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Y_8.txt
  index=0;
  while read -a line; do
    ESOS_Z_1[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_1.txt
  index=0;
  while read -a line; do
    ESOS_Z_2[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_2.txt
  index=0;
  while read -a line; do
    ESOS_Z_3[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_3.txt
  index=0;
  while read -a line; do
    ESOS_Z_4[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_4.txt
  index=0;
  while read -a line; do
    ESOS_Z_5[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_5.txt
  index=0;
  while read -a line; do
    ESOS_Z_6[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_6.txt
  index=0;
  while read -a line; do
    ESOS_Z_7[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_7.txt
  index=0;
  while read -a line; do
    ESOS_Z_8[$index]=${line[0]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_Z_8.txt
  SOSroots=$index

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "SPIN-ORBIT GRADIENT COMPUTED BY 8-POINT ATOM DISTORTION"                                                                       > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "FORMULA USED:                                          "                                                                      >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "           3*ENERGY(1) -32*ENERGY(2) +168*ENERGY(3) -672*ENERGY(4) +672*ENERGY(5) -168*ENERGY(6) +32*ENERGY(7) -3*ENERGY(8) " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "gradient = ---------------------------------------------------------------------------------------------------------------- " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "                                                                  840*delta                                                 " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo " where:                                  "                                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         delta     = $dist Bohr          "                                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(1) = ENERGY( -4*delta )  "                                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(2) = ENERGY( -3*delta )  "                                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(3) = ENERGY( -2*delta )  "                                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(4) = ENERGY( -  delta )  "                                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(5) = ENERGY( +  delta )  "                                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(6) = ENERGY( +2*delta )  "                                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(7) = ENERGY( +3*delta )  "                                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "         ENERGY(8) = ENERGY( +4*delta )  "                                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo " "                                                                                                                            >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "SPIN-ORBIT STATES"                                                                                                            >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "  Atom  | root |      SOS gradient-X     SOS gradient-Y     SOS gradient-Z   |       SOS gradient-X         SOS gradient-Y         SOS gradient-Z   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "Lbl. Nr.|      |         Ha/Bohr            Ha/Bohr           Ha/Bohr        |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
  fi

  iroot=0;
  for i in $(seq 1 $SOSroots ); do
    Xau=`echo "( 3* ${ESOS_X_1[$iroot]} -32* ${ESOS_X_2[$iroot]} +168* ${ESOS_X_3[$iroot]} -672* ${ESOS_X_4[$iroot]} +672* ${ESOS_X_5[$iroot]} -168* ${ESOS_X_6[$iroot]} +32* ${ESOS_X_7[$iroot]} -3* ${ESOS_X_8[$iroot]} ) / ( 840* $dist ) " | bc -l`
    Yau=`echo "( 3* ${ESOS_Y_1[$iroot]} -32* ${ESOS_Y_2[$iroot]} +168* ${ESOS_Y_3[$iroot]} -672* ${ESOS_Y_4[$iroot]} +672* ${ESOS_Y_5[$iroot]} -168* ${ESOS_Y_6[$iroot]} +32* ${ESOS_Y_7[$iroot]} -3* ${ESOS_Y_8[$iroot]} ) / ( 840* $dist ) " | bc -l`
    Zau=`echo "( 3* ${ESOS_Z_1[$iroot]} -32* ${ESOS_Z_2[$iroot]} +168* ${ESOS_Z_3[$iroot]} -672* ${ESOS_Z_4[$iroot]} +672* ${ESOS_Z_5[$iroot]} -168* ${ESOS_Z_6[$iroot]} +32* ${ESOS_Z_7[$iroot]} -3* ${ESOS_Z_8[$iroot]} ) / ( 840* $dist ) " | bc -l`
    Xcm=`echo "$Xau * 219474.7 / 0.529177210903" | bc -l`
    Ycm=`echo "$Yau * 219474.7 / 0.529177210903" | bc -l`
    Zcm=`echo "$Zau * 219474.7 / 0.529177210903" | bc -l`
    printf "%2s%4d %s %4d %s    %18.14f %18.14f %18.14f %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |" "$i" "|"  $Xau $Yau $Zau "|" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt
    ((iroot++))
  done
  echo "--------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SOS"$npoints"_gradient.txt

  # delete the unnecessary large files. They might clutter the disc space
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
    done
  done
  let indxATOM=$indxATOM+1 ;
  #end  of the main loop over atoms
done

#----------------------------------------------------------------------------
#elif   [[ $npoints -eq 8 ]] ; then
else
    echo "not yet implemented"
#----------------------------------------------------------------------------
#elif   [[ $npoints -eq 10 ]] ; then
    echo "not yet implemented"
#----------------------------------------------------------------------------
#elif   [[ $npoints -eq 12 ]] ; then
    echo "not yet implemented"
#----------------------------------------------------------------------------
fi


}




