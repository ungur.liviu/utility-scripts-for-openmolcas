#!/bin/bash

# run_alaska  $ms $number_of_procs_for_alaska  $roots  $separate_scripts
run_alaska_gradient_and_nac(){
   local ms=$1
   local nproc=$2
   local nroots=$3
   local separate_scripts=$4
   local roots_in_ALASKA=(`echo $(seq 1 $nroots)`)
   #  three  steps:
   #   -- run individual alaska calculations on each root, in batches over n_procs
   #   -- run individual alaska calculations on each pair of roots, in batches over n_procs
   #   -- grep the final gradient and assemble the final data file

   local natoms=$(head -n 1 $Complex.xyz)

   # check if the previous rasscf calculation was ok
   local rasscf_rc=0
   rasscf_rc=`grep 'Stop Module:' $CurrDir/"$Project"_rasscf_ms"$ms".out | grep 'rasscf' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
   echo `grep 'Stop Module:' $CurrDir/"$Project"_rasscf_ms"$ms".out | grep 'rasscf' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
   echo 'rasscf_rc =' $rasscf_rc

   #check if the "first MCLR step" finished OK
   local first_mclr_rc=0
   if [ -f $CurrDir/"$Project"_mclr_ms"$ms"_first_run.out ]; then
       first_mclr_rc=`grep 'Stop Module:' $CurrDir/"$Project"_mclr_ms"$ms"_first_run.out | grep 'mclr ' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l` 
   fi
   echo 'first_mclr_rc =' $first_mclr_rc


   if [ "$rasscf_rc" -eq 1 ] && [ "$first_mclr_rc" -eq 1 ]  && [ -f $FileDir/"$Project"_ms"$ms".JobIph ] && [ -f $FileDir/"$Project"_ms"$ms".RunFile ] ; then
       #check on previous runs
       local alaska_old_run=0
       if [ -f $CurrDir/"$Project"_alaska_ms"$ms"_all_roots.out ]; then
           alaska_old_run=`grep 'Stop Module:' $CurrDir/"$Project"_alaska_ms"$ms"_all_roots.out | grep 'alaska ' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l`
       fi
       nval=$(( $nroots + 1 )); echo nval = $nval

       if [[ $alaska_old_run -ne $nval ]] ; then
           delete $RootDir/$Project.JobIph
           copy $FileDir/"$Project"_ms"$ms".JobIph   $RootDir/$Project.JobIph
           copy $FileDir/"$Project"_ms"$ms".RunFile  $RootDir/$Project.RunFile

           if [ -z $separate_scripts ] || [ "$separate_scripts" == "no" ] || [ "$separate_scripts" == "not" ]; then
               # run diagonal gradeints and off_diagonal gradients (NACs)
               icounts=0
               for r1 in ${roots_in_ALASKA[@]}; do
                   for r2 in ${roots_in_ALASKA[@]}; do
                       if [[ $r1 -le $r2 ]] ; then
                           echo 'calling alaska for roots: ' $r1 ' and ' $r2
                           run_one_alaska_step  $ms  $r1  $r2  $nroots &
                           let icounts=$icounts+1;
                           rem=$(( $icounts % $nproc ))
                           if [[ $rem -eq 0 ]]; then
                                echo "Waiting for the alaska calculations to finish: icount=" $icounts
                                wait
                           fi
                       fi
                   done
               done
           fi
           wait


#           #=========================
#           # check if all single-root outputs are present in $CurrDir. If yes, then execute the following two commands:
           ic=0
           check=0
           for r1 in ${roots_in_ALASKA[@]}; do
               for r2 in ${roots_in_ALASKA[@]}; do
                   if [[ $r1 -le $r2 ]] ; then
                      let ic=$ic+1
                      let final_code[$ic]=0
                      if [ -f $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".out ] ; then
                         final_code[$ic]=`grep 'Stop Module:' $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".out | grep 'alaska' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l`
                         let check=$check+1
                      fi
                   fi
               done
           done
           echo "Number of single-state ALASKA calculations which finished OK: " $check
           echo "Final code " ${final_code[@]}
           total_runs=$(( $nroots * ( $nroots + 1 ) / 2 ))
           echo 'total_runs =' $total_runs
           echo 'check      =' $check

           if [ "$check" -eq "$total_runs" ]; then
#             grep_and_join_gradients $nroots $ms
              echo grep and join gradients
              if [ -f $CurrDir/"$Project"_alaska_ms"$ms"_all_roots.out ]; then  rm -rf $CurrDir/"$Project"_alaska_ms"$ms"_all_roots.out ; fi ;
              ic=0
              for r1 in ${roots_in_ALASKA[@]}; do
                  for r2 in ${roots_in_ALASKA[@]}; do
                      if [[ $r1 -le $r2 ]] ; then
                         let ic=$ic+1
                         cat $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".out >> $CurrDir/"$Project"_alaska_ms"$ms"_all_roots.out
                      fi
                  done
              done


              if [ -f  $CurrDir/"$Project"_alaska_ms"$ms"_init_full.out ]; then rm -rf $CurrDir/"$Project"_alaska_ms"$ms"_init_full.out ; fi ;
              for r1 in ${roots_in_ALASKA[@]}; do
                  for r2 in ${roots_in_ALASKA[@]}; do
                      if [[ $r1 -le $r2 ]] ; then
                          grep -A300 'Total derivative coupling \|Molecular gradients' $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".out  | \
                                     grep -B300 'Stop Module: alaska' | grep -v '\-\-\-' | grep -v  '\*\*\*' | grep -v '   X   ' |\
                                     grep -v ' norm' | grep -v '\* ' |  grep -v 'Irreducible' | grep -v 'Timing' | grep -v -e '^[[:space:]]*$' |\
                                     grep -v 'Happy' | grep -v '###'   >> $CurrDir/"$Project"_alaska_ms"$ms"_init_full.out
                      fi
                  done
              done

              get_full_gradient_matrix   $ms  $nroots  $natoms

              if [ -f  $CurrDir/"$Project"_alaska_ms"$ms"_init_grad.out ]; then rm -rf $CurrDir/"$Project"_alaska_ms"$ms"_init_grad.out ; fi ;
              for r1 in ${roots_in_ALASKA[@]}; do
                  grep -A300 'Molecular gradients' $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r1".out  | \
                             grep -B300 'Stop Module: alaska' | grep -v '\-\-\-' | grep -v  '\*\*\*' | grep -v '   X   ' |\
                             grep -v ' norm' | grep -v '\* ' |  grep -v 'Irreducible' | grep -v 'Timing' | grep -v -e '^[[:space:]]*$' |\
                             grep -v 'Happy' | grep -v '###'   >> $CurrDir/"$Project"_alaska_ms"$ms"_init_grad.out
              done

              get_only_gradient_matrix   $ms  $nroots  $natoms

#delete $CurrDir/"$Project"_alaska_ms"$ms"_init_grad.out
#             delete $CurrDir/"$Project"_alaska_ms"$ms"_init_full.out

      else
              echo "It seems that not all ALASKA calculations for MS="$ms" have finished successfully."
              ic=0
              for r1 in ${roots_in_ALASKA[@]}; do
                  for r2 in ${roots_in_ALASKA[@]}; do
                      if [[ $r1 -le $r2 ]] ; then
                         let ic=$ic+1
                         if [ "${final_code[$ic]}" -ne  1 ]; then
                            echo " --->  Single-state ALASKA calculation for MS="$ms" and ROOT="$r1"_"$r2" did not finish well."
                         fi
                      fi
                  done
              done
              echo "Please check again and eventually resubmit the original script"
           fi
       else
          echo "It looks like you had done this ALASKA calculation. If not satisified, please remove the following files and re-submit:"
          echo "$CurrDir/"$Project"_alaska_ms"$ms".out"
          echo "$CurrDir/"$Project"_alaska_ms"$ms"_all_roots.out"
       fi
   else
      echo "ALASKA calculation is probably not finished correctly, OR the required input files were not found. Please check."
      echo "The following files are needed: "
      echo "$FileDir/"$Project"_ms"$ms".JobIph"
      echo "$FileDir/"$Project"_ms"$ms".RunFile"
   fi
}
