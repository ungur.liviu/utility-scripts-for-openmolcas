#!/bin/bash

function generate_distorted_xyz_gradient(){
    local Complex=$1
    local atom_to_distort=$2
    local index_atom_to_distort=$3
    local npoints=$4
    local distortion=$5   # in Bohr

#   iDIST="0.010" # in Bohr.  = 0.01/0.529177210903 = 0.01889726124625770 Angstrom, positive
    iDIST=`echo " $distortion * 0.529177210903 " | bc -l`
    echo iDIST = $iDIST   # in Angstrom


    if [ -f $CurrDir/$Complex.xyz ]; then
       echo File $CurrDir/$Complex.xyz was found. Proceed to process it.
       dos2unix $CurrDir/$Complex.xyz

       # get the number of atoms from the input XYZ file
       natoms=$(head -n 1 $Complex.xyz)
       echo "gradient_by_shifting_atoms::   natoms="$natoms

       tail -n +3 $CurrDir/$Complex.xyz > $CurrDir/coords.tmp12."$atom_to_distort""$index_atom_to_distort"
       cat $CurrDir/coords.tmp12."$atom_to_distort""$index_atom_to_distort"

       tail -n +3 $CurrDir/$Complex.xyz | grep -v '^$' > $CurrDir/coords.tmp."$atom_to_distort""$index_atom_to_distort"
    else
       echo File $CurrDir/$Complex.xyz was NOT found. ERROR. Stop Now.
       exit 9
    fi

    cat  $CurrDir/coords.tmp."$atom_to_distort""$index_atom_to_distort"
    #--------------------------------------------------------------------------------
    # get the coordinates from XYZ file
    declare -a LABEL=( )
    declare -a X=( )
    declare -a Y=( )
    declare -a Z=( )
    declare -a XYZ_files=()

    echo "Coords read from the XYZ file"
    index=0;
    while read -a line; do
       echo 'line:' ${line[0]}  ${line[1]}  ${line[2]}  ${line[3]}
       COLS=${#line[@]}
       echo COLS=$COLS
       echo index=$index
       #LABEL[$index]=${line[0]}
       LABEL[$index]=$( echo "${line[0]}" | tr '[:lower:]' '[:upper:]' ) # make uppercase
       X[$index]=${line[1]}
       Y[$index]=${line[2]}
       Z[$index]=${line[3]}
       echo 'final:' ${LABEL[$index]} ${X[$index]} ${Y[$index]} ${Z[$index]}
       ((index++))
    done < $CurrDir/coords.tmp."$atom_to_distort""$index_atom_to_distort"

    rm -rf $CurrDir/coords.tmp."$atom_to_distort""$index_atom_to_distort"
    rm -rf $CurrDir/coords.tmp12."$atom_to_distort""$index_atom_to_distort"

    local natoms=${#LABEL[@]}
    echo generate_distorted_xyz_gradient::         Complex= $Complex
    echo generate_distorted_xyz_gradient::           LABEL= ${LABEL[@]}
    echo generate_distorted_xyz_gradient::               X= ${X[@]}
    echo generate_distorted_xyz_gradient::               Y= ${Y[@]}
    echo generate_distorted_xyz_gradient::               Z= ${Z[@]}
    echo generate_distorted_xyz_gradient:: atom_to_distort= $atom_to_distort
    echo generate_distorted_xyz_gradient:: index_atom_to_distort= $index_atom_to_distort
    echo generate_distorted_xyz_gradient::         npoints= $npoints
    echo generate_distorted_xyz_gradient::         natoms = $natoms
    root="$Complex"_dist_"$atom_to_distort""$index_atom_to_distort"

    # generate XYZ
    if [[ $npoints -eq 2 ]] ; then
        D1=`echo " $distortion * ( -1.0 ) " | bc -l`
        D2=`echo " $distortion * (  1.0 ) " | bc -l`
        file_X1="$root"_X_1.xyz
        file_X2="$root"_X_2.xyz
        file_Y1="$root"_Y_1.xyz
        file_Y2="$root"_Y_2.xyz
        file_Z1="$root"_Z_1.xyz
        file_Z2="$root"_Z_2.xyz
        echo $natoms > $file_X1
        echo $natoms > $file_X2
        echo $natoms > $file_Y1
        echo $natoms > $file_Y2
        echo $natoms > $file_Z1
        echo $natoms > $file_Z2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_1  Dist=$D1 Bohr "    >> $file_X1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_2  Dist=$D2 Bohr "    >> $file_X2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_1  Dist=$D1 Bohr "    >> $file_Y1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_2  Dist=$D2 Bohr "    >> $file_Y2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_1  Dist=$D1 Bohr "    >> $file_Z1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_2  Dist=$D2 Bohr "    >> $file_Z2


    elif [[ $npoints -eq 4 ]] ; then
        D1=`echo " $distortion * ( -2.0 ) " | bc -l`
        D2=`echo " $distortion * ( -1.0 ) " | bc -l`
        D3=`echo " $distortion * (  1.0 ) " | bc -l`
        D4=`echo " $distortion * (  2.0 ) " | bc -l`
        file_X1="$root"_X_1.xyz
        file_X2="$root"_X_2.xyz
        file_X3="$root"_X_3.xyz
        file_X4="$root"_X_4.xyz
        file_Y1="$root"_Y_1.xyz
        file_Y2="$root"_Y_2.xyz
        file_Y3="$root"_Y_3.xyz
        file_Y4="$root"_Y_4.xyz
        file_Z1="$root"_Z_1.xyz
        file_Z2="$root"_Z_2.xyz
        file_Z3="$root"_Z_3.xyz
        file_Z4="$root"_Z_4.xyz
        echo $natoms > $file_X1
        echo $natoms > $file_X2
        echo $natoms > $file_X3
        echo $natoms > $file_X4
        echo $natoms > $file_Y1
        echo $natoms > $file_Y2
        echo $natoms > $file_Y3
        echo $natoms > $file_Y4
        echo $natoms > $file_Z1
        echo $natoms > $file_Z2
        echo $natoms > $file_Z3
        echo $natoms > $file_Z4
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_1  Dist=$D1 Bohr "    >> $file_X1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_2  Dist=$D2 Bohr "    >> $file_X2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_3  Dist=$D3 Bohr "    >> $file_X3
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_4  Dist=$D4 Bohr "    >> $file_X4
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_1  Dist=$D1 Bohr "    >> $file_Y1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_2  Dist=$D2 Bohr "    >> $file_Y2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_3  Dist=$D3 Bohr "    >> $file_Y3
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_4  Dist=$D4 Bohr "    >> $file_Y4
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_1  Dist=$D1 Bohr "    >> $file_Z1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_2  Dist=$D2 Bohr "    >> $file_Z2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_3  Dist=$D3 Bohr "    >> $file_Z3
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_4  Dist=$D4 Bohr "    >> $file_Z4
    elif [[ $npoints -eq 6 ]] ; then
        D1=`echo " $distortion * ( -3.0 ) " | bc -l`
        D2=`echo " $distortion * ( -2.0 ) " | bc -l`
        D3=`echo " $distortion * ( -1.0 ) " | bc -l`
        D4=`echo " $distortion * (  1.0 ) " | bc -l`
        D5=`echo " $distortion * (  2.0 ) " | bc -l`
        D6=`echo " $distortion * (  3.0 ) " | bc -l`
        file_X1="$root"_X_1.xyz
        file_X2="$root"_X_2.xyz
        file_X3="$root"_X_3.xyz
        file_X4="$root"_X_4.xyz
        file_X5="$root"_X_5.xyz
        file_X6="$root"_X_6.xyz
        file_Y1="$root"_Y_1.xyz
        file_Y2="$root"_Y_2.xyz
        file_Y3="$root"_Y_3.xyz
        file_Y4="$root"_Y_4.xyz
        file_Y5="$root"_Y_5.xyz
        file_Y6="$root"_Y_6.xyz
        file_Z1="$root"_Z_1.xyz
        file_Z2="$root"_Z_2.xyz
        file_Z3="$root"_Z_3.xyz
        file_Z4="$root"_Z_4.xyz
        file_Z5="$root"_Z_5.xyz
        file_Z6="$root"_Z_6.xyz
        echo $natoms > $file_X1
        echo $natoms > $file_X2
        echo $natoms > $file_X3
        echo $natoms > $file_X4
        echo $natoms > $file_X5
        echo $natoms > $file_X6
        echo $natoms > $file_Y1
        echo $natoms > $file_Y2
        echo $natoms > $file_Y3
        echo $natoms > $file_Y4
        echo $natoms > $file_Y5
        echo $natoms > $file_Y6
        echo $natoms > $file_Z1
        echo $natoms > $file_Z2
        echo $natoms > $file_Z3
        echo $natoms > $file_Z4
        echo $natoms > $file_Z5
        echo $natoms > $file_Z6
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_1  Dist=$D1 Bohr "    >> $file_X1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_2  Dist=$D2 Bohr "    >> $file_X2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_3  Dist=$D3 Bohr "    >> $file_X3
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_4  Dist=$D4 Bohr "    >> $file_X4
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_5  Dist=$D5 Bohr "    >> $file_X5
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_6  Dist=$D6 Bohr "    >> $file_X6
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_1  Dist=$D1 Bohr "    >> $file_Y1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_2  Dist=$D2 Bohr "    >> $file_Y2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_3  Dist=$D3 Bohr "    >> $file_Y3
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_4  Dist=$D4 Bohr "    >> $file_Y4
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_5  Dist=$D5 Bohr "    >> $file_Y5
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_6  Dist=$D6 Bohr "    >> $file_Y6
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_1  Dist=$D1 Bohr "    >> $file_Z1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_2  Dist=$D2 Bohr "    >> $file_Z2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_3  Dist=$D3 Bohr "    >> $file_Z3
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_4  Dist=$D4 Bohr "    >> $file_Z4
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_5  Dist=$D5 Bohr "    >> $file_Z5
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_6  Dist=$D6 Bohr "    >> $file_Z6
    elif [[ $npoints -eq 8 ]] ; then
        D1=`echo " $distortion * ( -4.0 ) " | bc -l`
        D2=`echo " $distortion * ( -3.0 ) " | bc -l`
        D3=`echo " $distortion * ( -2.0 ) " | bc -l`
        D4=`echo " $distortion * ( -1.0 ) " | bc -l`
        D5=`echo " $distortion * (  1.0 ) " | bc -l`
        D6=`echo " $distortion * (  2.0 ) " | bc -l`
        D7=`echo " $distortion * (  3.0 ) " | bc -l`
        D8=`echo " $distortion * (  4.0 ) " | bc -l`
        file_X1="$root"_X_1.xyz
        file_X2="$root"_X_2.xyz
        file_X3="$root"_X_3.xyz
        file_X4="$root"_X_4.xyz
        file_X5="$root"_X_5.xyz
        file_X6="$root"_X_6.xyz
        file_X7="$root"_X_7.xyz
        file_X8="$root"_X_8.xyz
        file_Y1="$root"_Y_1.xyz
        file_Y2="$root"_Y_2.xyz
        file_Y3="$root"_Y_3.xyz
        file_Y4="$root"_Y_4.xyz
        file_Y5="$root"_Y_5.xyz
        file_Y6="$root"_Y_6.xyz
        file_Y7="$root"_Y_7.xyz
        file_Y8="$root"_Y_8.xyz
        file_Z1="$root"_Z_1.xyz
        file_Z2="$root"_Z_2.xyz
        file_Z3="$root"_Z_3.xyz
        file_Z4="$root"_Z_4.xyz
        file_Z5="$root"_Z_5.xyz
        file_Z6="$root"_Z_6.xyz
        file_Z7="$root"_Z_7.xyz
        file_Z8="$root"_Z_8.xyz
        echo $natoms > $file_X1
        echo $natoms > $file_X2
        echo $natoms > $file_X3
        echo $natoms > $file_X4
        echo $natoms > $file_X5
        echo $natoms > $file_X6
        echo $natoms > $file_X7
        echo $natoms > $file_X8
        echo $natoms > $file_Y1
        echo $natoms > $file_Y2
        echo $natoms > $file_Y3
        echo $natoms > $file_Y4
        echo $natoms > $file_Y5
        echo $natoms > $file_Y6
        echo $natoms > $file_Y7
        echo $natoms > $file_Y8
        echo $natoms > $file_Z1
        echo $natoms > $file_Z2
        echo $natoms > $file_Z3
        echo $natoms > $file_Z4
        echo $natoms > $file_Z5
        echo $natoms > $file_Z6
        echo $natoms > $file_Z7
        echo $natoms > $file_Z8
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_1  Dist=$D1 Bohr "    >> $file_X1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_2  Dist=$D2 Bohr "    >> $file_X2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_3  Dist=$D3 Bohr "    >> $file_X3
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_4  Dist=$D4 Bohr "    >> $file_X4
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_5  Dist=$D5 Bohr "    >> $file_X5
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_6  Dist=$D6 Bohr "    >> $file_X6
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_7  Dist=$D7 Bohr "    >> $file_X7
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_X_8  Dist=$D8 Bohr "    >> $file_X8
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_1  Dist=$D1 Bohr "    >> $file_Y1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_2  Dist=$D2 Bohr "    >> $file_Y2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_3  Dist=$D3 Bohr "    >> $file_Y3
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_4  Dist=$D4 Bohr "    >> $file_Y4
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_5  Dist=$D5 Bohr "    >> $file_Y5
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_6  Dist=$D6 Bohr "    >> $file_Y6
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_7  Dist=$D7 Bohr "    >> $file_Y7
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Y_8  Dist=$D8 Bohr "    >> $file_Y8
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_1  Dist=$D1 Bohr "    >> $file_Z1
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_2  Dist=$D2 Bohr "    >> $file_Z2
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_3  Dist=$D3 Bohr "    >> $file_Z3
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_4  Dist=$D4 Bohr "    >> $file_Z4
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_5  Dist=$D5 Bohr "    >> $file_Z5
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_6  Dist=$D6 Bohr "    >> $file_Z6
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_7  Dist=$D7 Bohr "    >> $file_Z7
        echo "$Complex distorted "$atom_to_distort""$index_atom_to_distort"_Z_8  Dist=$D8 Bohr "    >> $file_Z8
    else
        echo This npoints is not supported:  npoints = $npoints
        exit 9
    fi




    natoms=${#LABEL[@]}
    echo generate_distorted_xyz_gradient::         Complex= $Complex
    echo generate_distorted_xyz_gradient::             LABEL= ${LABEL[@]}
    echo generate_distorted_xyz_gradient::               X= ${X[@]}
    echo generate_distorted_xyz_gradient::               Y= ${Y[@]}
    echo generate_distorted_xyz_gradient::               Z= ${Z[@]}
    echo generate_distorted_xyz_gradient:: atom_to_distort= $atom_to_distort
    echo generate_distorted_xyz_gradient:: index_atom_to_distort= $index_atom_to_distort
    echo generate_distorted_xyz_gradient::         npoints= $npoints
    echo generate_distorted_xyz_gradient::         natoms = $natoms

    ind=0
    for i in ${!LABEL[@]} ; do

       if [[ $index_atom_to_distort -eq $ind ]]; then
          # this section concerns only the atom to be distorted

          if   [[ $npoints -eq 2 ]] ; then
              # calculate
              echo $X1 $X2 $Y1 $Y2 $Z1 $Z2
              X1=$(echo " ${X[$i]} - $iDIST" | bc -l )
              X2=$(echo " ${X[$i]} + $iDIST" | bc -l )
              Y1=$(echo " ${Y[$i]} - $iDIST" | bc -l )
              Y2=$(echo " ${Y[$i]} + $iDIST" | bc -l )
              Z1=$(echo " ${Z[$i]} - $iDIST" | bc -l )
              Z2=$(echo " ${Z[$i]} + $iDIST" | bc -l )
              #printf "%2s %.12f %.12f %.12f \n" ${LABEL[$i]} $X1 ${Y[$i]} ${Z[$i]}  >> $file_X1
              echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1
              echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2
              echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2

          elif [[ $npoints -eq 4 ]] ; then
              # calculate
              X1=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
              X2=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
              X3=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
              X4=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
              Y1=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
              Y2=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
              Y3=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
              Y4=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
              Z1=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
              Z2=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
              Z3=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
              Z4=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
              echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1
              echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2
              echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3
              echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4
              echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4

          elif [[ $npoints -eq 6 ]] ; then
              # calculate
              X1=$(echo " ${X[$i]} - 3 * $iDIST" | bc -l )
              X2=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
              X3=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
              X4=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
              X5=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
              X6=$(echo " ${X[$i]} + 3 * $iDIST" | bc -l )
              Y1=$(echo " ${Y[$i]} - 3 * $iDIST" | bc -l )
              Y2=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
              Y3=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
              Y4=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
              Y5=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
              Y6=$(echo " ${Y[$i]} + 3 * $iDIST" | bc -l )
              Z1=$(echo " ${Z[$i]} - 3 * $iDIST" | bc -l )
              Z2=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
              Z3=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
              Z4=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
              Z5=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
              Z6=$(echo " ${Z[$i]} + 3 * $iDIST" | bc -l )
              echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1
              echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2
              echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3
              echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4
              echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5
              echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6
              echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6

          elif [[ $npoints -eq 8 ]] ; then
              # calculate
              X1=$(echo " ${X[$i]} - 4 * $iDIST" | bc -l )
              X2=$(echo " ${X[$i]} - 3 * $iDIST" | bc -l )
              X3=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
              X4=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
              X5=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
              X6=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
              X7=$(echo " ${X[$i]} + 3 * $iDIST" | bc -l )
              X8=$(echo " ${X[$i]} + 4 * $iDIST" | bc -l )
              Y1=$(echo " ${Y[$i]} - 4 * $iDIST" | bc -l )
              Y2=$(echo " ${Y[$i]} - 3 * $iDIST" | bc -l )
              Y3=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
              Y4=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
              Y5=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
              Y6=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
              Y7=$(echo " ${Y[$i]} + 3 * $iDIST" | bc -l )
              Y8=$(echo " ${Y[$i]} + 4 * $iDIST" | bc -l )
              Z1=$(echo " ${Z[$i]} - 4 * $iDIST" | bc -l )
              Z2=$(echo " ${Z[$i]} - 3 * $iDIST" | bc -l )
              Z3=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
              Z4=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
              Z5=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
              Z6=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
              Z7=$(echo " ${Z[$i]} + 3 * $iDIST" | bc -l )
              Z8=$(echo " ${Z[$i]} + 4 * $iDIST" | bc -l )
              echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1
              echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2
              echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3
              echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4
              echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5
              echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6
              echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7
              echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8
              echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8
          fi


       else
          # this section concerns all other atom which are not be distorted


          if [[ $npoints -eq 2 ]] ; then
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2

          elif [[ $npoints -eq 4 ]] ; then
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4

          elif [[ $npoints -eq 6 ]] ; then
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6

          elif [[ $npoints -eq 8 ]] ; then
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8
          fi
       fi

       let ind=$ind+1

    done
}

