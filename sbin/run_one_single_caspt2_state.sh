#!/bin/bash


run_one_single_state_caspt2(){
  local ms=$1
  local root=$2
  local nroots=$3
  local start_root=$4
  local end_root=$5
  local type=$6        # SS-CASPT2 (ss), MS-CASPT2 (ms), XMS-CASPT2 (xms), DW-CASPT2 (dw), RCP-CASPT2 (rcp), etc.
  local only_root=$7

  local type=$( echo "$6" | tr '[:upper:]' '[:lower:]' ) # make lowercase
  echo "run_one_single_state_caspt2: execution mode: " $run_caspt2_on_separate_nodes

  export WorkDir=$RootDir/caspt2_ms"$ms"_state_"$root"

  #check on previous runs
  local old_run=0
  if [ -f $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".out ]; then
     old_run=`grep 'Stop Module:' $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".out | grep 'caspt2' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l`
  fi

  if [ ! -f $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".out ] || [ "$old_run" -ne 1 ] ; then
      rm -rf $WorkDir ; mkdir $WorkDir ;
      echo "&CASPT2"                                                    >  $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2

      # generate a specific input for this state
      if   [[ "$type" == "ss" ]]; then
           #----------------------------------------------
           echo "MULTI"                                                 >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
           echo $nroots  $(seq $start_root $end_root)                   >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2

      elif [[ "$type" == "ms" ]]; then
           #----------------------------------------------
           echo "MULTI"                                                 >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
           echo $nroots  $(seq $start_root $end_root)                   >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2

      elif [[ "$type" == "xms" ]]; then
           #----------------------------------------------
           echo "XMUL"                                                  >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
           echo $nroots  $(seq $start_root $end_root)                   >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2

      elif [[ "$type" == "dw" ]]; then
           #----------------------------------------------
           echo "DWMS"                                                  >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
           echo $nroots  $(seq $start_root $end_root)                   >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
           echo "DWMS"                                                  >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
           echo $dwms_zeta                                              >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
           echo "DWTY"                                                  >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
           echo $dwms_type                                              >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2

      elif [[ "$type" == "rcp" ]]; then
           #----------------------------------------------
           echo "RMUL"                                                  >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
           echo $nroots  $(seq $start_root $end_root)                   >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2

      else
          echo run_one_single_state_caspt2::  variable type=$type is undefined. This function does not know it.
          return 999
      fi

#      if [ "$ms" == ${spinMS[0]} ] || ( [ "$same_multiplet" == "true" ] && [[ "$ms" == ${spinMS[1]} ]] ) ; then
#          echo "XMUL"                                                   >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
#          echo $nroots  $(seq $start_root $end_root)                    >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
#      else
#          echo "XMUL"                                                   >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
#          echo "${#roots_in_CASPT2[*]} $(seq ${#roots_in_CASPT2[*]})  " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
#      fi
      echo "ONLY"                                                       >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "$only_root"                                                 >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "IMAG                                                      " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "  $imag_caspt2_shift                                      " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "IPEA                                                      " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "  $ipea_caspt2_shift                                      " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "NOMIX                                                     " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "NOORB                                                     " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "MAXITER                                                   " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "  100                                                     " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "THREsholds                                                " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      echo "1.0d-9  1.0d-07                                           " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2

      if [[ "$type" == "ss" ]] || ( [ "$ms" -ne ${spinMS[0]} ] && ( [ "$NOMULT" == "true" ] || [ "$NOMULT" == "TRUE" ] ) ) ; then
         echo "NOMULT                                                 " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
      fi
      echo "End Of Input                                              " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2

      # fold the very long lines to keep in line with MOLCAS's reading input function
      fold -w 120 -s $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2 > $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp

      # run_molcas in its own subfolder
      cd $WorkDir
      link $RootDir/"$Project".OrdInt          $WorkDir/"$Project".OrdInt
      link $RootDir/"$Project".OneInt          $WorkDir/"$Project".OneInt
      link $RootDir/"$Project".ChDiag          $WorkDir/"$Project".ChDiag
      link $RootDir/"$Project".ChMap           $WorkDir/"$Project".ChMap
      link $RootDir/"$Project".ChRst           $WorkDir/"$Project".ChRst
      link $RootDir/"$Project".ChRed           $WorkDir/"$Project".ChRed
      link $RootDir/"$Project".ChSel           $WorkDir/"$Project".ChSel
      link $RootDir/"$Project".ChVec           $WorkDir/"$Project".ChVec
      link $RootDir/"$Project".ChFV            $WorkDir/"$Project".ChFV
      link $RootDir/"$Project".ChRstL          $WorkDir/"$Project".ChRstL
      link $RootDir/"$Project".ChRdL           $WorkDir/"$Project".ChRdL
      link $RootDir/"$Project".ChVcl           $WorkDir/"$Project".ChVcl
      link $RootDir/"$Project".Vtmp            $WorkDir/"$Project".Vtmp
      link $RootDir/"$Project".LDFC            $WorkDir/"$Project".LDFC
      link $RootDir/"$Project".LDFAP           $WorkDir/"$Project".LDFAP
      link $RootDir/"$Project".OneRel          $WorkDir/"$Project".OneRel
      link $RootDir/"$Project".BasInt          $WorkDir/"$Project".BasInt
      link $RootDir/"$Project".SymInfo         $WorkDir/"$Project".SymInfo
      link $RootDir/"$Project".QVec            $WorkDir/"$Project".QVec
      link $RootDir/"$Project".ChVec1          $WorkDir/"$Project".ChVec1
      link $RootDir/"$Project".RunFile         $WorkDir/"$Project".RunFile
      link $FileDir/"$Project"_ms"$ms".JobIph  $WorkDir/"$Project".JobIph

      pymolcas $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp  >  \
               $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".out  2> \
               $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".err
      wait
      # add a barrier just for safety
      sleep 1

      cd $CurrDir
      if [ $? -eq 0 ]; then
          delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp2
          delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".inp
          delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".err
          delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$root".xml
          delete $WorkDir
      fi
  else
      echo The file "$Project"_"$type"_caspt2_ms"$ms"_state_"$root".out is present in the "$CurrDir". Nothing to do.
  fi
}



