#!/bin/bash

function generate_distorted_xyz_hessian(){
    local Complex=$1
    local atom1_to_distort=$2
    local index_atom1_to_distort=$3
    local atom2_to_distort=$4
    local index_atom2_to_distort=$5
    local npoints=$6
    local distortion=$7   # in Bohr

#   iDIST="0.010" # in Bohr.  = 0.01/0.529177210903 = 0.01889726124625770 Angstrom, positive
    iDIST=`echo " $distortion * 0.529177210903 " | bc -l`
    echo iDIST = $iDIST   # in Angstrom


    if [ -f $CurrDir/$Complex.xyz ]; then
       echo File $CurrDir/$Complex.xyz was found. Proceed to process it.
       dos2unix $CurrDir/$Complex.xyz

       # get the number of atoms from the input XYZ file
       natoms=$(head -n 1 $Complex.xyz)
       echo "generate_distorted_xyz_hessian::   natoms="$natoms

       fname="$atom1_to_distort""$index_atom1_to_distort"_"$atom2_to_distort""$index_atom2_to_distort"

       tail -n +3 $CurrDir/$Complex.xyz > $CurrDir/coords.tmp12."$fname"
       cat $CurrDir/coords.tmp12."$fname"

       tail -n +3 $CurrDir/$Complex.xyz | grep -v '^$' > $CurrDir/coords.tmp."$fname"
    else
       echo File $CurrDir/$Complex.xyz was NOT found. ERROR. Stop Now.
       exit 9
    fi

    cat  $CurrDir/coords.tmp."$fname"
    #--------------------------------------------------------------------------------
    # get the coordinates from XYZ file
    declare -a LABEL=( )
    declare -a X=( )
    declare -a Y=( )
    declare -a Z=( )
    declare -a XYZ_files=()

    echo "Coords read from the XYZ file"
    index=0;
    while read -a line; do
       echo 'line:' ${line[0]}  ${line[1]}  ${line[2]}  ${line[3]}
       COLS=${#line[@]}
       echo COLS=$COLS
       echo index=$index
       #LABEL[$index]=${line[0]}
       LABEL[$index]=$( echo "${line[0]}" | tr '[:lower:]' '[:upper:]' ) # make uppercase
       X[$index]=${line[1]}
       Y[$index]=${line[2]}
       Z[$index]=${line[3]}
       echo 'final:' ${LABEL[$index]} ${X[$index]} ${Y[$index]} ${Z[$index]}
       ((index++))
    done < $CurrDir/coords.tmp."$fname"

    rm -rf $CurrDir/coords.tmp."$fname"
    rm -rf $CurrDir/coords.tmp12."$fname"

    local natoms=${#LABEL[@]}
    echo generate_distorted_xyz_hessian::         Complex= $Complex
    echo generate_distorted_xyz_hessian::           LABEL= ${LABEL[@]}
    echo generate_distorted_xyz_hessian::               X= ${X[@]}
    echo generate_distorted_xyz_hessian::               Y= ${Y[@]}
    echo generate_distorted_xyz_hessian::               Z= ${Z[@]}
    echo generate_distorted_xyz_hessian:: atom1_to_distort= $atom1_to_distort
    echo generate_distorted_xyz_hessian:: index_atom1_to_distort= $index_atom1_to_distort
    echo generate_distorted_xyz_hessian:: atom2_to_distort= $atom2_to_distort
    echo generate_distorted_xyz_hessian:: index_atom2_to_distort= $index_atom2_to_distort
    echo generate_distorted_xyz_hessian::         npoints= $npoints
    echo generate_distorted_xyz_hessian::         natoms = $natoms
    root="$Complex"_dist_"$fname"

    # generate XYZ
    if [[ $npoints -eq 2 ]] ; then
        D1=`echo " $distortion * ( -1.0 ) " | bc -l`
        D2=`echo " $distortion * (  1.0 ) " | bc -l`
        file_X1_X1="$root"_X_1_X_1.xyz
        file_X1_X2="$root"_X_1_X_2.xyz
        file_X1_Y1="$root"_X_1_Y_1.xyz
        file_X1_Y2="$root"_X_1_Y_2.xyz
        file_X1_Z1="$root"_X_1_Z_1.xyz
        file_X1_Z2="$root"_X_1_Z_2.xyz
        file_X2_X1="$root"_X_2_X_1.xyz
        file_X2_X2="$root"_X_2_X_2.xyz
        file_X2_Y1="$root"_X_2_Y_1.xyz
        file_X2_Y2="$root"_X_2_Y_2.xyz
        file_X2_Z1="$root"_X_2_Z_1.xyz
        file_X2_Z2="$root"_X_2_Z_2.xyz
        file_Y1_X1="$root"_Y_1_X_1.xyz
        file_Y1_X2="$root"_Y_1_X_2.xyz
        file_Y1_Y1="$root"_Y_1_Y_1.xyz
        file_Y1_Y2="$root"_Y_1_Y_2.xyz
        file_Y1_Z1="$root"_Y_1_Z_1.xyz
        file_Y1_Z2="$root"_Y_1_Z_2.xyz
        file_Y2_X1="$root"_Y_2_X_1.xyz
        file_Y2_X2="$root"_Y_2_X_2.xyz
        file_Y2_Y1="$root"_Y_2_Y_1.xyz
        file_Y2_Y2="$root"_Y_2_Y_2.xyz
        file_Y2_Z1="$root"_Y_2_Z_1.xyz
        file_Y2_Z2="$root"_Y_2_Z_2.xyz
        file_Z1_X1="$root"_Z_1_X_1.xyz
        file_Z1_X2="$root"_Z_1_X_2.xyz
        file_Z1_Y1="$root"_Z_1_Y_1.xyz
        file_Z1_Y2="$root"_Z_1_Y_2.xyz
        file_Z1_Z1="$root"_Z_1_Z_1.xyz
        file_Z1_Z2="$root"_Z_1_Z_2.xyz
        file_Z2_X1="$root"_Z_2_X_1.xyz
        file_Z2_X2="$root"_Z_2_X_2.xyz
        file_Z2_Y1="$root"_Z_2_Y_1.xyz
        file_Z2_Y2="$root"_Z_2_Y_2.xyz
        file_Z2_Z1="$root"_Z_2_Z_1.xyz
        file_Z2_Z2="$root"_Z_2_Z_2.xyz
        echo $natoms > $file_X1_X1
        echo $natoms > $file_X1_X2
        echo $natoms > $file_X1_Y1
        echo $natoms > $file_X1_Y2
        echo $natoms > $file_X1_Z1
        echo $natoms > $file_X1_Z2
        echo $natoms > $file_X2_X1
        echo $natoms > $file_X2_X2
        echo $natoms > $file_X2_Y1
        echo $natoms > $file_X2_Y2
        echo $natoms > $file_X2_Z1
        echo $natoms > $file_X2_Z2
        echo $natoms > $file_Y1_X1
        echo $natoms > $file_Y1_X2
        echo $natoms > $file_Y1_Y1
        echo $natoms > $file_Y1_Y2
        echo $natoms > $file_Y1_Z1
        echo $natoms > $file_Y1_Z2
        echo $natoms > $file_Y2_X1
        echo $natoms > $file_Y2_X2
        echo $natoms > $file_Y2_Y1
        echo $natoms > $file_Y2_Y2
        echo $natoms > $file_Y2_Z1
        echo $natoms > $file_Y2_Z2
        echo $natoms > $file_Z1_X1
        echo $natoms > $file_Z1_X2
        echo $natoms > $file_Z1_Y1
        echo $natoms > $file_Z1_Y2
        echo $natoms > $file_Z1_Z1
        echo $natoms > $file_Z1_Z2
        echo $natoms > $file_Z2_X1
        echo $natoms > $file_Z2_X2
        echo $natoms > $file_Z2_Y1
        echo $natoms > $file_Z2_Y2
        echo $natoms > $file_Z2_Z1
        echo $natoms > $file_Z2_Z2
        echo "$Complex distorted "$fname"_X_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_X1
        echo "$Complex distorted "$fname"_X_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_X2
        echo "$Complex distorted "$fname"_X_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_Y1
        echo "$Complex distorted "$fname"_X_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_Y2
        echo "$Complex distorted "$fname"_X_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_Z1
        echo "$Complex distorted "$fname"_X_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_Z2
        echo "$Complex distorted "$fname"_X_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_X1
        echo "$Complex distorted "$fname"_X_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_X2
        echo "$Complex distorted "$fname"_X_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_Y1
        echo "$Complex distorted "$fname"_X_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_Y2
        echo "$Complex distorted "$fname"_X_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_Z1
        echo "$Complex distorted "$fname"_X_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_Z2
        echo "$Complex distorted "$fname"_Y_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_X1
        echo "$Complex distorted "$fname"_Y_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_X2
        echo "$Complex distorted "$fname"_Y_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_Y1
        echo "$Complex distorted "$fname"_Y_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_Y2
        echo "$Complex distorted "$fname"_Y_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_Z1
        echo "$Complex distorted "$fname"_Y_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_Z2
        echo "$Complex distorted "$fname"_Y_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_X1
        echo "$Complex distorted "$fname"_Y_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_X2
        echo "$Complex distorted "$fname"_Y_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_Y1
        echo "$Complex distorted "$fname"_Y_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_Y2
        echo "$Complex distorted "$fname"_Y_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_Z1
        echo "$Complex distorted "$fname"_Y_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_Z2
        echo "$Complex distorted "$fname"_Z_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_X1
        echo "$Complex distorted "$fname"_Z_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_X2
        echo "$Complex distorted "$fname"_Z_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_Y1
        echo "$Complex distorted "$fname"_Z_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_Y2
        echo "$Complex distorted "$fname"_Z_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_Z1
        echo "$Complex distorted "$fname"_Z_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_Z2
        echo "$Complex distorted "$fname"_Z_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_X1
        echo "$Complex distorted "$fname"_Z_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_X2
        echo "$Complex distorted "$fname"_Z_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_Y1
        echo "$Complex distorted "$fname"_Z_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_Y2
        echo "$Complex distorted "$fname"_Z_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_Z1
        echo "$Complex distorted "$fname"_Z_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_Z2


    elif [[ $npoints -eq 4 ]] ; then
        D1=`echo " $distortion * ( -2.0 ) " | bc -l`
        D2=`echo " $distortion * ( -1.0 ) " | bc -l`
        D3=`echo " $distortion * (  1.0 ) " | bc -l`
        D4=`echo " $distortion * (  2.0 ) " | bc -l`
        file_X1_X1="$root"_X_1_X_1.xyz
        file_X1_X2="$root"_X_1_X_2.xyz
        file_X1_X3="$root"_X_1_X_3.xyz
        file_X1_X4="$root"_X_1_X_4.xyz
        file_X1_Y1="$root"_X_1_Y_1.xyz
        file_X1_Y2="$root"_X_1_Y_2.xyz
        file_X1_Y3="$root"_X_1_Y_3.xyz
        file_X1_Y4="$root"_X_1_Y_4.xyz
        file_X1_Z1="$root"_X_1_Z_1.xyz
        file_X1_Z2="$root"_X_1_Z_2.xyz
        file_X1_Z3="$root"_X_1_Z_3.xyz
        file_X1_Z4="$root"_X_1_Z_4.xyz
        file_X2_X1="$root"_X_2_X_1.xyz
        file_X2_X2="$root"_X_2_X_2.xyz
        file_X2_X3="$root"_X_2_X_3.xyz
        file_X2_X4="$root"_X_2_X_4.xyz
        file_X2_Y1="$root"_X_2_Y_1.xyz
        file_X2_Y2="$root"_X_2_Y_2.xyz
        file_X2_Y3="$root"_X_2_Y_3.xyz
        file_X2_Y4="$root"_X_2_Y_4.xyz
        file_X2_Z1="$root"_X_2_Z_1.xyz
        file_X2_Z2="$root"_X_2_Z_2.xyz
        file_X2_Z3="$root"_X_2_Z_3.xyz
        file_X2_Z4="$root"_X_2_Z_4.xyz
        file_X3_X1="$root"_X_3_X_1.xyz
        file_X3_X2="$root"_X_3_X_2.xyz
        file_X3_X3="$root"_X_3_X_3.xyz
        file_X3_X4="$root"_X_3_X_4.xyz
        file_X3_Y1="$root"_X_3_Y_1.xyz
        file_X3_Y2="$root"_X_3_Y_2.xyz
        file_X3_Y3="$root"_X_3_Y_3.xyz
        file_X3_Y4="$root"_X_3_Y_4.xyz
        file_X3_Z1="$root"_X_3_Z_1.xyz
        file_X3_Z2="$root"_X_3_Z_2.xyz
        file_X3_Z3="$root"_X_3_Z_3.xyz
        file_X3_Z4="$root"_X_3_Z_4.xyz
        file_X4_X1="$root"_X_4_X_1.xyz
        file_X4_X2="$root"_X_4_X_2.xyz
        file_X4_X3="$root"_X_4_X_3.xyz
        file_X4_X4="$root"_X_4_X_4.xyz
        file_X4_Y1="$root"_X_4_Y_1.xyz
        file_X4_Y2="$root"_X_4_Y_2.xyz
        file_X4_Y3="$root"_X_4_Y_3.xyz
        file_X4_Y4="$root"_X_4_Y_4.xyz
        file_X4_Z1="$root"_X_4_Z_1.xyz
        file_X4_Z2="$root"_X_4_Z_2.xyz
        file_X4_Z3="$root"_X_4_Z_3.xyz
        file_X4_Z4="$root"_X_4_Z_4.xyz
        file_Y1_X1="$root"_Y_1_X_1.xyz
        file_Y1_X2="$root"_Y_1_X_2.xyz
        file_Y1_X3="$root"_Y_1_X_3.xyz
        file_Y1_X4="$root"_Y_1_X_4.xyz
        file_Y1_Y1="$root"_Y_1_Y_1.xyz
        file_Y1_Y2="$root"_Y_1_Y_2.xyz
        file_Y1_Y3="$root"_Y_1_Y_3.xyz
        file_Y1_Y4="$root"_Y_1_Y_4.xyz
        file_Y1_Z1="$root"_Y_1_Z_1.xyz
        file_Y1_Z2="$root"_Y_1_Z_2.xyz
        file_Y1_Z3="$root"_Y_1_Z_3.xyz
        file_Y1_Z4="$root"_Y_1_Z_4.xyz
        file_Y2_X1="$root"_Y_2_X_1.xyz
        file_Y2_X2="$root"_Y_2_X_2.xyz
        file_Y2_X3="$root"_Y_2_X_3.xyz
        file_Y2_X4="$root"_Y_2_X_4.xyz
        file_Y2_Y1="$root"_Y_2_Y_1.xyz
        file_Y2_Y2="$root"_Y_2_Y_2.xyz
        file_Y2_Y3="$root"_Y_2_Y_3.xyz
        file_Y2_Y4="$root"_Y_2_Y_4.xyz
        file_Y2_Z1="$root"_Y_2_Z_1.xyz
        file_Y2_Z2="$root"_Y_2_Z_2.xyz
        file_Y2_Z3="$root"_Y_2_Z_3.xyz
        file_Y2_Z4="$root"_Y_2_Z_4.xyz
        file_Y3_X1="$root"_Y_3_X_1.xyz
        file_Y3_X2="$root"_Y_3_X_2.xyz
        file_Y3_X3="$root"_Y_3_X_3.xyz
        file_Y3_X4="$root"_Y_3_X_4.xyz
        file_Y3_Y1="$root"_Y_3_Y_1.xyz
        file_Y3_Y2="$root"_Y_3_Y_2.xyz
        file_Y3_Y3="$root"_Y_3_Y_3.xyz
        file_Y3_Y4="$root"_Y_3_Y_4.xyz
        file_Y3_Z1="$root"_Y_3_Z_1.xyz
        file_Y3_Z2="$root"_Y_3_Z_2.xyz
        file_Y3_Z3="$root"_Y_3_Z_3.xyz
        file_Y3_Z4="$root"_Y_3_Z_4.xyz
        file_Y4_X1="$root"_Y_4_X_1.xyz
        file_Y4_X2="$root"_Y_4_X_2.xyz
        file_Y4_X3="$root"_Y_4_X_3.xyz
        file_Y4_X4="$root"_Y_4_X_4.xyz
        file_Y4_Y1="$root"_Y_4_Y_1.xyz
        file_Y4_Y2="$root"_Y_4_Y_2.xyz
        file_Y4_Y3="$root"_Y_4_Y_3.xyz
        file_Y4_Y4="$root"_Y_4_Y_4.xyz
        file_Y4_Z1="$root"_Y_4_Z_1.xyz
        file_Y4_Z2="$root"_Y_4_Z_2.xyz
        file_Y4_Z3="$root"_Y_4_Z_3.xyz
        file_Y4_Z4="$root"_Y_4_Z_4.xyz
        file_Z1_X1="$root"_Z_1_X_1.xyz
        file_Z1_X2="$root"_Z_1_X_2.xyz
        file_Z1_X3="$root"_Z_1_X_3.xyz
        file_Z1_X4="$root"_Z_1_X_4.xyz
        file_Z1_Y1="$root"_Z_1_Y_1.xyz
        file_Z1_Y2="$root"_Z_1_Y_2.xyz
        file_Z1_Y3="$root"_Z_1_Y_3.xyz
        file_Z1_Y4="$root"_Z_1_Y_4.xyz
        file_Z1_Z1="$root"_Z_1_Z_1.xyz
        file_Z1_Z2="$root"_Z_1_Z_2.xyz
        file_Z1_Z3="$root"_Z_1_Z_3.xyz
        file_Z1_Z4="$root"_Z_1_Z_4.xyz
        file_Z2_X1="$root"_Z_2_X_1.xyz
        file_Z2_X2="$root"_Z_2_X_2.xyz
        file_Z2_X3="$root"_Z_2_X_3.xyz
        file_Z2_X4="$root"_Z_2_X_4.xyz
        file_Z2_Y1="$root"_Z_2_Y_1.xyz
        file_Z2_Y2="$root"_Z_2_Y_2.xyz
        file_Z2_Y3="$root"_Z_2_Y_3.xyz
        file_Z2_Y4="$root"_Z_2_Y_4.xyz
        file_Z2_Z1="$root"_Z_2_Z_1.xyz
        file_Z2_Z2="$root"_Z_2_Z_2.xyz
        file_Z2_Z3="$root"_Z_2_Z_3.xyz
        file_Z2_Z4="$root"_Z_2_Z_4.xyz
        file_Z3_X1="$root"_Z_3_X_1.xyz
        file_Z3_X2="$root"_Z_3_X_2.xyz
        file_Z3_X3="$root"_Z_3_X_3.xyz
        file_Z3_X4="$root"_Z_3_X_4.xyz
        file_Z3_Y1="$root"_Z_3_Y_1.xyz
        file_Z3_Y2="$root"_Z_3_Y_2.xyz
        file_Z3_Y3="$root"_Z_3_Y_3.xyz
        file_Z3_Y4="$root"_Z_3_Y_4.xyz
        file_Z3_Z1="$root"_Z_3_Z_1.xyz
        file_Z3_Z2="$root"_Z_3_Z_2.xyz
        file_Z3_Z3="$root"_Z_3_Z_3.xyz
        file_Z3_Z4="$root"_Z_3_Z_4.xyz
        file_Z4_X1="$root"_Z_4_X_1.xyz
        file_Z4_X2="$root"_Z_4_X_2.xyz
        file_Z4_X3="$root"_Z_4_X_3.xyz
        file_Z4_X4="$root"_Z_4_X_4.xyz
        file_Z4_Y1="$root"_Z_4_Y_1.xyz
        file_Z4_Y2="$root"_Z_4_Y_2.xyz
        file_Z4_Y3="$root"_Z_4_Y_3.xyz
        file_Z4_Y4="$root"_Z_4_Y_4.xyz
        file_Z4_Z1="$root"_Z_4_Z_1.xyz
        file_Z4_Z2="$root"_Z_4_Z_2.xyz
        file_Z4_Z3="$root"_Z_4_Z_3.xyz
        file_Z4_Z4="$root"_Z_4_Z_4.xyz

        echo $natoms > $file_X1_X1
        echo $natoms > $file_X1_X2
        echo $natoms > $file_X1_X3
        echo $natoms > $file_X1_X4
        echo $natoms > $file_X1_Y1
        echo $natoms > $file_X1_Y2
        echo $natoms > $file_X1_Y3
        echo $natoms > $file_X1_Y4
        echo $natoms > $file_X1_Z1
        echo $natoms > $file_X1_Z2
        echo $natoms > $file_X1_Z3
        echo $natoms > $file_X1_Z4
        echo $natoms > $file_X2_X1
        echo $natoms > $file_X2_X2
        echo $natoms > $file_X2_X3
        echo $natoms > $file_X2_X4
        echo $natoms > $file_X2_Y1
        echo $natoms > $file_X2_Y2
        echo $natoms > $file_X2_Y3
        echo $natoms > $file_X2_Y4
        echo $natoms > $file_X2_Z1
        echo $natoms > $file_X2_Z2
        echo $natoms > $file_X2_Z3
        echo $natoms > $file_X2_Z4
        echo $natoms > $file_X3_X1
        echo $natoms > $file_X3_X2
        echo $natoms > $file_X3_X3
        echo $natoms > $file_X3_X4
        echo $natoms > $file_X3_Y1
        echo $natoms > $file_X3_Y2
        echo $natoms > $file_X3_Y3
        echo $natoms > $file_X3_Y4
        echo $natoms > $file_X3_Z1
        echo $natoms > $file_X3_Z2
        echo $natoms > $file_X3_Z3
        echo $natoms > $file_X3_Z4
        echo $natoms > $file_X4_X1
        echo $natoms > $file_X4_X2
        echo $natoms > $file_X4_X3
        echo $natoms > $file_X4_X4
        echo $natoms > $file_X4_Y1
        echo $natoms > $file_X4_Y2
        echo $natoms > $file_X4_Y3
        echo $natoms > $file_X4_Y4
        echo $natoms > $file_X4_Z1
        echo $natoms > $file_X4_Z2
        echo $natoms > $file_X4_Z3
        echo $natoms > $file_X4_Z4
        echo $natoms > $file_Y1_X1
        echo $natoms > $file_Y1_X2
        echo $natoms > $file_Y1_X3
        echo $natoms > $file_Y1_X4
        echo $natoms > $file_Y1_Y1
        echo $natoms > $file_Y1_Y2
        echo $natoms > $file_Y1_Y3
        echo $natoms > $file_Y1_Y4
        echo $natoms > $file_Y1_Z1
        echo $natoms > $file_Y1_Z2
        echo $natoms > $file_Y1_Z3
        echo $natoms > $file_Y1_Z4
        echo $natoms > $file_Y2_X1
        echo $natoms > $file_Y2_X2
        echo $natoms > $file_Y2_X3
        echo $natoms > $file_Y2_X4
        echo $natoms > $file_Y2_Y1
        echo $natoms > $file_Y2_Y2
        echo $natoms > $file_Y2_Y3
        echo $natoms > $file_Y2_Y4
        echo $natoms > $file_Y2_Z1
        echo $natoms > $file_Y2_Z2
        echo $natoms > $file_Y2_Z3
        echo $natoms > $file_Y2_Z4
        echo $natoms > $file_Y3_X1
        echo $natoms > $file_Y3_X2
        echo $natoms > $file_Y3_X3
        echo $natoms > $file_Y3_X4
        echo $natoms > $file_Y3_Y1
        echo $natoms > $file_Y3_Y2
        echo $natoms > $file_Y3_Y3
        echo $natoms > $file_Y3_Y4
        echo $natoms > $file_Y3_Z1
        echo $natoms > $file_Y3_Z2
        echo $natoms > $file_Y3_Z3
        echo $natoms > $file_Y3_Z4
        echo $natoms > $file_Y4_X1
        echo $natoms > $file_Y4_X2
        echo $natoms > $file_Y4_X3
        echo $natoms > $file_Y4_X4
        echo $natoms > $file_Y4_Y1
        echo $natoms > $file_Y4_Y2
        echo $natoms > $file_Y4_Y3
        echo $natoms > $file_Y4_Y4
        echo $natoms > $file_Y4_Z1
        echo $natoms > $file_Y4_Z2
        echo $natoms > $file_Y4_Z3
        echo $natoms > $file_Y4_Z4
        echo $natoms > $file_Z1_X1
        echo $natoms > $file_Z1_X2
        echo $natoms > $file_Z1_X3
        echo $natoms > $file_Z1_X4
        echo $natoms > $file_Z1_Y1
        echo $natoms > $file_Z1_Y2
        echo $natoms > $file_Z1_Y3
        echo $natoms > $file_Z1_Y4
        echo $natoms > $file_Z1_Z1
        echo $natoms > $file_Z1_Z2
        echo $natoms > $file_Z1_Z3
        echo $natoms > $file_Z1_Z4
        echo $natoms > $file_Z2_X1
        echo $natoms > $file_Z2_X2
        echo $natoms > $file_Z2_X3
        echo $natoms > $file_Z2_X4
        echo $natoms > $file_Z2_Y1
        echo $natoms > $file_Z2_Y2
        echo $natoms > $file_Z2_Y3
        echo $natoms > $file_Z2_Y4
        echo $natoms > $file_Z2_Z1
        echo $natoms > $file_Z2_Z2
        echo $natoms > $file_Z2_Z3
        echo $natoms > $file_Z2_Z4
        echo $natoms > $file_Z3_X1
        echo $natoms > $file_Z3_X2
        echo $natoms > $file_Z3_X3
        echo $natoms > $file_Z3_X4
        echo $natoms > $file_Z3_Y1
        echo $natoms > $file_Z3_Y2
        echo $natoms > $file_Z3_Y3
        echo $natoms > $file_Z3_Y4
        echo $natoms > $file_Z3_Z1
        echo $natoms > $file_Z3_Z2
        echo $natoms > $file_Z3_Z3
        echo $natoms > $file_Z3_Z4
        echo $natoms > $file_Z4_X1
        echo $natoms > $file_Z4_X2
        echo $natoms > $file_Z4_X3
        echo $natoms > $file_Z4_X4
        echo $natoms > $file_Z4_Y1
        echo $natoms > $file_Z4_Y2
        echo $natoms > $file_Z4_Y3
        echo $natoms > $file_Z4_Y4
        echo $natoms > $file_Z4_Z1
        echo $natoms > $file_Z4_Z2
        echo $natoms > $file_Z4_Z3
        echo $natoms > $file_Z4_Z4

        echo "$Complex distorted "$fname"_X_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_X1
        echo "$Complex distorted "$fname"_X_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_X2
        echo "$Complex distorted "$fname"_X_1_X_3  D1=$D1 D2=$D3 Bohr "    >> $file_X1_X3
        echo "$Complex distorted "$fname"_X_1_X_4  D1=$D1 D2=$D4 Bohr "    >> $file_X1_X4
        echo "$Complex distorted "$fname"_X_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_Y1
        echo "$Complex distorted "$fname"_X_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_Y2
        echo "$Complex distorted "$fname"_X_1_Y_3  D1=$D1 D2=$D3 Bohr "    >> $file_X1_Y3
        echo "$Complex distorted "$fname"_X_1_Y_4  D1=$D1 D2=$D4 Bohr "    >> $file_X1_Y4
        echo "$Complex distorted "$fname"_X_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_Z1
        echo "$Complex distorted "$fname"_X_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_Z2
        echo "$Complex distorted "$fname"_X_1_Z_3  D1=$D1 D2=$D3 Bohr "    >> $file_X1_Z3
        echo "$Complex distorted "$fname"_X_1_Z_4  D1=$D1 D2=$D4 Bohr "    >> $file_X1_Z4
        echo "$Complex distorted "$fname"_X_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_X1
        echo "$Complex distorted "$fname"_X_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_X2
        echo "$Complex distorted "$fname"_X_2_X_3  D1=$D2 D2=$D3 Bohr "    >> $file_X2_X3
        echo "$Complex distorted "$fname"_X_2_X_4  D1=$D2 D2=$D4 Bohr "    >> $file_X2_X4
        echo "$Complex distorted "$fname"_X_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_Y1
        echo "$Complex distorted "$fname"_X_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_Y2
        echo "$Complex distorted "$fname"_X_2_Y_3  D1=$D2 D2=$D3 Bohr "    >> $file_X2_Y3
        echo "$Complex distorted "$fname"_X_2_Y_4  D1=$D2 D2=$D4 Bohr "    >> $file_X2_Y4
        echo "$Complex distorted "$fname"_X_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_Z1
        echo "$Complex distorted "$fname"_X_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_Z2
        echo "$Complex distorted "$fname"_X_2_Z_3  D1=$D2 D2=$D3 Bohr "    >> $file_X2_Z3
        echo "$Complex distorted "$fname"_X_2_Z_4  D1=$D2 D2=$D4 Bohr "    >> $file_X2_Z4
        echo "$Complex distorted "$fname"_X_3_X_1  D1=$D3 D2=$D1 Bohr "    >> $file_X3_X1
        echo "$Complex distorted "$fname"_X_3_X_2  D1=$D3 D2=$D2 Bohr "    >> $file_X3_X2
        echo "$Complex distorted "$fname"_X_3_X_3  D1=$D3 D2=$D3 Bohr "    >> $file_X3_X3
        echo "$Complex distorted "$fname"_X_3_X_4  D1=$D3 D2=$D4 Bohr "    >> $file_X3_X4
        echo "$Complex distorted "$fname"_X_3_Y_1  D1=$D3 D2=$D1 Bohr "    >> $file_X3_Y1
        echo "$Complex distorted "$fname"_X_3_Y_2  D1=$D3 D2=$D2 Bohr "    >> $file_X3_Y2
        echo "$Complex distorted "$fname"_X_3_Y_3  D1=$D3 D2=$D3 Bohr "    >> $file_X3_Y3
        echo "$Complex distorted "$fname"_X_3_Y_4  D1=$D3 D2=$D4 Bohr "    >> $file_X3_Y4
        echo "$Complex distorted "$fname"_X_3_Z_1  D1=$D3 D2=$D1 Bohr "    >> $file_X3_Z1
        echo "$Complex distorted "$fname"_X_3_Z_2  D1=$D3 D2=$D2 Bohr "    >> $file_X3_Z2
        echo "$Complex distorted "$fname"_X_3_Z_3  D1=$D3 D2=$D3 Bohr "    >> $file_X3_Z3
        echo "$Complex distorted "$fname"_X_3_Z_4  D1=$D3 D2=$D4 Bohr "    >> $file_X3_Z4
        echo "$Complex distorted "$fname"_X_4_X_1  D1=$D4 D2=$D1 Bohr "    >> $file_X4_X1
        echo "$Complex distorted "$fname"_X_4_X_2  D1=$D4 D2=$D2 Bohr "    >> $file_X4_X2
        echo "$Complex distorted "$fname"_X_4_X_3  D1=$D4 D2=$D3 Bohr "    >> $file_X4_X3
        echo "$Complex distorted "$fname"_X_4_X_4  D1=$D4 D2=$D4 Bohr "    >> $file_X4_X4
        echo "$Complex distorted "$fname"_X_4_Y_1  D1=$D4 D2=$D1 Bohr "    >> $file_X4_Y1
        echo "$Complex distorted "$fname"_X_4_Y_2  D1=$D4 D2=$D2 Bohr "    >> $file_X4_Y2
        echo "$Complex distorted "$fname"_X_4_Y_3  D1=$D4 D2=$D3 Bohr "    >> $file_X4_Y3
        echo "$Complex distorted "$fname"_X_4_Y_4  D1=$D4 D2=$D4 Bohr "    >> $file_X4_Y4
        echo "$Complex distorted "$fname"_X_4_Z_1  D1=$D4 D2=$D1 Bohr "    >> $file_X4_Z1
        echo "$Complex distorted "$fname"_X_4_Z_2  D1=$D4 D2=$D2 Bohr "    >> $file_X4_Z2
        echo "$Complex distorted "$fname"_X_4_Z_3  D1=$D4 D2=$D3 Bohr "    >> $file_X4_Z3
        echo "$Complex distorted "$fname"_X_4_Z_4  D1=$D4 D2=$D4 Bohr "    >> $file_X4_Z4
        echo "$Complex distorted "$fname"_Y_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_X1
        echo "$Complex distorted "$fname"_Y_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_X2
        echo "$Complex distorted "$fname"_Y_1_X_3  D1=$D1 D2=$D3 Bohr "    >> $file_Y1_X3
        echo "$Complex distorted "$fname"_Y_1_X_4  D1=$D1 D2=$D4 Bohr "    >> $file_Y1_X4
        echo "$Complex distorted "$fname"_Y_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_Y1
        echo "$Complex distorted "$fname"_Y_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_Y2
        echo "$Complex distorted "$fname"_Y_1_Y_3  D1=$D1 D2=$D3 Bohr "    >> $file_Y1_Y3
        echo "$Complex distorted "$fname"_Y_1_Y_4  D1=$D1 D2=$D4 Bohr "    >> $file_Y1_Y4
        echo "$Complex distorted "$fname"_Y_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_Z1
        echo "$Complex distorted "$fname"_Y_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_Z2
        echo "$Complex distorted "$fname"_Y_1_Z_3  D1=$D1 D2=$D3 Bohr "    >> $file_Y1_Z3
        echo "$Complex distorted "$fname"_Y_1_Z_4  D1=$D1 D2=$D4 Bohr "    >> $file_Y1_Z4
        echo "$Complex distorted "$fname"_Y_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_X1
        echo "$Complex distorted "$fname"_Y_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_X2
        echo "$Complex distorted "$fname"_Y_2_X_3  D1=$D2 D2=$D3 Bohr "    >> $file_Y2_X3
        echo "$Complex distorted "$fname"_Y_2_X_4  D1=$D2 D2=$D4 Bohr "    >> $file_Y2_X4
        echo "$Complex distorted "$fname"_Y_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_Y1
        echo "$Complex distorted "$fname"_Y_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_Y2
        echo "$Complex distorted "$fname"_Y_2_Y_3  D1=$D2 D2=$D3 Bohr "    >> $file_Y2_Y3
        echo "$Complex distorted "$fname"_Y_2_Y_4  D1=$D2 D2=$D4 Bohr "    >> $file_Y2_Y4
        echo "$Complex distorted "$fname"_Y_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_Z1
        echo "$Complex distorted "$fname"_Y_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_Z2
        echo "$Complex distorted "$fname"_Y_2_Z_3  D1=$D2 D2=$D3 Bohr "    >> $file_Y2_Z3
        echo "$Complex distorted "$fname"_Y_2_Z_4  D1=$D2 D2=$D4 Bohr "    >> $file_Y2_Z4
        echo "$Complex distorted "$fname"_Y_3_X_1  D1=$D3 D2=$D1 Bohr "    >> $file_Y3_X1
        echo "$Complex distorted "$fname"_Y_3_X_2  D1=$D3 D2=$D2 Bohr "    >> $file_Y3_X2
        echo "$Complex distorted "$fname"_Y_3_X_3  D1=$D3 D2=$D3 Bohr "    >> $file_Y3_X3
        echo "$Complex distorted "$fname"_Y_3_X_4  D1=$D3 D2=$D4 Bohr "    >> $file_Y3_X4
        echo "$Complex distorted "$fname"_Y_3_Y_1  D1=$D3 D2=$D1 Bohr "    >> $file_Y3_Y1
        echo "$Complex distorted "$fname"_Y_3_Y_2  D1=$D3 D2=$D2 Bohr "    >> $file_Y3_Y2
        echo "$Complex distorted "$fname"_Y_3_Y_3  D1=$D3 D2=$D3 Bohr "    >> $file_Y3_Y3
        echo "$Complex distorted "$fname"_Y_3_Y_4  D1=$D3 D2=$D4 Bohr "    >> $file_Y3_Y4
        echo "$Complex distorted "$fname"_Y_3_Z_1  D1=$D3 D2=$D1 Bohr "    >> $file_Y3_Z1
        echo "$Complex distorted "$fname"_Y_3_Z_2  D1=$D3 D2=$D2 Bohr "    >> $file_Y3_Z2
        echo "$Complex distorted "$fname"_Y_3_Z_3  D1=$D3 D2=$D3 Bohr "    >> $file_Y3_Z3
        echo "$Complex distorted "$fname"_Y_3_Z_4  D1=$D3 D2=$D4 Bohr "    >> $file_Y3_Z4
        echo "$Complex distorted "$fname"_Y_4_X_1  D1=$D4 D2=$D1 Bohr "    >> $file_Y4_X1
        echo "$Complex distorted "$fname"_Y_4_X_2  D1=$D4 D2=$D2 Bohr "    >> $file_Y4_X2
        echo "$Complex distorted "$fname"_Y_4_X_3  D1=$D4 D2=$D3 Bohr "    >> $file_Y4_X3
        echo "$Complex distorted "$fname"_Y_4_X_4  D1=$D4 D2=$D4 Bohr "    >> $file_Y4_X4
        echo "$Complex distorted "$fname"_Y_4_Y_1  D1=$D4 D2=$D1 Bohr "    >> $file_Y4_Y1
        echo "$Complex distorted "$fname"_Y_4_Y_2  D1=$D4 D2=$D2 Bohr "    >> $file_Y4_Y2
        echo "$Complex distorted "$fname"_Y_4_Y_3  D1=$D4 D2=$D3 Bohr "    >> $file_Y4_Y3
        echo "$Complex distorted "$fname"_Y_4_Y_4  D1=$D4 D2=$D4 Bohr "    >> $file_Y4_Y4
        echo "$Complex distorted "$fname"_Y_4_Z_1  D1=$D4 D2=$D1 Bohr "    >> $file_Y4_Z1
        echo "$Complex distorted "$fname"_Y_4_Z_2  D1=$D4 D2=$D2 Bohr "    >> $file_Y4_Z2
        echo "$Complex distorted "$fname"_Y_4_Z_3  D1=$D4 D2=$D3 Bohr "    >> $file_Y4_Z3
        echo "$Complex distorted "$fname"_Y_4_Z_4  D1=$D4 D2=$D4 Bohr "    >> $file_Y4_Z4
        echo "$Complex distorted "$fname"_Z_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_X1
        echo "$Complex distorted "$fname"_Z_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_X2
        echo "$Complex distorted "$fname"_Z_1_X_3  D1=$D1 D2=$D3 Bohr "    >> $file_Z1_X3
        echo "$Complex distorted "$fname"_Z_1_X_4  D1=$D1 D2=$D4 Bohr "    >> $file_Z1_X4
        echo "$Complex distorted "$fname"_Z_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_Y1
        echo "$Complex distorted "$fname"_Z_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_Y2
        echo "$Complex distorted "$fname"_Z_1_Y_3  D1=$D1 D2=$D3 Bohr "    >> $file_Z1_Y3
        echo "$Complex distorted "$fname"_Z_1_Y_4  D1=$D1 D2=$D4 Bohr "    >> $file_Z1_Y4
        echo "$Complex distorted "$fname"_Z_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_Z1
        echo "$Complex distorted "$fname"_Z_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_Z2
        echo "$Complex distorted "$fname"_Z_1_Z_3  D1=$D1 D2=$D3 Bohr "    >> $file_Z1_Z3
        echo "$Complex distorted "$fname"_Z_1_Z_4  D1=$D1 D2=$D4 Bohr "    >> $file_Z1_Z4
        echo "$Complex distorted "$fname"_Z_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_X1
        echo "$Complex distorted "$fname"_Z_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_X2
        echo "$Complex distorted "$fname"_Z_2_X_3  D1=$D2 D2=$D3 Bohr "    >> $file_Z2_X3
        echo "$Complex distorted "$fname"_Z_2_X_4  D1=$D2 D2=$D4 Bohr "    >> $file_Z2_X4
        echo "$Complex distorted "$fname"_Z_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_Y1
        echo "$Complex distorted "$fname"_Z_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_Y2
        echo "$Complex distorted "$fname"_Z_2_Y_3  D1=$D2 D2=$D3 Bohr "    >> $file_Z2_Y3
        echo "$Complex distorted "$fname"_Z_2_Y_4  D1=$D2 D2=$D4 Bohr "    >> $file_Z2_Y4
        echo "$Complex distorted "$fname"_Z_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_Z1
        echo "$Complex distorted "$fname"_Z_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_Z2
        echo "$Complex distorted "$fname"_Z_2_Z_3  D1=$D2 D2=$D3 Bohr "    >> $file_Z2_Z3
        echo "$Complex distorted "$fname"_Z_2_Z_4  D1=$D2 D2=$D4 Bohr "    >> $file_Z2_Z4
        echo "$Complex distorted "$fname"_Z_3_X_1  D1=$D3 D2=$D1 Bohr "    >> $file_Z3_X1
        echo "$Complex distorted "$fname"_Z_3_X_2  D1=$D3 D2=$D2 Bohr "    >> $file_Z3_X2
        echo "$Complex distorted "$fname"_Z_3_X_3  D1=$D3 D2=$D3 Bohr "    >> $file_Z3_X3
        echo "$Complex distorted "$fname"_Z_3_X_4  D1=$D3 D2=$D4 Bohr "    >> $file_Z3_X4
        echo "$Complex distorted "$fname"_Z_3_Y_1  D1=$D3 D2=$D1 Bohr "    >> $file_Z3_Y1
        echo "$Complex distorted "$fname"_Z_3_Y_2  D1=$D3 D2=$D2 Bohr "    >> $file_Z3_Y2
        echo "$Complex distorted "$fname"_Z_3_Y_3  D1=$D3 D2=$D3 Bohr "    >> $file_Z3_Y3
        echo "$Complex distorted "$fname"_Z_3_Y_4  D1=$D3 D2=$D4 Bohr "    >> $file_Z3_Y4
        echo "$Complex distorted "$fname"_Z_3_Z_1  D1=$D3 D2=$D1 Bohr "    >> $file_Z3_Z1
        echo "$Complex distorted "$fname"_Z_3_Z_2  D1=$D3 D2=$D2 Bohr "    >> $file_Z3_Z2
        echo "$Complex distorted "$fname"_Z_3_Z_3  D1=$D3 D2=$D3 Bohr "    >> $file_Z3_Z3
        echo "$Complex distorted "$fname"_Z_3_Z_4  D1=$D3 D2=$D4 Bohr "    >> $file_Z3_Z4
        echo "$Complex distorted "$fname"_Z_4_X_1  D1=$D4 D2=$D1 Bohr "    >> $file_Z4_X1
        echo "$Complex distorted "$fname"_Z_4_X_2  D1=$D4 D2=$D2 Bohr "    >> $file_Z4_X2
        echo "$Complex distorted "$fname"_Z_4_X_3  D1=$D4 D2=$D3 Bohr "    >> $file_Z4_X3
        echo "$Complex distorted "$fname"_Z_4_X_4  D1=$D4 D2=$D4 Bohr "    >> $file_Z4_X4
        echo "$Complex distorted "$fname"_Z_4_Y_1  D1=$D4 D2=$D1 Bohr "    >> $file_Z4_Y1
        echo "$Complex distorted "$fname"_Z_4_Y_2  D1=$D4 D2=$D2 Bohr "    >> $file_Z4_Y2
        echo "$Complex distorted "$fname"_Z_4_Y_3  D1=$D4 D2=$D3 Bohr "    >> $file_Z4_Y3
        echo "$Complex distorted "$fname"_Z_4_Y_4  D1=$D4 D2=$D4 Bohr "    >> $file_Z4_Y4
        echo "$Complex distorted "$fname"_Z_4_Z_1  D1=$D4 D2=$D1 Bohr "    >> $file_Z4_Z1
        echo "$Complex distorted "$fname"_Z_4_Z_2  D1=$D4 D2=$D2 Bohr "    >> $file_Z4_Z2
        echo "$Complex distorted "$fname"_Z_4_Z_3  D1=$D4 D2=$D3 Bohr "    >> $file_Z4_Z3
        echo "$Complex distorted "$fname"_Z_4_Z_4  D1=$D4 D2=$D4 Bohr "    >> $file_Z4_Z4


    elif [[ $npoints -eq 6 ]] ; then
        D1=`echo " $distortion * ( -3.0 ) " | bc -l`
        D2=`echo " $distortion * ( -2.0 ) " | bc -l`
        D3=`echo " $distortion * ( -1.0 ) " | bc -l`
        D4=`echo " $distortion * (  1.0 ) " | bc -l`
        D5=`echo " $distortion * (  2.0 ) " | bc -l`
        D6=`echo " $distortion * (  3.0 ) " | bc -l`
        file_X1_X1="$root"_X_1_X_1.xyz
        file_X1_X2="$root"_X_1_X_2.xyz
        file_X1_X3="$root"_X_1_X_3.xyz
        file_X1_X4="$root"_X_1_X_4.xyz
        file_X1_X5="$root"_X_1_X_5.xyz
        file_X1_X6="$root"_X_1_X_6.xyz
        file_X1_Y1="$root"_X_1_Y_1.xyz
        file_X1_Y2="$root"_X_1_Y_2.xyz
        file_X1_Y3="$root"_X_1_Y_3.xyz
        file_X1_Y4="$root"_X_1_Y_4.xyz
        file_X1_Y5="$root"_X_1_Y_5.xyz
        file_X1_Y6="$root"_X_1_Y_6.xyz
        file_X1_Z1="$root"_X_1_Z_1.xyz
        file_X1_Z2="$root"_X_1_Z_2.xyz
        file_X1_Z3="$root"_X_1_Z_3.xyz
        file_X1_Z4="$root"_X_1_Z_4.xyz
        file_X1_Z5="$root"_X_1_Z_5.xyz
        file_X1_Z6="$root"_X_1_Z_6.xyz
        file_X2_X1="$root"_X_2_X_1.xyz
        file_X2_X2="$root"_X_2_X_2.xyz
        file_X2_X3="$root"_X_2_X_3.xyz
        file_X2_X4="$root"_X_2_X_4.xyz
        file_X2_X5="$root"_X_2_X_5.xyz
        file_X2_X6="$root"_X_2_X_6.xyz
        file_X2_Y1="$root"_X_2_Y_1.xyz
        file_X2_Y2="$root"_X_2_Y_2.xyz
        file_X2_Y3="$root"_X_2_Y_3.xyz
        file_X2_Y4="$root"_X_2_Y_4.xyz
        file_X2_Y5="$root"_X_2_Y_5.xyz
        file_X2_Y6="$root"_X_2_Y_6.xyz
        file_X2_Z1="$root"_X_2_Z_1.xyz
        file_X2_Z2="$root"_X_2_Z_2.xyz
        file_X2_Z3="$root"_X_2_Z_3.xyz
        file_X2_Z4="$root"_X_2_Z_4.xyz
        file_X2_Z5="$root"_X_2_Z_5.xyz
        file_X2_Z6="$root"_X_2_Z_6.xyz
        file_X3_X1="$root"_X_3_X_1.xyz
        file_X3_X2="$root"_X_3_X_2.xyz
        file_X3_X3="$root"_X_3_X_3.xyz
        file_X3_X4="$root"_X_3_X_4.xyz
        file_X3_X5="$root"_X_3_X_5.xyz
        file_X3_X6="$root"_X_3_X_6.xyz
        file_X3_Y1="$root"_X_3_Y_1.xyz
        file_X3_Y2="$root"_X_3_Y_2.xyz
        file_X3_Y3="$root"_X_3_Y_3.xyz
        file_X3_Y4="$root"_X_3_Y_4.xyz
        file_X3_Y5="$root"_X_3_Y_5.xyz
        file_X3_Y6="$root"_X_3_Y_6.xyz
        file_X3_Z1="$root"_X_3_Z_1.xyz
        file_X3_Z2="$root"_X_3_Z_2.xyz
        file_X3_Z3="$root"_X_3_Z_3.xyz
        file_X3_Z4="$root"_X_3_Z_4.xyz
        file_X3_Z5="$root"_X_3_Z_5.xyz
        file_X3_Z6="$root"_X_3_Z_6.xyz
        file_X4_X1="$root"_X_4_X_1.xyz
        file_X4_X2="$root"_X_4_X_2.xyz
        file_X4_X3="$root"_X_4_X_3.xyz
        file_X4_X4="$root"_X_4_X_4.xyz
        file_X4_X5="$root"_X_4_X_5.xyz
        file_X4_X6="$root"_X_4_X_6.xyz
        file_X4_Y1="$root"_X_4_Y_1.xyz
        file_X4_Y2="$root"_X_4_Y_2.xyz
        file_X4_Y3="$root"_X_4_Y_3.xyz
        file_X4_Y4="$root"_X_4_Y_4.xyz
        file_X4_Y5="$root"_X_4_Y_5.xyz
        file_X4_Y6="$root"_X_4_Y_6.xyz
        file_X4_Z1="$root"_X_4_Z_1.xyz
        file_X4_Z2="$root"_X_4_Z_2.xyz
        file_X4_Z3="$root"_X_4_Z_3.xyz
        file_X4_Z4="$root"_X_4_Z_4.xyz
        file_X4_Z5="$root"_X_4_Z_5.xyz
        file_X4_Z6="$root"_X_4_Z_6.xyz
        file_X5_X1="$root"_X_5_X_1.xyz
        file_X5_X2="$root"_X_5_X_2.xyz
        file_X5_X3="$root"_X_5_X_3.xyz
        file_X5_X4="$root"_X_5_X_4.xyz
        file_X5_X5="$root"_X_5_X_5.xyz
        file_X5_X6="$root"_X_5_X_6.xyz
        file_X5_Y1="$root"_X_5_Y_1.xyz
        file_X5_Y2="$root"_X_5_Y_2.xyz
        file_X5_Y3="$root"_X_5_Y_3.xyz
        file_X5_Y4="$root"_X_5_Y_4.xyz
        file_X5_Y5="$root"_X_5_Y_5.xyz
        file_X5_Y6="$root"_X_5_Y_6.xyz
        file_X5_Z1="$root"_X_5_Z_1.xyz
        file_X5_Z2="$root"_X_5_Z_2.xyz
        file_X5_Z3="$root"_X_5_Z_3.xyz
        file_X5_Z4="$root"_X_5_Z_4.xyz
        file_X5_Z5="$root"_X_5_Z_5.xyz
        file_X5_Z6="$root"_X_5_Z_6.xyz
        file_X6_X1="$root"_X_6_X_1.xyz
        file_X6_X2="$root"_X_6_X_2.xyz
        file_X6_X3="$root"_X_6_X_3.xyz
        file_X6_X4="$root"_X_6_X_4.xyz
        file_X6_X5="$root"_X_6_X_5.xyz
        file_X6_X6="$root"_X_6_X_6.xyz
        file_X6_Y1="$root"_X_6_Y_1.xyz
        file_X6_Y2="$root"_X_6_Y_2.xyz
        file_X6_Y3="$root"_X_6_Y_3.xyz
        file_X6_Y4="$root"_X_6_Y_4.xyz
        file_X6_Y5="$root"_X_6_Y_5.xyz
        file_X6_Y6="$root"_X_6_Y_6.xyz
        file_X6_Z1="$root"_X_6_Z_1.xyz
        file_X6_Z2="$root"_X_6_Z_2.xyz
        file_X6_Z3="$root"_X_6_Z_3.xyz
        file_X6_Z4="$root"_X_6_Z_4.xyz
        file_X6_Z5="$root"_X_6_Z_5.xyz
        file_X6_Z6="$root"_X_6_Z_6.xyz
        file_Y1_X1="$root"_Y_1_X_1.xyz
        file_Y1_X2="$root"_Y_1_X_2.xyz
        file_Y1_X3="$root"_Y_1_X_3.xyz
        file_Y1_X4="$root"_Y_1_X_4.xyz
        file_Y1_X5="$root"_Y_1_X_5.xyz
        file_Y1_X6="$root"_Y_1_X_6.xyz
        file_Y1_Y1="$root"_Y_1_Y_1.xyz
        file_Y1_Y2="$root"_Y_1_Y_2.xyz
        file_Y1_Y3="$root"_Y_1_Y_3.xyz
        file_Y1_Y4="$root"_Y_1_Y_4.xyz
        file_Y1_Y5="$root"_Y_1_Y_5.xyz
        file_Y1_Y6="$root"_Y_1_Y_6.xyz
        file_Y1_Z1="$root"_Y_1_Z_1.xyz
        file_Y1_Z2="$root"_Y_1_Z_2.xyz
        file_Y1_Z3="$root"_Y_1_Z_3.xyz
        file_Y1_Z4="$root"_Y_1_Z_4.xyz
        file_Y1_Z5="$root"_Y_1_Z_5.xyz
        file_Y1_Z6="$root"_Y_1_Z_6.xyz
        file_Y2_X1="$root"_Y_2_X_1.xyz
        file_Y2_X2="$root"_Y_2_X_2.xyz
        file_Y2_X3="$root"_Y_2_X_3.xyz
        file_Y2_X4="$root"_Y_2_X_4.xyz
        file_Y2_X5="$root"_Y_2_X_5.xyz
        file_Y2_X6="$root"_Y_2_X_6.xyz
        file_Y2_Y1="$root"_Y_2_Y_1.xyz
        file_Y2_Y2="$root"_Y_2_Y_2.xyz
        file_Y2_Y3="$root"_Y_2_Y_3.xyz
        file_Y2_Y4="$root"_Y_2_Y_4.xyz
        file_Y2_Y5="$root"_Y_2_Y_5.xyz
        file_Y2_Y6="$root"_Y_2_Y_6.xyz
        file_Y2_Z1="$root"_Y_2_Z_1.xyz
        file_Y2_Z2="$root"_Y_2_Z_2.xyz
        file_Y2_Z3="$root"_Y_2_Z_3.xyz
        file_Y2_Z4="$root"_Y_2_Z_4.xyz
        file_Y2_Z5="$root"_Y_2_Z_5.xyz
        file_Y2_Z6="$root"_Y_2_Z_6.xyz
        file_Y3_X1="$root"_Y_3_X_1.xyz
        file_Y3_X2="$root"_Y_3_X_2.xyz
        file_Y3_X3="$root"_Y_3_X_3.xyz
        file_Y3_X4="$root"_Y_3_X_4.xyz
        file_Y3_X5="$root"_Y_3_X_5.xyz
        file_Y3_X6="$root"_Y_3_X_6.xyz
        file_Y3_Y1="$root"_Y_3_Y_1.xyz
        file_Y3_Y2="$root"_Y_3_Y_2.xyz
        file_Y3_Y3="$root"_Y_3_Y_3.xyz
        file_Y3_Y4="$root"_Y_3_Y_4.xyz
        file_Y3_Y5="$root"_Y_3_Y_5.xyz
        file_Y3_Y6="$root"_Y_3_Y_6.xyz
        file_Y3_Z1="$root"_Y_3_Z_1.xyz
        file_Y3_Z2="$root"_Y_3_Z_2.xyz
        file_Y3_Z3="$root"_Y_3_Z_3.xyz
        file_Y3_Z4="$root"_Y_3_Z_4.xyz
        file_Y3_Z5="$root"_Y_3_Z_5.xyz
        file_Y3_Z6="$root"_Y_3_Z_6.xyz
        file_Y4_X1="$root"_Y_4_X_1.xyz
        file_Y4_X2="$root"_Y_4_X_2.xyz
        file_Y4_X3="$root"_Y_4_X_3.xyz
        file_Y4_X4="$root"_Y_4_X_4.xyz
        file_Y4_X5="$root"_Y_4_X_5.xyz
        file_Y4_X6="$root"_Y_4_X_6.xyz
        file_Y4_Y1="$root"_Y_4_Y_1.xyz
        file_Y4_Y2="$root"_Y_4_Y_2.xyz
        file_Y4_Y3="$root"_Y_4_Y_3.xyz
        file_Y4_Y4="$root"_Y_4_Y_4.xyz
        file_Y4_Y5="$root"_Y_4_Y_5.xyz
        file_Y4_Y6="$root"_Y_4_Y_6.xyz
        file_Y4_Z1="$root"_Y_4_Z_1.xyz
        file_Y4_Z2="$root"_Y_4_Z_2.xyz
        file_Y4_Z3="$root"_Y_4_Z_3.xyz
        file_Y4_Z4="$root"_Y_4_Z_4.xyz
        file_Y4_Z5="$root"_Y_4_Z_5.xyz
        file_Y4_Z6="$root"_Y_4_Z_6.xyz
        file_Y5_X1="$root"_Y_5_X_1.xyz
        file_Y5_X2="$root"_Y_5_X_2.xyz
        file_Y5_X3="$root"_Y_5_X_3.xyz
        file_Y5_X4="$root"_Y_5_X_4.xyz
        file_Y5_X5="$root"_Y_5_X_5.xyz
        file_Y5_X6="$root"_Y_5_X_6.xyz
        file_Y5_Y1="$root"_Y_5_Y_1.xyz
        file_Y5_Y2="$root"_Y_5_Y_2.xyz
        file_Y5_Y3="$root"_Y_5_Y_3.xyz
        file_Y5_Y4="$root"_Y_5_Y_4.xyz
        file_Y5_Y5="$root"_Y_5_Y_5.xyz
        file_Y5_Y6="$root"_Y_5_Y_6.xyz
        file_Y5_Z1="$root"_Y_5_Z_1.xyz
        file_Y5_Z2="$root"_Y_5_Z_2.xyz
        file_Y5_Z3="$root"_Y_5_Z_3.xyz
        file_Y5_Z4="$root"_Y_5_Z_4.xyz
        file_Y5_Z5="$root"_Y_5_Z_5.xyz
        file_Y5_Z6="$root"_Y_5_Z_6.xyz
        file_Y6_X1="$root"_Y_6_X_1.xyz
        file_Y6_X2="$root"_Y_6_X_2.xyz
        file_Y6_X3="$root"_Y_6_X_3.xyz
        file_Y6_X4="$root"_Y_6_X_4.xyz
        file_Y6_X5="$root"_Y_6_X_5.xyz
        file_Y6_X6="$root"_Y_6_X_6.xyz
        file_Y6_Y1="$root"_Y_6_Y_1.xyz
        file_Y6_Y2="$root"_Y_6_Y_2.xyz
        file_Y6_Y3="$root"_Y_6_Y_3.xyz
        file_Y6_Y4="$root"_Y_6_Y_4.xyz
        file_Y6_Y5="$root"_Y_6_Y_5.xyz
        file_Y6_Y6="$root"_Y_6_Y_6.xyz
        file_Y6_Z1="$root"_Y_6_Z_1.xyz
        file_Y6_Z2="$root"_Y_6_Z_2.xyz
        file_Y6_Z3="$root"_Y_6_Z_3.xyz
        file_Y6_Z4="$root"_Y_6_Z_4.xyz
        file_Y6_Z5="$root"_Y_6_Z_5.xyz
        file_Y6_Z6="$root"_Y_6_Z_6.xyz
        file_Z1_X1="$root"_Z_1_X_1.xyz
        file_Z1_X2="$root"_Z_1_X_2.xyz
        file_Z1_X3="$root"_Z_1_X_3.xyz
        file_Z1_X4="$root"_Z_1_X_4.xyz
        file_Z1_X5="$root"_Z_1_X_5.xyz
        file_Z1_X6="$root"_Z_1_X_6.xyz
        file_Z1_Y1="$root"_Z_1_Y_1.xyz
        file_Z1_Y2="$root"_Z_1_Y_2.xyz
        file_Z1_Y3="$root"_Z_1_Y_3.xyz
        file_Z1_Y4="$root"_Z_1_Y_4.xyz
        file_Z1_Y5="$root"_Z_1_Y_5.xyz
        file_Z1_Y6="$root"_Z_1_Y_6.xyz
        file_Z1_Z1="$root"_Z_1_Z_1.xyz
        file_Z1_Z2="$root"_Z_1_Z_2.xyz
        file_Z1_Z3="$root"_Z_1_Z_3.xyz
        file_Z1_Z4="$root"_Z_1_Z_4.xyz
        file_Z1_Z5="$root"_Z_1_Z_5.xyz
        file_Z1_Z6="$root"_Z_1_Z_6.xyz
        file_Z2_X1="$root"_Z_2_X_1.xyz
        file_Z2_X2="$root"_Z_2_X_2.xyz
        file_Z2_X3="$root"_Z_2_X_3.xyz
        file_Z2_X4="$root"_Z_2_X_4.xyz
        file_Z2_X5="$root"_Z_2_X_5.xyz
        file_Z2_X6="$root"_Z_2_X_6.xyz
        file_Z2_Y1="$root"_Z_2_Y_1.xyz
        file_Z2_Y2="$root"_Z_2_Y_2.xyz
        file_Z2_Y3="$root"_Z_2_Y_3.xyz
        file_Z2_Y4="$root"_Z_2_Y_4.xyz
        file_Z2_Y5="$root"_Z_2_Y_5.xyz
        file_Z2_Y6="$root"_Z_2_Y_6.xyz
        file_Z2_Z1="$root"_Z_2_Z_1.xyz
        file_Z2_Z2="$root"_Z_2_Z_2.xyz
        file_Z2_Z3="$root"_Z_2_Z_3.xyz
        file_Z2_Z4="$root"_Z_2_Z_4.xyz
        file_Z2_Z5="$root"_Z_2_Z_5.xyz
        file_Z2_Z6="$root"_Z_2_Z_6.xyz
        file_Z3_X1="$root"_Z_3_X_1.xyz
        file_Z3_X2="$root"_Z_3_X_2.xyz
        file_Z3_X3="$root"_Z_3_X_3.xyz
        file_Z3_X4="$root"_Z_3_X_4.xyz
        file_Z3_X5="$root"_Z_3_X_5.xyz
        file_Z3_X6="$root"_Z_3_X_6.xyz
        file_Z3_Y1="$root"_Z_3_Y_1.xyz
        file_Z3_Y2="$root"_Z_3_Y_2.xyz
        file_Z3_Y3="$root"_Z_3_Y_3.xyz
        file_Z3_Y4="$root"_Z_3_Y_4.xyz
        file_Z3_Y5="$root"_Z_3_Y_5.xyz
        file_Z3_Y6="$root"_Z_3_Y_6.xyz
        file_Z3_Z1="$root"_Z_3_Z_1.xyz
        file_Z3_Z2="$root"_Z_3_Z_2.xyz
        file_Z3_Z3="$root"_Z_3_Z_3.xyz
        file_Z3_Z4="$root"_Z_3_Z_4.xyz
        file_Z3_Z5="$root"_Z_3_Z_5.xyz
        file_Z3_Z6="$root"_Z_3_Z_6.xyz
        file_Z4_X1="$root"_Z_4_X_1.xyz
        file_Z4_X2="$root"_Z_4_X_2.xyz
        file_Z4_X3="$root"_Z_4_X_3.xyz
        file_Z4_X4="$root"_Z_4_X_4.xyz
        file_Z4_X5="$root"_Z_4_X_5.xyz
        file_Z4_X6="$root"_Z_4_X_6.xyz
        file_Z4_Y1="$root"_Z_4_Y_1.xyz
        file_Z4_Y2="$root"_Z_4_Y_2.xyz
        file_Z4_Y3="$root"_Z_4_Y_3.xyz
        file_Z4_Y4="$root"_Z_4_Y_4.xyz
        file_Z4_Y5="$root"_Z_4_Y_5.xyz
        file_Z4_Y6="$root"_Z_4_Y_6.xyz
        file_Z4_Z1="$root"_Z_4_Z_1.xyz
        file_Z4_Z2="$root"_Z_4_Z_2.xyz
        file_Z4_Z3="$root"_Z_4_Z_3.xyz
        file_Z4_Z4="$root"_Z_4_Z_4.xyz
        file_Z4_Z5="$root"_Z_4_Z_5.xyz
        file_Z4_Z6="$root"_Z_4_Z_6.xyz
        file_Z5_X1="$root"_Z_5_X_1.xyz
        file_Z5_X2="$root"_Z_5_X_2.xyz
        file_Z5_X3="$root"_Z_5_X_3.xyz
        file_Z5_X4="$root"_Z_5_X_4.xyz
        file_Z5_X5="$root"_Z_5_X_5.xyz
        file_Z5_X6="$root"_Z_5_X_6.xyz
        file_Z5_Y1="$root"_Z_5_Y_1.xyz
        file_Z5_Y2="$root"_Z_5_Y_2.xyz
        file_Z5_Y3="$root"_Z_5_Y_3.xyz
        file_Z5_Y4="$root"_Z_5_Y_4.xyz
        file_Z5_Y5="$root"_Z_5_Y_5.xyz
        file_Z5_Y6="$root"_Z_5_Y_6.xyz
        file_Z5_Z1="$root"_Z_5_Z_1.xyz
        file_Z5_Z2="$root"_Z_5_Z_2.xyz
        file_Z5_Z3="$root"_Z_5_Z_3.xyz
        file_Z5_Z4="$root"_Z_5_Z_4.xyz
        file_Z5_Z5="$root"_Z_5_Z_5.xyz
        file_Z5_Z6="$root"_Z_5_Z_6.xyz
        file_Z6_X1="$root"_Z_6_X_1.xyz
        file_Z6_X2="$root"_Z_6_X_2.xyz
        file_Z6_X3="$root"_Z_6_X_3.xyz
        file_Z6_X4="$root"_Z_6_X_4.xyz
        file_Z6_X5="$root"_Z_6_X_5.xyz
        file_Z6_X6="$root"_Z_6_X_6.xyz
        file_Z6_Y1="$root"_Z_6_Y_1.xyz
        file_Z6_Y2="$root"_Z_6_Y_2.xyz
        file_Z6_Y3="$root"_Z_6_Y_3.xyz
        file_Z6_Y4="$root"_Z_6_Y_4.xyz
        file_Z6_Y5="$root"_Z_6_Y_5.xyz
        file_Z6_Y6="$root"_Z_6_Y_6.xyz
        file_Z6_Z1="$root"_Z_6_Z_1.xyz
        file_Z6_Z2="$root"_Z_6_Z_2.xyz
        file_Z6_Z3="$root"_Z_6_Z_3.xyz
        file_Z6_Z4="$root"_Z_6_Z_4.xyz
        file_Z6_Z5="$root"_Z_6_Z_5.xyz
        file_Z6_Z6="$root"_Z_6_Z_6.xyz

        echo $natoms > $file_X1_X1
        echo $natoms > $file_X1_X2
        echo $natoms > $file_X1_X3
        echo $natoms > $file_X1_X4
        echo $natoms > $file_X1_X5
        echo $natoms > $file_X1_X6
        echo $natoms > $file_X1_Y1
        echo $natoms > $file_X1_Y2
        echo $natoms > $file_X1_Y3
        echo $natoms > $file_X1_Y4
        echo $natoms > $file_X1_Y5
        echo $natoms > $file_X1_Y6
        echo $natoms > $file_X1_Z1
        echo $natoms > $file_X1_Z2
        echo $natoms > $file_X1_Z3
        echo $natoms > $file_X1_Z4
        echo $natoms > $file_X1_Z5
        echo $natoms > $file_X1_Z6
        echo $natoms > $file_X2_X1
        echo $natoms > $file_X2_X2
        echo $natoms > $file_X2_X3
        echo $natoms > $file_X2_X4
        echo $natoms > $file_X2_X5
        echo $natoms > $file_X2_X6
        echo $natoms > $file_X2_Y1
        echo $natoms > $file_X2_Y2
        echo $natoms > $file_X2_Y3
        echo $natoms > $file_X2_Y4
        echo $natoms > $file_X2_Y5
        echo $natoms > $file_X2_Y6
        echo $natoms > $file_X2_Z1
        echo $natoms > $file_X2_Z2
        echo $natoms > $file_X2_Z3
        echo $natoms > $file_X2_Z4
        echo $natoms > $file_X2_Z5
        echo $natoms > $file_X2_Z6
        echo $natoms > $file_X3_X1
        echo $natoms > $file_X3_X2
        echo $natoms > $file_X3_X3
        echo $natoms > $file_X3_X4
        echo $natoms > $file_X3_X5
        echo $natoms > $file_X3_X6
        echo $natoms > $file_X3_Y1
        echo $natoms > $file_X3_Y2
        echo $natoms > $file_X3_Y3
        echo $natoms > $file_X3_Y4
        echo $natoms > $file_X3_Y5
        echo $natoms > $file_X3_Y6
        echo $natoms > $file_X3_Z1
        echo $natoms > $file_X3_Z2
        echo $natoms > $file_X3_Z3
        echo $natoms > $file_X3_Z4
        echo $natoms > $file_X3_Z5
        echo $natoms > $file_X3_Z6
        echo $natoms > $file_X4_X1
        echo $natoms > $file_X4_X2
        echo $natoms > $file_X4_X3
        echo $natoms > $file_X4_X4
        echo $natoms > $file_X4_X5
        echo $natoms > $file_X4_X6
        echo $natoms > $file_X4_Y1
        echo $natoms > $file_X4_Y2
        echo $natoms > $file_X4_Y3
        echo $natoms > $file_X4_Y4
        echo $natoms > $file_X4_Y5
        echo $natoms > $file_X4_Y6
        echo $natoms > $file_X4_Z1
        echo $natoms > $file_X4_Z2
        echo $natoms > $file_X4_Z3
        echo $natoms > $file_X4_Z4
        echo $natoms > $file_X4_Z5
        echo $natoms > $file_X4_Z6
        echo $natoms > $file_X5_X1
        echo $natoms > $file_X5_X2
        echo $natoms > $file_X5_X3
        echo $natoms > $file_X5_X4
        echo $natoms > $file_X5_X5
        echo $natoms > $file_X5_X6
        echo $natoms > $file_X5_Y1
        echo $natoms > $file_X5_Y2
        echo $natoms > $file_X5_Y3
        echo $natoms > $file_X5_Y4
        echo $natoms > $file_X5_Y5
        echo $natoms > $file_X5_Y6
        echo $natoms > $file_X5_Z1
        echo $natoms > $file_X5_Z2
        echo $natoms > $file_X5_Z3
        echo $natoms > $file_X5_Z4
        echo $natoms > $file_X5_Z5
        echo $natoms > $file_X5_Z6
        echo $natoms > $file_X6_X1
        echo $natoms > $file_X6_X2
        echo $natoms > $file_X6_X3
        echo $natoms > $file_X6_X4
        echo $natoms > $file_X6_X5
        echo $natoms > $file_X6_X6
        echo $natoms > $file_X6_Y1
        echo $natoms > $file_X6_Y2
        echo $natoms > $file_X6_Y3
        echo $natoms > $file_X6_Y4
        echo $natoms > $file_X6_Y5
        echo $natoms > $file_X6_Y6
        echo $natoms > $file_X6_Z1
        echo $natoms > $file_X6_Z2
        echo $natoms > $file_X6_Z3
        echo $natoms > $file_X6_Z4
        echo $natoms > $file_X6_Z5
        echo $natoms > $file_X6_Z6
        echo $natoms > $file_Y1_X1
        echo $natoms > $file_Y1_X2
        echo $natoms > $file_Y1_X3
        echo $natoms > $file_Y1_X4
        echo $natoms > $file_Y1_X5
        echo $natoms > $file_Y1_X6
        echo $natoms > $file_Y1_Y1
        echo $natoms > $file_Y1_Y2
        echo $natoms > $file_Y1_Y3
        echo $natoms > $file_Y1_Y4
        echo $natoms > $file_Y1_Y5
        echo $natoms > $file_Y1_Y6
        echo $natoms > $file_Y1_Z1
        echo $natoms > $file_Y1_Z2
        echo $natoms > $file_Y1_Z3
        echo $natoms > $file_Y1_Z4
        echo $natoms > $file_Y1_Z5
        echo $natoms > $file_Y1_Z6
        echo $natoms > $file_Y2_X1
        echo $natoms > $file_Y2_X2
        echo $natoms > $file_Y2_X3
        echo $natoms > $file_Y2_X4
        echo $natoms > $file_Y2_X5
        echo $natoms > $file_Y2_X6
        echo $natoms > $file_Y2_Y1
        echo $natoms > $file_Y2_Y2
        echo $natoms > $file_Y2_Y3
        echo $natoms > $file_Y2_Y4
        echo $natoms > $file_Y2_Y5
        echo $natoms > $file_Y2_Y6
        echo $natoms > $file_Y2_Z1
        echo $natoms > $file_Y2_Z2
        echo $natoms > $file_Y2_Z3
        echo $natoms > $file_Y2_Z4
        echo $natoms > $file_Y2_Z5
        echo $natoms > $file_Y2_Z6
        echo $natoms > $file_Y3_X1
        echo $natoms > $file_Y3_X2
        echo $natoms > $file_Y3_X3
        echo $natoms > $file_Y3_X4
        echo $natoms > $file_Y3_X5
        echo $natoms > $file_Y3_X6
        echo $natoms > $file_Y3_Y1
        echo $natoms > $file_Y3_Y2
        echo $natoms > $file_Y3_Y3
        echo $natoms > $file_Y3_Y4
        echo $natoms > $file_Y3_Y5
        echo $natoms > $file_Y3_Y6
        echo $natoms > $file_Y3_Z1
        echo $natoms > $file_Y3_Z2
        echo $natoms > $file_Y3_Z3
        echo $natoms > $file_Y3_Z4
        echo $natoms > $file_Y3_Z5
        echo $natoms > $file_Y3_Z6
        echo $natoms > $file_Y4_X1
        echo $natoms > $file_Y4_X2
        echo $natoms > $file_Y4_X3
        echo $natoms > $file_Y4_X4
        echo $natoms > $file_Y4_X5
        echo $natoms > $file_Y4_X6
        echo $natoms > $file_Y4_Y1
        echo $natoms > $file_Y4_Y2
        echo $natoms > $file_Y4_Y3
        echo $natoms > $file_Y4_Y4
        echo $natoms > $file_Y4_Y5
        echo $natoms > $file_Y4_Y6
        echo $natoms > $file_Y4_Z1
        echo $natoms > $file_Y4_Z2
        echo $natoms > $file_Y4_Z3
        echo $natoms > $file_Y4_Z4
        echo $natoms > $file_Y4_Z5
        echo $natoms > $file_Y4_Z6
        echo $natoms > $file_Y5_X1
        echo $natoms > $file_Y5_X2
        echo $natoms > $file_Y5_X3
        echo $natoms > $file_Y5_X4
        echo $natoms > $file_Y5_X5
        echo $natoms > $file_Y5_X6
        echo $natoms > $file_Y5_Y1
        echo $natoms > $file_Y5_Y2
        echo $natoms > $file_Y5_Y3
        echo $natoms > $file_Y5_Y4
        echo $natoms > $file_Y5_Y5
        echo $natoms > $file_Y5_Y6
        echo $natoms > $file_Y5_Z1
        echo $natoms > $file_Y5_Z2
        echo $natoms > $file_Y5_Z3
        echo $natoms > $file_Y5_Z4
        echo $natoms > $file_Y5_Z5
        echo $natoms > $file_Y5_Z6
        echo $natoms > $file_Y6_X1
        echo $natoms > $file_Y6_X2
        echo $natoms > $file_Y6_X3
        echo $natoms > $file_Y6_X4
        echo $natoms > $file_Y6_X5
        echo $natoms > $file_Y6_X6
        echo $natoms > $file_Y6_Y1
        echo $natoms > $file_Y6_Y2
        echo $natoms > $file_Y6_Y3
        echo $natoms > $file_Y6_Y4
        echo $natoms > $file_Y6_Y5
        echo $natoms > $file_Y6_Y6
        echo $natoms > $file_Y6_Z1
        echo $natoms > $file_Y6_Z2
        echo $natoms > $file_Y6_Z3
        echo $natoms > $file_Y6_Z4
        echo $natoms > $file_Y6_Z5
        echo $natoms > $file_Y6_Z6
        echo $natoms > $file_Z1_X1
        echo $natoms > $file_Z1_X2
        echo $natoms > $file_Z1_X3
        echo $natoms > $file_Z1_X4
        echo $natoms > $file_Z1_X5
        echo $natoms > $file_Z1_X6
        echo $natoms > $file_Z1_Y1
        echo $natoms > $file_Z1_Y2
        echo $natoms > $file_Z1_Y3
        echo $natoms > $file_Z1_Y4
        echo $natoms > $file_Z1_Y5
        echo $natoms > $file_Z1_Y6
        echo $natoms > $file_Z1_Z1
        echo $natoms > $file_Z1_Z2
        echo $natoms > $file_Z1_Z3
        echo $natoms > $file_Z1_Z4
        echo $natoms > $file_Z1_Z5
        echo $natoms > $file_Z1_Z6
        echo $natoms > $file_Z2_X1
        echo $natoms > $file_Z2_X2
        echo $natoms > $file_Z2_X3
        echo $natoms > $file_Z2_X4
        echo $natoms > $file_Z2_X5
        echo $natoms > $file_Z2_X6
        echo $natoms > $file_Z2_Y1
        echo $natoms > $file_Z2_Y2
        echo $natoms > $file_Z2_Y3
        echo $natoms > $file_Z2_Y4
        echo $natoms > $file_Z2_Y5
        echo $natoms > $file_Z2_Y6
        echo $natoms > $file_Z2_Z1
        echo $natoms > $file_Z2_Z2
        echo $natoms > $file_Z2_Z3
        echo $natoms > $file_Z2_Z4
        echo $natoms > $file_Z2_Z5
        echo $natoms > $file_Z2_Z6
        echo $natoms > $file_Z3_X1
        echo $natoms > $file_Z3_X2
        echo $natoms > $file_Z3_X3
        echo $natoms > $file_Z3_X4
        echo $natoms > $file_Z3_X5
        echo $natoms > $file_Z3_X6
        echo $natoms > $file_Z3_Y1
        echo $natoms > $file_Z3_Y2
        echo $natoms > $file_Z3_Y3
        echo $natoms > $file_Z3_Y4
        echo $natoms > $file_Z3_Y5
        echo $natoms > $file_Z3_Y6
        echo $natoms > $file_Z3_Z1
        echo $natoms > $file_Z3_Z2
        echo $natoms > $file_Z3_Z3
        echo $natoms > $file_Z3_Z4
        echo $natoms > $file_Z3_Z5
        echo $natoms > $file_Z3_Z6
        echo $natoms > $file_Z4_X1
        echo $natoms > $file_Z4_X2
        echo $natoms > $file_Z4_X3
        echo $natoms > $file_Z4_X4
        echo $natoms > $file_Z4_X5
        echo $natoms > $file_Z4_X6
        echo $natoms > $file_Z4_Y1
        echo $natoms > $file_Z4_Y2
        echo $natoms > $file_Z4_Y3
        echo $natoms > $file_Z4_Y4
        echo $natoms > $file_Z4_Y5
        echo $natoms > $file_Z4_Y6
        echo $natoms > $file_Z4_Z1
        echo $natoms > $file_Z4_Z2
        echo $natoms > $file_Z4_Z3
        echo $natoms > $file_Z4_Z4
        echo $natoms > $file_Z4_Z5
        echo $natoms > $file_Z4_Z6
        echo $natoms > $file_Z5_X1
        echo $natoms > $file_Z5_X2
        echo $natoms > $file_Z5_X3
        echo $natoms > $file_Z5_X4
        echo $natoms > $file_Z5_X5
        echo $natoms > $file_Z5_X6
        echo $natoms > $file_Z5_Y1
        echo $natoms > $file_Z5_Y2
        echo $natoms > $file_Z5_Y3
        echo $natoms > $file_Z5_Y4
        echo $natoms > $file_Z5_Y5
        echo $natoms > $file_Z5_Y6
        echo $natoms > $file_Z5_Z1
        echo $natoms > $file_Z5_Z2
        echo $natoms > $file_Z5_Z3
        echo $natoms > $file_Z5_Z4
        echo $natoms > $file_Z5_Z5
        echo $natoms > $file_Z5_Z6
        echo $natoms > $file_Z6_X1
        echo $natoms > $file_Z6_X2
        echo $natoms > $file_Z6_X3
        echo $natoms > $file_Z6_X4
        echo $natoms > $file_Z6_X5
        echo $natoms > $file_Z6_X6
        echo $natoms > $file_Z6_Y1
        echo $natoms > $file_Z6_Y2
        echo $natoms > $file_Z6_Y3
        echo $natoms > $file_Z6_Y4
        echo $natoms > $file_Z6_Y5
        echo $natoms > $file_Z6_Y6
        echo $natoms > $file_Z6_Z1
        echo $natoms > $file_Z6_Z2
        echo $natoms > $file_Z6_Z3
        echo $natoms > $file_Z6_Z4
        echo $natoms > $file_Z6_Z5
        echo $natoms > $file_Z6_Z6

        echo "$Complex distorted "$fname"_X_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_X1
        echo "$Complex distorted "$fname"_X_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_X2
        echo "$Complex distorted "$fname"_X_1_X_3  D1=$D1 D2=$D3 Bohr "    >> $file_X1_X3
        echo "$Complex distorted "$fname"_X_1_X_4  D1=$D1 D2=$D4 Bohr "    >> $file_X1_X4
        echo "$Complex distorted "$fname"_X_1_X_5  D1=$D1 D2=$D5 Bohr "    >> $file_X1_X5
        echo "$Complex distorted "$fname"_X_1_X_6  D1=$D1 D2=$D6 Bohr "    >> $file_X1_X6
        echo "$Complex distorted "$fname"_X_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_Y1
        echo "$Complex distorted "$fname"_X_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_Y2
        echo "$Complex distorted "$fname"_X_1_Y_3  D1=$D1 D2=$D3 Bohr "    >> $file_X1_Y3
        echo "$Complex distorted "$fname"_X_1_Y_4  D1=$D1 D2=$D4 Bohr "    >> $file_X1_Y4
        echo "$Complex distorted "$fname"_X_1_Y_5  D1=$D1 D2=$D5 Bohr "    >> $file_X1_Y5
        echo "$Complex distorted "$fname"_X_1_Y_6  D1=$D1 D2=$D6 Bohr "    >> $file_X1_Y6
        echo "$Complex distorted "$fname"_X_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_Z1
        echo "$Complex distorted "$fname"_X_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_Z2
        echo "$Complex distorted "$fname"_X_1_Z_3  D1=$D1 D2=$D3 Bohr "    >> $file_X1_Z3
        echo "$Complex distorted "$fname"_X_1_Z_4  D1=$D1 D2=$D4 Bohr "    >> $file_X1_Z4
        echo "$Complex distorted "$fname"_X_1_Z_5  D1=$D1 D2=$D5 Bohr "    >> $file_X1_Z5
        echo "$Complex distorted "$fname"_X_1_Z_6  D1=$D1 D2=$D6 Bohr "    >> $file_X1_Z6
        echo "$Complex distorted "$fname"_X_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_X1
        echo "$Complex distorted "$fname"_X_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_X2
        echo "$Complex distorted "$fname"_X_2_X_3  D1=$D2 D2=$D3 Bohr "    >> $file_X2_X3
        echo "$Complex distorted "$fname"_X_2_X_4  D1=$D2 D2=$D4 Bohr "    >> $file_X2_X4
        echo "$Complex distorted "$fname"_X_2_X_5  D1=$D2 D2=$D5 Bohr "    >> $file_X2_X5
        echo "$Complex distorted "$fname"_X_2_X_6  D1=$D2 D2=$D6 Bohr "    >> $file_X2_X6
        echo "$Complex distorted "$fname"_X_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_Y1
        echo "$Complex distorted "$fname"_X_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_Y2
        echo "$Complex distorted "$fname"_X_2_Y_3  D1=$D2 D2=$D3 Bohr "    >> $file_X2_Y3
        echo "$Complex distorted "$fname"_X_2_Y_4  D1=$D2 D2=$D4 Bohr "    >> $file_X2_Y4
        echo "$Complex distorted "$fname"_X_2_Y_5  D1=$D2 D2=$D5 Bohr "    >> $file_X2_Y5
        echo "$Complex distorted "$fname"_X_2_Y_6  D1=$D2 D2=$D6 Bohr "    >> $file_X2_Y6
        echo "$Complex distorted "$fname"_X_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_Z1
        echo "$Complex distorted "$fname"_X_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_Z2
        echo "$Complex distorted "$fname"_X_2_Z_3  D1=$D2 D2=$D3 Bohr "    >> $file_X2_Z3
        echo "$Complex distorted "$fname"_X_2_Z_4  D1=$D2 D2=$D4 Bohr "    >> $file_X2_Z4
        echo "$Complex distorted "$fname"_X_2_Z_5  D1=$D2 D2=$D5 Bohr "    >> $file_X2_Z5
        echo "$Complex distorted "$fname"_X_2_Z_6  D1=$D2 D2=$D6 Bohr "    >> $file_X2_Z6
        echo "$Complex distorted "$fname"_X_3_X_1  D1=$D3 D2=$D1 Bohr "    >> $file_X3_X1
        echo "$Complex distorted "$fname"_X_3_X_2  D1=$D3 D2=$D2 Bohr "    >> $file_X3_X2
        echo "$Complex distorted "$fname"_X_3_X_3  D1=$D3 D2=$D3 Bohr "    >> $file_X3_X3
        echo "$Complex distorted "$fname"_X_3_X_4  D1=$D3 D2=$D4 Bohr "    >> $file_X3_X4
        echo "$Complex distorted "$fname"_X_3_X_5  D1=$D3 D2=$D5 Bohr "    >> $file_X3_X5
        echo "$Complex distorted "$fname"_X_3_X_6  D1=$D3 D2=$D6 Bohr "    >> $file_X3_X6
        echo "$Complex distorted "$fname"_X_3_Y_1  D1=$D3 D2=$D1 Bohr "    >> $file_X3_Y1
        echo "$Complex distorted "$fname"_X_3_Y_2  D1=$D3 D2=$D2 Bohr "    >> $file_X3_Y2
        echo "$Complex distorted "$fname"_X_3_Y_3  D1=$D3 D2=$D3 Bohr "    >> $file_X3_Y3
        echo "$Complex distorted "$fname"_X_3_Y_4  D1=$D3 D2=$D4 Bohr "    >> $file_X3_Y4
        echo "$Complex distorted "$fname"_X_3_Y_5  D1=$D3 D2=$D5 Bohr "    >> $file_X3_Y5
        echo "$Complex distorted "$fname"_X_3_Y_6  D1=$D3 D2=$D6 Bohr "    >> $file_X3_Y6
        echo "$Complex distorted "$fname"_X_3_Z_1  D1=$D3 D2=$D1 Bohr "    >> $file_X3_Z1
        echo "$Complex distorted "$fname"_X_3_Z_2  D1=$D3 D2=$D2 Bohr "    >> $file_X3_Z2
        echo "$Complex distorted "$fname"_X_3_Z_3  D1=$D3 D2=$D3 Bohr "    >> $file_X3_Z3
        echo "$Complex distorted "$fname"_X_3_Z_4  D1=$D3 D2=$D4 Bohr "    >> $file_X3_Z4
        echo "$Complex distorted "$fname"_X_3_Z_5  D1=$D3 D2=$D5 Bohr "    >> $file_X3_Z5
        echo "$Complex distorted "$fname"_X_3_Z_6  D1=$D3 D2=$D6 Bohr "    >> $file_X3_Z6
        echo "$Complex distorted "$fname"_X_4_X_1  D1=$D4 D2=$D1 Bohr "    >> $file_X4_X1
        echo "$Complex distorted "$fname"_X_4_X_2  D1=$D4 D2=$D2 Bohr "    >> $file_X4_X2
        echo "$Complex distorted "$fname"_X_4_X_3  D1=$D4 D2=$D3 Bohr "    >> $file_X4_X3
        echo "$Complex distorted "$fname"_X_4_X_4  D1=$D4 D2=$D4 Bohr "    >> $file_X4_X4
        echo "$Complex distorted "$fname"_X_4_X_5  D1=$D4 D2=$D5 Bohr "    >> $file_X4_X5
        echo "$Complex distorted "$fname"_X_4_X_6  D1=$D4 D2=$D6 Bohr "    >> $file_X4_X6
        echo "$Complex distorted "$fname"_X_4_Y_1  D1=$D4 D2=$D1 Bohr "    >> $file_X4_Y1
        echo "$Complex distorted "$fname"_X_4_Y_2  D1=$D4 D2=$D2 Bohr "    >> $file_X4_Y2
        echo "$Complex distorted "$fname"_X_4_Y_3  D1=$D4 D2=$D3 Bohr "    >> $file_X4_Y3
        echo "$Complex distorted "$fname"_X_4_Y_4  D1=$D4 D2=$D4 Bohr "    >> $file_X4_Y4
        echo "$Complex distorted "$fname"_X_4_Y_5  D1=$D4 D2=$D5 Bohr "    >> $file_X4_Y5
        echo "$Complex distorted "$fname"_X_4_Y_6  D1=$D4 D2=$D6 Bohr "    >> $file_X4_Y6
        echo "$Complex distorted "$fname"_X_4_Z_1  D1=$D4 D2=$D1 Bohr "    >> $file_X4_Z1
        echo "$Complex distorted "$fname"_X_4_Z_2  D1=$D4 D2=$D2 Bohr "    >> $file_X4_Z2
        echo "$Complex distorted "$fname"_X_4_Z_3  D1=$D4 D2=$D3 Bohr "    >> $file_X4_Z3
        echo "$Complex distorted "$fname"_X_4_Z_4  D1=$D4 D2=$D4 Bohr "    >> $file_X4_Z4
        echo "$Complex distorted "$fname"_X_4_Z_5  D1=$D4 D2=$D5 Bohr "    >> $file_X4_Z5
        echo "$Complex distorted "$fname"_X_4_Z_6  D1=$D4 D2=$D6 Bohr "    >> $file_X4_Z6
        echo "$Complex distorted "$fname"_X_5_X_1  D1=$D5 D2=$D1 Bohr "    >> $file_X5_X1
        echo "$Complex distorted "$fname"_X_5_X_2  D1=$D5 D2=$D2 Bohr "    >> $file_X5_X2
        echo "$Complex distorted "$fname"_X_5_X_3  D1=$D5 D2=$D3 Bohr "    >> $file_X5_X3
        echo "$Complex distorted "$fname"_X_5_X_4  D1=$D5 D2=$D4 Bohr "    >> $file_X5_X4
        echo "$Complex distorted "$fname"_X_5_X_5  D1=$D5 D2=$D5 Bohr "    >> $file_X5_X5
        echo "$Complex distorted "$fname"_X_5_X_6  D1=$D5 D2=$D6 Bohr "    >> $file_X5_X6
        echo "$Complex distorted "$fname"_X_5_Y_1  D1=$D5 D2=$D1 Bohr "    >> $file_X5_Y1
        echo "$Complex distorted "$fname"_X_5_Y_2  D1=$D5 D2=$D2 Bohr "    >> $file_X5_Y2
        echo "$Complex distorted "$fname"_X_5_Y_3  D1=$D5 D2=$D3 Bohr "    >> $file_X5_Y3
        echo "$Complex distorted "$fname"_X_5_Y_4  D1=$D5 D2=$D4 Bohr "    >> $file_X5_Y4
        echo "$Complex distorted "$fname"_X_5_Y_5  D1=$D5 D2=$D5 Bohr "    >> $file_X5_Y5
        echo "$Complex distorted "$fname"_X_5_Y_6  D1=$D5 D2=$D6 Bohr "    >> $file_X5_Y6
        echo "$Complex distorted "$fname"_X_5_Z_1  D1=$D5 D2=$D1 Bohr "    >> $file_X5_Z1
        echo "$Complex distorted "$fname"_X_5_Z_2  D1=$D5 D2=$D2 Bohr "    >> $file_X5_Z2
        echo "$Complex distorted "$fname"_X_5_Z_3  D1=$D5 D2=$D3 Bohr "    >> $file_X5_Z3
        echo "$Complex distorted "$fname"_X_5_Z_4  D1=$D5 D2=$D4 Bohr "    >> $file_X5_Z4
        echo "$Complex distorted "$fname"_X_5_Z_5  D1=$D5 D2=$D5 Bohr "    >> $file_X5_Z5
        echo "$Complex distorted "$fname"_X_5_Z_6  D1=$D5 D2=$D6 Bohr "    >> $file_X5_Z6
        echo "$Complex distorted "$fname"_X_6_X_1  D1=$D6 D2=$D1 Bohr "    >> $file_X6_X1
        echo "$Complex distorted "$fname"_X_6_X_2  D1=$D6 D2=$D2 Bohr "    >> $file_X6_X2
        echo "$Complex distorted "$fname"_X_6_X_3  D1=$D6 D2=$D3 Bohr "    >> $file_X6_X3
        echo "$Complex distorted "$fname"_X_6_X_4  D1=$D6 D2=$D4 Bohr "    >> $file_X6_X4
        echo "$Complex distorted "$fname"_X_6_X_5  D1=$D6 D2=$D5 Bohr "    >> $file_X6_X5
        echo "$Complex distorted "$fname"_X_6_X_6  D1=$D6 D2=$D6 Bohr "    >> $file_X6_X6
        echo "$Complex distorted "$fname"_X_6_Y_1  D1=$D6 D2=$D1 Bohr "    >> $file_X6_Y1
        echo "$Complex distorted "$fname"_X_6_Y_2  D1=$D6 D2=$D2 Bohr "    >> $file_X6_Y2
        echo "$Complex distorted "$fname"_X_6_Y_3  D1=$D6 D2=$D3 Bohr "    >> $file_X6_Y3
        echo "$Complex distorted "$fname"_X_6_Y_4  D1=$D6 D2=$D4 Bohr "    >> $file_X6_Y4
        echo "$Complex distorted "$fname"_X_6_Y_5  D1=$D6 D2=$D5 Bohr "    >> $file_X6_Y5
        echo "$Complex distorted "$fname"_X_6_Y_6  D1=$D6 D2=$D6 Bohr "    >> $file_X6_Y6
        echo "$Complex distorted "$fname"_X_6_Z_1  D1=$D6 D2=$D1 Bohr "    >> $file_X6_Z1
        echo "$Complex distorted "$fname"_X_6_Z_2  D1=$D6 D2=$D2 Bohr "    >> $file_X6_Z2
        echo "$Complex distorted "$fname"_X_6_Z_3  D1=$D6 D2=$D3 Bohr "    >> $file_X6_Z3
        echo "$Complex distorted "$fname"_X_6_Z_4  D1=$D6 D2=$D4 Bohr "    >> $file_X6_Z4
        echo "$Complex distorted "$fname"_X_6_Z_5  D1=$D6 D2=$D5 Bohr "    >> $file_X6_Z5
        echo "$Complex distorted "$fname"_X_6_Z_6  D1=$D6 D2=$D6 Bohr "    >> $file_X6_Z6
        echo "$Complex distorted "$fname"_Y_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_X1
        echo "$Complex distorted "$fname"_Y_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_X2
        echo "$Complex distorted "$fname"_Y_1_X_3  D1=$D1 D2=$D3 Bohr "    >> $file_Y1_X3
        echo "$Complex distorted "$fname"_Y_1_X_4  D1=$D1 D2=$D4 Bohr "    >> $file_Y1_X4
        echo "$Complex distorted "$fname"_Y_1_X_5  D1=$D1 D2=$D5 Bohr "    >> $file_Y1_X5
        echo "$Complex distorted "$fname"_Y_1_X_6  D1=$D1 D2=$D6 Bohr "    >> $file_Y1_X6
        echo "$Complex distorted "$fname"_Y_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_Y1
        echo "$Complex distorted "$fname"_Y_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_Y2
        echo "$Complex distorted "$fname"_Y_1_Y_3  D1=$D1 D2=$D3 Bohr "    >> $file_Y1_Y3
        echo "$Complex distorted "$fname"_Y_1_Y_4  D1=$D1 D2=$D4 Bohr "    >> $file_Y1_Y4
        echo "$Complex distorted "$fname"_Y_1_Y_5  D1=$D1 D2=$D5 Bohr "    >> $file_Y1_Y5
        echo "$Complex distorted "$fname"_Y_1_Y_6  D1=$D1 D2=$D6 Bohr "    >> $file_Y1_Y6
        echo "$Complex distorted "$fname"_Y_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_Z1
        echo "$Complex distorted "$fname"_Y_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_Z2
        echo "$Complex distorted "$fname"_Y_1_Z_3  D1=$D1 D2=$D3 Bohr "    >> $file_Y1_Z3
        echo "$Complex distorted "$fname"_Y_1_Z_4  D1=$D1 D2=$D4 Bohr "    >> $file_Y1_Z4
        echo "$Complex distorted "$fname"_Y_1_Z_5  D1=$D1 D2=$D5 Bohr "    >> $file_Y1_Z5
        echo "$Complex distorted "$fname"_Y_1_Z_6  D1=$D1 D2=$D6 Bohr "    >> $file_Y1_Z6
        echo "$Complex distorted "$fname"_Y_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_X1
        echo "$Complex distorted "$fname"_Y_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_X2
        echo "$Complex distorted "$fname"_Y_2_X_3  D1=$D2 D2=$D3 Bohr "    >> $file_Y2_X3
        echo "$Complex distorted "$fname"_Y_2_X_4  D1=$D2 D2=$D4 Bohr "    >> $file_Y2_X4
        echo "$Complex distorted "$fname"_Y_2_X_5  D1=$D2 D2=$D5 Bohr "    >> $file_Y2_X5
        echo "$Complex distorted "$fname"_Y_2_X_6  D1=$D2 D2=$D6 Bohr "    >> $file_Y2_X6
        echo "$Complex distorted "$fname"_Y_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_Y1
        echo "$Complex distorted "$fname"_Y_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_Y2
        echo "$Complex distorted "$fname"_Y_2_Y_3  D1=$D2 D2=$D3 Bohr "    >> $file_Y2_Y3
        echo "$Complex distorted "$fname"_Y_2_Y_4  D1=$D2 D2=$D4 Bohr "    >> $file_Y2_Y4
        echo "$Complex distorted "$fname"_Y_2_Y_5  D1=$D2 D2=$D5 Bohr "    >> $file_Y2_Y5
        echo "$Complex distorted "$fname"_Y_2_Y_6  D1=$D2 D2=$D6 Bohr "    >> $file_Y2_Y6
        echo "$Complex distorted "$fname"_Y_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_Z1
        echo "$Complex distorted "$fname"_Y_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_Z2
        echo "$Complex distorted "$fname"_Y_2_Z_3  D1=$D2 D2=$D3 Bohr "    >> $file_Y2_Z3
        echo "$Complex distorted "$fname"_Y_2_Z_4  D1=$D2 D2=$D4 Bohr "    >> $file_Y2_Z4
        echo "$Complex distorted "$fname"_Y_2_Z_5  D1=$D2 D2=$D5 Bohr "    >> $file_Y2_Z5
        echo "$Complex distorted "$fname"_Y_2_Z_6  D1=$D2 D2=$D6 Bohr "    >> $file_Y2_Z6
        echo "$Complex distorted "$fname"_Y_3_X_1  D1=$D3 D2=$D1 Bohr "    >> $file_Y3_X1
        echo "$Complex distorted "$fname"_Y_3_X_2  D1=$D3 D2=$D2 Bohr "    >> $file_Y3_X2
        echo "$Complex distorted "$fname"_Y_3_X_3  D1=$D3 D2=$D3 Bohr "    >> $file_Y3_X3
        echo "$Complex distorted "$fname"_Y_3_X_4  D1=$D3 D2=$D4 Bohr "    >> $file_Y3_X4
        echo "$Complex distorted "$fname"_Y_3_X_5  D1=$D3 D2=$D5 Bohr "    >> $file_Y3_X5
        echo "$Complex distorted "$fname"_Y_3_X_6  D1=$D3 D2=$D6 Bohr "    >> $file_Y3_X6
        echo "$Complex distorted "$fname"_Y_3_Y_1  D1=$D3 D2=$D1 Bohr "    >> $file_Y3_Y1
        echo "$Complex distorted "$fname"_Y_3_Y_2  D1=$D3 D2=$D2 Bohr "    >> $file_Y3_Y2
        echo "$Complex distorted "$fname"_Y_3_Y_3  D1=$D3 D2=$D3 Bohr "    >> $file_Y3_Y3
        echo "$Complex distorted "$fname"_Y_3_Y_4  D1=$D3 D2=$D4 Bohr "    >> $file_Y3_Y4
        echo "$Complex distorted "$fname"_Y_3_Y_5  D1=$D3 D2=$D5 Bohr "    >> $file_Y3_Y5
        echo "$Complex distorted "$fname"_Y_3_Y_6  D1=$D3 D2=$D6 Bohr "    >> $file_Y3_Y6
        echo "$Complex distorted "$fname"_Y_3_Z_1  D1=$D3 D2=$D1 Bohr "    >> $file_Y3_Z1
        echo "$Complex distorted "$fname"_Y_3_Z_2  D1=$D3 D2=$D2 Bohr "    >> $file_Y3_Z2
        echo "$Complex distorted "$fname"_Y_3_Z_3  D1=$D3 D2=$D3 Bohr "    >> $file_Y3_Z3
        echo "$Complex distorted "$fname"_Y_3_Z_4  D1=$D3 D2=$D4 Bohr "    >> $file_Y3_Z4
        echo "$Complex distorted "$fname"_Y_3_Z_5  D1=$D3 D2=$D5 Bohr "    >> $file_Y3_Z5
        echo "$Complex distorted "$fname"_Y_3_Z_6  D1=$D3 D2=$D6 Bohr "    >> $file_Y3_Z6
        echo "$Complex distorted "$fname"_Y_4_X_1  D1=$D4 D2=$D1 Bohr "    >> $file_Y4_X1
        echo "$Complex distorted "$fname"_Y_4_X_2  D1=$D4 D2=$D2 Bohr "    >> $file_Y4_X2
        echo "$Complex distorted "$fname"_Y_4_X_3  D1=$D4 D2=$D3 Bohr "    >> $file_Y4_X3
        echo "$Complex distorted "$fname"_Y_4_X_4  D1=$D4 D2=$D4 Bohr "    >> $file_Y4_X4
        echo "$Complex distorted "$fname"_Y_4_X_5  D1=$D4 D2=$D5 Bohr "    >> $file_Y4_X5
        echo "$Complex distorted "$fname"_Y_4_X_6  D1=$D4 D2=$D6 Bohr "    >> $file_Y4_X6
        echo "$Complex distorted "$fname"_Y_4_Y_1  D1=$D4 D2=$D1 Bohr "    >> $file_Y4_Y1
        echo "$Complex distorted "$fname"_Y_4_Y_2  D1=$D4 D2=$D2 Bohr "    >> $file_Y4_Y2
        echo "$Complex distorted "$fname"_Y_4_Y_3  D1=$D4 D2=$D3 Bohr "    >> $file_Y4_Y3
        echo "$Complex distorted "$fname"_Y_4_Y_4  D1=$D4 D2=$D4 Bohr "    >> $file_Y4_Y4
        echo "$Complex distorted "$fname"_Y_4_Y_5  D1=$D4 D2=$D5 Bohr "    >> $file_Y4_Y5
        echo "$Complex distorted "$fname"_Y_4_Y_6  D1=$D4 D2=$D6 Bohr "    >> $file_Y4_Y6
        echo "$Complex distorted "$fname"_Y_4_Z_1  D1=$D4 D2=$D1 Bohr "    >> $file_Y4_Z1
        echo "$Complex distorted "$fname"_Y_4_Z_2  D1=$D4 D2=$D2 Bohr "    >> $file_Y4_Z2
        echo "$Complex distorted "$fname"_Y_4_Z_3  D1=$D4 D2=$D3 Bohr "    >> $file_Y4_Z3
        echo "$Complex distorted "$fname"_Y_4_Z_4  D1=$D4 D2=$D4 Bohr "    >> $file_Y4_Z4
        echo "$Complex distorted "$fname"_Y_4_Z_5  D1=$D4 D2=$D5 Bohr "    >> $file_Y4_Z5
        echo "$Complex distorted "$fname"_Y_4_Z_6  D1=$D4 D2=$D6 Bohr "    >> $file_Y4_Z6
        echo "$Complex distorted "$fname"_Y_5_X_1  D1=$D5 D2=$D1 Bohr "    >> $file_Y5_X1
        echo "$Complex distorted "$fname"_Y_5_X_2  D1=$D5 D2=$D2 Bohr "    >> $file_Y5_X2
        echo "$Complex distorted "$fname"_Y_5_X_3  D1=$D5 D2=$D3 Bohr "    >> $file_Y5_X3
        echo "$Complex distorted "$fname"_Y_5_X_4  D1=$D5 D2=$D4 Bohr "    >> $file_Y5_X4
        echo "$Complex distorted "$fname"_Y_5_X_5  D1=$D5 D2=$D5 Bohr "    >> $file_Y5_X5
        echo "$Complex distorted "$fname"_Y_5_X_6  D1=$D5 D2=$D6 Bohr "    >> $file_Y5_X6
        echo "$Complex distorted "$fname"_Y_5_Y_1  D1=$D5 D2=$D1 Bohr "    >> $file_Y5_Y1
        echo "$Complex distorted "$fname"_Y_5_Y_2  D1=$D5 D2=$D2 Bohr "    >> $file_Y5_Y2
        echo "$Complex distorted "$fname"_Y_5_Y_3  D1=$D5 D2=$D3 Bohr "    >> $file_Y5_Y3
        echo "$Complex distorted "$fname"_Y_5_Y_4  D1=$D5 D2=$D4 Bohr "    >> $file_Y5_Y4
        echo "$Complex distorted "$fname"_Y_5_Y_5  D1=$D5 D2=$D5 Bohr "    >> $file_Y5_Y5
        echo "$Complex distorted "$fname"_Y_5_Y_6  D1=$D5 D2=$D6 Bohr "    >> $file_Y5_Y6
        echo "$Complex distorted "$fname"_Y_5_Z_1  D1=$D5 D2=$D1 Bohr "    >> $file_Y5_Z1
        echo "$Complex distorted "$fname"_Y_5_Z_2  D1=$D5 D2=$D2 Bohr "    >> $file_Y5_Z2
        echo "$Complex distorted "$fname"_Y_5_Z_3  D1=$D5 D2=$D3 Bohr "    >> $file_Y5_Z3
        echo "$Complex distorted "$fname"_Y_5_Z_4  D1=$D5 D2=$D4 Bohr "    >> $file_Y5_Z4
        echo "$Complex distorted "$fname"_Y_5_Z_5  D1=$D5 D2=$D5 Bohr "    >> $file_Y5_Z5
        echo "$Complex distorted "$fname"_Y_5_Z_6  D1=$D5 D2=$D6 Bohr "    >> $file_Y5_Z6
        echo "$Complex distorted "$fname"_Y_6_X_1  D1=$D6 D2=$D1 Bohr "    >> $file_Y6_X1
        echo "$Complex distorted "$fname"_Y_6_X_2  D1=$D6 D2=$D2 Bohr "    >> $file_Y6_X2
        echo "$Complex distorted "$fname"_Y_6_X_3  D1=$D6 D2=$D3 Bohr "    >> $file_Y6_X3
        echo "$Complex distorted "$fname"_Y_6_X_4  D1=$D6 D2=$D4 Bohr "    >> $file_Y6_X4
        echo "$Complex distorted "$fname"_Y_6_X_5  D1=$D6 D2=$D5 Bohr "    >> $file_Y6_X5
        echo "$Complex distorted "$fname"_Y_6_X_6  D1=$D6 D2=$D6 Bohr "    >> $file_Y6_X6
        echo "$Complex distorted "$fname"_Y_6_Y_1  D1=$D6 D2=$D1 Bohr "    >> $file_Y6_Y1
        echo "$Complex distorted "$fname"_Y_6_Y_2  D1=$D6 D2=$D2 Bohr "    >> $file_Y6_Y2
        echo "$Complex distorted "$fname"_Y_6_Y_3  D1=$D6 D2=$D3 Bohr "    >> $file_Y6_Y3
        echo "$Complex distorted "$fname"_Y_6_Y_4  D1=$D6 D2=$D4 Bohr "    >> $file_Y6_Y4
        echo "$Complex distorted "$fname"_Y_6_Y_5  D1=$D6 D2=$D5 Bohr "    >> $file_Y6_Y5
        echo "$Complex distorted "$fname"_Y_6_Y_6  D1=$D6 D2=$D6 Bohr "    >> $file_Y6_Y6
        echo "$Complex distorted "$fname"_Y_6_Z_1  D1=$D6 D2=$D1 Bohr "    >> $file_Y6_Z1
        echo "$Complex distorted "$fname"_Y_6_Z_2  D1=$D6 D2=$D2 Bohr "    >> $file_Y6_Z2
        echo "$Complex distorted "$fname"_Y_6_Z_3  D1=$D6 D2=$D3 Bohr "    >> $file_Y6_Z3
        echo "$Complex distorted "$fname"_Y_6_Z_4  D1=$D6 D2=$D4 Bohr "    >> $file_Y6_Z4
        echo "$Complex distorted "$fname"_Y_6_Z_5  D1=$D6 D2=$D5 Bohr "    >> $file_Y6_Z5
        echo "$Complex distorted "$fname"_Y_6_Z_6  D1=$D6 D2=$D6 Bohr "    >> $file_Y6_Z6
        echo "$Complex distorted "$fname"_Z_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_X1
        echo "$Complex distorted "$fname"_Z_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_X2
        echo "$Complex distorted "$fname"_Z_1_X_3  D1=$D1 D2=$D3 Bohr "    >> $file_Z1_X3
        echo "$Complex distorted "$fname"_Z_1_X_4  D1=$D1 D2=$D4 Bohr "    >> $file_Z1_X4
        echo "$Complex distorted "$fname"_Z_1_X_5  D1=$D1 D2=$D5 Bohr "    >> $file_Z1_X5
        echo "$Complex distorted "$fname"_Z_1_X_6  D1=$D1 D2=$D6 Bohr "    >> $file_Z1_X6
        echo "$Complex distorted "$fname"_Z_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_Y1
        echo "$Complex distorted "$fname"_Z_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_Y2
        echo "$Complex distorted "$fname"_Z_1_Y_3  D1=$D1 D2=$D3 Bohr "    >> $file_Z1_Y3
        echo "$Complex distorted "$fname"_Z_1_Y_4  D1=$D1 D2=$D4 Bohr "    >> $file_Z1_Y4
        echo "$Complex distorted "$fname"_Z_1_Y_5  D1=$D1 D2=$D5 Bohr "    >> $file_Z1_Y5
        echo "$Complex distorted "$fname"_Z_1_Y_6  D1=$D1 D2=$D6 Bohr "    >> $file_Z1_Y6
        echo "$Complex distorted "$fname"_Z_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_Z1
        echo "$Complex distorted "$fname"_Z_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_Z2
        echo "$Complex distorted "$fname"_Z_1_Z_3  D1=$D1 D2=$D3 Bohr "    >> $file_Z1_Z3
        echo "$Complex distorted "$fname"_Z_1_Z_4  D1=$D1 D2=$D4 Bohr "    >> $file_Z1_Z4
        echo "$Complex distorted "$fname"_Z_1_Z_5  D1=$D1 D2=$D5 Bohr "    >> $file_Z1_Z5
        echo "$Complex distorted "$fname"_Z_1_Z_6  D1=$D1 D2=$D6 Bohr "    >> $file_Z1_Z6
        echo "$Complex distorted "$fname"_Z_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_X1
        echo "$Complex distorted "$fname"_Z_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_X2
        echo "$Complex distorted "$fname"_Z_2_X_3  D1=$D2 D2=$D3 Bohr "    >> $file_Z2_X3
        echo "$Complex distorted "$fname"_Z_2_X_4  D1=$D2 D2=$D4 Bohr "    >> $file_Z2_X4
        echo "$Complex distorted "$fname"_Z_2_X_5  D1=$D2 D2=$D5 Bohr "    >> $file_Z2_X5
        echo "$Complex distorted "$fname"_Z_2_X_6  D1=$D2 D2=$D6 Bohr "    >> $file_Z2_X6
        echo "$Complex distorted "$fname"_Z_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_Y1
        echo "$Complex distorted "$fname"_Z_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_Y2
        echo "$Complex distorted "$fname"_Z_2_Y_3  D1=$D2 D2=$D3 Bohr "    >> $file_Z2_Y3
        echo "$Complex distorted "$fname"_Z_2_Y_4  D1=$D2 D2=$D4 Bohr "    >> $file_Z2_Y4
        echo "$Complex distorted "$fname"_Z_2_Y_5  D1=$D2 D2=$D5 Bohr "    >> $file_Z2_Y5
        echo "$Complex distorted "$fname"_Z_2_Y_6  D1=$D2 D2=$D6 Bohr "    >> $file_Z2_Y6
        echo "$Complex distorted "$fname"_Z_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_Z1
        echo "$Complex distorted "$fname"_Z_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_Z2
        echo "$Complex distorted "$fname"_Z_2_Z_3  D1=$D2 D2=$D3 Bohr "    >> $file_Z2_Z3
        echo "$Complex distorted "$fname"_Z_2_Z_4  D1=$D2 D2=$D4 Bohr "    >> $file_Z2_Z4
        echo "$Complex distorted "$fname"_Z_2_Z_5  D1=$D2 D2=$D5 Bohr "    >> $file_Z2_Z5
        echo "$Complex distorted "$fname"_Z_2_Z_6  D1=$D2 D2=$D6 Bohr "    >> $file_Z2_Z6
        echo "$Complex distorted "$fname"_Z_3_X_1  D1=$D3 D2=$D1 Bohr "    >> $file_Z3_X1
        echo "$Complex distorted "$fname"_Z_3_X_2  D1=$D3 D2=$D2 Bohr "    >> $file_Z3_X2
        echo "$Complex distorted "$fname"_Z_3_X_3  D1=$D3 D2=$D3 Bohr "    >> $file_Z3_X3
        echo "$Complex distorted "$fname"_Z_3_X_4  D1=$D3 D2=$D4 Bohr "    >> $file_Z3_X4
        echo "$Complex distorted "$fname"_Z_3_X_5  D1=$D3 D2=$D5 Bohr "    >> $file_Z3_X5
        echo "$Complex distorted "$fname"_Z_3_X_6  D1=$D3 D2=$D6 Bohr "    >> $file_Z3_X6
        echo "$Complex distorted "$fname"_Z_3_Y_1  D1=$D3 D2=$D1 Bohr "    >> $file_Z3_Y1
        echo "$Complex distorted "$fname"_Z_3_Y_2  D1=$D3 D2=$D2 Bohr "    >> $file_Z3_Y2
        echo "$Complex distorted "$fname"_Z_3_Y_3  D1=$D3 D2=$D3 Bohr "    >> $file_Z3_Y3
        echo "$Complex distorted "$fname"_Z_3_Y_4  D1=$D3 D2=$D4 Bohr "    >> $file_Z3_Y4
        echo "$Complex distorted "$fname"_Z_3_Y_5  D1=$D3 D2=$D5 Bohr "    >> $file_Z3_Y5
        echo "$Complex distorted "$fname"_Z_3_Y_6  D1=$D3 D2=$D6 Bohr "    >> $file_Z3_Y6
        echo "$Complex distorted "$fname"_Z_3_Z_1  D1=$D3 D2=$D1 Bohr "    >> $file_Z3_Z1
        echo "$Complex distorted "$fname"_Z_3_Z_2  D1=$D3 D2=$D2 Bohr "    >> $file_Z3_Z2
        echo "$Complex distorted "$fname"_Z_3_Z_3  D1=$D3 D2=$D3 Bohr "    >> $file_Z3_Z3
        echo "$Complex distorted "$fname"_Z_3_Z_4  D1=$D3 D2=$D4 Bohr "    >> $file_Z3_Z4
        echo "$Complex distorted "$fname"_Z_3_Z_5  D1=$D3 D2=$D5 Bohr "    >> $file_Z3_Z5
        echo "$Complex distorted "$fname"_Z_3_Z_6  D1=$D3 D2=$D6 Bohr "    >> $file_Z3_Z6
        echo "$Complex distorted "$fname"_Z_4_X_1  D1=$D4 D2=$D1 Bohr "    >> $file_Z4_X1
        echo "$Complex distorted "$fname"_Z_4_X_2  D1=$D4 D2=$D2 Bohr "    >> $file_Z4_X2
        echo "$Complex distorted "$fname"_Z_4_X_3  D1=$D4 D2=$D3 Bohr "    >> $file_Z4_X3
        echo "$Complex distorted "$fname"_Z_4_X_4  D1=$D4 D2=$D4 Bohr "    >> $file_Z4_X4
        echo "$Complex distorted "$fname"_Z_4_X_5  D1=$D4 D2=$D5 Bohr "    >> $file_Z4_X5
        echo "$Complex distorted "$fname"_Z_4_X_6  D1=$D4 D2=$D6 Bohr "    >> $file_Z4_X6
        echo "$Complex distorted "$fname"_Z_4_Y_1  D1=$D4 D2=$D1 Bohr "    >> $file_Z4_Y1
        echo "$Complex distorted "$fname"_Z_4_Y_2  D1=$D4 D2=$D2 Bohr "    >> $file_Z4_Y2
        echo "$Complex distorted "$fname"_Z_4_Y_3  D1=$D4 D2=$D3 Bohr "    >> $file_Z4_Y3
        echo "$Complex distorted "$fname"_Z_4_Y_4  D1=$D4 D2=$D4 Bohr "    >> $file_Z4_Y4
        echo "$Complex distorted "$fname"_Z_4_Y_5  D1=$D4 D2=$D5 Bohr "    >> $file_Z4_Y5
        echo "$Complex distorted "$fname"_Z_4_Y_6  D1=$D4 D2=$D6 Bohr "    >> $file_Z4_Y6
        echo "$Complex distorted "$fname"_Z_4_Z_1  D1=$D4 D2=$D1 Bohr "    >> $file_Z4_Z1
        echo "$Complex distorted "$fname"_Z_4_Z_2  D1=$D4 D2=$D2 Bohr "    >> $file_Z4_Z2
        echo "$Complex distorted "$fname"_Z_4_Z_3  D1=$D4 D2=$D3 Bohr "    >> $file_Z4_Z3
        echo "$Complex distorted "$fname"_Z_4_Z_4  D1=$D4 D2=$D4 Bohr "    >> $file_Z4_Z4
        echo "$Complex distorted "$fname"_Z_4_Z_5  D1=$D4 D2=$D5 Bohr "    >> $file_Z4_Z5
        echo "$Complex distorted "$fname"_Z_4_Z_6  D1=$D4 D2=$D6 Bohr "    >> $file_Z4_Z6
        echo "$Complex distorted "$fname"_Z_5_X_1  D1=$D5 D2=$D1 Bohr "    >> $file_Z5_X1
        echo "$Complex distorted "$fname"_Z_5_X_2  D1=$D5 D2=$D2 Bohr "    >> $file_Z5_X2
        echo "$Complex distorted "$fname"_Z_5_X_3  D1=$D5 D2=$D3 Bohr "    >> $file_Z5_X3
        echo "$Complex distorted "$fname"_Z_5_X_4  D1=$D5 D2=$D4 Bohr "    >> $file_Z5_X4
        echo "$Complex distorted "$fname"_Z_5_X_5  D1=$D5 D2=$D5 Bohr "    >> $file_Z5_X5
        echo "$Complex distorted "$fname"_Z_5_X_6  D1=$D5 D2=$D6 Bohr "    >> $file_Z5_X6
        echo "$Complex distorted "$fname"_Z_5_Y_1  D1=$D5 D2=$D1 Bohr "    >> $file_Z5_Y1
        echo "$Complex distorted "$fname"_Z_5_Y_2  D1=$D5 D2=$D2 Bohr "    >> $file_Z5_Y2
        echo "$Complex distorted "$fname"_Z_5_Y_3  D1=$D5 D2=$D3 Bohr "    >> $file_Z5_Y3
        echo "$Complex distorted "$fname"_Z_5_Y_4  D1=$D5 D2=$D4 Bohr "    >> $file_Z5_Y4
        echo "$Complex distorted "$fname"_Z_5_Y_5  D1=$D5 D2=$D5 Bohr "    >> $file_Z5_Y5
        echo "$Complex distorted "$fname"_Z_5_Y_6  D1=$D5 D2=$D6 Bohr "    >> $file_Z5_Y6
        echo "$Complex distorted "$fname"_Z_5_Z_1  D1=$D5 D2=$D1 Bohr "    >> $file_Z5_Z1
        echo "$Complex distorted "$fname"_Z_5_Z_2  D1=$D5 D2=$D2 Bohr "    >> $file_Z5_Z2
        echo "$Complex distorted "$fname"_Z_5_Z_3  D1=$D5 D2=$D3 Bohr "    >> $file_Z5_Z3
        echo "$Complex distorted "$fname"_Z_5_Z_4  D1=$D5 D2=$D4 Bohr "    >> $file_Z5_Z4
        echo "$Complex distorted "$fname"_Z_5_Z_5  D1=$D5 D2=$D5 Bohr "    >> $file_Z5_Z5
        echo "$Complex distorted "$fname"_Z_5_Z_6  D1=$D5 D2=$D6 Bohr "    >> $file_Z5_Z6
        echo "$Complex distorted "$fname"_Z_6_X_1  D1=$D6 D2=$D1 Bohr "    >> $file_Z6_X1
        echo "$Complex distorted "$fname"_Z_6_X_2  D1=$D6 D2=$D2 Bohr "    >> $file_Z6_X2
        echo "$Complex distorted "$fname"_Z_6_X_3  D1=$D6 D2=$D3 Bohr "    >> $file_Z6_X3
        echo "$Complex distorted "$fname"_Z_6_X_4  D1=$D6 D2=$D4 Bohr "    >> $file_Z6_X4
        echo "$Complex distorted "$fname"_Z_6_X_5  D1=$D6 D2=$D5 Bohr "    >> $file_Z6_X5
        echo "$Complex distorted "$fname"_Z_6_X_6  D1=$D6 D2=$D6 Bohr "    >> $file_Z6_X6
        echo "$Complex distorted "$fname"_Z_6_Y_1  D1=$D6 D2=$D1 Bohr "    >> $file_Z6_Y1
        echo "$Complex distorted "$fname"_Z_6_Y_2  D1=$D6 D2=$D2 Bohr "    >> $file_Z6_Y2
        echo "$Complex distorted "$fname"_Z_6_Y_3  D1=$D6 D2=$D3 Bohr "    >> $file_Z6_Y3
        echo "$Complex distorted "$fname"_Z_6_Y_4  D1=$D6 D2=$D4 Bohr "    >> $file_Z6_Y4
        echo "$Complex distorted "$fname"_Z_6_Y_5  D1=$D6 D2=$D5 Bohr "    >> $file_Z6_Y5
        echo "$Complex distorted "$fname"_Z_6_Y_6  D1=$D6 D2=$D6 Bohr "    >> $file_Z6_Y6
        echo "$Complex distorted "$fname"_Z_6_Z_1  D1=$D6 D2=$D1 Bohr "    >> $file_Z6_Z1
        echo "$Complex distorted "$fname"_Z_6_Z_2  D1=$D6 D2=$D2 Bohr "    >> $file_Z6_Z2
        echo "$Complex distorted "$fname"_Z_6_Z_3  D1=$D6 D2=$D3 Bohr "    >> $file_Z6_Z3
        echo "$Complex distorted "$fname"_Z_6_Z_4  D1=$D6 D2=$D4 Bohr "    >> $file_Z6_Z4
        echo "$Complex distorted "$fname"_Z_6_Z_5  D1=$D6 D2=$D5 Bohr "    >> $file_Z6_Z5
        echo "$Complex distorted "$fname"_Z_6_Z_6  D1=$D6 D2=$D6 Bohr "    >> $file_Z6_Z6

    elif [[ $npoints -eq 8 ]] ; then
        D1=`echo " $distortion * ( -4.0 ) " | bc -l`
        D2=`echo " $distortion * ( -3.0 ) " | bc -l`
        D3=`echo " $distortion * ( -2.0 ) " | bc -l`
        D4=`echo " $distortion * ( -1.0 ) " | bc -l`
        D5=`echo " $distortion * (  1.0 ) " | bc -l`
        D6=`echo " $distortion * (  2.0 ) " | bc -l`
        D7=`echo " $distortion * (  3.0 ) " | bc -l`
        D8=`echo " $distortion * (  4.0 ) " | bc -l`

        file_X1_X1="$root"_X_1_X_1.xyz
        file_X1_X2="$root"_X_1_X_2.xyz
        file_X1_X3="$root"_X_1_X_3.xyz
        file_X1_X4="$root"_X_1_X_4.xyz
        file_X1_X5="$root"_X_1_X_5.xyz
        file_X1_X6="$root"_X_1_X_6.xyz
        file_X1_X7="$root"_X_1_X_7.xyz
        file_X1_X8="$root"_X_1_X_8.xyz
        file_X1_Y1="$root"_X_1_Y_1.xyz
        file_X1_Y2="$root"_X_1_Y_2.xyz
        file_X1_Y3="$root"_X_1_Y_3.xyz
        file_X1_Y4="$root"_X_1_Y_4.xyz
        file_X1_Y5="$root"_X_1_Y_5.xyz
        file_X1_Y6="$root"_X_1_Y_6.xyz
        file_X1_Y7="$root"_X_1_Y_7.xyz
        file_X1_Y8="$root"_X_1_Y_8.xyz
        file_X1_Z1="$root"_X_1_Z_1.xyz
        file_X1_Z2="$root"_X_1_Z_2.xyz
        file_X1_Z3="$root"_X_1_Z_3.xyz
        file_X1_Z4="$root"_X_1_Z_4.xyz
        file_X1_Z5="$root"_X_1_Z_5.xyz
        file_X1_Z6="$root"_X_1_Z_6.xyz
        file_X1_Z7="$root"_X_1_Z_7.xyz
        file_X1_Z8="$root"_X_1_Z_8.xyz
        file_X2_X1="$root"_X_2_X_1.xyz
        file_X2_X2="$root"_X_2_X_2.xyz
        file_X2_X3="$root"_X_2_X_3.xyz
        file_X2_X4="$root"_X_2_X_4.xyz
        file_X2_X5="$root"_X_2_X_5.xyz
        file_X2_X6="$root"_X_2_X_6.xyz
        file_X2_X7="$root"_X_2_X_7.xyz
        file_X2_X8="$root"_X_2_X_8.xyz
        file_X2_Y1="$root"_X_2_Y_1.xyz
        file_X2_Y2="$root"_X_2_Y_2.xyz
        file_X2_Y3="$root"_X_2_Y_3.xyz
        file_X2_Y4="$root"_X_2_Y_4.xyz
        file_X2_Y5="$root"_X_2_Y_5.xyz
        file_X2_Y6="$root"_X_2_Y_6.xyz
        file_X2_Y7="$root"_X_2_Y_7.xyz
        file_X2_Y8="$root"_X_2_Y_8.xyz
        file_X2_Z1="$root"_X_2_Z_1.xyz
        file_X2_Z2="$root"_X_2_Z_2.xyz
        file_X2_Z3="$root"_X_2_Z_3.xyz
        file_X2_Z4="$root"_X_2_Z_4.xyz
        file_X2_Z5="$root"_X_2_Z_5.xyz
        file_X2_Z6="$root"_X_2_Z_6.xyz
        file_X2_Z7="$root"_X_2_Z_7.xyz
        file_X2_Z8="$root"_X_2_Z_8.xyz
        file_X3_X1="$root"_X_3_X_1.xyz
        file_X3_X2="$root"_X_3_X_2.xyz
        file_X3_X3="$root"_X_3_X_3.xyz
        file_X3_X4="$root"_X_3_X_4.xyz
        file_X3_X5="$root"_X_3_X_5.xyz
        file_X3_X6="$root"_X_3_X_6.xyz
        file_X3_X7="$root"_X_3_X_7.xyz
        file_X3_X8="$root"_X_3_X_8.xyz
        file_X3_Y1="$root"_X_3_Y_1.xyz
        file_X3_Y2="$root"_X_3_Y_2.xyz
        file_X3_Y3="$root"_X_3_Y_3.xyz
        file_X3_Y4="$root"_X_3_Y_4.xyz
        file_X3_Y5="$root"_X_3_Y_5.xyz
        file_X3_Y6="$root"_X_3_Y_6.xyz
        file_X3_Y7="$root"_X_3_Y_7.xyz
        file_X3_Y8="$root"_X_3_Y_8.xyz
        file_X3_Z1="$root"_X_3_Z_1.xyz
        file_X3_Z2="$root"_X_3_Z_2.xyz
        file_X3_Z3="$root"_X_3_Z_3.xyz
        file_X3_Z4="$root"_X_3_Z_4.xyz
        file_X3_Z5="$root"_X_3_Z_5.xyz
        file_X3_Z6="$root"_X_3_Z_6.xyz
        file_X3_Z7="$root"_X_3_Z_7.xyz
        file_X3_Z8="$root"_X_3_Z_8.xyz
        file_X4_X1="$root"_X_4_X_1.xyz
        file_X4_X2="$root"_X_4_X_2.xyz
        file_X4_X3="$root"_X_4_X_3.xyz
        file_X4_X4="$root"_X_4_X_4.xyz
        file_X4_X5="$root"_X_4_X_5.xyz
        file_X4_X6="$root"_X_4_X_6.xyz
        file_X4_X7="$root"_X_4_X_7.xyz
        file_X4_X8="$root"_X_4_X_8.xyz
        file_X4_Y1="$root"_X_4_Y_1.xyz
        file_X4_Y2="$root"_X_4_Y_2.xyz
        file_X4_Y3="$root"_X_4_Y_3.xyz
        file_X4_Y4="$root"_X_4_Y_4.xyz
        file_X4_Y5="$root"_X_4_Y_5.xyz
        file_X4_Y6="$root"_X_4_Y_6.xyz
        file_X4_Y7="$root"_X_4_Y_7.xyz
        file_X4_Y8="$root"_X_4_Y_8.xyz
        file_X4_Z1="$root"_X_4_Z_1.xyz
        file_X4_Z2="$root"_X_4_Z_2.xyz
        file_X4_Z3="$root"_X_4_Z_3.xyz
        file_X4_Z4="$root"_X_4_Z_4.xyz
        file_X4_Z5="$root"_X_4_Z_5.xyz
        file_X4_Z6="$root"_X_4_Z_6.xyz
        file_X4_Z7="$root"_X_4_Z_7.xyz
        file_X4_Z8="$root"_X_4_Z_8.xyz
        file_X5_X1="$root"_X_5_X_1.xyz
        file_X5_X2="$root"_X_5_X_2.xyz
        file_X5_X3="$root"_X_5_X_3.xyz
        file_X5_X4="$root"_X_5_X_4.xyz
        file_X5_X5="$root"_X_5_X_5.xyz
        file_X5_X6="$root"_X_5_X_6.xyz
        file_X5_X7="$root"_X_5_X_7.xyz
        file_X5_X8="$root"_X_5_X_8.xyz
        file_X5_Y1="$root"_X_5_Y_1.xyz
        file_X5_Y2="$root"_X_5_Y_2.xyz
        file_X5_Y3="$root"_X_5_Y_3.xyz
        file_X5_Y4="$root"_X_5_Y_4.xyz
        file_X5_Y5="$root"_X_5_Y_5.xyz
        file_X5_Y6="$root"_X_5_Y_6.xyz
        file_X5_Y7="$root"_X_5_Y_7.xyz
        file_X5_Y8="$root"_X_5_Y_8.xyz
        file_X5_Z1="$root"_X_5_Z_1.xyz
        file_X5_Z2="$root"_X_5_Z_2.xyz
        file_X5_Z3="$root"_X_5_Z_3.xyz
        file_X5_Z4="$root"_X_5_Z_4.xyz
        file_X5_Z5="$root"_X_5_Z_5.xyz
        file_X5_Z6="$root"_X_5_Z_6.xyz
        file_X5_Z7="$root"_X_5_Z_7.xyz
        file_X5_Z8="$root"_X_5_Z_8.xyz
        file_X6_X1="$root"_X_6_X_1.xyz
        file_X6_X2="$root"_X_6_X_2.xyz
        file_X6_X3="$root"_X_6_X_3.xyz
        file_X6_X4="$root"_X_6_X_4.xyz
        file_X6_X5="$root"_X_6_X_5.xyz
        file_X6_X6="$root"_X_6_X_6.xyz
        file_X6_X7="$root"_X_6_X_7.xyz
        file_X6_X8="$root"_X_6_X_8.xyz
        file_X6_Y1="$root"_X_6_Y_1.xyz
        file_X6_Y2="$root"_X_6_Y_2.xyz
        file_X6_Y3="$root"_X_6_Y_3.xyz
        file_X6_Y4="$root"_X_6_Y_4.xyz
        file_X6_Y5="$root"_X_6_Y_5.xyz
        file_X6_Y6="$root"_X_6_Y_6.xyz
        file_X6_Y7="$root"_X_6_Y_7.xyz
        file_X6_Y8="$root"_X_6_Y_8.xyz
        file_X6_Z1="$root"_X_6_Z_1.xyz
        file_X6_Z2="$root"_X_6_Z_2.xyz
        file_X6_Z3="$root"_X_6_Z_3.xyz
        file_X6_Z4="$root"_X_6_Z_4.xyz
        file_X6_Z5="$root"_X_6_Z_5.xyz
        file_X6_Z6="$root"_X_6_Z_6.xyz
        file_X6_Z7="$root"_X_6_Z_7.xyz
        file_X6_Z8="$root"_X_6_Z_8.xyz
        file_X7_X1="$root"_X_7_X_1.xyz
        file_X7_X2="$root"_X_7_X_2.xyz
        file_X7_X3="$root"_X_7_X_3.xyz
        file_X7_X4="$root"_X_7_X_4.xyz
        file_X7_X5="$root"_X_7_X_5.xyz
        file_X7_X6="$root"_X_7_X_6.xyz
        file_X7_X7="$root"_X_7_X_7.xyz
        file_X7_X8="$root"_X_7_X_8.xyz
        file_X7_Y1="$root"_X_7_Y_1.xyz
        file_X7_Y2="$root"_X_7_Y_2.xyz
        file_X7_Y3="$root"_X_7_Y_3.xyz
        file_X7_Y4="$root"_X_7_Y_4.xyz
        file_X7_Y5="$root"_X_7_Y_5.xyz
        file_X7_Y6="$root"_X_7_Y_6.xyz
        file_X7_Y7="$root"_X_7_Y_7.xyz
        file_X7_Y8="$root"_X_7_Y_8.xyz
        file_X7_Z1="$root"_X_7_Z_1.xyz
        file_X7_Z2="$root"_X_7_Z_2.xyz
        file_X7_Z3="$root"_X_7_Z_3.xyz
        file_X7_Z4="$root"_X_7_Z_4.xyz
        file_X7_Z5="$root"_X_7_Z_5.xyz
        file_X7_Z6="$root"_X_7_Z_6.xyz
        file_X7_Z7="$root"_X_7_Z_7.xyz
        file_X7_Z8="$root"_X_7_Z_8.xyz
        file_X8_X1="$root"_X_8_X_1.xyz
        file_X8_X2="$root"_X_8_X_2.xyz
        file_X8_X3="$root"_X_8_X_3.xyz
        file_X8_X4="$root"_X_8_X_4.xyz
        file_X8_X5="$root"_X_8_X_5.xyz
        file_X8_X6="$root"_X_8_X_6.xyz
        file_X8_X7="$root"_X_8_X_7.xyz
        file_X8_X8="$root"_X_8_X_8.xyz
        file_X8_Y1="$root"_X_8_Y_1.xyz
        file_X8_Y2="$root"_X_8_Y_2.xyz
        file_X8_Y3="$root"_X_8_Y_3.xyz
        file_X8_Y4="$root"_X_8_Y_4.xyz
        file_X8_Y5="$root"_X_8_Y_5.xyz
        file_X8_Y6="$root"_X_8_Y_6.xyz
        file_X8_Y7="$root"_X_8_Y_7.xyz
        file_X8_Y8="$root"_X_8_Y_8.xyz
        file_X8_Z1="$root"_X_8_Z_1.xyz
        file_X8_Z2="$root"_X_8_Z_2.xyz
        file_X8_Z3="$root"_X_8_Z_3.xyz
        file_X8_Z4="$root"_X_8_Z_4.xyz
        file_X8_Z5="$root"_X_8_Z_5.xyz
        file_X8_Z6="$root"_X_8_Z_6.xyz
        file_X8_Z7="$root"_X_8_Z_7.xyz
        file_X8_Z8="$root"_X_8_Z_8.xyz
        file_Y1_X1="$root"_Y_1_X_1.xyz
        file_Y1_X2="$root"_Y_1_X_2.xyz
        file_Y1_X3="$root"_Y_1_X_3.xyz
        file_Y1_X4="$root"_Y_1_X_4.xyz
        file_Y1_X5="$root"_Y_1_X_5.xyz
        file_Y1_X6="$root"_Y_1_X_6.xyz
        file_Y1_X7="$root"_Y_1_X_7.xyz
        file_Y1_X8="$root"_Y_1_X_8.xyz
        file_Y1_Y1="$root"_Y_1_Y_1.xyz
        file_Y1_Y2="$root"_Y_1_Y_2.xyz
        file_Y1_Y3="$root"_Y_1_Y_3.xyz
        file_Y1_Y4="$root"_Y_1_Y_4.xyz
        file_Y1_Y5="$root"_Y_1_Y_5.xyz
        file_Y1_Y6="$root"_Y_1_Y_6.xyz
        file_Y1_Y7="$root"_Y_1_Y_7.xyz
        file_Y1_Y8="$root"_Y_1_Y_8.xyz
        file_Y1_Z1="$root"_Y_1_Z_1.xyz
        file_Y1_Z2="$root"_Y_1_Z_2.xyz
        file_Y1_Z3="$root"_Y_1_Z_3.xyz
        file_Y1_Z4="$root"_Y_1_Z_4.xyz
        file_Y1_Z5="$root"_Y_1_Z_5.xyz
        file_Y1_Z6="$root"_Y_1_Z_6.xyz
        file_Y1_Z7="$root"_Y_1_Z_7.xyz
        file_Y1_Z8="$root"_Y_1_Z_8.xyz
        file_Y2_X1="$root"_Y_2_X_1.xyz
        file_Y2_X2="$root"_Y_2_X_2.xyz
        file_Y2_X3="$root"_Y_2_X_3.xyz
        file_Y2_X4="$root"_Y_2_X_4.xyz
        file_Y2_X5="$root"_Y_2_X_5.xyz
        file_Y2_X6="$root"_Y_2_X_6.xyz
        file_Y2_X7="$root"_Y_2_X_7.xyz
        file_Y2_X8="$root"_Y_2_X_8.xyz
        file_Y2_Y1="$root"_Y_2_Y_1.xyz
        file_Y2_Y2="$root"_Y_2_Y_2.xyz
        file_Y2_Y3="$root"_Y_2_Y_3.xyz
        file_Y2_Y4="$root"_Y_2_Y_4.xyz
        file_Y2_Y5="$root"_Y_2_Y_5.xyz
        file_Y2_Y6="$root"_Y_2_Y_6.xyz
        file_Y2_Y7="$root"_Y_2_Y_7.xyz
        file_Y2_Y8="$root"_Y_2_Y_8.xyz
        file_Y2_Z1="$root"_Y_2_Z_1.xyz
        file_Y2_Z2="$root"_Y_2_Z_2.xyz
        file_Y2_Z3="$root"_Y_2_Z_3.xyz
        file_Y2_Z4="$root"_Y_2_Z_4.xyz
        file_Y2_Z5="$root"_Y_2_Z_5.xyz
        file_Y2_Z6="$root"_Y_2_Z_6.xyz
        file_Y2_Z7="$root"_Y_2_Z_7.xyz
        file_Y2_Z8="$root"_Y_2_Z_8.xyz
        file_Y3_X1="$root"_Y_3_X_1.xyz
        file_Y3_X2="$root"_Y_3_X_2.xyz
        file_Y3_X3="$root"_Y_3_X_3.xyz
        file_Y3_X4="$root"_Y_3_X_4.xyz
        file_Y3_X5="$root"_Y_3_X_5.xyz
        file_Y3_X6="$root"_Y_3_X_6.xyz
        file_Y3_X7="$root"_Y_3_X_7.xyz
        file_Y3_X8="$root"_Y_3_X_8.xyz
        file_Y3_Y1="$root"_Y_3_Y_1.xyz
        file_Y3_Y2="$root"_Y_3_Y_2.xyz
        file_Y3_Y3="$root"_Y_3_Y_3.xyz
        file_Y3_Y4="$root"_Y_3_Y_4.xyz
        file_Y3_Y5="$root"_Y_3_Y_5.xyz
        file_Y3_Y6="$root"_Y_3_Y_6.xyz
        file_Y3_Y7="$root"_Y_3_Y_7.xyz
        file_Y3_Y8="$root"_Y_3_Y_8.xyz
        file_Y3_Z1="$root"_Y_3_Z_1.xyz
        file_Y3_Z2="$root"_Y_3_Z_2.xyz
        file_Y3_Z3="$root"_Y_3_Z_3.xyz
        file_Y3_Z4="$root"_Y_3_Z_4.xyz
        file_Y3_Z5="$root"_Y_3_Z_5.xyz
        file_Y3_Z6="$root"_Y_3_Z_6.xyz
        file_Y3_Z7="$root"_Y_3_Z_7.xyz
        file_Y3_Z8="$root"_Y_3_Z_8.xyz
        file_Y4_X1="$root"_Y_4_X_1.xyz
        file_Y4_X2="$root"_Y_4_X_2.xyz
        file_Y4_X3="$root"_Y_4_X_3.xyz
        file_Y4_X4="$root"_Y_4_X_4.xyz
        file_Y4_X5="$root"_Y_4_X_5.xyz
        file_Y4_X6="$root"_Y_4_X_6.xyz
        file_Y4_X7="$root"_Y_4_X_7.xyz
        file_Y4_X8="$root"_Y_4_X_8.xyz
        file_Y4_Y1="$root"_Y_4_Y_1.xyz
        file_Y4_Y2="$root"_Y_4_Y_2.xyz
        file_Y4_Y3="$root"_Y_4_Y_3.xyz
        file_Y4_Y4="$root"_Y_4_Y_4.xyz
        file_Y4_Y5="$root"_Y_4_Y_5.xyz
        file_Y4_Y6="$root"_Y_4_Y_6.xyz
        file_Y4_Y7="$root"_Y_4_Y_7.xyz
        file_Y4_Y8="$root"_Y_4_Y_8.xyz
        file_Y4_Z1="$root"_Y_4_Z_1.xyz
        file_Y4_Z2="$root"_Y_4_Z_2.xyz
        file_Y4_Z3="$root"_Y_4_Z_3.xyz
        file_Y4_Z4="$root"_Y_4_Z_4.xyz
        file_Y4_Z5="$root"_Y_4_Z_5.xyz
        file_Y4_Z6="$root"_Y_4_Z_6.xyz
        file_Y4_Z7="$root"_Y_4_Z_7.xyz
        file_Y4_Z8="$root"_Y_4_Z_8.xyz
        file_Y5_X1="$root"_Y_5_X_1.xyz
        file_Y5_X2="$root"_Y_5_X_2.xyz
        file_Y5_X3="$root"_Y_5_X_3.xyz
        file_Y5_X4="$root"_Y_5_X_4.xyz
        file_Y5_X5="$root"_Y_5_X_5.xyz
        file_Y5_X6="$root"_Y_5_X_6.xyz
        file_Y5_X7="$root"_Y_5_X_7.xyz
        file_Y5_X8="$root"_Y_5_X_8.xyz
        file_Y5_Y1="$root"_Y_5_Y_1.xyz
        file_Y5_Y2="$root"_Y_5_Y_2.xyz
        file_Y5_Y3="$root"_Y_5_Y_3.xyz
        file_Y5_Y4="$root"_Y_5_Y_4.xyz
        file_Y5_Y5="$root"_Y_5_Y_5.xyz
        file_Y5_Y6="$root"_Y_5_Y_6.xyz
        file_Y5_Y7="$root"_Y_5_Y_7.xyz
        file_Y5_Y8="$root"_Y_5_Y_8.xyz
        file_Y5_Z1="$root"_Y_5_Z_1.xyz
        file_Y5_Z2="$root"_Y_5_Z_2.xyz
        file_Y5_Z3="$root"_Y_5_Z_3.xyz
        file_Y5_Z4="$root"_Y_5_Z_4.xyz
        file_Y5_Z5="$root"_Y_5_Z_5.xyz
        file_Y5_Z6="$root"_Y_5_Z_6.xyz
        file_Y5_Z7="$root"_Y_5_Z_7.xyz
        file_Y5_Z8="$root"_Y_5_Z_8.xyz
        file_Y6_X1="$root"_Y_6_X_1.xyz
        file_Y6_X2="$root"_Y_6_X_2.xyz
        file_Y6_X3="$root"_Y_6_X_3.xyz
        file_Y6_X4="$root"_Y_6_X_4.xyz
        file_Y6_X5="$root"_Y_6_X_5.xyz
        file_Y6_X6="$root"_Y_6_X_6.xyz
        file_Y6_X7="$root"_Y_6_X_7.xyz
        file_Y6_X8="$root"_Y_6_X_8.xyz
        file_Y6_Y1="$root"_Y_6_Y_1.xyz
        file_Y6_Y2="$root"_Y_6_Y_2.xyz
        file_Y6_Y3="$root"_Y_6_Y_3.xyz
        file_Y6_Y4="$root"_Y_6_Y_4.xyz
        file_Y6_Y5="$root"_Y_6_Y_5.xyz
        file_Y6_Y6="$root"_Y_6_Y_6.xyz
        file_Y6_Y7="$root"_Y_6_Y_7.xyz
        file_Y6_Y8="$root"_Y_6_Y_8.xyz
        file_Y6_Z1="$root"_Y_6_Z_1.xyz
        file_Y6_Z2="$root"_Y_6_Z_2.xyz
        file_Y6_Z3="$root"_Y_6_Z_3.xyz
        file_Y6_Z4="$root"_Y_6_Z_4.xyz
        file_Y6_Z5="$root"_Y_6_Z_5.xyz
        file_Y6_Z6="$root"_Y_6_Z_6.xyz
        file_Y6_Z7="$root"_Y_6_Z_7.xyz
        file_Y6_Z8="$root"_Y_6_Z_8.xyz
        file_Y7_X1="$root"_Y_7_X_1.xyz
        file_Y7_X2="$root"_Y_7_X_2.xyz
        file_Y7_X3="$root"_Y_7_X_3.xyz
        file_Y7_X4="$root"_Y_7_X_4.xyz
        file_Y7_X5="$root"_Y_7_X_5.xyz
        file_Y7_X6="$root"_Y_7_X_6.xyz
        file_Y7_X7="$root"_Y_7_X_7.xyz
        file_Y7_X8="$root"_Y_7_X_8.xyz
        file_Y7_Y1="$root"_Y_7_Y_1.xyz
        file_Y7_Y2="$root"_Y_7_Y_2.xyz
        file_Y7_Y3="$root"_Y_7_Y_3.xyz
        file_Y7_Y4="$root"_Y_7_Y_4.xyz
        file_Y7_Y5="$root"_Y_7_Y_5.xyz
        file_Y7_Y6="$root"_Y_7_Y_6.xyz
        file_Y7_Y7="$root"_Y_7_Y_7.xyz
        file_Y7_Y8="$root"_Y_7_Y_8.xyz
        file_Y7_Z1="$root"_Y_7_Z_1.xyz
        file_Y7_Z2="$root"_Y_7_Z_2.xyz
        file_Y7_Z3="$root"_Y_7_Z_3.xyz
        file_Y7_Z4="$root"_Y_7_Z_4.xyz
        file_Y7_Z5="$root"_Y_7_Z_5.xyz
        file_Y7_Z6="$root"_Y_7_Z_6.xyz
        file_Y7_Z7="$root"_Y_7_Z_7.xyz
        file_Y7_Z8="$root"_Y_7_Z_8.xyz
        file_Y8_X1="$root"_Y_8_X_1.xyz
        file_Y8_X2="$root"_Y_8_X_2.xyz
        file_Y8_X3="$root"_Y_8_X_3.xyz
        file_Y8_X4="$root"_Y_8_X_4.xyz
        file_Y8_X5="$root"_Y_8_X_5.xyz
        file_Y8_X6="$root"_Y_8_X_6.xyz
        file_Y8_X7="$root"_Y_8_X_7.xyz
        file_Y8_X8="$root"_Y_8_X_8.xyz
        file_Y8_Y1="$root"_Y_8_Y_1.xyz
        file_Y8_Y2="$root"_Y_8_Y_2.xyz
        file_Y8_Y3="$root"_Y_8_Y_3.xyz
        file_Y8_Y4="$root"_Y_8_Y_4.xyz
        file_Y8_Y5="$root"_Y_8_Y_5.xyz
        file_Y8_Y6="$root"_Y_8_Y_6.xyz
        file_Y8_Y7="$root"_Y_8_Y_7.xyz
        file_Y8_Y8="$root"_Y_8_Y_8.xyz
        file_Y8_Z1="$root"_Y_8_Z_1.xyz
        file_Y8_Z2="$root"_Y_8_Z_2.xyz
        file_Y8_Z3="$root"_Y_8_Z_3.xyz
        file_Y8_Z4="$root"_Y_8_Z_4.xyz
        file_Y8_Z5="$root"_Y_8_Z_5.xyz
        file_Y8_Z6="$root"_Y_8_Z_6.xyz
        file_Y8_Z7="$root"_Y_8_Z_7.xyz
        file_Y8_Z8="$root"_Y_8_Z_8.xyz
        file_Z1_X1="$root"_Z_1_X_1.xyz
        file_Z1_X2="$root"_Z_1_X_2.xyz
        file_Z1_X3="$root"_Z_1_X_3.xyz
        file_Z1_X4="$root"_Z_1_X_4.xyz
        file_Z1_X5="$root"_Z_1_X_5.xyz
        file_Z1_X6="$root"_Z_1_X_6.xyz
        file_Z1_X7="$root"_Z_1_X_7.xyz
        file_Z1_X8="$root"_Z_1_X_8.xyz
        file_Z1_Y1="$root"_Z_1_Y_1.xyz
        file_Z1_Y2="$root"_Z_1_Y_2.xyz
        file_Z1_Y3="$root"_Z_1_Y_3.xyz
        file_Z1_Y4="$root"_Z_1_Y_4.xyz
        file_Z1_Y5="$root"_Z_1_Y_5.xyz
        file_Z1_Y6="$root"_Z_1_Y_6.xyz
        file_Z1_Y7="$root"_Z_1_Y_7.xyz
        file_Z1_Y8="$root"_Z_1_Y_8.xyz
        file_Z1_Z1="$root"_Z_1_Z_1.xyz
        file_Z1_Z2="$root"_Z_1_Z_2.xyz
        file_Z1_Z3="$root"_Z_1_Z_3.xyz
        file_Z1_Z4="$root"_Z_1_Z_4.xyz
        file_Z1_Z5="$root"_Z_1_Z_5.xyz
        file_Z1_Z6="$root"_Z_1_Z_6.xyz
        file_Z1_Z7="$root"_Z_1_Z_7.xyz
        file_Z1_Z8="$root"_Z_1_Z_8.xyz
        file_Z2_X1="$root"_Z_2_X_1.xyz
        file_Z2_X2="$root"_Z_2_X_2.xyz
        file_Z2_X3="$root"_Z_2_X_3.xyz
        file_Z2_X4="$root"_Z_2_X_4.xyz
        file_Z2_X5="$root"_Z_2_X_5.xyz
        file_Z2_X6="$root"_Z_2_X_6.xyz
        file_Z2_X7="$root"_Z_2_X_7.xyz
        file_Z2_X8="$root"_Z_2_X_8.xyz
        file_Z2_Y1="$root"_Z_2_Y_1.xyz
        file_Z2_Y2="$root"_Z_2_Y_2.xyz
        file_Z2_Y3="$root"_Z_2_Y_3.xyz
        file_Z2_Y4="$root"_Z_2_Y_4.xyz
        file_Z2_Y5="$root"_Z_2_Y_5.xyz
        file_Z2_Y6="$root"_Z_2_Y_6.xyz
        file_Z2_Y7="$root"_Z_2_Y_7.xyz
        file_Z2_Y8="$root"_Z_2_Y_8.xyz
        file_Z2_Z1="$root"_Z_2_Z_1.xyz
        file_Z2_Z2="$root"_Z_2_Z_2.xyz
        file_Z2_Z3="$root"_Z_2_Z_3.xyz
        file_Z2_Z4="$root"_Z_2_Z_4.xyz
        file_Z2_Z5="$root"_Z_2_Z_5.xyz
        file_Z2_Z6="$root"_Z_2_Z_6.xyz
        file_Z2_Z7="$root"_Z_2_Z_7.xyz
        file_Z2_Z8="$root"_Z_2_Z_8.xyz
        file_Z3_X1="$root"_Z_3_X_1.xyz
        file_Z3_X2="$root"_Z_3_X_2.xyz
        file_Z3_X3="$root"_Z_3_X_3.xyz
        file_Z3_X4="$root"_Z_3_X_4.xyz
        file_Z3_X5="$root"_Z_3_X_5.xyz
        file_Z3_X6="$root"_Z_3_X_6.xyz
        file_Z3_X7="$root"_Z_3_X_7.xyz
        file_Z3_X8="$root"_Z_3_X_8.xyz
        file_Z3_Y1="$root"_Z_3_Y_1.xyz
        file_Z3_Y2="$root"_Z_3_Y_2.xyz
        file_Z3_Y3="$root"_Z_3_Y_3.xyz
        file_Z3_Y4="$root"_Z_3_Y_4.xyz
        file_Z3_Y5="$root"_Z_3_Y_5.xyz
        file_Z3_Y6="$root"_Z_3_Y_6.xyz
        file_Z3_Y7="$root"_Z_3_Y_7.xyz
        file_Z3_Y8="$root"_Z_3_Y_8.xyz
        file_Z3_Z1="$root"_Z_3_Z_1.xyz
        file_Z3_Z2="$root"_Z_3_Z_2.xyz
        file_Z3_Z3="$root"_Z_3_Z_3.xyz
        file_Z3_Z4="$root"_Z_3_Z_4.xyz
        file_Z3_Z5="$root"_Z_3_Z_5.xyz
        file_Z3_Z6="$root"_Z_3_Z_6.xyz
        file_Z3_Z7="$root"_Z_3_Z_7.xyz
        file_Z3_Z8="$root"_Z_3_Z_8.xyz
        file_Z4_X1="$root"_Z_4_X_1.xyz
        file_Z4_X2="$root"_Z_4_X_2.xyz
        file_Z4_X3="$root"_Z_4_X_3.xyz
        file_Z4_X4="$root"_Z_4_X_4.xyz
        file_Z4_X5="$root"_Z_4_X_5.xyz
        file_Z4_X6="$root"_Z_4_X_6.xyz
        file_Z4_X7="$root"_Z_4_X_7.xyz
        file_Z4_X8="$root"_Z_4_X_8.xyz
        file_Z4_Y1="$root"_Z_4_Y_1.xyz
        file_Z4_Y2="$root"_Z_4_Y_2.xyz
        file_Z4_Y3="$root"_Z_4_Y_3.xyz
        file_Z4_Y4="$root"_Z_4_Y_4.xyz
        file_Z4_Y5="$root"_Z_4_Y_5.xyz
        file_Z4_Y6="$root"_Z_4_Y_6.xyz
        file_Z4_Y7="$root"_Z_4_Y_7.xyz
        file_Z4_Y8="$root"_Z_4_Y_8.xyz
        file_Z4_Z1="$root"_Z_4_Z_1.xyz
        file_Z4_Z2="$root"_Z_4_Z_2.xyz
        file_Z4_Z3="$root"_Z_4_Z_3.xyz
        file_Z4_Z4="$root"_Z_4_Z_4.xyz
        file_Z4_Z5="$root"_Z_4_Z_5.xyz
        file_Z4_Z6="$root"_Z_4_Z_6.xyz
        file_Z4_Z7="$root"_Z_4_Z_7.xyz
        file_Z4_Z8="$root"_Z_4_Z_8.xyz
        file_Z5_X1="$root"_Z_5_X_1.xyz
        file_Z5_X2="$root"_Z_5_X_2.xyz
        file_Z5_X3="$root"_Z_5_X_3.xyz
        file_Z5_X4="$root"_Z_5_X_4.xyz
        file_Z5_X5="$root"_Z_5_X_5.xyz
        file_Z5_X6="$root"_Z_5_X_6.xyz
        file_Z5_X7="$root"_Z_5_X_7.xyz
        file_Z5_X8="$root"_Z_5_X_8.xyz
        file_Z5_Y1="$root"_Z_5_Y_1.xyz
        file_Z5_Y2="$root"_Z_5_Y_2.xyz
        file_Z5_Y3="$root"_Z_5_Y_3.xyz
        file_Z5_Y4="$root"_Z_5_Y_4.xyz
        file_Z5_Y5="$root"_Z_5_Y_5.xyz
        file_Z5_Y6="$root"_Z_5_Y_6.xyz
        file_Z5_Y7="$root"_Z_5_Y_7.xyz
        file_Z5_Y8="$root"_Z_5_Y_8.xyz
        file_Z5_Z1="$root"_Z_5_Z_1.xyz
        file_Z5_Z2="$root"_Z_5_Z_2.xyz
        file_Z5_Z3="$root"_Z_5_Z_3.xyz
        file_Z5_Z4="$root"_Z_5_Z_4.xyz
        file_Z5_Z5="$root"_Z_5_Z_5.xyz
        file_Z5_Z6="$root"_Z_5_Z_6.xyz
        file_Z5_Z7="$root"_Z_5_Z_7.xyz
        file_Z5_Z8="$root"_Z_5_Z_8.xyz
        file_Z6_X1="$root"_Z_6_X_1.xyz
        file_Z6_X2="$root"_Z_6_X_2.xyz
        file_Z6_X3="$root"_Z_6_X_3.xyz
        file_Z6_X4="$root"_Z_6_X_4.xyz
        file_Z6_X5="$root"_Z_6_X_5.xyz
        file_Z6_X6="$root"_Z_6_X_6.xyz
        file_Z6_X7="$root"_Z_6_X_7.xyz
        file_Z6_X8="$root"_Z_6_X_8.xyz
        file_Z6_Y1="$root"_Z_6_Y_1.xyz
        file_Z6_Y2="$root"_Z_6_Y_2.xyz
        file_Z6_Y3="$root"_Z_6_Y_3.xyz
        file_Z6_Y4="$root"_Z_6_Y_4.xyz
        file_Z6_Y5="$root"_Z_6_Y_5.xyz
        file_Z6_Y6="$root"_Z_6_Y_6.xyz
        file_Z6_Y7="$root"_Z_6_Y_7.xyz
        file_Z6_Y8="$root"_Z_6_Y_8.xyz
        file_Z6_Z1="$root"_Z_6_Z_1.xyz
        file_Z6_Z2="$root"_Z_6_Z_2.xyz
        file_Z6_Z3="$root"_Z_6_Z_3.xyz
        file_Z6_Z4="$root"_Z_6_Z_4.xyz
        file_Z6_Z5="$root"_Z_6_Z_5.xyz
        file_Z6_Z6="$root"_Z_6_Z_6.xyz
        file_Z6_Z7="$root"_Z_6_Z_7.xyz
        file_Z6_Z8="$root"_Z_6_Z_8.xyz
        file_Z7_X1="$root"_Z_7_X_1.xyz
        file_Z7_X2="$root"_Z_7_X_2.xyz
        file_Z7_X3="$root"_Z_7_X_3.xyz
        file_Z7_X4="$root"_Z_7_X_4.xyz
        file_Z7_X5="$root"_Z_7_X_5.xyz
        file_Z7_X6="$root"_Z_7_X_6.xyz
        file_Z7_X7="$root"_Z_7_X_7.xyz
        file_Z7_X8="$root"_Z_7_X_8.xyz
        file_Z7_Y1="$root"_Z_7_Y_1.xyz
        file_Z7_Y2="$root"_Z_7_Y_2.xyz
        file_Z7_Y3="$root"_Z_7_Y_3.xyz
        file_Z7_Y4="$root"_Z_7_Y_4.xyz
        file_Z7_Y5="$root"_Z_7_Y_5.xyz
        file_Z7_Y6="$root"_Z_7_Y_6.xyz
        file_Z7_Y7="$root"_Z_7_Y_7.xyz
        file_Z7_Y8="$root"_Z_7_Y_8.xyz
        file_Z7_Z1="$root"_Z_7_Z_1.xyz
        file_Z7_Z2="$root"_Z_7_Z_2.xyz
        file_Z7_Z3="$root"_Z_7_Z_3.xyz
        file_Z7_Z4="$root"_Z_7_Z_4.xyz
        file_Z7_Z5="$root"_Z_7_Z_5.xyz
        file_Z7_Z6="$root"_Z_7_Z_6.xyz
        file_Z7_Z7="$root"_Z_7_Z_7.xyz
        file_Z7_Z8="$root"_Z_7_Z_8.xyz
        file_Z8_X1="$root"_Z_8_X_1.xyz
        file_Z8_X2="$root"_Z_8_X_2.xyz
        file_Z8_X3="$root"_Z_8_X_3.xyz
        file_Z8_X4="$root"_Z_8_X_4.xyz
        file_Z8_X5="$root"_Z_8_X_5.xyz
        file_Z8_X6="$root"_Z_8_X_6.xyz
        file_Z8_X7="$root"_Z_8_X_7.xyz
        file_Z8_X8="$root"_Z_8_X_8.xyz
        file_Z8_Y1="$root"_Z_8_Y_1.xyz
        file_Z8_Y2="$root"_Z_8_Y_2.xyz
        file_Z8_Y3="$root"_Z_8_Y_3.xyz
        file_Z8_Y4="$root"_Z_8_Y_4.xyz
        file_Z8_Y5="$root"_Z_8_Y_5.xyz
        file_Z8_Y6="$root"_Z_8_Y_6.xyz
        file_Z8_Y7="$root"_Z_8_Y_7.xyz
        file_Z8_Y8="$root"_Z_8_Y_8.xyz
        file_Z8_Z1="$root"_Z_8_Z_1.xyz
        file_Z8_Z2="$root"_Z_8_Z_2.xyz
        file_Z8_Z3="$root"_Z_8_Z_3.xyz
        file_Z8_Z4="$root"_Z_8_Z_4.xyz
        file_Z8_Z5="$root"_Z_8_Z_5.xyz
        file_Z8_Z6="$root"_Z_8_Z_6.xyz
        file_Z8_Z7="$root"_Z_8_Z_7.xyz
        file_Z8_Z8="$root"_Z_8_Z_8.xyz

        echo $natoms > $file_X1_X1
        echo $natoms > $file_X1_X2
        echo $natoms > $file_X1_X3
        echo $natoms > $file_X1_X4
        echo $natoms > $file_X1_X5
        echo $natoms > $file_X1_X6
        echo $natoms > $file_X1_X7
        echo $natoms > $file_X1_X8
        echo $natoms > $file_X1_Y1
        echo $natoms > $file_X1_Y2
        echo $natoms > $file_X1_Y3
        echo $natoms > $file_X1_Y4
        echo $natoms > $file_X1_Y5
        echo $natoms > $file_X1_Y6
        echo $natoms > $file_X1_Y7
        echo $natoms > $file_X1_Y8
        echo $natoms > $file_X1_Z1
        echo $natoms > $file_X1_Z2
        echo $natoms > $file_X1_Z3
        echo $natoms > $file_X1_Z4
        echo $natoms > $file_X1_Z5
        echo $natoms > $file_X1_Z6
        echo $natoms > $file_X1_Z7
        echo $natoms > $file_X1_Z8
        echo $natoms > $file_X2_X1
        echo $natoms > $file_X2_X2
        echo $natoms > $file_X2_X3
        echo $natoms > $file_X2_X4
        echo $natoms > $file_X2_X5
        echo $natoms > $file_X2_X6
        echo $natoms > $file_X2_X7
        echo $natoms > $file_X2_X8
        echo $natoms > $file_X2_Y1
        echo $natoms > $file_X2_Y2
        echo $natoms > $file_X2_Y3
        echo $natoms > $file_X2_Y4
        echo $natoms > $file_X2_Y5
        echo $natoms > $file_X2_Y6
        echo $natoms > $file_X2_Y7
        echo $natoms > $file_X2_Y8
        echo $natoms > $file_X2_Z1
        echo $natoms > $file_X2_Z2
        echo $natoms > $file_X2_Z3
        echo $natoms > $file_X2_Z4
        echo $natoms > $file_X2_Z5
        echo $natoms > $file_X2_Z6
        echo $natoms > $file_X2_Z7
        echo $natoms > $file_X2_Z8
        echo $natoms > $file_X3_X1
        echo $natoms > $file_X3_X2
        echo $natoms > $file_X3_X3
        echo $natoms > $file_X3_X4
        echo $natoms > $file_X3_X5
        echo $natoms > $file_X3_X6
        echo $natoms > $file_X3_X7
        echo $natoms > $file_X3_X8
        echo $natoms > $file_X3_Y1
        echo $natoms > $file_X3_Y2
        echo $natoms > $file_X3_Y3
        echo $natoms > $file_X3_Y4
        echo $natoms > $file_X3_Y5
        echo $natoms > $file_X3_Y6
        echo $natoms > $file_X3_Y7
        echo $natoms > $file_X3_Y8
        echo $natoms > $file_X3_Z1
        echo $natoms > $file_X3_Z2
        echo $natoms > $file_X3_Z3
        echo $natoms > $file_X3_Z4
        echo $natoms > $file_X3_Z5
        echo $natoms > $file_X3_Z6
        echo $natoms > $file_X3_Z7
        echo $natoms > $file_X3_Z8
        echo $natoms > $file_X4_X1
        echo $natoms > $file_X4_X2
        echo $natoms > $file_X4_X3
        echo $natoms > $file_X4_X4
        echo $natoms > $file_X4_X5
        echo $natoms > $file_X4_X6
        echo $natoms > $file_X4_X7
        echo $natoms > $file_X4_X8
        echo $natoms > $file_X4_Y1
        echo $natoms > $file_X4_Y2
        echo $natoms > $file_X4_Y3
        echo $natoms > $file_X4_Y4
        echo $natoms > $file_X4_Y5
        echo $natoms > $file_X4_Y6
        echo $natoms > $file_X4_Y7
        echo $natoms > $file_X4_Y8
        echo $natoms > $file_X4_Z1
        echo $natoms > $file_X4_Z2
        echo $natoms > $file_X4_Z3
        echo $natoms > $file_X4_Z4
        echo $natoms > $file_X4_Z5
        echo $natoms > $file_X4_Z6
        echo $natoms > $file_X4_Z7
        echo $natoms > $file_X4_Z8
        echo $natoms > $file_X5_X1
        echo $natoms > $file_X5_X2
        echo $natoms > $file_X5_X3
        echo $natoms > $file_X5_X4
        echo $natoms > $file_X5_X5
        echo $natoms > $file_X5_X6
        echo $natoms > $file_X5_X7
        echo $natoms > $file_X5_X8
        echo $natoms > $file_X5_Y1
        echo $natoms > $file_X5_Y2
        echo $natoms > $file_X5_Y3
        echo $natoms > $file_X5_Y4
        echo $natoms > $file_X5_Y5
        echo $natoms > $file_X5_Y6
        echo $natoms > $file_X5_Y7
        echo $natoms > $file_X5_Y8
        echo $natoms > $file_X5_Z1
        echo $natoms > $file_X5_Z2
        echo $natoms > $file_X5_Z3
        echo $natoms > $file_X5_Z4
        echo $natoms > $file_X5_Z5
        echo $natoms > $file_X5_Z6
        echo $natoms > $file_X5_Z7
        echo $natoms > $file_X5_Z8
        echo $natoms > $file_X6_X1
        echo $natoms > $file_X6_X2
        echo $natoms > $file_X6_X3
        echo $natoms > $file_X6_X4
        echo $natoms > $file_X6_X5
        echo $natoms > $file_X6_X6
        echo $natoms > $file_X6_X7
        echo $natoms > $file_X6_X8
        echo $natoms > $file_X6_Y1
        echo $natoms > $file_X6_Y2
        echo $natoms > $file_X6_Y3
        echo $natoms > $file_X6_Y4
        echo $natoms > $file_X6_Y5
        echo $natoms > $file_X6_Y6
        echo $natoms > $file_X6_Y7
        echo $natoms > $file_X6_Y8
        echo $natoms > $file_X6_Z1
        echo $natoms > $file_X6_Z2
        echo $natoms > $file_X6_Z3
        echo $natoms > $file_X6_Z4
        echo $natoms > $file_X6_Z5
        echo $natoms > $file_X6_Z6
        echo $natoms > $file_X6_Z7
        echo $natoms > $file_X6_Z8
        echo $natoms > $file_X7_X1
        echo $natoms > $file_X7_X2
        echo $natoms > $file_X7_X3
        echo $natoms > $file_X7_X4
        echo $natoms > $file_X7_X5
        echo $natoms > $file_X7_X6
        echo $natoms > $file_X7_X7
        echo $natoms > $file_X7_X8
        echo $natoms > $file_X7_Y1
        echo $natoms > $file_X7_Y2
        echo $natoms > $file_X7_Y3
        echo $natoms > $file_X7_Y4
        echo $natoms > $file_X7_Y5
        echo $natoms > $file_X7_Y6
        echo $natoms > $file_X7_Y7
        echo $natoms > $file_X7_Y8
        echo $natoms > $file_X7_Z1
        echo $natoms > $file_X7_Z2
        echo $natoms > $file_X7_Z3
        echo $natoms > $file_X7_Z4
        echo $natoms > $file_X7_Z5
        echo $natoms > $file_X7_Z6
        echo $natoms > $file_X7_Z7
        echo $natoms > $file_X7_Z8
        echo $natoms > $file_X8_X1
        echo $natoms > $file_X8_X2
        echo $natoms > $file_X8_X3
        echo $natoms > $file_X8_X4
        echo $natoms > $file_X8_X5
        echo $natoms > $file_X8_X6
        echo $natoms > $file_X8_X7
        echo $natoms > $file_X8_X8
        echo $natoms > $file_X8_Y1
        echo $natoms > $file_X8_Y2
        echo $natoms > $file_X8_Y3
        echo $natoms > $file_X8_Y4
        echo $natoms > $file_X8_Y5
        echo $natoms > $file_X8_Y6
        echo $natoms > $file_X8_Y7
        echo $natoms > $file_X8_Y8
        echo $natoms > $file_X8_Z1
        echo $natoms > $file_X8_Z2
        echo $natoms > $file_X8_Z3
        echo $natoms > $file_X8_Z4
        echo $natoms > $file_X8_Z5
        echo $natoms > $file_X8_Z6
        echo $natoms > $file_X8_Z7
        echo $natoms > $file_X8_Z8
        echo $natoms > $file_Y1_X1
        echo $natoms > $file_Y1_X2
        echo $natoms > $file_Y1_X3
        echo $natoms > $file_Y1_X4
        echo $natoms > $file_Y1_X5
        echo $natoms > $file_Y1_X6
        echo $natoms > $file_Y1_X7
        echo $natoms > $file_Y1_X8
        echo $natoms > $file_Y1_Y1
        echo $natoms > $file_Y1_Y2
        echo $natoms > $file_Y1_Y3
        echo $natoms > $file_Y1_Y4
        echo $natoms > $file_Y1_Y5
        echo $natoms > $file_Y1_Y6
        echo $natoms > $file_Y1_Y7
        echo $natoms > $file_Y1_Y8
        echo $natoms > $file_Y1_Z1
        echo $natoms > $file_Y1_Z2
        echo $natoms > $file_Y1_Z3
        echo $natoms > $file_Y1_Z4
        echo $natoms > $file_Y1_Z5
        echo $natoms > $file_Y1_Z6
        echo $natoms > $file_Y1_Z7
        echo $natoms > $file_Y1_Z8
        echo $natoms > $file_Y2_X1
        echo $natoms > $file_Y2_X2
        echo $natoms > $file_Y2_X3
        echo $natoms > $file_Y2_X4
        echo $natoms > $file_Y2_X5
        echo $natoms > $file_Y2_X6
        echo $natoms > $file_Y2_X7
        echo $natoms > $file_Y2_X8
        echo $natoms > $file_Y2_Y1
        echo $natoms > $file_Y2_Y2
        echo $natoms > $file_Y2_Y3
        echo $natoms > $file_Y2_Y4
        echo $natoms > $file_Y2_Y5
        echo $natoms > $file_Y2_Y6
        echo $natoms > $file_Y2_Y7
        echo $natoms > $file_Y2_Y8
        echo $natoms > $file_Y2_Z1
        echo $natoms > $file_Y2_Z2
        echo $natoms > $file_Y2_Z3
        echo $natoms > $file_Y2_Z4
        echo $natoms > $file_Y2_Z5
        echo $natoms > $file_Y2_Z6
        echo $natoms > $file_Y2_Z7
        echo $natoms > $file_Y2_Z8
        echo $natoms > $file_Y3_X1
        echo $natoms > $file_Y3_X2
        echo $natoms > $file_Y3_X3
        echo $natoms > $file_Y3_X4
        echo $natoms > $file_Y3_X5
        echo $natoms > $file_Y3_X6
        echo $natoms > $file_Y3_X7
        echo $natoms > $file_Y3_X8
        echo $natoms > $file_Y3_Y1
        echo $natoms > $file_Y3_Y2
        echo $natoms > $file_Y3_Y3
        echo $natoms > $file_Y3_Y4
        echo $natoms > $file_Y3_Y5
        echo $natoms > $file_Y3_Y6
        echo $natoms > $file_Y3_Y7
        echo $natoms > $file_Y3_Y8
        echo $natoms > $file_Y3_Z1
        echo $natoms > $file_Y3_Z2
        echo $natoms > $file_Y3_Z3
        echo $natoms > $file_Y3_Z4
        echo $natoms > $file_Y3_Z5
        echo $natoms > $file_Y3_Z6
        echo $natoms > $file_Y3_Z7
        echo $natoms > $file_Y3_Z8
        echo $natoms > $file_Y4_X1
        echo $natoms > $file_Y4_X2
        echo $natoms > $file_Y4_X3
        echo $natoms > $file_Y4_X4
        echo $natoms > $file_Y4_X5
        echo $natoms > $file_Y4_X6
        echo $natoms > $file_Y4_X7
        echo $natoms > $file_Y4_X8
        echo $natoms > $file_Y4_Y1
        echo $natoms > $file_Y4_Y2
        echo $natoms > $file_Y4_Y3
        echo $natoms > $file_Y4_Y4
        echo $natoms > $file_Y4_Y5
        echo $natoms > $file_Y4_Y6
        echo $natoms > $file_Y4_Y7
        echo $natoms > $file_Y4_Y8
        echo $natoms > $file_Y4_Z1
        echo $natoms > $file_Y4_Z2
        echo $natoms > $file_Y4_Z3
        echo $natoms > $file_Y4_Z4
        echo $natoms > $file_Y4_Z5
        echo $natoms > $file_Y4_Z6
        echo $natoms > $file_Y4_Z7
        echo $natoms > $file_Y4_Z8
        echo $natoms > $file_Y5_X1
        echo $natoms > $file_Y5_X2
        echo $natoms > $file_Y5_X3
        echo $natoms > $file_Y5_X4
        echo $natoms > $file_Y5_X5
        echo $natoms > $file_Y5_X6
        echo $natoms > $file_Y5_X7
        echo $natoms > $file_Y5_X8
        echo $natoms > $file_Y5_Y1
        echo $natoms > $file_Y5_Y2
        echo $natoms > $file_Y5_Y3
        echo $natoms > $file_Y5_Y4
        echo $natoms > $file_Y5_Y5
        echo $natoms > $file_Y5_Y6
        echo $natoms > $file_Y5_Y7
        echo $natoms > $file_Y5_Y8
        echo $natoms > $file_Y5_Z1
        echo $natoms > $file_Y5_Z2
        echo $natoms > $file_Y5_Z3
        echo $natoms > $file_Y5_Z4
        echo $natoms > $file_Y5_Z5
        echo $natoms > $file_Y5_Z6
        echo $natoms > $file_Y5_Z7
        echo $natoms > $file_Y5_Z8
        echo $natoms > $file_Y6_X1
        echo $natoms > $file_Y6_X2
        echo $natoms > $file_Y6_X3
        echo $natoms > $file_Y6_X4
        echo $natoms > $file_Y6_X5
        echo $natoms > $file_Y6_X6
        echo $natoms > $file_Y6_X7
        echo $natoms > $file_Y6_X8
        echo $natoms > $file_Y6_Y1
        echo $natoms > $file_Y6_Y2
        echo $natoms > $file_Y6_Y3
        echo $natoms > $file_Y6_Y4
        echo $natoms > $file_Y6_Y5
        echo $natoms > $file_Y6_Y6
        echo $natoms > $file_Y6_Y7
        echo $natoms > $file_Y6_Y8
        echo $natoms > $file_Y6_Z1
        echo $natoms > $file_Y6_Z2
        echo $natoms > $file_Y6_Z3
        echo $natoms > $file_Y6_Z4
        echo $natoms > $file_Y6_Z5
        echo $natoms > $file_Y6_Z6
        echo $natoms > $file_Y6_Z7
        echo $natoms > $file_Y6_Z8
        echo $natoms > $file_Y7_X1
        echo $natoms > $file_Y7_X2
        echo $natoms > $file_Y7_X3
        echo $natoms > $file_Y7_X4
        echo $natoms > $file_Y7_X5
        echo $natoms > $file_Y7_X6
        echo $natoms > $file_Y7_X7
        echo $natoms > $file_Y7_X8
        echo $natoms > $file_Y7_Y1
        echo $natoms > $file_Y7_Y2
        echo $natoms > $file_Y7_Y3
        echo $natoms > $file_Y7_Y4
        echo $natoms > $file_Y7_Y5
        echo $natoms > $file_Y7_Y6
        echo $natoms > $file_Y7_Y7
        echo $natoms > $file_Y7_Y8
        echo $natoms > $file_Y7_Z1
        echo $natoms > $file_Y7_Z2
        echo $natoms > $file_Y7_Z3
        echo $natoms > $file_Y7_Z4
        echo $natoms > $file_Y7_Z5
        echo $natoms > $file_Y7_Z6
        echo $natoms > $file_Y7_Z7
        echo $natoms > $file_Y7_Z8
        echo $natoms > $file_Y8_X1
        echo $natoms > $file_Y8_X2
        echo $natoms > $file_Y8_X3
        echo $natoms > $file_Y8_X4
        echo $natoms > $file_Y8_X5
        echo $natoms > $file_Y8_X6
        echo $natoms > $file_Y8_X7
        echo $natoms > $file_Y8_X8
        echo $natoms > $file_Y8_Y1
        echo $natoms > $file_Y8_Y2
        echo $natoms > $file_Y8_Y3
        echo $natoms > $file_Y8_Y4
        echo $natoms > $file_Y8_Y5
        echo $natoms > $file_Y8_Y6
        echo $natoms > $file_Y8_Y7
        echo $natoms > $file_Y8_Y8
        echo $natoms > $file_Y8_Z1
        echo $natoms > $file_Y8_Z2
        echo $natoms > $file_Y8_Z3
        echo $natoms > $file_Y8_Z4
        echo $natoms > $file_Y8_Z5
        echo $natoms > $file_Y8_Z6
        echo $natoms > $file_Y8_Z7
        echo $natoms > $file_Y8_Z8
        echo $natoms > $file_Z1_X1
        echo $natoms > $file_Z1_X2
        echo $natoms > $file_Z1_X3
        echo $natoms > $file_Z1_X4
        echo $natoms > $file_Z1_X5
        echo $natoms > $file_Z1_X6
        echo $natoms > $file_Z1_X7
        echo $natoms > $file_Z1_X8
        echo $natoms > $file_Z1_Y1
        echo $natoms > $file_Z1_Y2
        echo $natoms > $file_Z1_Y3
        echo $natoms > $file_Z1_Y4
        echo $natoms > $file_Z1_Y5
        echo $natoms > $file_Z1_Y6
        echo $natoms > $file_Z1_Y7
        echo $natoms > $file_Z1_Y8
        echo $natoms > $file_Z1_Z1
        echo $natoms > $file_Z1_Z2
        echo $natoms > $file_Z1_Z3
        echo $natoms > $file_Z1_Z4
        echo $natoms > $file_Z1_Z5
        echo $natoms > $file_Z1_Z6
        echo $natoms > $file_Z1_Z7
        echo $natoms > $file_Z1_Z8
        echo $natoms > $file_Z2_X1
        echo $natoms > $file_Z2_X2
        echo $natoms > $file_Z2_X3
        echo $natoms > $file_Z2_X4
        echo $natoms > $file_Z2_X5
        echo $natoms > $file_Z2_X6
        echo $natoms > $file_Z2_X7
        echo $natoms > $file_Z2_X8
        echo $natoms > $file_Z2_Y1
        echo $natoms > $file_Z2_Y2
        echo $natoms > $file_Z2_Y3
        echo $natoms > $file_Z2_Y4
        echo $natoms > $file_Z2_Y5
        echo $natoms > $file_Z2_Y6
        echo $natoms > $file_Z2_Y7
        echo $natoms > $file_Z2_Y8
        echo $natoms > $file_Z2_Z1
        echo $natoms > $file_Z2_Z2
        echo $natoms > $file_Z2_Z3
        echo $natoms > $file_Z2_Z4
        echo $natoms > $file_Z2_Z5
        echo $natoms > $file_Z2_Z6
        echo $natoms > $file_Z2_Z7
        echo $natoms > $file_Z2_Z8
        echo $natoms > $file_Z3_X1
        echo $natoms > $file_Z3_X2
        echo $natoms > $file_Z3_X3
        echo $natoms > $file_Z3_X4
        echo $natoms > $file_Z3_X5
        echo $natoms > $file_Z3_X6
        echo $natoms > $file_Z3_X7
        echo $natoms > $file_Z3_X8
        echo $natoms > $file_Z3_Y1
        echo $natoms > $file_Z3_Y2
        echo $natoms > $file_Z3_Y3
        echo $natoms > $file_Z3_Y4
        echo $natoms > $file_Z3_Y5
        echo $natoms > $file_Z3_Y6
        echo $natoms > $file_Z3_Y7
        echo $natoms > $file_Z3_Y8
        echo $natoms > $file_Z3_Z1
        echo $natoms > $file_Z3_Z2
        echo $natoms > $file_Z3_Z3
        echo $natoms > $file_Z3_Z4
        echo $natoms > $file_Z3_Z5
        echo $natoms > $file_Z3_Z6
        echo $natoms > $file_Z3_Z7
        echo $natoms > $file_Z3_Z8
        echo $natoms > $file_Z4_X1
        echo $natoms > $file_Z4_X2
        echo $natoms > $file_Z4_X3
        echo $natoms > $file_Z4_X4
        echo $natoms > $file_Z4_X5
        echo $natoms > $file_Z4_X6
        echo $natoms > $file_Z4_X7
        echo $natoms > $file_Z4_X8
        echo $natoms > $file_Z4_Y1
        echo $natoms > $file_Z4_Y2
        echo $natoms > $file_Z4_Y3
        echo $natoms > $file_Z4_Y4
        echo $natoms > $file_Z4_Y5
        echo $natoms > $file_Z4_Y6
        echo $natoms > $file_Z4_Y7
        echo $natoms > $file_Z4_Y8
        echo $natoms > $file_Z4_Z1
        echo $natoms > $file_Z4_Z2
        echo $natoms > $file_Z4_Z3
        echo $natoms > $file_Z4_Z4
        echo $natoms > $file_Z4_Z5
        echo $natoms > $file_Z4_Z6
        echo $natoms > $file_Z4_Z7
        echo $natoms > $file_Z4_Z8
        echo $natoms > $file_Z5_X1
        echo $natoms > $file_Z5_X2
        echo $natoms > $file_Z5_X3
        echo $natoms > $file_Z5_X4
        echo $natoms > $file_Z5_X5
        echo $natoms > $file_Z5_X6
        echo $natoms > $file_Z5_X7
        echo $natoms > $file_Z5_X8
        echo $natoms > $file_Z5_Y1
        echo $natoms > $file_Z5_Y2
        echo $natoms > $file_Z5_Y3
        echo $natoms > $file_Z5_Y4
        echo $natoms > $file_Z5_Y5
        echo $natoms > $file_Z5_Y6
        echo $natoms > $file_Z5_Y7
        echo $natoms > $file_Z5_Y8
        echo $natoms > $file_Z5_Z1
        echo $natoms > $file_Z5_Z2
        echo $natoms > $file_Z5_Z3
        echo $natoms > $file_Z5_Z4
        echo $natoms > $file_Z5_Z5
        echo $natoms > $file_Z5_Z6
        echo $natoms > $file_Z5_Z7
        echo $natoms > $file_Z5_Z8
        echo $natoms > $file_Z6_X1
        echo $natoms > $file_Z6_X2
        echo $natoms > $file_Z6_X3
        echo $natoms > $file_Z6_X4
        echo $natoms > $file_Z6_X5
        echo $natoms > $file_Z6_X6
        echo $natoms > $file_Z6_X7
        echo $natoms > $file_Z6_X8
        echo $natoms > $file_Z6_Y1
        echo $natoms > $file_Z6_Y2
        echo $natoms > $file_Z6_Y3
        echo $natoms > $file_Z6_Y4
        echo $natoms > $file_Z6_Y5
        echo $natoms > $file_Z6_Y6
        echo $natoms > $file_Z6_Y7
        echo $natoms > $file_Z6_Y8
        echo $natoms > $file_Z6_Z1
        echo $natoms > $file_Z6_Z2
        echo $natoms > $file_Z6_Z3
        echo $natoms > $file_Z6_Z4
        echo $natoms > $file_Z6_Z5
        echo $natoms > $file_Z6_Z6
        echo $natoms > $file_Z6_Z7
        echo $natoms > $file_Z6_Z8
        echo $natoms > $file_Z7_X1
        echo $natoms > $file_Z7_X2
        echo $natoms > $file_Z7_X3
        echo $natoms > $file_Z7_X4
        echo $natoms > $file_Z7_X5
        echo $natoms > $file_Z7_X6
        echo $natoms > $file_Z7_X7
        echo $natoms > $file_Z7_X8
        echo $natoms > $file_Z7_Y1
        echo $natoms > $file_Z7_Y2
        echo $natoms > $file_Z7_Y3
        echo $natoms > $file_Z7_Y4
        echo $natoms > $file_Z7_Y5
        echo $natoms > $file_Z7_Y6
        echo $natoms > $file_Z7_Y7
        echo $natoms > $file_Z7_Y8
        echo $natoms > $file_Z7_Z1
        echo $natoms > $file_Z7_Z2
        echo $natoms > $file_Z7_Z3
        echo $natoms > $file_Z7_Z4
        echo $natoms > $file_Z7_Z5
        echo $natoms > $file_Z7_Z6
        echo $natoms > $file_Z7_Z7
        echo $natoms > $file_Z7_Z8
        echo $natoms > $file_Z8_X1
        echo $natoms > $file_Z8_X2
        echo $natoms > $file_Z8_X3
        echo $natoms > $file_Z8_X4
        echo $natoms > $file_Z8_X5
        echo $natoms > $file_Z8_X6
        echo $natoms > $file_Z8_X7
        echo $natoms > $file_Z8_X8
        echo $natoms > $file_Z8_Y1
        echo $natoms > $file_Z8_Y2
        echo $natoms > $file_Z8_Y3
        echo $natoms > $file_Z8_Y4
        echo $natoms > $file_Z8_Y5
        echo $natoms > $file_Z8_Y6
        echo $natoms > $file_Z8_Y7
        echo $natoms > $file_Z8_Y8
        echo $natoms > $file_Z8_Z1
        echo $natoms > $file_Z8_Z2
        echo $natoms > $file_Z8_Z3
        echo $natoms > $file_Z8_Z4
        echo $natoms > $file_Z8_Z5
        echo $natoms > $file_Z8_Z6
        echo $natoms > $file_Z8_Z7
        echo $natoms > $file_Z8_Z8
        echo "$Complex distorted "$fname"_X_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_X1
        echo "$Complex distorted "$fname"_X_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_X2
        echo "$Complex distorted "$fname"_X_1_X_3  D1=$D1 D2=$D3 Bohr "    >> $file_X1_X3
        echo "$Complex distorted "$fname"_X_1_X_4  D1=$D1 D2=$D4 Bohr "    >> $file_X1_X4
        echo "$Complex distorted "$fname"_X_1_X_5  D1=$D1 D2=$D5 Bohr "    >> $file_X1_X5
        echo "$Complex distorted "$fname"_X_1_X_6  D1=$D1 D2=$D6 Bohr "    >> $file_X1_X6
        echo "$Complex distorted "$fname"_X_1_X_7  D1=$D1 D2=$D7 Bohr "    >> $file_X1_X7
        echo "$Complex distorted "$fname"_X_1_X_8  D1=$D1 D2=$D8 Bohr "    >> $file_X1_X8
        echo "$Complex distorted "$fname"_X_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_Y1
        echo "$Complex distorted "$fname"_X_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_Y2
        echo "$Complex distorted "$fname"_X_1_Y_3  D1=$D1 D2=$D3 Bohr "    >> $file_X1_Y3
        echo "$Complex distorted "$fname"_X_1_Y_4  D1=$D1 D2=$D4 Bohr "    >> $file_X1_Y4
        echo "$Complex distorted "$fname"_X_1_Y_5  D1=$D1 D2=$D5 Bohr "    >> $file_X1_Y5
        echo "$Complex distorted "$fname"_X_1_Y_6  D1=$D1 D2=$D6 Bohr "    >> $file_X1_Y6
        echo "$Complex distorted "$fname"_X_1_Y_7  D1=$D1 D2=$D7 Bohr "    >> $file_X1_Y7
        echo "$Complex distorted "$fname"_X_1_Y_8  D1=$D1 D2=$D8 Bohr "    >> $file_X1_Y8
        echo "$Complex distorted "$fname"_X_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_X1_Z1
        echo "$Complex distorted "$fname"_X_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_X1_Z2
        echo "$Complex distorted "$fname"_X_1_Z_3  D1=$D1 D2=$D3 Bohr "    >> $file_X1_Z3
        echo "$Complex distorted "$fname"_X_1_Z_4  D1=$D1 D2=$D4 Bohr "    >> $file_X1_Z4
        echo "$Complex distorted "$fname"_X_1_Z_5  D1=$D1 D2=$D5 Bohr "    >> $file_X1_Z5
        echo "$Complex distorted "$fname"_X_1_Z_6  D1=$D1 D2=$D6 Bohr "    >> $file_X1_Z6
        echo "$Complex distorted "$fname"_X_1_Z_7  D1=$D1 D2=$D7 Bohr "    >> $file_X1_Z7
        echo "$Complex distorted "$fname"_X_1_Z_8  D1=$D1 D2=$D8 Bohr "    >> $file_X1_Z8
        echo "$Complex distorted "$fname"_X_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_X1
        echo "$Complex distorted "$fname"_X_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_X2
        echo "$Complex distorted "$fname"_X_2_X_3  D1=$D2 D2=$D3 Bohr "    >> $file_X2_X3
        echo "$Complex distorted "$fname"_X_2_X_4  D1=$D2 D2=$D4 Bohr "    >> $file_X2_X4
        echo "$Complex distorted "$fname"_X_2_X_5  D1=$D2 D2=$D5 Bohr "    >> $file_X2_X5
        echo "$Complex distorted "$fname"_X_2_X_6  D1=$D2 D2=$D6 Bohr "    >> $file_X2_X6
        echo "$Complex distorted "$fname"_X_2_X_7  D1=$D2 D2=$D7 Bohr "    >> $file_X2_X7
        echo "$Complex distorted "$fname"_X_2_X_8  D1=$D2 D2=$D8 Bohr "    >> $file_X2_X8
        echo "$Complex distorted "$fname"_X_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_Y1
        echo "$Complex distorted "$fname"_X_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_Y2
        echo "$Complex distorted "$fname"_X_2_Y_3  D1=$D2 D2=$D3 Bohr "    >> $file_X2_Y3
        echo "$Complex distorted "$fname"_X_2_Y_4  D1=$D2 D2=$D4 Bohr "    >> $file_X2_Y4
        echo "$Complex distorted "$fname"_X_2_Y_5  D1=$D2 D2=$D5 Bohr "    >> $file_X2_Y5
        echo "$Complex distorted "$fname"_X_2_Y_6  D1=$D2 D2=$D6 Bohr "    >> $file_X2_Y6
        echo "$Complex distorted "$fname"_X_2_Y_7  D1=$D2 D2=$D7 Bohr "    >> $file_X2_Y7
        echo "$Complex distorted "$fname"_X_2_Y_8  D1=$D2 D2=$D8 Bohr "    >> $file_X2_Y8
        echo "$Complex distorted "$fname"_X_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_X2_Z1
        echo "$Complex distorted "$fname"_X_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_X2_Z2
        echo "$Complex distorted "$fname"_X_2_Z_3  D1=$D2 D2=$D3 Bohr "    >> $file_X2_Z3
        echo "$Complex distorted "$fname"_X_2_Z_4  D1=$D2 D2=$D4 Bohr "    >> $file_X2_Z4
        echo "$Complex distorted "$fname"_X_2_Z_5  D1=$D2 D2=$D5 Bohr "    >> $file_X2_Z5
        echo "$Complex distorted "$fname"_X_2_Z_6  D1=$D2 D2=$D6 Bohr "    >> $file_X2_Z6
        echo "$Complex distorted "$fname"_X_2_Z_7  D1=$D2 D2=$D7 Bohr "    >> $file_X2_Z7
        echo "$Complex distorted "$fname"_X_2_Z_8  D1=$D2 D2=$D8 Bohr "    >> $file_X2_Z8
        echo "$Complex distorted "$fname"_X_3_X_1  D1=$D3 D2=$D1 Bohr "    >> $file_X3_X1
        echo "$Complex distorted "$fname"_X_3_X_2  D1=$D3 D2=$D2 Bohr "    >> $file_X3_X2
        echo "$Complex distorted "$fname"_X_3_X_3  D1=$D3 D2=$D3 Bohr "    >> $file_X3_X3
        echo "$Complex distorted "$fname"_X_3_X_4  D1=$D3 D2=$D4 Bohr "    >> $file_X3_X4
        echo "$Complex distorted "$fname"_X_3_X_5  D1=$D3 D2=$D5 Bohr "    >> $file_X3_X5
        echo "$Complex distorted "$fname"_X_3_X_6  D1=$D3 D2=$D6 Bohr "    >> $file_X3_X6
        echo "$Complex distorted "$fname"_X_3_X_7  D1=$D3 D2=$D7 Bohr "    >> $file_X3_X7
        echo "$Complex distorted "$fname"_X_3_X_8  D1=$D3 D2=$D8 Bohr "    >> $file_X3_X8
        echo "$Complex distorted "$fname"_X_3_Y_1  D1=$D3 D2=$D1 Bohr "    >> $file_X3_Y1
        echo "$Complex distorted "$fname"_X_3_Y_2  D1=$D3 D2=$D2 Bohr "    >> $file_X3_Y2
        echo "$Complex distorted "$fname"_X_3_Y_3  D1=$D3 D2=$D3 Bohr "    >> $file_X3_Y3
        echo "$Complex distorted "$fname"_X_3_Y_4  D1=$D3 D2=$D4 Bohr "    >> $file_X3_Y4
        echo "$Complex distorted "$fname"_X_3_Y_5  D1=$D3 D2=$D5 Bohr "    >> $file_X3_Y5
        echo "$Complex distorted "$fname"_X_3_Y_6  D1=$D3 D2=$D6 Bohr "    >> $file_X3_Y6
        echo "$Complex distorted "$fname"_X_3_Y_7  D1=$D3 D2=$D7 Bohr "    >> $file_X3_Y7
        echo "$Complex distorted "$fname"_X_3_Y_8  D1=$D3 D2=$D8 Bohr "    >> $file_X3_Y8
        echo "$Complex distorted "$fname"_X_3_Z_1  D1=$D3 D2=$D1 Bohr "    >> $file_X3_Z1
        echo "$Complex distorted "$fname"_X_3_Z_2  D1=$D3 D2=$D2 Bohr "    >> $file_X3_Z2
        echo "$Complex distorted "$fname"_X_3_Z_3  D1=$D3 D2=$D3 Bohr "    >> $file_X3_Z3
        echo "$Complex distorted "$fname"_X_3_Z_4  D1=$D3 D2=$D4 Bohr "    >> $file_X3_Z4
        echo "$Complex distorted "$fname"_X_3_Z_5  D1=$D3 D2=$D5 Bohr "    >> $file_X3_Z5
        echo "$Complex distorted "$fname"_X_3_Z_6  D1=$D3 D2=$D6 Bohr "    >> $file_X3_Z6
        echo "$Complex distorted "$fname"_X_3_Z_7  D1=$D3 D2=$D7 Bohr "    >> $file_X3_Z7
        echo "$Complex distorted "$fname"_X_3_Z_8  D1=$D3 D2=$D8 Bohr "    >> $file_X3_Z8
        echo "$Complex distorted "$fname"_X_4_X_1  D1=$D4 D2=$D1 Bohr "    >> $file_X4_X1
        echo "$Complex distorted "$fname"_X_4_X_2  D1=$D4 D2=$D2 Bohr "    >> $file_X4_X2
        echo "$Complex distorted "$fname"_X_4_X_3  D1=$D4 D2=$D3 Bohr "    >> $file_X4_X3
        echo "$Complex distorted "$fname"_X_4_X_4  D1=$D4 D2=$D4 Bohr "    >> $file_X4_X4
        echo "$Complex distorted "$fname"_X_4_X_5  D1=$D4 D2=$D5 Bohr "    >> $file_X4_X5
        echo "$Complex distorted "$fname"_X_4_X_6  D1=$D4 D2=$D6 Bohr "    >> $file_X4_X6
        echo "$Complex distorted "$fname"_X_4_X_7  D1=$D4 D2=$D7 Bohr "    >> $file_X4_X7
        echo "$Complex distorted "$fname"_X_4_X_8  D1=$D4 D2=$D8 Bohr "    >> $file_X4_X8
        echo "$Complex distorted "$fname"_X_4_Y_1  D1=$D4 D2=$D1 Bohr "    >> $file_X4_Y1
        echo "$Complex distorted "$fname"_X_4_Y_2  D1=$D4 D2=$D2 Bohr "    >> $file_X4_Y2
        echo "$Complex distorted "$fname"_X_4_Y_3  D1=$D4 D2=$D3 Bohr "    >> $file_X4_Y3
        echo "$Complex distorted "$fname"_X_4_Y_4  D1=$D4 D2=$D4 Bohr "    >> $file_X4_Y4
        echo "$Complex distorted "$fname"_X_4_Y_5  D1=$D4 D2=$D5 Bohr "    >> $file_X4_Y5
        echo "$Complex distorted "$fname"_X_4_Y_6  D1=$D4 D2=$D6 Bohr "    >> $file_X4_Y6
        echo "$Complex distorted "$fname"_X_4_Y_7  D1=$D4 D2=$D7 Bohr "    >> $file_X4_Y7
        echo "$Complex distorted "$fname"_X_4_Y_8  D1=$D4 D2=$D8 Bohr "    >> $file_X4_Y8
        echo "$Complex distorted "$fname"_X_4_Z_1  D1=$D4 D2=$D1 Bohr "    >> $file_X4_Z1
        echo "$Complex distorted "$fname"_X_4_Z_2  D1=$D4 D2=$D2 Bohr "    >> $file_X4_Z2
        echo "$Complex distorted "$fname"_X_4_Z_3  D1=$D4 D2=$D3 Bohr "    >> $file_X4_Z3
        echo "$Complex distorted "$fname"_X_4_Z_4  D1=$D4 D2=$D4 Bohr "    >> $file_X4_Z4
        echo "$Complex distorted "$fname"_X_4_Z_5  D1=$D4 D2=$D5 Bohr "    >> $file_X4_Z5
        echo "$Complex distorted "$fname"_X_4_Z_6  D1=$D4 D2=$D6 Bohr "    >> $file_X4_Z6
        echo "$Complex distorted "$fname"_X_4_Z_7  D1=$D4 D2=$D7 Bohr "    >> $file_X4_Z7
        echo "$Complex distorted "$fname"_X_4_Z_8  D1=$D4 D2=$D8 Bohr "    >> $file_X4_Z8
        echo "$Complex distorted "$fname"_X_5_X_1  D1=$D5 D2=$D1 Bohr "    >> $file_X5_X1
        echo "$Complex distorted "$fname"_X_5_X_2  D1=$D5 D2=$D2 Bohr "    >> $file_X5_X2
        echo "$Complex distorted "$fname"_X_5_X_3  D1=$D5 D2=$D3 Bohr "    >> $file_X5_X3
        echo "$Complex distorted "$fname"_X_5_X_4  D1=$D5 D2=$D4 Bohr "    >> $file_X5_X4
        echo "$Complex distorted "$fname"_X_5_X_5  D1=$D5 D2=$D5 Bohr "    >> $file_X5_X5
        echo "$Complex distorted "$fname"_X_5_X_6  D1=$D5 D2=$D6 Bohr "    >> $file_X5_X6
        echo "$Complex distorted "$fname"_X_5_X_7  D1=$D5 D2=$D7 Bohr "    >> $file_X5_X7
        echo "$Complex distorted "$fname"_X_5_X_8  D1=$D5 D2=$D8 Bohr "    >> $file_X5_X8
        echo "$Complex distorted "$fname"_X_5_Y_1  D1=$D5 D2=$D1 Bohr "    >> $file_X5_Y1
        echo "$Complex distorted "$fname"_X_5_Y_2  D1=$D5 D2=$D2 Bohr "    >> $file_X5_Y2
        echo "$Complex distorted "$fname"_X_5_Y_3  D1=$D5 D2=$D3 Bohr "    >> $file_X5_Y3
        echo "$Complex distorted "$fname"_X_5_Y_4  D1=$D5 D2=$D4 Bohr "    >> $file_X5_Y4
        echo "$Complex distorted "$fname"_X_5_Y_5  D1=$D5 D2=$D5 Bohr "    >> $file_X5_Y5
        echo "$Complex distorted "$fname"_X_5_Y_6  D1=$D5 D2=$D6 Bohr "    >> $file_X5_Y6
        echo "$Complex distorted "$fname"_X_5_Y_7  D1=$D5 D2=$D7 Bohr "    >> $file_X5_Y7
        echo "$Complex distorted "$fname"_X_5_Y_8  D1=$D5 D2=$D8 Bohr "    >> $file_X5_Y8
        echo "$Complex distorted "$fname"_X_5_Z_1  D1=$D5 D2=$D1 Bohr "    >> $file_X5_Z1
        echo "$Complex distorted "$fname"_X_5_Z_2  D1=$D5 D2=$D2 Bohr "    >> $file_X5_Z2
        echo "$Complex distorted "$fname"_X_5_Z_3  D1=$D5 D2=$D3 Bohr "    >> $file_X5_Z3
        echo "$Complex distorted "$fname"_X_5_Z_4  D1=$D5 D2=$D4 Bohr "    >> $file_X5_Z4
        echo "$Complex distorted "$fname"_X_5_Z_5  D1=$D5 D2=$D5 Bohr "    >> $file_X5_Z5
        echo "$Complex distorted "$fname"_X_5_Z_6  D1=$D5 D2=$D6 Bohr "    >> $file_X5_Z6
        echo "$Complex distorted "$fname"_X_5_Z_7  D1=$D5 D2=$D7 Bohr "    >> $file_X5_Z7
        echo "$Complex distorted "$fname"_X_5_Z_8  D1=$D5 D2=$D8 Bohr "    >> $file_X5_Z8
        echo "$Complex distorted "$fname"_X_6_X_1  D1=$D6 D2=$D1 Bohr "    >> $file_X6_X1
        echo "$Complex distorted "$fname"_X_6_X_2  D1=$D6 D2=$D2 Bohr "    >> $file_X6_X2
        echo "$Complex distorted "$fname"_X_6_X_3  D1=$D6 D2=$D3 Bohr "    >> $file_X6_X3
        echo "$Complex distorted "$fname"_X_6_X_4  D1=$D6 D2=$D4 Bohr "    >> $file_X6_X4
        echo "$Complex distorted "$fname"_X_6_X_5  D1=$D6 D2=$D5 Bohr "    >> $file_X6_X5
        echo "$Complex distorted "$fname"_X_6_X_6  D1=$D6 D2=$D6 Bohr "    >> $file_X6_X6
        echo "$Complex distorted "$fname"_X_6_X_7  D1=$D6 D2=$D7 Bohr "    >> $file_X6_X7
        echo "$Complex distorted "$fname"_X_6_X_8  D1=$D6 D2=$D8 Bohr "    >> $file_X6_X8
        echo "$Complex distorted "$fname"_X_6_Y_1  D1=$D6 D2=$D1 Bohr "    >> $file_X6_Y1
        echo "$Complex distorted "$fname"_X_6_Y_2  D1=$D6 D2=$D2 Bohr "    >> $file_X6_Y2
        echo "$Complex distorted "$fname"_X_6_Y_3  D1=$D6 D2=$D3 Bohr "    >> $file_X6_Y3
        echo "$Complex distorted "$fname"_X_6_Y_4  D1=$D6 D2=$D4 Bohr "    >> $file_X6_Y4
        echo "$Complex distorted "$fname"_X_6_Y_5  D1=$D6 D2=$D5 Bohr "    >> $file_X6_Y5
        echo "$Complex distorted "$fname"_X_6_Y_6  D1=$D6 D2=$D6 Bohr "    >> $file_X6_Y6
        echo "$Complex distorted "$fname"_X_6_Y_7  D1=$D6 D2=$D7 Bohr "    >> $file_X6_Y7
        echo "$Complex distorted "$fname"_X_6_Y_8  D1=$D6 D2=$D8 Bohr "    >> $file_X6_Y8
        echo "$Complex distorted "$fname"_X_6_Z_1  D1=$D6 D2=$D1 Bohr "    >> $file_X6_Z1
        echo "$Complex distorted "$fname"_X_6_Z_2  D1=$D6 D2=$D2 Bohr "    >> $file_X6_Z2
        echo "$Complex distorted "$fname"_X_6_Z_3  D1=$D6 D2=$D3 Bohr "    >> $file_X6_Z3
        echo "$Complex distorted "$fname"_X_6_Z_4  D1=$D6 D2=$D4 Bohr "    >> $file_X6_Z4
        echo "$Complex distorted "$fname"_X_6_Z_5  D1=$D6 D2=$D5 Bohr "    >> $file_X6_Z5
        echo "$Complex distorted "$fname"_X_6_Z_6  D1=$D6 D2=$D6 Bohr "    >> $file_X6_Z6
        echo "$Complex distorted "$fname"_X_6_Z_7  D1=$D6 D2=$D7 Bohr "    >> $file_X6_Z7
        echo "$Complex distorted "$fname"_X_6_Z_8  D1=$D6 D2=$D8 Bohr "    >> $file_X6_Z8
        echo "$Complex distorted "$fname"_X_7_X_1  D1=$D7 D2=$D1 Bohr "    >> $file_X7_X1
        echo "$Complex distorted "$fname"_X_7_X_2  D1=$D7 D2=$D2 Bohr "    >> $file_X7_X2
        echo "$Complex distorted "$fname"_X_7_X_3  D1=$D7 D2=$D3 Bohr "    >> $file_X7_X3
        echo "$Complex distorted "$fname"_X_7_X_4  D1=$D7 D2=$D4 Bohr "    >> $file_X7_X4
        echo "$Complex distorted "$fname"_X_7_X_5  D1=$D7 D2=$D5 Bohr "    >> $file_X7_X5
        echo "$Complex distorted "$fname"_X_7_X_6  D1=$D7 D2=$D6 Bohr "    >> $file_X7_X6
        echo "$Complex distorted "$fname"_X_7_X_7  D1=$D7 D2=$D7 Bohr "    >> $file_X7_X7
        echo "$Complex distorted "$fname"_X_7_X_8  D1=$D7 D2=$D8 Bohr "    >> $file_X7_X8
        echo "$Complex distorted "$fname"_X_7_Y_1  D1=$D7 D2=$D1 Bohr "    >> $file_X7_Y1
        echo "$Complex distorted "$fname"_X_7_Y_2  D1=$D7 D2=$D2 Bohr "    >> $file_X7_Y2
        echo "$Complex distorted "$fname"_X_7_Y_3  D1=$D7 D2=$D3 Bohr "    >> $file_X7_Y3
        echo "$Complex distorted "$fname"_X_7_Y_4  D1=$D7 D2=$D4 Bohr "    >> $file_X7_Y4
        echo "$Complex distorted "$fname"_X_7_Y_5  D1=$D7 D2=$D5 Bohr "    >> $file_X7_Y5
        echo "$Complex distorted "$fname"_X_7_Y_6  D1=$D7 D2=$D6 Bohr "    >> $file_X7_Y6
        echo "$Complex distorted "$fname"_X_7_Y_7  D1=$D7 D2=$D7 Bohr "    >> $file_X7_Y7
        echo "$Complex distorted "$fname"_X_7_Y_8  D1=$D7 D2=$D8 Bohr "    >> $file_X7_Y8
        echo "$Complex distorted "$fname"_X_7_Z_1  D1=$D7 D2=$D1 Bohr "    >> $file_X7_Z1
        echo "$Complex distorted "$fname"_X_7_Z_2  D1=$D7 D2=$D2 Bohr "    >> $file_X7_Z2
        echo "$Complex distorted "$fname"_X_7_Z_3  D1=$D7 D2=$D3 Bohr "    >> $file_X7_Z3
        echo "$Complex distorted "$fname"_X_7_Z_4  D1=$D7 D2=$D4 Bohr "    >> $file_X7_Z4
        echo "$Complex distorted "$fname"_X_7_Z_5  D1=$D7 D2=$D5 Bohr "    >> $file_X7_Z5
        echo "$Complex distorted "$fname"_X_7_Z_6  D1=$D7 D2=$D6 Bohr "    >> $file_X7_Z6
        echo "$Complex distorted "$fname"_X_7_Z_7  D1=$D7 D2=$D7 Bohr "    >> $file_X7_Z7
        echo "$Complex distorted "$fname"_X_7_Z_8  D1=$D7 D2=$D8 Bohr "    >> $file_X7_Z8
        echo "$Complex distorted "$fname"_X_8_X_1  D1=$D8 D2=$D1 Bohr "    >> $file_X8_X1
        echo "$Complex distorted "$fname"_X_8_X_2  D1=$D8 D2=$D2 Bohr "    >> $file_X8_X2
        echo "$Complex distorted "$fname"_X_8_X_3  D1=$D8 D2=$D3 Bohr "    >> $file_X8_X3
        echo "$Complex distorted "$fname"_X_8_X_4  D1=$D8 D2=$D4 Bohr "    >> $file_X8_X4
        echo "$Complex distorted "$fname"_X_8_X_5  D1=$D8 D2=$D5 Bohr "    >> $file_X8_X5
        echo "$Complex distorted "$fname"_X_8_X_6  D1=$D8 D2=$D6 Bohr "    >> $file_X8_X6
        echo "$Complex distorted "$fname"_X_8_X_7  D1=$D8 D2=$D7 Bohr "    >> $file_X8_X7
        echo "$Complex distorted "$fname"_X_8_X_8  D1=$D8 D2=$D8 Bohr "    >> $file_X8_X8
        echo "$Complex distorted "$fname"_X_8_Y_1  D1=$D8 D2=$D1 Bohr "    >> $file_X8_Y1
        echo "$Complex distorted "$fname"_X_8_Y_2  D1=$D8 D2=$D2 Bohr "    >> $file_X8_Y2
        echo "$Complex distorted "$fname"_X_8_Y_3  D1=$D8 D2=$D3 Bohr "    >> $file_X8_Y3
        echo "$Complex distorted "$fname"_X_8_Y_4  D1=$D8 D2=$D4 Bohr "    >> $file_X8_Y4
        echo "$Complex distorted "$fname"_X_8_Y_5  D1=$D8 D2=$D5 Bohr "    >> $file_X8_Y5
        echo "$Complex distorted "$fname"_X_8_Y_6  D1=$D8 D2=$D6 Bohr "    >> $file_X8_Y6
        echo "$Complex distorted "$fname"_X_8_Y_7  D1=$D8 D2=$D7 Bohr "    >> $file_X8_Y7
        echo "$Complex distorted "$fname"_X_8_Y_8  D1=$D8 D2=$D8 Bohr "    >> $file_X8_Y8
        echo "$Complex distorted "$fname"_X_8_Z_1  D1=$D8 D2=$D1 Bohr "    >> $file_X8_Z1
        echo "$Complex distorted "$fname"_X_8_Z_2  D1=$D8 D2=$D2 Bohr "    >> $file_X8_Z2
        echo "$Complex distorted "$fname"_X_8_Z_3  D1=$D8 D2=$D3 Bohr "    >> $file_X8_Z3
        echo "$Complex distorted "$fname"_X_8_Z_4  D1=$D8 D2=$D4 Bohr "    >> $file_X8_Z4
        echo "$Complex distorted "$fname"_X_8_Z_5  D1=$D8 D2=$D5 Bohr "    >> $file_X8_Z5
        echo "$Complex distorted "$fname"_X_8_Z_6  D1=$D8 D2=$D6 Bohr "    >> $file_X8_Z6
        echo "$Complex distorted "$fname"_X_8_Z_7  D1=$D8 D2=$D7 Bohr "    >> $file_X8_Z7
        echo "$Complex distorted "$fname"_X_8_Z_8  D1=$D8 D2=$D8 Bohr "    >> $file_X8_Z8
        echo "$Complex distorted "$fname"_Y_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_X1
        echo "$Complex distorted "$fname"_Y_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_X2
        echo "$Complex distorted "$fname"_Y_1_X_3  D1=$D1 D2=$D3 Bohr "    >> $file_Y1_X3
        echo "$Complex distorted "$fname"_Y_1_X_4  D1=$D1 D2=$D4 Bohr "    >> $file_Y1_X4
        echo "$Complex distorted "$fname"_Y_1_X_5  D1=$D1 D2=$D5 Bohr "    >> $file_Y1_X5
        echo "$Complex distorted "$fname"_Y_1_X_6  D1=$D1 D2=$D6 Bohr "    >> $file_Y1_X6
        echo "$Complex distorted "$fname"_Y_1_X_7  D1=$D1 D2=$D7 Bohr "    >> $file_Y1_X7
        echo "$Complex distorted "$fname"_Y_1_X_8  D1=$D1 D2=$D8 Bohr "    >> $file_Y1_X8
        echo "$Complex distorted "$fname"_Y_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_Y1
        echo "$Complex distorted "$fname"_Y_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_Y2
        echo "$Complex distorted "$fname"_Y_1_Y_3  D1=$D1 D2=$D3 Bohr "    >> $file_Y1_Y3
        echo "$Complex distorted "$fname"_Y_1_Y_4  D1=$D1 D2=$D4 Bohr "    >> $file_Y1_Y4
        echo "$Complex distorted "$fname"_Y_1_Y_5  D1=$D1 D2=$D5 Bohr "    >> $file_Y1_Y5
        echo "$Complex distorted "$fname"_Y_1_Y_6  D1=$D1 D2=$D6 Bohr "    >> $file_Y1_Y6
        echo "$Complex distorted "$fname"_Y_1_Y_7  D1=$D1 D2=$D7 Bohr "    >> $file_Y1_Y7
        echo "$Complex distorted "$fname"_Y_1_Y_8  D1=$D1 D2=$D8 Bohr "    >> $file_Y1_Y8
        echo "$Complex distorted "$fname"_Y_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_Y1_Z1
        echo "$Complex distorted "$fname"_Y_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_Y1_Z2
        echo "$Complex distorted "$fname"_Y_1_Z_3  D1=$D1 D2=$D3 Bohr "    >> $file_Y1_Z3
        echo "$Complex distorted "$fname"_Y_1_Z_4  D1=$D1 D2=$D4 Bohr "    >> $file_Y1_Z4
        echo "$Complex distorted "$fname"_Y_1_Z_5  D1=$D1 D2=$D5 Bohr "    >> $file_Y1_Z5
        echo "$Complex distorted "$fname"_Y_1_Z_6  D1=$D1 D2=$D6 Bohr "    >> $file_Y1_Z6
        echo "$Complex distorted "$fname"_Y_1_Z_7  D1=$D1 D2=$D7 Bohr "    >> $file_Y1_Z7
        echo "$Complex distorted "$fname"_Y_1_Z_8  D1=$D1 D2=$D8 Bohr "    >> $file_Y1_Z8
        echo "$Complex distorted "$fname"_Y_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_X1
        echo "$Complex distorted "$fname"_Y_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_X2
        echo "$Complex distorted "$fname"_Y_2_X_3  D1=$D2 D2=$D3 Bohr "    >> $file_Y2_X3
        echo "$Complex distorted "$fname"_Y_2_X_4  D1=$D2 D2=$D4 Bohr "    >> $file_Y2_X4
        echo "$Complex distorted "$fname"_Y_2_X_5  D1=$D2 D2=$D5 Bohr "    >> $file_Y2_X5
        echo "$Complex distorted "$fname"_Y_2_X_6  D1=$D2 D2=$D6 Bohr "    >> $file_Y2_X6
        echo "$Complex distorted "$fname"_Y_2_X_7  D1=$D2 D2=$D7 Bohr "    >> $file_Y2_X7
        echo "$Complex distorted "$fname"_Y_2_X_8  D1=$D2 D2=$D8 Bohr "    >> $file_Y2_X8
        echo "$Complex distorted "$fname"_Y_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_Y1
        echo "$Complex distorted "$fname"_Y_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_Y2
        echo "$Complex distorted "$fname"_Y_2_Y_3  D1=$D2 D2=$D3 Bohr "    >> $file_Y2_Y3
        echo "$Complex distorted "$fname"_Y_2_Y_4  D1=$D2 D2=$D4 Bohr "    >> $file_Y2_Y4
        echo "$Complex distorted "$fname"_Y_2_Y_5  D1=$D2 D2=$D5 Bohr "    >> $file_Y2_Y5
        echo "$Complex distorted "$fname"_Y_2_Y_6  D1=$D2 D2=$D6 Bohr "    >> $file_Y2_Y6
        echo "$Complex distorted "$fname"_Y_2_Y_7  D1=$D2 D2=$D7 Bohr "    >> $file_Y2_Y7
        echo "$Complex distorted "$fname"_Y_2_Y_8  D1=$D2 D2=$D8 Bohr "    >> $file_Y2_Y8
        echo "$Complex distorted "$fname"_Y_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_Y2_Z1
        echo "$Complex distorted "$fname"_Y_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_Y2_Z2
        echo "$Complex distorted "$fname"_Y_2_Z_3  D1=$D2 D2=$D3 Bohr "    >> $file_Y2_Z3
        echo "$Complex distorted "$fname"_Y_2_Z_4  D1=$D2 D2=$D4 Bohr "    >> $file_Y2_Z4
        echo "$Complex distorted "$fname"_Y_2_Z_5  D1=$D2 D2=$D5 Bohr "    >> $file_Y2_Z5
        echo "$Complex distorted "$fname"_Y_2_Z_6  D1=$D2 D2=$D6 Bohr "    >> $file_Y2_Z6
        echo "$Complex distorted "$fname"_Y_2_Z_7  D1=$D2 D2=$D7 Bohr "    >> $file_Y2_Z7
        echo "$Complex distorted "$fname"_Y_2_Z_8  D1=$D2 D2=$D8 Bohr "    >> $file_Y2_Z8
        echo "$Complex distorted "$fname"_Y_3_X_1  D1=$D3 D2=$D1 Bohr "    >> $file_Y3_X1
        echo "$Complex distorted "$fname"_Y_3_X_2  D1=$D3 D2=$D2 Bohr "    >> $file_Y3_X2
        echo "$Complex distorted "$fname"_Y_3_X_3  D1=$D3 D2=$D3 Bohr "    >> $file_Y3_X3
        echo "$Complex distorted "$fname"_Y_3_X_4  D1=$D3 D2=$D4 Bohr "    >> $file_Y3_X4
        echo "$Complex distorted "$fname"_Y_3_X_5  D1=$D3 D2=$D5 Bohr "    >> $file_Y3_X5
        echo "$Complex distorted "$fname"_Y_3_X_6  D1=$D3 D2=$D6 Bohr "    >> $file_Y3_X6
        echo "$Complex distorted "$fname"_Y_3_X_7  D1=$D3 D2=$D7 Bohr "    >> $file_Y3_X7
        echo "$Complex distorted "$fname"_Y_3_X_8  D1=$D3 D2=$D8 Bohr "    >> $file_Y3_X8
        echo "$Complex distorted "$fname"_Y_3_Y_1  D1=$D3 D2=$D1 Bohr "    >> $file_Y3_Y1
        echo "$Complex distorted "$fname"_Y_3_Y_2  D1=$D3 D2=$D2 Bohr "    >> $file_Y3_Y2
        echo "$Complex distorted "$fname"_Y_3_Y_3  D1=$D3 D2=$D3 Bohr "    >> $file_Y3_Y3
        echo "$Complex distorted "$fname"_Y_3_Y_4  D1=$D3 D2=$D4 Bohr "    >> $file_Y3_Y4
        echo "$Complex distorted "$fname"_Y_3_Y_5  D1=$D3 D2=$D5 Bohr "    >> $file_Y3_Y5
        echo "$Complex distorted "$fname"_Y_3_Y_6  D1=$D3 D2=$D6 Bohr "    >> $file_Y3_Y6
        echo "$Complex distorted "$fname"_Y_3_Y_7  D1=$D3 D2=$D7 Bohr "    >> $file_Y3_Y7
        echo "$Complex distorted "$fname"_Y_3_Y_8  D1=$D3 D2=$D8 Bohr "    >> $file_Y3_Y8
        echo "$Complex distorted "$fname"_Y_3_Z_1  D1=$D3 D2=$D1 Bohr "    >> $file_Y3_Z1
        echo "$Complex distorted "$fname"_Y_3_Z_2  D1=$D3 D2=$D2 Bohr "    >> $file_Y3_Z2
        echo "$Complex distorted "$fname"_Y_3_Z_3  D1=$D3 D2=$D3 Bohr "    >> $file_Y3_Z3
        echo "$Complex distorted "$fname"_Y_3_Z_4  D1=$D3 D2=$D4 Bohr "    >> $file_Y3_Z4
        echo "$Complex distorted "$fname"_Y_3_Z_5  D1=$D3 D2=$D5 Bohr "    >> $file_Y3_Z5
        echo "$Complex distorted "$fname"_Y_3_Z_6  D1=$D3 D2=$D6 Bohr "    >> $file_Y3_Z6
        echo "$Complex distorted "$fname"_Y_3_Z_7  D1=$D3 D2=$D7 Bohr "    >> $file_Y3_Z7
        echo "$Complex distorted "$fname"_Y_3_Z_8  D1=$D3 D2=$D8 Bohr "    >> $file_Y3_Z8
        echo "$Complex distorted "$fname"_Y_4_X_1  D1=$D4 D2=$D1 Bohr "    >> $file_Y4_X1
        echo "$Complex distorted "$fname"_Y_4_X_2  D1=$D4 D2=$D2 Bohr "    >> $file_Y4_X2
        echo "$Complex distorted "$fname"_Y_4_X_3  D1=$D4 D2=$D3 Bohr "    >> $file_Y4_X3
        echo "$Complex distorted "$fname"_Y_4_X_4  D1=$D4 D2=$D4 Bohr "    >> $file_Y4_X4
        echo "$Complex distorted "$fname"_Y_4_X_5  D1=$D4 D2=$D5 Bohr "    >> $file_Y4_X5
        echo "$Complex distorted "$fname"_Y_4_X_6  D1=$D4 D2=$D6 Bohr "    >> $file_Y4_X6
        echo "$Complex distorted "$fname"_Y_4_X_7  D1=$D4 D2=$D7 Bohr "    >> $file_Y4_X7
        echo "$Complex distorted "$fname"_Y_4_X_8  D1=$D4 D2=$D8 Bohr "    >> $file_Y4_X8
        echo "$Complex distorted "$fname"_Y_4_Y_1  D1=$D4 D2=$D1 Bohr "    >> $file_Y4_Y1
        echo "$Complex distorted "$fname"_Y_4_Y_2  D1=$D4 D2=$D2 Bohr "    >> $file_Y4_Y2
        echo "$Complex distorted "$fname"_Y_4_Y_3  D1=$D4 D2=$D3 Bohr "    >> $file_Y4_Y3
        echo "$Complex distorted "$fname"_Y_4_Y_4  D1=$D4 D2=$D4 Bohr "    >> $file_Y4_Y4
        echo "$Complex distorted "$fname"_Y_4_Y_5  D1=$D4 D2=$D5 Bohr "    >> $file_Y4_Y5
        echo "$Complex distorted "$fname"_Y_4_Y_6  D1=$D4 D2=$D6 Bohr "    >> $file_Y4_Y6
        echo "$Complex distorted "$fname"_Y_4_Y_7  D1=$D4 D2=$D7 Bohr "    >> $file_Y4_Y7
        echo "$Complex distorted "$fname"_Y_4_Y_8  D1=$D4 D2=$D8 Bohr "    >> $file_Y4_Y8
        echo "$Complex distorted "$fname"_Y_4_Z_1  D1=$D4 D2=$D1 Bohr "    >> $file_Y4_Z1
        echo "$Complex distorted "$fname"_Y_4_Z_2  D1=$D4 D2=$D2 Bohr "    >> $file_Y4_Z2
        echo "$Complex distorted "$fname"_Y_4_Z_3  D1=$D4 D2=$D3 Bohr "    >> $file_Y4_Z3
        echo "$Complex distorted "$fname"_Y_4_Z_4  D1=$D4 D2=$D4 Bohr "    >> $file_Y4_Z4
        echo "$Complex distorted "$fname"_Y_4_Z_5  D1=$D4 D2=$D5 Bohr "    >> $file_Y4_Z5
        echo "$Complex distorted "$fname"_Y_4_Z_6  D1=$D4 D2=$D6 Bohr "    >> $file_Y4_Z6
        echo "$Complex distorted "$fname"_Y_4_Z_7  D1=$D4 D2=$D7 Bohr "    >> $file_Y4_Z7
        echo "$Complex distorted "$fname"_Y_4_Z_8  D1=$D4 D2=$D8 Bohr "    >> $file_Y4_Z8
        echo "$Complex distorted "$fname"_Y_5_X_1  D1=$D5 D2=$D1 Bohr "    >> $file_Y5_X1
        echo "$Complex distorted "$fname"_Y_5_X_2  D1=$D5 D2=$D2 Bohr "    >> $file_Y5_X2
        echo "$Complex distorted "$fname"_Y_5_X_3  D1=$D5 D2=$D3 Bohr "    >> $file_Y5_X3
        echo "$Complex distorted "$fname"_Y_5_X_4  D1=$D5 D2=$D4 Bohr "    >> $file_Y5_X4
        echo "$Complex distorted "$fname"_Y_5_X_5  D1=$D5 D2=$D5 Bohr "    >> $file_Y5_X5
        echo "$Complex distorted "$fname"_Y_5_X_6  D1=$D5 D2=$D6 Bohr "    >> $file_Y5_X6
        echo "$Complex distorted "$fname"_Y_5_X_7  D1=$D5 D2=$D7 Bohr "    >> $file_Y5_X7
        echo "$Complex distorted "$fname"_Y_5_X_8  D1=$D5 D2=$D8 Bohr "    >> $file_Y5_X8
        echo "$Complex distorted "$fname"_Y_5_Y_1  D1=$D5 D2=$D1 Bohr "    >> $file_Y5_Y1
        echo "$Complex distorted "$fname"_Y_5_Y_2  D1=$D5 D2=$D2 Bohr "    >> $file_Y5_Y2
        echo "$Complex distorted "$fname"_Y_5_Y_3  D1=$D5 D2=$D3 Bohr "    >> $file_Y5_Y3
        echo "$Complex distorted "$fname"_Y_5_Y_4  D1=$D5 D2=$D4 Bohr "    >> $file_Y5_Y4
        echo "$Complex distorted "$fname"_Y_5_Y_5  D1=$D5 D2=$D5 Bohr "    >> $file_Y5_Y5
        echo "$Complex distorted "$fname"_Y_5_Y_6  D1=$D5 D2=$D6 Bohr "    >> $file_Y5_Y6
        echo "$Complex distorted "$fname"_Y_5_Y_7  D1=$D5 D2=$D7 Bohr "    >> $file_Y5_Y7
        echo "$Complex distorted "$fname"_Y_5_Y_8  D1=$D5 D2=$D8 Bohr "    >> $file_Y5_Y8
        echo "$Complex distorted "$fname"_Y_5_Z_1  D1=$D5 D2=$D1 Bohr "    >> $file_Y5_Z1
        echo "$Complex distorted "$fname"_Y_5_Z_2  D1=$D5 D2=$D2 Bohr "    >> $file_Y5_Z2
        echo "$Complex distorted "$fname"_Y_5_Z_3  D1=$D5 D2=$D3 Bohr "    >> $file_Y5_Z3
        echo "$Complex distorted "$fname"_Y_5_Z_4  D1=$D5 D2=$D4 Bohr "    >> $file_Y5_Z4
        echo "$Complex distorted "$fname"_Y_5_Z_5  D1=$D5 D2=$D5 Bohr "    >> $file_Y5_Z5
        echo "$Complex distorted "$fname"_Y_5_Z_6  D1=$D5 D2=$D6 Bohr "    >> $file_Y5_Z6
        echo "$Complex distorted "$fname"_Y_5_Z_7  D1=$D5 D2=$D7 Bohr "    >> $file_Y5_Z7
        echo "$Complex distorted "$fname"_Y_5_Z_8  D1=$D5 D2=$D8 Bohr "    >> $file_Y5_Z8
        echo "$Complex distorted "$fname"_Y_6_X_1  D1=$D6 D2=$D1 Bohr "    >> $file_Y6_X1
        echo "$Complex distorted "$fname"_Y_6_X_2  D1=$D6 D2=$D2 Bohr "    >> $file_Y6_X2
        echo "$Complex distorted "$fname"_Y_6_X_3  D1=$D6 D2=$D3 Bohr "    >> $file_Y6_X3
        echo "$Complex distorted "$fname"_Y_6_X_4  D1=$D6 D2=$D4 Bohr "    >> $file_Y6_X4
        echo "$Complex distorted "$fname"_Y_6_X_5  D1=$D6 D2=$D5 Bohr "    >> $file_Y6_X5
        echo "$Complex distorted "$fname"_Y_6_X_6  D1=$D6 D2=$D6 Bohr "    >> $file_Y6_X6
        echo "$Complex distorted "$fname"_Y_6_X_7  D1=$D6 D2=$D7 Bohr "    >> $file_Y6_X7
        echo "$Complex distorted "$fname"_Y_6_X_8  D1=$D6 D2=$D8 Bohr "    >> $file_Y6_X8
        echo "$Complex distorted "$fname"_Y_6_Y_1  D1=$D6 D2=$D1 Bohr "    >> $file_Y6_Y1
        echo "$Complex distorted "$fname"_Y_6_Y_2  D1=$D6 D2=$D2 Bohr "    >> $file_Y6_Y2
        echo "$Complex distorted "$fname"_Y_6_Y_3  D1=$D6 D2=$D3 Bohr "    >> $file_Y6_Y3
        echo "$Complex distorted "$fname"_Y_6_Y_4  D1=$D6 D2=$D4 Bohr "    >> $file_Y6_Y4
        echo "$Complex distorted "$fname"_Y_6_Y_5  D1=$D6 D2=$D5 Bohr "    >> $file_Y6_Y5
        echo "$Complex distorted "$fname"_Y_6_Y_6  D1=$D6 D2=$D6 Bohr "    >> $file_Y6_Y6
        echo "$Complex distorted "$fname"_Y_6_Y_7  D1=$D6 D2=$D7 Bohr "    >> $file_Y6_Y7
        echo "$Complex distorted "$fname"_Y_6_Y_8  D1=$D6 D2=$D8 Bohr "    >> $file_Y6_Y8
        echo "$Complex distorted "$fname"_Y_6_Z_1  D1=$D6 D2=$D1 Bohr "    >> $file_Y6_Z1
        echo "$Complex distorted "$fname"_Y_6_Z_2  D1=$D6 D2=$D2 Bohr "    >> $file_Y6_Z2
        echo "$Complex distorted "$fname"_Y_6_Z_3  D1=$D6 D2=$D3 Bohr "    >> $file_Y6_Z3
        echo "$Complex distorted "$fname"_Y_6_Z_4  D1=$D6 D2=$D4 Bohr "    >> $file_Y6_Z4
        echo "$Complex distorted "$fname"_Y_6_Z_5  D1=$D6 D2=$D5 Bohr "    >> $file_Y6_Z5
        echo "$Complex distorted "$fname"_Y_6_Z_6  D1=$D6 D2=$D6 Bohr "    >> $file_Y6_Z6
        echo "$Complex distorted "$fname"_Y_6_Z_7  D1=$D6 D2=$D7 Bohr "    >> $file_Y6_Z7
        echo "$Complex distorted "$fname"_Y_6_Z_8  D1=$D6 D2=$D8 Bohr "    >> $file_Y6_Z8
        echo "$Complex distorted "$fname"_Y_7_X_1  D1=$D7 D2=$D1 Bohr "    >> $file_Y7_X1
        echo "$Complex distorted "$fname"_Y_7_X_2  D1=$D7 D2=$D2 Bohr "    >> $file_Y7_X2
        echo "$Complex distorted "$fname"_Y_7_X_3  D1=$D7 D2=$D3 Bohr "    >> $file_Y7_X3
        echo "$Complex distorted "$fname"_Y_7_X_4  D1=$D7 D2=$D4 Bohr "    >> $file_Y7_X4
        echo "$Complex distorted "$fname"_Y_7_X_5  D1=$D7 D2=$D5 Bohr "    >> $file_Y7_X5
        echo "$Complex distorted "$fname"_Y_7_X_6  D1=$D7 D2=$D6 Bohr "    >> $file_Y7_X6
        echo "$Complex distorted "$fname"_Y_7_X_7  D1=$D7 D2=$D7 Bohr "    >> $file_Y7_X7
        echo "$Complex distorted "$fname"_Y_7_X_8  D1=$D7 D2=$D8 Bohr "    >> $file_Y7_X8
        echo "$Complex distorted "$fname"_Y_7_Y_1  D1=$D7 D2=$D1 Bohr "    >> $file_Y7_Y1
        echo "$Complex distorted "$fname"_Y_7_Y_2  D1=$D7 D2=$D2 Bohr "    >> $file_Y7_Y2
        echo "$Complex distorted "$fname"_Y_7_Y_3  D1=$D7 D2=$D3 Bohr "    >> $file_Y7_Y3
        echo "$Complex distorted "$fname"_Y_7_Y_4  D1=$D7 D2=$D4 Bohr "    >> $file_Y7_Y4
        echo "$Complex distorted "$fname"_Y_7_Y_5  D1=$D7 D2=$D5 Bohr "    >> $file_Y7_Y5
        echo "$Complex distorted "$fname"_Y_7_Y_6  D1=$D7 D2=$D6 Bohr "    >> $file_Y7_Y6
        echo "$Complex distorted "$fname"_Y_7_Y_7  D1=$D7 D2=$D7 Bohr "    >> $file_Y7_Y7
        echo "$Complex distorted "$fname"_Y_7_Y_8  D1=$D7 D2=$D8 Bohr "    >> $file_Y7_Y8
        echo "$Complex distorted "$fname"_Y_7_Z_1  D1=$D7 D2=$D1 Bohr "    >> $file_Y7_Z1
        echo "$Complex distorted "$fname"_Y_7_Z_2  D1=$D7 D2=$D2 Bohr "    >> $file_Y7_Z2
        echo "$Complex distorted "$fname"_Y_7_Z_3  D1=$D7 D2=$D3 Bohr "    >> $file_Y7_Z3
        echo "$Complex distorted "$fname"_Y_7_Z_4  D1=$D7 D2=$D4 Bohr "    >> $file_Y7_Z4
        echo "$Complex distorted "$fname"_Y_7_Z_5  D1=$D7 D2=$D5 Bohr "    >> $file_Y7_Z5
        echo "$Complex distorted "$fname"_Y_7_Z_6  D1=$D7 D2=$D6 Bohr "    >> $file_Y7_Z6
        echo "$Complex distorted "$fname"_Y_7_Z_7  D1=$D7 D2=$D7 Bohr "    >> $file_Y7_Z7
        echo "$Complex distorted "$fname"_Y_7_Z_8  D1=$D7 D2=$D8 Bohr "    >> $file_Y7_Z8
        echo "$Complex distorted "$fname"_Y_8_X_1  D1=$D8 D2=$D1 Bohr "    >> $file_Y8_X1
        echo "$Complex distorted "$fname"_Y_8_X_2  D1=$D8 D2=$D2 Bohr "    >> $file_Y8_X2
        echo "$Complex distorted "$fname"_Y_8_X_3  D1=$D8 D2=$D3 Bohr "    >> $file_Y8_X3
        echo "$Complex distorted "$fname"_Y_8_X_4  D1=$D8 D2=$D4 Bohr "    >> $file_Y8_X4
        echo "$Complex distorted "$fname"_Y_8_X_5  D1=$D8 D2=$D5 Bohr "    >> $file_Y8_X5
        echo "$Complex distorted "$fname"_Y_8_X_6  D1=$D8 D2=$D6 Bohr "    >> $file_Y8_X6
        echo "$Complex distorted "$fname"_Y_8_X_7  D1=$D8 D2=$D7 Bohr "    >> $file_Y8_X7
        echo "$Complex distorted "$fname"_Y_8_X_8  D1=$D8 D2=$D8 Bohr "    >> $file_Y8_X8
        echo "$Complex distorted "$fname"_Y_8_Y_1  D1=$D8 D2=$D1 Bohr "    >> $file_Y8_Y1
        echo "$Complex distorted "$fname"_Y_8_Y_2  D1=$D8 D2=$D2 Bohr "    >> $file_Y8_Y2
        echo "$Complex distorted "$fname"_Y_8_Y_3  D1=$D8 D2=$D3 Bohr "    >> $file_Y8_Y3
        echo "$Complex distorted "$fname"_Y_8_Y_4  D1=$D8 D2=$D4 Bohr "    >> $file_Y8_Y4
        echo "$Complex distorted "$fname"_Y_8_Y_5  D1=$D8 D2=$D5 Bohr "    >> $file_Y8_Y5
        echo "$Complex distorted "$fname"_Y_8_Y_6  D1=$D8 D2=$D6 Bohr "    >> $file_Y8_Y6
        echo "$Complex distorted "$fname"_Y_8_Y_7  D1=$D8 D2=$D7 Bohr "    >> $file_Y8_Y7
        echo "$Complex distorted "$fname"_Y_8_Y_8  D1=$D8 D2=$D8 Bohr "    >> $file_Y8_Y8
        echo "$Complex distorted "$fname"_Y_8_Z_1  D1=$D8 D2=$D1 Bohr "    >> $file_Y8_Z1
        echo "$Complex distorted "$fname"_Y_8_Z_2  D1=$D8 D2=$D2 Bohr "    >> $file_Y8_Z2
        echo "$Complex distorted "$fname"_Y_8_Z_3  D1=$D8 D2=$D3 Bohr "    >> $file_Y8_Z3
        echo "$Complex distorted "$fname"_Y_8_Z_4  D1=$D8 D2=$D4 Bohr "    >> $file_Y8_Z4
        echo "$Complex distorted "$fname"_Y_8_Z_5  D1=$D8 D2=$D5 Bohr "    >> $file_Y8_Z5
        echo "$Complex distorted "$fname"_Y_8_Z_6  D1=$D8 D2=$D6 Bohr "    >> $file_Y8_Z6
        echo "$Complex distorted "$fname"_Y_8_Z_7  D1=$D8 D2=$D7 Bohr "    >> $file_Y8_Z7
        echo "$Complex distorted "$fname"_Y_8_Z_8  D1=$D8 D2=$D8 Bohr "    >> $file_Y8_Z8
        echo "$Complex distorted "$fname"_Z_1_X_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_X1
        echo "$Complex distorted "$fname"_Z_1_X_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_X2
        echo "$Complex distorted "$fname"_Z_1_X_3  D1=$D1 D2=$D3 Bohr "    >> $file_Z1_X3
        echo "$Complex distorted "$fname"_Z_1_X_4  D1=$D1 D2=$D4 Bohr "    >> $file_Z1_X4
        echo "$Complex distorted "$fname"_Z_1_X_5  D1=$D1 D2=$D5 Bohr "    >> $file_Z1_X5
        echo "$Complex distorted "$fname"_Z_1_X_6  D1=$D1 D2=$D6 Bohr "    >> $file_Z1_X6
        echo "$Complex distorted "$fname"_Z_1_X_7  D1=$D1 D2=$D7 Bohr "    >> $file_Z1_X7
        echo "$Complex distorted "$fname"_Z_1_X_8  D1=$D1 D2=$D8 Bohr "    >> $file_Z1_X8
        echo "$Complex distorted "$fname"_Z_1_Y_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_Y1
        echo "$Complex distorted "$fname"_Z_1_Y_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_Y2
        echo "$Complex distorted "$fname"_Z_1_Y_3  D1=$D1 D2=$D3 Bohr "    >> $file_Z1_Y3
        echo "$Complex distorted "$fname"_Z_1_Y_4  D1=$D1 D2=$D4 Bohr "    >> $file_Z1_Y4
        echo "$Complex distorted "$fname"_Z_1_Y_5  D1=$D1 D2=$D5 Bohr "    >> $file_Z1_Y5
        echo "$Complex distorted "$fname"_Z_1_Y_6  D1=$D1 D2=$D6 Bohr "    >> $file_Z1_Y6
        echo "$Complex distorted "$fname"_Z_1_Y_7  D1=$D1 D2=$D7 Bohr "    >> $file_Z1_Y7
        echo "$Complex distorted "$fname"_Z_1_Y_8  D1=$D1 D2=$D8 Bohr "    >> $file_Z1_Y8
        echo "$Complex distorted "$fname"_Z_1_Z_1  D1=$D1 D2=$D1 Bohr "    >> $file_Z1_Z1
        echo "$Complex distorted "$fname"_Z_1_Z_2  D1=$D1 D2=$D2 Bohr "    >> $file_Z1_Z2
        echo "$Complex distorted "$fname"_Z_1_Z_3  D1=$D1 D2=$D3 Bohr "    >> $file_Z1_Z3
        echo "$Complex distorted "$fname"_Z_1_Z_4  D1=$D1 D2=$D4 Bohr "    >> $file_Z1_Z4
        echo "$Complex distorted "$fname"_Z_1_Z_5  D1=$D1 D2=$D5 Bohr "    >> $file_Z1_Z5
        echo "$Complex distorted "$fname"_Z_1_Z_6  D1=$D1 D2=$D6 Bohr "    >> $file_Z1_Z6
        echo "$Complex distorted "$fname"_Z_1_Z_7  D1=$D1 D2=$D7 Bohr "    >> $file_Z1_Z7
        echo "$Complex distorted "$fname"_Z_1_Z_8  D1=$D1 D2=$D8 Bohr "    >> $file_Z1_Z8
        echo "$Complex distorted "$fname"_Z_2_X_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_X1
        echo "$Complex distorted "$fname"_Z_2_X_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_X2
        echo "$Complex distorted "$fname"_Z_2_X_3  D1=$D2 D2=$D3 Bohr "    >> $file_Z2_X3
        echo "$Complex distorted "$fname"_Z_2_X_4  D1=$D2 D2=$D4 Bohr "    >> $file_Z2_X4
        echo "$Complex distorted "$fname"_Z_2_X_5  D1=$D2 D2=$D5 Bohr "    >> $file_Z2_X5
        echo "$Complex distorted "$fname"_Z_2_X_6  D1=$D2 D2=$D6 Bohr "    >> $file_Z2_X6
        echo "$Complex distorted "$fname"_Z_2_X_7  D1=$D2 D2=$D7 Bohr "    >> $file_Z2_X7
        echo "$Complex distorted "$fname"_Z_2_X_8  D1=$D2 D2=$D8 Bohr "    >> $file_Z2_X8
        echo "$Complex distorted "$fname"_Z_2_Y_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_Y1
        echo "$Complex distorted "$fname"_Z_2_Y_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_Y2
        echo "$Complex distorted "$fname"_Z_2_Y_3  D1=$D2 D2=$D3 Bohr "    >> $file_Z2_Y3
        echo "$Complex distorted "$fname"_Z_2_Y_4  D1=$D2 D2=$D4 Bohr "    >> $file_Z2_Y4
        echo "$Complex distorted "$fname"_Z_2_Y_5  D1=$D2 D2=$D5 Bohr "    >> $file_Z2_Y5
        echo "$Complex distorted "$fname"_Z_2_Y_6  D1=$D2 D2=$D6 Bohr "    >> $file_Z2_Y6
        echo "$Complex distorted "$fname"_Z_2_Y_7  D1=$D2 D2=$D7 Bohr "    >> $file_Z2_Y7
        echo "$Complex distorted "$fname"_Z_2_Y_8  D1=$D2 D2=$D8 Bohr "    >> $file_Z2_Y8
        echo "$Complex distorted "$fname"_Z_2_Z_1  D1=$D2 D2=$D1 Bohr "    >> $file_Z2_Z1
        echo "$Complex distorted "$fname"_Z_2_Z_2  D1=$D2 D2=$D2 Bohr "    >> $file_Z2_Z2
        echo "$Complex distorted "$fname"_Z_2_Z_3  D1=$D2 D2=$D3 Bohr "    >> $file_Z2_Z3
        echo "$Complex distorted "$fname"_Z_2_Z_4  D1=$D2 D2=$D4 Bohr "    >> $file_Z2_Z4
        echo "$Complex distorted "$fname"_Z_2_Z_5  D1=$D2 D2=$D5 Bohr "    >> $file_Z2_Z5
        echo "$Complex distorted "$fname"_Z_2_Z_6  D1=$D2 D2=$D6 Bohr "    >> $file_Z2_Z6
        echo "$Complex distorted "$fname"_Z_2_Z_7  D1=$D2 D2=$D7 Bohr "    >> $file_Z2_Z7
        echo "$Complex distorted "$fname"_Z_2_Z_8  D1=$D2 D2=$D8 Bohr "    >> $file_Z2_Z8
        echo "$Complex distorted "$fname"_Z_3_X_1  D1=$D3 D2=$D1 Bohr "    >> $file_Z3_X1
        echo "$Complex distorted "$fname"_Z_3_X_2  D1=$D3 D2=$D2 Bohr "    >> $file_Z3_X2
        echo "$Complex distorted "$fname"_Z_3_X_3  D1=$D3 D2=$D3 Bohr "    >> $file_Z3_X3
        echo "$Complex distorted "$fname"_Z_3_X_4  D1=$D3 D2=$D4 Bohr "    >> $file_Z3_X4
        echo "$Complex distorted "$fname"_Z_3_X_5  D1=$D3 D2=$D5 Bohr "    >> $file_Z3_X5
        echo "$Complex distorted "$fname"_Z_3_X_6  D1=$D3 D2=$D6 Bohr "    >> $file_Z3_X6
        echo "$Complex distorted "$fname"_Z_3_X_7  D1=$D3 D2=$D7 Bohr "    >> $file_Z3_X7
        echo "$Complex distorted "$fname"_Z_3_X_8  D1=$D3 D2=$D8 Bohr "    >> $file_Z3_X8
        echo "$Complex distorted "$fname"_Z_3_Y_1  D1=$D3 D2=$D1 Bohr "    >> $file_Z3_Y1
        echo "$Complex distorted "$fname"_Z_3_Y_2  D1=$D3 D2=$D2 Bohr "    >> $file_Z3_Y2
        echo "$Complex distorted "$fname"_Z_3_Y_3  D1=$D3 D2=$D3 Bohr "    >> $file_Z3_Y3
        echo "$Complex distorted "$fname"_Z_3_Y_4  D1=$D3 D2=$D4 Bohr "    >> $file_Z3_Y4
        echo "$Complex distorted "$fname"_Z_3_Y_5  D1=$D3 D2=$D5 Bohr "    >> $file_Z3_Y5
        echo "$Complex distorted "$fname"_Z_3_Y_6  D1=$D3 D2=$D6 Bohr "    >> $file_Z3_Y6
        echo "$Complex distorted "$fname"_Z_3_Y_7  D1=$D3 D2=$D7 Bohr "    >> $file_Z3_Y7
        echo "$Complex distorted "$fname"_Z_3_Y_8  D1=$D3 D2=$D8 Bohr "    >> $file_Z3_Y8
        echo "$Complex distorted "$fname"_Z_3_Z_1  D1=$D3 D2=$D1 Bohr "    >> $file_Z3_Z1
        echo "$Complex distorted "$fname"_Z_3_Z_2  D1=$D3 D2=$D2 Bohr "    >> $file_Z3_Z2
        echo "$Complex distorted "$fname"_Z_3_Z_3  D1=$D3 D2=$D3 Bohr "    >> $file_Z3_Z3
        echo "$Complex distorted "$fname"_Z_3_Z_4  D1=$D3 D2=$D4 Bohr "    >> $file_Z3_Z4
        echo "$Complex distorted "$fname"_Z_3_Z_5  D1=$D3 D2=$D5 Bohr "    >> $file_Z3_Z5
        echo "$Complex distorted "$fname"_Z_3_Z_6  D1=$D3 D2=$D6 Bohr "    >> $file_Z3_Z6
        echo "$Complex distorted "$fname"_Z_3_Z_7  D1=$D3 D2=$D7 Bohr "    >> $file_Z3_Z7
        echo "$Complex distorted "$fname"_Z_3_Z_8  D1=$D3 D2=$D8 Bohr "    >> $file_Z3_Z8
        echo "$Complex distorted "$fname"_Z_4_X_1  D1=$D4 D2=$D1 Bohr "    >> $file_Z4_X1
        echo "$Complex distorted "$fname"_Z_4_X_2  D1=$D4 D2=$D2 Bohr "    >> $file_Z4_X2
        echo "$Complex distorted "$fname"_Z_4_X_3  D1=$D4 D2=$D3 Bohr "    >> $file_Z4_X3
        echo "$Complex distorted "$fname"_Z_4_X_4  D1=$D4 D2=$D4 Bohr "    >> $file_Z4_X4
        echo "$Complex distorted "$fname"_Z_4_X_5  D1=$D4 D2=$D5 Bohr "    >> $file_Z4_X5
        echo "$Complex distorted "$fname"_Z_4_X_6  D1=$D4 D2=$D6 Bohr "    >> $file_Z4_X6
        echo "$Complex distorted "$fname"_Z_4_X_7  D1=$D4 D2=$D7 Bohr "    >> $file_Z4_X7
        echo "$Complex distorted "$fname"_Z_4_X_8  D1=$D4 D2=$D8 Bohr "    >> $file_Z4_X8
        echo "$Complex distorted "$fname"_Z_4_Y_1  D1=$D4 D2=$D1 Bohr "    >> $file_Z4_Y1
        echo "$Complex distorted "$fname"_Z_4_Y_2  D1=$D4 D2=$D2 Bohr "    >> $file_Z4_Y2
        echo "$Complex distorted "$fname"_Z_4_Y_3  D1=$D4 D2=$D3 Bohr "    >> $file_Z4_Y3
        echo "$Complex distorted "$fname"_Z_4_Y_4  D1=$D4 D2=$D4 Bohr "    >> $file_Z4_Y4
        echo "$Complex distorted "$fname"_Z_4_Y_5  D1=$D4 D2=$D5 Bohr "    >> $file_Z4_Y5
        echo "$Complex distorted "$fname"_Z_4_Y_6  D1=$D4 D2=$D6 Bohr "    >> $file_Z4_Y6
        echo "$Complex distorted "$fname"_Z_4_Y_7  D1=$D4 D2=$D7 Bohr "    >> $file_Z4_Y7
        echo "$Complex distorted "$fname"_Z_4_Y_8  D1=$D4 D2=$D8 Bohr "    >> $file_Z4_Y8
        echo "$Complex distorted "$fname"_Z_4_Z_1  D1=$D4 D2=$D1 Bohr "    >> $file_Z4_Z1
        echo "$Complex distorted "$fname"_Z_4_Z_2  D1=$D4 D2=$D2 Bohr "    >> $file_Z4_Z2
        echo "$Complex distorted "$fname"_Z_4_Z_3  D1=$D4 D2=$D3 Bohr "    >> $file_Z4_Z3
        echo "$Complex distorted "$fname"_Z_4_Z_4  D1=$D4 D2=$D4 Bohr "    >> $file_Z4_Z4
        echo "$Complex distorted "$fname"_Z_4_Z_5  D1=$D4 D2=$D5 Bohr "    >> $file_Z4_Z5
        echo "$Complex distorted "$fname"_Z_4_Z_6  D1=$D4 D2=$D6 Bohr "    >> $file_Z4_Z6
        echo "$Complex distorted "$fname"_Z_4_Z_7  D1=$D4 D2=$D7 Bohr "    >> $file_Z4_Z7
        echo "$Complex distorted "$fname"_Z_4_Z_8  D1=$D4 D2=$D8 Bohr "    >> $file_Z4_Z8
        echo "$Complex distorted "$fname"_Z_5_X_1  D1=$D5 D2=$D1 Bohr "    >> $file_Z5_X1
        echo "$Complex distorted "$fname"_Z_5_X_2  D1=$D5 D2=$D2 Bohr "    >> $file_Z5_X2
        echo "$Complex distorted "$fname"_Z_5_X_3  D1=$D5 D2=$D3 Bohr "    >> $file_Z5_X3
        echo "$Complex distorted "$fname"_Z_5_X_4  D1=$D5 D2=$D4 Bohr "    >> $file_Z5_X4
        echo "$Complex distorted "$fname"_Z_5_X_5  D1=$D5 D2=$D5 Bohr "    >> $file_Z5_X5
        echo "$Complex distorted "$fname"_Z_5_X_6  D1=$D5 D2=$D6 Bohr "    >> $file_Z5_X6
        echo "$Complex distorted "$fname"_Z_5_X_7  D1=$D5 D2=$D7 Bohr "    >> $file_Z5_X7
        echo "$Complex distorted "$fname"_Z_5_X_8  D1=$D5 D2=$D8 Bohr "    >> $file_Z5_X8
        echo "$Complex distorted "$fname"_Z_5_Y_1  D1=$D5 D2=$D1 Bohr "    >> $file_Z5_Y1
        echo "$Complex distorted "$fname"_Z_5_Y_2  D1=$D5 D2=$D2 Bohr "    >> $file_Z5_Y2
        echo "$Complex distorted "$fname"_Z_5_Y_3  D1=$D5 D2=$D3 Bohr "    >> $file_Z5_Y3
        echo "$Complex distorted "$fname"_Z_5_Y_4  D1=$D5 D2=$D4 Bohr "    >> $file_Z5_Y4
        echo "$Complex distorted "$fname"_Z_5_Y_5  D1=$D5 D2=$D5 Bohr "    >> $file_Z5_Y5
        echo "$Complex distorted "$fname"_Z_5_Y_6  D1=$D5 D2=$D6 Bohr "    >> $file_Z5_Y6
        echo "$Complex distorted "$fname"_Z_5_Y_7  D1=$D5 D2=$D7 Bohr "    >> $file_Z5_Y7
        echo "$Complex distorted "$fname"_Z_5_Y_8  D1=$D5 D2=$D8 Bohr "    >> $file_Z5_Y8
        echo "$Complex distorted "$fname"_Z_5_Z_1  D1=$D5 D2=$D1 Bohr "    >> $file_Z5_Z1
        echo "$Complex distorted "$fname"_Z_5_Z_2  D1=$D5 D2=$D2 Bohr "    >> $file_Z5_Z2
        echo "$Complex distorted "$fname"_Z_5_Z_3  D1=$D5 D2=$D3 Bohr "    >> $file_Z5_Z3
        echo "$Complex distorted "$fname"_Z_5_Z_4  D1=$D5 D2=$D4 Bohr "    >> $file_Z5_Z4
        echo "$Complex distorted "$fname"_Z_5_Z_5  D1=$D5 D2=$D5 Bohr "    >> $file_Z5_Z5
        echo "$Complex distorted "$fname"_Z_5_Z_6  D1=$D5 D2=$D6 Bohr "    >> $file_Z5_Z6
        echo "$Complex distorted "$fname"_Z_5_Z_7  D1=$D5 D2=$D7 Bohr "    >> $file_Z5_Z7
        echo "$Complex distorted "$fname"_Z_5_Z_8  D1=$D5 D2=$D8 Bohr "    >> $file_Z5_Z8
        echo "$Complex distorted "$fname"_Z_6_X_1  D1=$D6 D2=$D1 Bohr "    >> $file_Z6_X1
        echo "$Complex distorted "$fname"_Z_6_X_2  D1=$D6 D2=$D2 Bohr "    >> $file_Z6_X2
        echo "$Complex distorted "$fname"_Z_6_X_3  D1=$D6 D2=$D3 Bohr "    >> $file_Z6_X3
        echo "$Complex distorted "$fname"_Z_6_X_4  D1=$D6 D2=$D4 Bohr "    >> $file_Z6_X4
        echo "$Complex distorted "$fname"_Z_6_X_5  D1=$D6 D2=$D5 Bohr "    >> $file_Z6_X5
        echo "$Complex distorted "$fname"_Z_6_X_6  D1=$D6 D2=$D6 Bohr "    >> $file_Z6_X6
        echo "$Complex distorted "$fname"_Z_6_X_7  D1=$D6 D2=$D7 Bohr "    >> $file_Z6_X7
        echo "$Complex distorted "$fname"_Z_6_X_8  D1=$D6 D2=$D8 Bohr "    >> $file_Z6_X8
        echo "$Complex distorted "$fname"_Z_6_Y_1  D1=$D6 D2=$D1 Bohr "    >> $file_Z6_Y1
        echo "$Complex distorted "$fname"_Z_6_Y_2  D1=$D6 D2=$D2 Bohr "    >> $file_Z6_Y2
        echo "$Complex distorted "$fname"_Z_6_Y_3  D1=$D6 D2=$D3 Bohr "    >> $file_Z6_Y3
        echo "$Complex distorted "$fname"_Z_6_Y_4  D1=$D6 D2=$D4 Bohr "    >> $file_Z6_Y4
        echo "$Complex distorted "$fname"_Z_6_Y_5  D1=$D6 D2=$D5 Bohr "    >> $file_Z6_Y5
        echo "$Complex distorted "$fname"_Z_6_Y_6  D1=$D6 D2=$D6 Bohr "    >> $file_Z6_Y6
        echo "$Complex distorted "$fname"_Z_6_Y_7  D1=$D6 D2=$D7 Bohr "    >> $file_Z6_Y7
        echo "$Complex distorted "$fname"_Z_6_Y_8  D1=$D6 D2=$D8 Bohr "    >> $file_Z6_Y8
        echo "$Complex distorted "$fname"_Z_6_Z_1  D1=$D6 D2=$D1 Bohr "    >> $file_Z6_Z1
        echo "$Complex distorted "$fname"_Z_6_Z_2  D1=$D6 D2=$D2 Bohr "    >> $file_Z6_Z2
        echo "$Complex distorted "$fname"_Z_6_Z_3  D1=$D6 D2=$D3 Bohr "    >> $file_Z6_Z3
        echo "$Complex distorted "$fname"_Z_6_Z_4  D1=$D6 D2=$D4 Bohr "    >> $file_Z6_Z4
        echo "$Complex distorted "$fname"_Z_6_Z_5  D1=$D6 D2=$D5 Bohr "    >> $file_Z6_Z5
        echo "$Complex distorted "$fname"_Z_6_Z_6  D1=$D6 D2=$D6 Bohr "    >> $file_Z6_Z6
        echo "$Complex distorted "$fname"_Z_6_Z_7  D1=$D6 D2=$D7 Bohr "    >> $file_Z6_Z7
        echo "$Complex distorted "$fname"_Z_6_Z_8  D1=$D6 D2=$D8 Bohr "    >> $file_Z6_Z8
        echo "$Complex distorted "$fname"_Z_7_X_1  D1=$D7 D2=$D1 Bohr "    >> $file_Z7_X1
        echo "$Complex distorted "$fname"_Z_7_X_2  D1=$D7 D2=$D2 Bohr "    >> $file_Z7_X2
        echo "$Complex distorted "$fname"_Z_7_X_3  D1=$D7 D2=$D3 Bohr "    >> $file_Z7_X3
        echo "$Complex distorted "$fname"_Z_7_X_4  D1=$D7 D2=$D4 Bohr "    >> $file_Z7_X4
        echo "$Complex distorted "$fname"_Z_7_X_5  D1=$D7 D2=$D5 Bohr "    >> $file_Z7_X5
        echo "$Complex distorted "$fname"_Z_7_X_6  D1=$D7 D2=$D6 Bohr "    >> $file_Z7_X6
        echo "$Complex distorted "$fname"_Z_7_X_7  D1=$D7 D2=$D7 Bohr "    >> $file_Z7_X7
        echo "$Complex distorted "$fname"_Z_7_X_8  D1=$D7 D2=$D8 Bohr "    >> $file_Z7_X8
        echo "$Complex distorted "$fname"_Z_7_Y_1  D1=$D7 D2=$D1 Bohr "    >> $file_Z7_Y1
        echo "$Complex distorted "$fname"_Z_7_Y_2  D1=$D7 D2=$D2 Bohr "    >> $file_Z7_Y2
        echo "$Complex distorted "$fname"_Z_7_Y_3  D1=$D7 D2=$D3 Bohr "    >> $file_Z7_Y3
        echo "$Complex distorted "$fname"_Z_7_Y_4  D1=$D7 D2=$D4 Bohr "    >> $file_Z7_Y4
        echo "$Complex distorted "$fname"_Z_7_Y_5  D1=$D7 D2=$D5 Bohr "    >> $file_Z7_Y5
        echo "$Complex distorted "$fname"_Z_7_Y_6  D1=$D7 D2=$D6 Bohr "    >> $file_Z7_Y6
        echo "$Complex distorted "$fname"_Z_7_Y_7  D1=$D7 D2=$D7 Bohr "    >> $file_Z7_Y7
        echo "$Complex distorted "$fname"_Z_7_Y_8  D1=$D7 D2=$D8 Bohr "    >> $file_Z7_Y8
        echo "$Complex distorted "$fname"_Z_7_Z_1  D1=$D7 D2=$D1 Bohr "    >> $file_Z7_Z1
        echo "$Complex distorted "$fname"_Z_7_Z_2  D1=$D7 D2=$D2 Bohr "    >> $file_Z7_Z2
        echo "$Complex distorted "$fname"_Z_7_Z_3  D1=$D7 D2=$D3 Bohr "    >> $file_Z7_Z3
        echo "$Complex distorted "$fname"_Z_7_Z_4  D1=$D7 D2=$D4 Bohr "    >> $file_Z7_Z4
        echo "$Complex distorted "$fname"_Z_7_Z_5  D1=$D7 D2=$D5 Bohr "    >> $file_Z7_Z5
        echo "$Complex distorted "$fname"_Z_7_Z_6  D1=$D7 D2=$D6 Bohr "    >> $file_Z7_Z6
        echo "$Complex distorted "$fname"_Z_7_Z_7  D1=$D7 D2=$D7 Bohr "    >> $file_Z7_Z7
        echo "$Complex distorted "$fname"_Z_7_Z_8  D1=$D7 D2=$D8 Bohr "    >> $file_Z7_Z8
        echo "$Complex distorted "$fname"_Z_8_X_1  D1=$D8 D2=$D1 Bohr "    >> $file_Z8_X1
        echo "$Complex distorted "$fname"_Z_8_X_2  D1=$D8 D2=$D2 Bohr "    >> $file_Z8_X2
        echo "$Complex distorted "$fname"_Z_8_X_3  D1=$D8 D2=$D3 Bohr "    >> $file_Z8_X3
        echo "$Complex distorted "$fname"_Z_8_X_4  D1=$D8 D2=$D4 Bohr "    >> $file_Z8_X4
        echo "$Complex distorted "$fname"_Z_8_X_5  D1=$D8 D2=$D5 Bohr "    >> $file_Z8_X5
        echo "$Complex distorted "$fname"_Z_8_X_6  D1=$D8 D2=$D6 Bohr "    >> $file_Z8_X6
        echo "$Complex distorted "$fname"_Z_8_X_7  D1=$D8 D2=$D7 Bohr "    >> $file_Z8_X7
        echo "$Complex distorted "$fname"_Z_8_X_8  D1=$D8 D2=$D8 Bohr "    >> $file_Z8_X8
        echo "$Complex distorted "$fname"_Z_8_Y_1  D1=$D8 D2=$D1 Bohr "    >> $file_Z8_Y1
        echo "$Complex distorted "$fname"_Z_8_Y_2  D1=$D8 D2=$D2 Bohr "    >> $file_Z8_Y2
        echo "$Complex distorted "$fname"_Z_8_Y_3  D1=$D8 D2=$D3 Bohr "    >> $file_Z8_Y3
        echo "$Complex distorted "$fname"_Z_8_Y_4  D1=$D8 D2=$D4 Bohr "    >> $file_Z8_Y4
        echo "$Complex distorted "$fname"_Z_8_Y_5  D1=$D8 D2=$D5 Bohr "    >> $file_Z8_Y5
        echo "$Complex distorted "$fname"_Z_8_Y_6  D1=$D8 D2=$D6 Bohr "    >> $file_Z8_Y6
        echo "$Complex distorted "$fname"_Z_8_Y_7  D1=$D8 D2=$D7 Bohr "    >> $file_Z8_Y7
        echo "$Complex distorted "$fname"_Z_8_Y_8  D1=$D8 D2=$D8 Bohr "    >> $file_Z8_Y8
        echo "$Complex distorted "$fname"_Z_8_Z_1  D1=$D8 D2=$D1 Bohr "    >> $file_Z8_Z1
        echo "$Complex distorted "$fname"_Z_8_Z_2  D1=$D8 D2=$D2 Bohr "    >> $file_Z8_Z2
        echo "$Complex distorted "$fname"_Z_8_Z_3  D1=$D8 D2=$D3 Bohr "    >> $file_Z8_Z3
        echo "$Complex distorted "$fname"_Z_8_Z_4  D1=$D8 D2=$D4 Bohr "    >> $file_Z8_Z4
        echo "$Complex distorted "$fname"_Z_8_Z_5  D1=$D8 D2=$D5 Bohr "    >> $file_Z8_Z5
        echo "$Complex distorted "$fname"_Z_8_Z_6  D1=$D8 D2=$D6 Bohr "    >> $file_Z8_Z6
        echo "$Complex distorted "$fname"_Z_8_Z_7  D1=$D8 D2=$D7 Bohr "    >> $file_Z8_Z7
        echo "$Complex distorted "$fname"_Z_8_Z_8  D1=$D8 D2=$D8 Bohr "    >> $file_Z8_Z8
    else
        echo This npoints is not supported:  npoints = $npoints
        exit 9
    fi




    natoms=${#LABEL[@]}
    echo generate_distorted_xyz_hessian::         Complex= $Complex
    echo generate_distorted_xyz_hessian::             LABEL= ${LABEL[@]}
    echo generate_distorted_xyz_hessian::               X= ${X[@]}
    echo generate_distorted_xyz_hessian::               Y= ${Y[@]}
    echo generate_distorted_xyz_hessian::               Z= ${Z[@]}
    echo generate_distorted_xyz_hessian:: atom1_to_distort= $atom1_to_distort
    echo generate_distorted_xyz_hessian:: index_atom1_to_distort= $index_atom1_to_distort
    echo generate_distorted_xyz_hessian:: atom2_to_distort= $atom2_to_distort
    echo generate_distorted_xyz_hessian:: index_atom2_to_distort= $index_atom2_to_distort
    echo generate_distorted_xyz_hessian::         npoints= $npoints
    echo generate_distorted_xyz_hessian::         natoms = $natoms

    ind=0
    for i in ${!LABEL[@]} ; do
       if [[ $index_atom1_to_distort -eq $ind ]] || [[ $index_atom2_to_distort -eq $ind ]] ; then
          # this section concerns only the atom to be distorted

          if [[ $npoints -eq 2 ]] ; then

              if [[ $index_atom1_to_distort -eq $index_atom2_to_distort ]] ; then
                  # calculate
                  X1=$(echo " ${X[$i]} - $iDIST" | bc -l )
                  X2=$(echo " ${X[$i]} + $iDIST" | bc -l )
                  Y1=$(echo " ${Y[$i]} - $iDIST" | bc -l )
                  Y2=$(echo " ${Y[$i]} + $iDIST" | bc -l )
                  Z1=$(echo " ${Z[$i]} - $iDIST" | bc -l )
                  Z2=$(echo " ${Z[$i]} + $iDIST" | bc -l )

                  X1_X1=$(echo " ${X[$i]} - $iDIST - $iDIST" | bc -l )
                  X1_X2=$(echo " ${X[$i]} - $iDIST + $iDIST" | bc -l )
                  X2_X1=$(echo " ${X[$i]} + $iDIST - $iDIST" | bc -l )
                  X2_X2=$(echo " ${X[$i]} + $iDIST + $iDIST" | bc -l )
                  Y1_Y1=$(echo " ${Y[$i]} - $iDIST - $iDIST" | bc -l )
                  Y1_Y2=$(echo " ${Y[$i]} - $iDIST + $iDIST" | bc -l )
                  Y2_Y1=$(echo " ${Y[$i]} + $iDIST - $iDIST" | bc -l )
                  Y2_Y2=$(echo " ${Y[$i]} + $iDIST + $iDIST" | bc -l )
                  Z1_Z1=$(echo " ${Z[$i]} - $iDIST - $iDIST" | bc -l )
                  Z1_Z2=$(echo " ${Z[$i]} - $iDIST + $iDIST" | bc -l )
                  Z2_Z1=$(echo " ${Z[$i]} + $iDIST - $iDIST" | bc -l )
                  Z2_Z2=$(echo " ${Z[$i]} + $iDIST + $iDIST" | bc -l )

                  # write              X          Y          Z           file
                  echo  ${LABEL[$i]}  $X1_X1    ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                  echo  ${LABEL[$i]}  $X1_X2    ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                  echo  ${LABEL[$i]}  $X1       $Y1       ${Z[$i]} >> $file_X1_Y1
                  echo  ${LABEL[$i]}  $X1       $Y2       ${Z[$i]} >> $file_X1_Y2
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z1      >> $file_X1_Z1
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z2      >> $file_X1_Z2
                  echo  ${LABEL[$i]}  $X2_X1    ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                  echo  ${LABEL[$i]}  $X2_X2    ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                  echo  ${LABEL[$i]}  $X2       $Y1       ${Z[$i]} >> $file_X2_Y1
                  echo  ${LABEL[$i]}  $X2       $Y2       ${Z[$i]} >> $file_X2_Y2
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z1      >> $file_X2_Z1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z2      >> $file_X2_Z2
                  echo  ${LABEL[$i]}  $X1       $Y1       ${Z[$i]} >> $file_Y1_X1
                  echo  ${LABEL[$i]}  $X2       $Y1       ${Z[$i]} >> $file_Y1_X2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y1    ${Z[$i]} >> $file_Y1_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y2    ${Z[$i]} >> $file_Y1_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z1      >> $file_Y1_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z2      >> $file_Y1_Z2
                  echo  ${LABEL[$i]}  $X1       $Y2       ${Z[$i]} >> $file_Y2_X1
                  echo  ${LABEL[$i]}  $X2       $Y2       ${Z[$i]} >> $file_Y2_X2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y1    ${Z[$i]} >> $file_Y2_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y2    ${Z[$i]} >> $file_Y2_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z1      >> $file_Y2_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z2      >> $file_Y2_Z2
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z1      >> $file_Z1_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z1      >> $file_Z1_X2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z1      >> $file_Z1_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z1      >> $file_Z1_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z1   >> $file_Z1_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z2   >> $file_Z1_Z2
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z2      >> $file_Z2_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z2      >> $file_Z2_X2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z2      >> $file_Z2_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z2      >> $file_Z2_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z1   >> $file_Z2_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z2   >> $file_Z2_Z2
              else
                  if [[ $index_atom1_to_distort -eq $ind ]]; then
                      X1=$(echo " ${X[$i]} - $iDIST" | bc -l )
                      X2=$(echo " ${X[$i]} + $iDIST" | bc -l )
                      Y1=$(echo " ${Y[$i]} - $iDIST" | bc -l )
                      Y2=$(echo " ${Y[$i]} + $iDIST" | bc -l )
                      Z1=$(echo " ${Z[$i]} - $iDIST" | bc -l )
                      Z2=$(echo " ${Z[$i]} + $iDIST" | bc -l )
                      # write              X          Y          Z           file
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z2
                  fi
                  if [[ $index_atom2_to_distort -eq $ind ]]; then
                      X1=$(echo " ${X[$i]} - $iDIST" | bc -l )
                      X2=$(echo " ${X[$i]} + $iDIST" | bc -l )
                      Y1=$(echo " ${Y[$i]} - $iDIST" | bc -l )
                      Y2=$(echo " ${Y[$i]} + $iDIST" | bc -l )
                      Z1=$(echo " ${Z[$i]} - $iDIST" | bc -l )
                      Z2=$(echo " ${Z[$i]} + $iDIST" | bc -l )
                      # write              X          Y          Z           file
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X1_Z2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X2_Z2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y1_Z2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y2_Z2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z1_Z2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z2
                  fi
              fi


          elif [[ $npoints -eq 4 ]] ; then

              if [[ $index_atom1_to_distort -eq $index_atom2_to_distort ]] ; then
                  # calculate
                  X1=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
                  X2=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
                  X3=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
                  X4=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
                  Y1=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
                  Y2=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
                  Y3=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
                  Y4=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
                  Z1=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
                  Z2=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
                  Z3=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
                  Z4=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )

                  X1_X1=$(echo " ${X[$i]} - 2 * $iDIST - 2 * $iDIST" | bc -l )
                  X1_X2=$(echo " ${X[$i]} - 2 * $iDIST - 1 * $iDIST" | bc -l )
                  X1_X3=$(echo " ${X[$i]} - 2 * $iDIST + 1 * $iDIST" | bc -l )
                  X1_X4=$(echo " ${X[$i]} - 2 * $iDIST + 2 * $iDIST" | bc -l )
                  X2_X1=$(echo " ${X[$i]} - 1 * $iDIST - 2 * $iDIST" | bc -l )
                  X2_X2=$(echo " ${X[$i]} - 1 * $iDIST - 1 * $iDIST" | bc -l )
                  X2_X3=$(echo " ${X[$i]} - 1 * $iDIST + 1 * $iDIST" | bc -l )
                  X2_X4=$(echo " ${X[$i]} - 1 * $iDIST + 2 * $iDIST" | bc -l )
                  X3_X1=$(echo " ${X[$i]} + 1 * $iDIST - 2 * $iDIST" | bc -l )
                  X3_X2=$(echo " ${X[$i]} + 1 * $iDIST - 1 * $iDIST" | bc -l )
                  X3_X3=$(echo " ${X[$i]} + 1 * $iDIST + 1 * $iDIST" | bc -l )
                  X3_X4=$(echo " ${X[$i]} + 1 * $iDIST + 2 * $iDIST" | bc -l )
                  X4_X1=$(echo " ${X[$i]} + 2 * $iDIST - 2 * $iDIST" | bc -l )
                  X4_X2=$(echo " ${X[$i]} + 2 * $iDIST - 1 * $iDIST" | bc -l )
                  X4_X3=$(echo " ${X[$i]} + 2 * $iDIST + 1 * $iDIST" | bc -l )
                  X4_X4=$(echo " ${X[$i]} + 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Y1_Y1=$(echo " ${Y[$i]} - 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Y1_Y2=$(echo " ${Y[$i]} - 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Y1_Y3=$(echo " ${Y[$i]} - 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Y1_Y4=$(echo " ${Y[$i]} - 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Y2_Y1=$(echo " ${Y[$i]} - 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Y2_Y2=$(echo " ${Y[$i]} - 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Y2_Y3=$(echo " ${Y[$i]} - 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Y2_Y4=$(echo " ${Y[$i]} - 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Y3_Y1=$(echo " ${Y[$i]} + 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Y3_Y2=$(echo " ${Y[$i]} + 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Y3_Y3=$(echo " ${Y[$i]} + 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Y3_Y4=$(echo " ${Y[$i]} + 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Y4_Y1=$(echo " ${Y[$i]} + 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Y4_Y2=$(echo " ${Y[$i]} + 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Y4_Y3=$(echo " ${Y[$i]} + 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Y4_Y4=$(echo " ${Y[$i]} + 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Z1_Z1=$(echo " ${Z[$i]} - 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Z1_Z2=$(echo " ${Z[$i]} - 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Z1_Z3=$(echo " ${Z[$i]} - 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Z1_Z4=$(echo " ${Z[$i]} - 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Z2_Z1=$(echo " ${Z[$i]} - 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Z2_Z2=$(echo " ${Z[$i]} - 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Z2_Z3=$(echo " ${Z[$i]} - 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Z2_Z4=$(echo " ${Z[$i]} - 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Z3_Z1=$(echo " ${Z[$i]} + 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Z3_Z2=$(echo " ${Z[$i]} + 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Z3_Z3=$(echo " ${Z[$i]} + 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Z3_Z4=$(echo " ${Z[$i]} + 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Z4_Z1=$(echo " ${Z[$i]} + 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Z4_Z2=$(echo " ${Z[$i]} + 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Z4_Z3=$(echo " ${Z[$i]} + 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Z4_Z4=$(echo " ${Z[$i]} + 2 * $iDIST + 2 * $iDIST" | bc -l )

                  # write              X          Y          Z           file
                  echo  ${LABEL[$i]}  $X1_X1    ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                  echo  ${LABEL[$i]}  $X1_X2    ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                  echo  ${LABEL[$i]}  $X1_X3    ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
                  echo  ${LABEL[$i]}  $X1_X4    ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
                  echo  ${LABEL[$i]}  $X1       $Y1       ${Z[$i]} >> $file_X1_Y1
                  echo  ${LABEL[$i]}  $X1       $Y2       ${Z[$i]} >> $file_X1_Y2
                  echo  ${LABEL[$i]}  $X1       $Y3       ${Z[$i]} >> $file_X1_Y3
                  echo  ${LABEL[$i]}  $X1       $Y4       ${Z[$i]} >> $file_X1_Y4
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z1      >> $file_X1_Z1
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z2      >> $file_X1_Z2
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z3      >> $file_X1_Z3
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z4      >> $file_X1_Z4
                  echo  ${LABEL[$i]}  $X2_X1    ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                  echo  ${LABEL[$i]}  $X2_X2    ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                  echo  ${LABEL[$i]}  $X2_X3    ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
                  echo  ${LABEL[$i]}  $X2_X4    ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
                  echo  ${LABEL[$i]}  $X2       $Y1       ${Z[$i]} >> $file_X2_Y1
                  echo  ${LABEL[$i]}  $X2       $Y2       ${Z[$i]} >> $file_X2_Y2
                  echo  ${LABEL[$i]}  $X2       $Y3       ${Z[$i]} >> $file_X2_Y3
                  echo  ${LABEL[$i]}  $X2       $Y4       ${Z[$i]} >> $file_X2_Y4
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z1      >> $file_X2_Z1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z2      >> $file_X2_Z2
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z3      >> $file_X2_Z3
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z4      >> $file_X2_Z4
                  echo  ${LABEL[$i]}  $X3_X1    ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
                  echo  ${LABEL[$i]}  $X3_X2    ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
                  echo  ${LABEL[$i]}  $X3_X3    ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
                  echo  ${LABEL[$i]}  $X3_X4    ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
                  echo  ${LABEL[$i]}  $X3       $Y1       ${Z[$i]} >> $file_X3_Y1
                  echo  ${LABEL[$i]}  $X3       $Y2       ${Z[$i]} >> $file_X3_Y2
                  echo  ${LABEL[$i]}  $X3       $Y3       ${Z[$i]} >> $file_X3_Y3
                  echo  ${LABEL[$i]}  $X3       $Y4       ${Z[$i]} >> $file_X3_Y4
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z1      >> $file_X3_Z1
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z2      >> $file_X3_Z2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z3      >> $file_X3_Z3
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z4      >> $file_X3_Z4
                  echo  ${LABEL[$i]}  $X4_X1    ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
                  echo  ${LABEL[$i]}  $X4_X2    ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
                  echo  ${LABEL[$i]}  $X4_X3    ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
                  echo  ${LABEL[$i]}  $X4_X4    ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
                  echo  ${LABEL[$i]}  $X4       $Y1       ${Z[$i]} >> $file_X4_Y1
                  echo  ${LABEL[$i]}  $X4       $Y2       ${Z[$i]} >> $file_X4_Y2
                  echo  ${LABEL[$i]}  $X4       $Y3       ${Z[$i]} >> $file_X4_Y3
                  echo  ${LABEL[$i]}  $X4       $Y4       ${Z[$i]} >> $file_X4_Y4
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z1      >> $file_X4_Z1
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z2      >> $file_X4_Z2
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z3      >> $file_X4_Z3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z4      >> $file_X4_Z4
                  echo  ${LABEL[$i]}  $X1       $Y1       ${Z[$i]} >> $file_Y1_X1
                  echo  ${LABEL[$i]}  $X2       $Y1       ${Z[$i]} >> $file_Y1_X2
                  echo  ${LABEL[$i]}  $X3       $Y1       ${Z[$i]} >> $file_Y1_X3
                  echo  ${LABEL[$i]}  $X4       $Y1       ${Z[$i]} >> $file_Y1_X4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y1    ${Z[$i]} >> $file_Y1_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y2    ${Z[$i]} >> $file_Y1_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y3    ${Z[$i]} >> $file_Y1_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y4    ${Z[$i]} >> $file_Y1_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z1      >> $file_Y1_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z2      >> $file_Y1_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z3      >> $file_Y1_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z4      >> $file_Y1_Z4
                  echo  ${LABEL[$i]}  $X1       $Y2       ${Z[$i]} >> $file_Y2_X1
                  echo  ${LABEL[$i]}  $X2       $Y2       ${Z[$i]} >> $file_Y2_X2
                  echo  ${LABEL[$i]}  $X3       $Y2       ${Z[$i]} >> $file_Y2_X3
                  echo  ${LABEL[$i]}  $X4       $Y2       ${Z[$i]} >> $file_Y2_X4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y1    ${Z[$i]} >> $file_Y2_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y2    ${Z[$i]} >> $file_Y2_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y3    ${Z[$i]} >> $file_Y2_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y4    ${Z[$i]} >> $file_Y2_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z1      >> $file_Y2_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z2      >> $file_Y2_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z3      >> $file_Y2_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z4      >> $file_Y2_Z4
                  echo  ${LABEL[$i]}  $X1       $Y3       ${Z[$i]} >> $file_Y3_X1
                  echo  ${LABEL[$i]}  $X2       $Y3       ${Z[$i]} >> $file_Y3_X2
                  echo  ${LABEL[$i]}  $X3       $Y3       ${Z[$i]} >> $file_Y3_X3
                  echo  ${LABEL[$i]}  $X4       $Y3       ${Z[$i]} >> $file_Y3_X4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y1    ${Z[$i]} >> $file_Y3_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y2    ${Z[$i]} >> $file_Y3_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y3    ${Z[$i]} >> $file_Y3_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y4    ${Z[$i]} >> $file_Y3_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z1      >> $file_Y3_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z2      >> $file_Y3_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z3      >> $file_Y3_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z4      >> $file_Y3_Z4
                  echo  ${LABEL[$i]}  $X1       $Y4       ${Z[$i]} >> $file_Y4_X1
                  echo  ${LABEL[$i]}  $X2       $Y4       ${Z[$i]} >> $file_Y4_X2
                  echo  ${LABEL[$i]}  $X3       $Y4       ${Z[$i]} >> $file_Y4_X3
                  echo  ${LABEL[$i]}  $X4       $Y4       ${Z[$i]} >> $file_Y4_X4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y1    ${Z[$i]} >> $file_Y4_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y2    ${Z[$i]} >> $file_Y4_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y3    ${Z[$i]} >> $file_Y4_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y4    ${Z[$i]} >> $file_Y4_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z1      >> $file_Y4_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z2      >> $file_Y4_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z3      >> $file_Y4_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z4      >> $file_Y4_Z4
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z1      >> $file_Z1_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z1      >> $file_Z1_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z1      >> $file_Z1_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z1      >> $file_Z1_X4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z1      >> $file_Z1_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z1      >> $file_Z1_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z1      >> $file_Z1_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z1      >> $file_Z1_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z1   >> $file_Z1_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z2   >> $file_Z1_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z3   >> $file_Z1_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z4   >> $file_Z1_Z4
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z2      >> $file_Z2_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z2      >> $file_Z2_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z2      >> $file_Z2_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z2      >> $file_Z2_X4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z2      >> $file_Z2_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z2      >> $file_Z2_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z2      >> $file_Z2_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z2      >> $file_Z2_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z1   >> $file_Z2_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z2   >> $file_Z2_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z3   >> $file_Z2_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z4   >> $file_Z2_Z4
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z3      >> $file_Z3_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z3      >> $file_Z3_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z3      >> $file_Z3_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z3      >> $file_Z3_X4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z3      >> $file_Z3_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z3      >> $file_Z3_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z3      >> $file_Z3_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z3      >> $file_Z3_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z1   >> $file_Z3_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z2   >> $file_Z3_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z3   >> $file_Z3_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z4   >> $file_Z3_Z4
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z4      >> $file_Z4_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z4      >> $file_Z4_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z4      >> $file_Z4_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z4      >> $file_Z4_X4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z4      >> $file_Z4_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z4      >> $file_Z4_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z4      >> $file_Z4_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z4      >> $file_Z4_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z1   >> $file_Z4_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z2   >> $file_Z4_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z3   >> $file_Z4_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z4   >> $file_Z4_Z4

              else
                  if [[ $index_atom1_to_distort -eq $ind ]]; then
                      # calculate
                      X1=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
                      X2=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
                      X3=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
                      X4=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
                      Y1=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
                      Y2=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
                      Y3=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
                      Y4=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
                      Z1=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
                      Z2=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
                      Z3=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
                      Z4=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
                      # write              X          Y          Z           file
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y3
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z3
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z4
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y3
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y4
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z3
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z4
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y1
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y3
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y4
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z1
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z3
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z4
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y1
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y2
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y4
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z1
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z2
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z4

                  fi
                  if [[ $index_atom2_to_distort -eq $ind ]]; then
                      # calculate
                      X1=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
                      X2=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
                      X3=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
                      X4=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
                      Y1=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
                      Y2=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
                      Y3=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
                      Y4=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
                      Z1=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
                      Z2=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
                      Z3=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
                      Z4=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
                      # write              X          Y          Z           file
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X1_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X2_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X3_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X4_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y1_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y2_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y3_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y4_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z1_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z2_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z3_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z4
                  fi
              fi

          elif [[ $npoints -eq 6 ]] ; then

              if [[ $index_atom1_to_distort -eq $index_atom2_to_distort ]] ; then
                  # calculate
                  X1=$(echo " ${X[$i]} - 3 * $iDIST" | bc -l )
                  X2=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
                  X3=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
                  X4=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
                  X5=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
                  X6=$(echo " ${X[$i]} + 3 * $iDIST" | bc -l )
                  Y1=$(echo " ${Y[$i]} - 3 * $iDIST" | bc -l )
                  Y2=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
                  Y3=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
                  Y4=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
                  Y5=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
                  Y6=$(echo " ${Y[$i]} + 3 * $iDIST" | bc -l )
                  Z1=$(echo " ${Z[$i]} - 3 * $iDIST" | bc -l )
                  Z2=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
                  Z3=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
                  Z4=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
                  Z5=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
                  Z6=$(echo " ${Z[$i]} + 3 * $iDIST" | bc -l )

                  X1_X1=$(echo " ${X[$i]} - 3 * $iDIST - 3 * $iDIST" | bc -l )
                  X1_X2=$(echo " ${X[$i]} - 3 * $iDIST - 2 * $iDIST" | bc -l )
                  X1_X3=$(echo " ${X[$i]} - 3 * $iDIST - 1 * $iDIST" | bc -l )
                  X1_X4=$(echo " ${X[$i]} - 3 * $iDIST + 1 * $iDIST" | bc -l )
                  X1_X5=$(echo " ${X[$i]} - 3 * $iDIST + 2 * $iDIST" | bc -l )
                  X1_X6=$(echo " ${X[$i]} - 3 * $iDIST + 3 * $iDIST" | bc -l )
                  X2_X1=$(echo " ${X[$i]} - 2 * $iDIST - 3 * $iDIST" | bc -l )
                  X2_X2=$(echo " ${X[$i]} - 2 * $iDIST - 2 * $iDIST" | bc -l )
                  X2_X3=$(echo " ${X[$i]} - 2 * $iDIST - 1 * $iDIST" | bc -l )
                  X2_X4=$(echo " ${X[$i]} - 2 * $iDIST + 1 * $iDIST" | bc -l )
                  X2_X5=$(echo " ${X[$i]} - 2 * $iDIST + 2 * $iDIST" | bc -l )
                  X2_X6=$(echo " ${X[$i]} - 2 * $iDIST + 3 * $iDIST" | bc -l )
                  X3_X1=$(echo " ${X[$i]} - 1 * $iDIST - 3 * $iDIST" | bc -l )
                  X3_X2=$(echo " ${X[$i]} - 1 * $iDIST - 2 * $iDIST" | bc -l )
                  X3_X3=$(echo " ${X[$i]} - 1 * $iDIST - 1 * $iDIST" | bc -l )
                  X3_X4=$(echo " ${X[$i]} - 1 * $iDIST + 1 * $iDIST" | bc -l )
                  X3_X5=$(echo " ${X[$i]} - 1 * $iDIST + 2 * $iDIST" | bc -l )
                  X3_X6=$(echo " ${X[$i]} - 1 * $iDIST + 3 * $iDIST" | bc -l )
                  X4_X1=$(echo " ${X[$i]} + 1 * $iDIST - 3 * $iDIST" | bc -l )
                  X4_X2=$(echo " ${X[$i]} + 1 * $iDIST - 2 * $iDIST" | bc -l )
                  X4_X3=$(echo " ${X[$i]} + 1 * $iDIST - 1 * $iDIST" | bc -l )
                  X4_X4=$(echo " ${X[$i]} + 1 * $iDIST + 1 * $iDIST" | bc -l )
                  X4_X5=$(echo " ${X[$i]} + 1 * $iDIST + 2 * $iDIST" | bc -l )
                  X4_X6=$(echo " ${X[$i]} + 1 * $iDIST + 3 * $iDIST" | bc -l )
                  X5_X1=$(echo " ${X[$i]} + 2 * $iDIST - 3 * $iDIST" | bc -l )
                  X5_X2=$(echo " ${X[$i]} + 2 * $iDIST - 2 * $iDIST" | bc -l )
                  X5_X3=$(echo " ${X[$i]} + 2 * $iDIST - 1 * $iDIST" | bc -l )
                  X5_X4=$(echo " ${X[$i]} + 2 * $iDIST + 1 * $iDIST" | bc -l )
                  X5_X5=$(echo " ${X[$i]} + 2 * $iDIST + 2 * $iDIST" | bc -l )
                  X5_X6=$(echo " ${X[$i]} + 2 * $iDIST + 3 * $iDIST" | bc -l )
                  X6_X1=$(echo " ${X[$i]} + 3 * $iDIST - 3 * $iDIST" | bc -l )
                  X6_X2=$(echo " ${X[$i]} + 3 * $iDIST - 2 * $iDIST" | bc -l )
                  X6_X3=$(echo " ${X[$i]} + 3 * $iDIST - 1 * $iDIST" | bc -l )
                  X6_X4=$(echo " ${X[$i]} + 3 * $iDIST + 1 * $iDIST" | bc -l )
                  X6_X5=$(echo " ${X[$i]} + 3 * $iDIST + 2 * $iDIST" | bc -l )
                  X6_X6=$(echo " ${X[$i]} + 3 * $iDIST + 3 * $iDIST" | bc -l )
                  Y1_Y1=$(echo " ${Y[$i]} - 3 * $iDIST - 3 * $iDIST" | bc -l )
                  Y1_Y2=$(echo " ${Y[$i]} - 3 * $iDIST - 2 * $iDIST" | bc -l )
                  Y1_Y3=$(echo " ${Y[$i]} - 3 * $iDIST - 1 * $iDIST" | bc -l )
                  Y1_Y4=$(echo " ${Y[$i]} - 3 * $iDIST + 1 * $iDIST" | bc -l )
                  Y1_Y5=$(echo " ${Y[$i]} - 3 * $iDIST + 2 * $iDIST" | bc -l )
                  Y1_Y6=$(echo " ${Y[$i]} - 3 * $iDIST + 3 * $iDIST" | bc -l )
                  Y2_Y1=$(echo " ${Y[$i]} - 2 * $iDIST - 3 * $iDIST" | bc -l )
                  Y2_Y2=$(echo " ${Y[$i]} - 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Y2_Y3=$(echo " ${Y[$i]} - 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Y2_Y4=$(echo " ${Y[$i]} - 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Y2_Y5=$(echo " ${Y[$i]} - 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Y2_Y6=$(echo " ${Y[$i]} - 2 * $iDIST + 3 * $iDIST" | bc -l )
                  Y3_Y1=$(echo " ${Y[$i]} - 1 * $iDIST - 3 * $iDIST" | bc -l )
                  Y3_Y2=$(echo " ${Y[$i]} - 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Y3_Y3=$(echo " ${Y[$i]} - 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Y3_Y4=$(echo " ${Y[$i]} - 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Y3_Y5=$(echo " ${Y[$i]} - 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Y3_Y6=$(echo " ${Y[$i]} - 1 * $iDIST + 3 * $iDIST" | bc -l )
                  Y4_Y1=$(echo " ${Y[$i]} + 1 * $iDIST - 3 * $iDIST" | bc -l )
                  Y4_Y2=$(echo " ${Y[$i]} + 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Y4_Y3=$(echo " ${Y[$i]} + 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Y4_Y4=$(echo " ${Y[$i]} + 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Y4_Y5=$(echo " ${Y[$i]} + 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Y4_Y6=$(echo " ${Y[$i]} + 1 * $iDIST + 3 * $iDIST" | bc -l )
                  Y5_Y1=$(echo " ${Y[$i]} + 2 * $iDIST - 3 * $iDIST" | bc -l )
                  Y5_Y2=$(echo " ${Y[$i]} + 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Y5_Y3=$(echo " ${Y[$i]} + 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Y5_Y4=$(echo " ${Y[$i]} + 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Y5_Y5=$(echo " ${Y[$i]} + 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Y5_Y6=$(echo " ${Y[$i]} + 2 * $iDIST + 3 * $iDIST" | bc -l )
                  Y6_Y1=$(echo " ${Y[$i]} + 3 * $iDIST - 3 * $iDIST" | bc -l )
                  Y6_Y2=$(echo " ${Y[$i]} + 3 * $iDIST - 2 * $iDIST" | bc -l )
                  Y6_Y3=$(echo " ${Y[$i]} + 3 * $iDIST - 1 * $iDIST" | bc -l )
                  Y6_Y4=$(echo " ${Y[$i]} + 3 * $iDIST + 1 * $iDIST" | bc -l )
                  Y6_Y5=$(echo " ${Y[$i]} + 3 * $iDIST + 2 * $iDIST" | bc -l )
                  Y6_Y6=$(echo " ${Y[$i]} + 3 * $iDIST + 3 * $iDIST" | bc -l )
                  Z1_Z1=$(echo " ${Z[$i]} - 3 * $iDIST - 3 * $iDIST" | bc -l )
                  Z1_Z2=$(echo " ${Z[$i]} - 3 * $iDIST - 2 * $iDIST" | bc -l )
                  Z1_Z3=$(echo " ${Z[$i]} - 3 * $iDIST - 1 * $iDIST" | bc -l )
                  Z1_Z4=$(echo " ${Z[$i]} - 3 * $iDIST + 1 * $iDIST" | bc -l )
                  Z1_Z5=$(echo " ${Z[$i]} - 3 * $iDIST + 2 * $iDIST" | bc -l )
                  Z1_Z6=$(echo " ${Z[$i]} - 3 * $iDIST + 3 * $iDIST" | bc -l )
                  Z2_Z1=$(echo " ${Z[$i]} - 2 * $iDIST - 3 * $iDIST" | bc -l )
                  Z2_Z2=$(echo " ${Z[$i]} - 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Z2_Z3=$(echo " ${Z[$i]} - 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Z2_Z4=$(echo " ${Z[$i]} - 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Z2_Z5=$(echo " ${Z[$i]} - 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Z2_Z6=$(echo " ${Z[$i]} - 2 * $iDIST + 3 * $iDIST" | bc -l )
                  Z3_Z1=$(echo " ${Z[$i]} - 1 * $iDIST - 3 * $iDIST" | bc -l )
                  Z3_Z2=$(echo " ${Z[$i]} - 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Z3_Z3=$(echo " ${Z[$i]} - 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Z3_Z4=$(echo " ${Z[$i]} - 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Z3_Z5=$(echo " ${Z[$i]} - 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Z3_Z6=$(echo " ${Z[$i]} - 1 * $iDIST + 3 * $iDIST" | bc -l )
                  Z4_Z1=$(echo " ${Z[$i]} + 1 * $iDIST - 3 * $iDIST" | bc -l )
                  Z4_Z2=$(echo " ${Z[$i]} + 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Z4_Z3=$(echo " ${Z[$i]} + 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Z4_Z4=$(echo " ${Z[$i]} + 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Z4_Z5=$(echo " ${Z[$i]} + 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Z4_Z6=$(echo " ${Z[$i]} + 1 * $iDIST + 3 * $iDIST" | bc -l )
                  Z5_Z1=$(echo " ${Z[$i]} + 2 * $iDIST - 3 * $iDIST" | bc -l )
                  Z5_Z2=$(echo " ${Z[$i]} + 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Z5_Z3=$(echo " ${Z[$i]} + 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Z5_Z4=$(echo " ${Z[$i]} + 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Z5_Z5=$(echo " ${Z[$i]} + 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Z5_Z6=$(echo " ${Z[$i]} + 2 * $iDIST + 3 * $iDIST" | bc -l )
                  Z6_Z1=$(echo " ${Z[$i]} + 3 * $iDIST - 3 * $iDIST" | bc -l )
                  Z6_Z2=$(echo " ${Z[$i]} + 3 * $iDIST - 2 * $iDIST" | bc -l )
                  Z6_Z3=$(echo " ${Z[$i]} + 3 * $iDIST - 1 * $iDIST" | bc -l )
                  Z6_Z4=$(echo " ${Z[$i]} + 3 * $iDIST + 1 * $iDIST" | bc -l )
                  Z6_Z5=$(echo " ${Z[$i]} + 3 * $iDIST + 2 * $iDIST" | bc -l )
                  Z6_Z6=$(echo " ${Z[$i]} + 3 * $iDIST + 3 * $iDIST" | bc -l )

                  # write              X          Y          Z           file
                  echo  ${LABEL[$i]}  $X1_X1    ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                  echo  ${LABEL[$i]}  $X1_X2    ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                  echo  ${LABEL[$i]}  $X1_X3    ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
                  echo  ${LABEL[$i]}  $X1_X4    ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
                  echo  ${LABEL[$i]}  $X1_X5    ${Y[$i]}  ${Z[$i]} >> $file_X1_X5
                  echo  ${LABEL[$i]}  $X1_X6    ${Y[$i]}  ${Z[$i]} >> $file_X1_X6
                  echo  ${LABEL[$i]}  $X1       $Y1       ${Z[$i]} >> $file_X1_Y1
                  echo  ${LABEL[$i]}  $X1       $Y2       ${Z[$i]} >> $file_X1_Y2
                  echo  ${LABEL[$i]}  $X1       $Y3       ${Z[$i]} >> $file_X1_Y3
                  echo  ${LABEL[$i]}  $X1       $Y4       ${Z[$i]} >> $file_X1_Y4
                  echo  ${LABEL[$i]}  $X1       $Y5       ${Z[$i]} >> $file_X1_Y5
                  echo  ${LABEL[$i]}  $X1       $Y6       ${Z[$i]} >> $file_X1_Y6
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z1      >> $file_X1_Z1
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z2      >> $file_X1_Z2
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z3      >> $file_X1_Z3
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z4      >> $file_X1_Z4
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z5      >> $file_X1_Z5
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z6      >> $file_X1_Z6
                  echo  ${LABEL[$i]}  $X2_X1    ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                  echo  ${LABEL[$i]}  $X2_X2    ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                  echo  ${LABEL[$i]}  $X2_X3    ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
                  echo  ${LABEL[$i]}  $X2_X4    ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
                  echo  ${LABEL[$i]}  $X2_X5    ${Y[$i]}  ${Z[$i]} >> $file_X2_X5
                  echo  ${LABEL[$i]}  $X2_X6    ${Y[$i]}  ${Z[$i]} >> $file_X2_X6
                  echo  ${LABEL[$i]}  $X2       $Y1       ${Z[$i]} >> $file_X2_Y1
                  echo  ${LABEL[$i]}  $X2       $Y2       ${Z[$i]} >> $file_X2_Y2
                  echo  ${LABEL[$i]}  $X2       $Y3       ${Z[$i]} >> $file_X2_Y3
                  echo  ${LABEL[$i]}  $X2       $Y4       ${Z[$i]} >> $file_X2_Y4
                  echo  ${LABEL[$i]}  $X2       $Y5       ${Z[$i]} >> $file_X2_Y5
                  echo  ${LABEL[$i]}  $X2       $Y6       ${Z[$i]} >> $file_X2_Y6
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z1      >> $file_X2_Z1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z2      >> $file_X2_Z2
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z3      >> $file_X2_Z3
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z4      >> $file_X2_Z4
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z5      >> $file_X2_Z5
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z6      >> $file_X2_Z6
                  echo  ${LABEL[$i]}  $X3_X1    ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
                  echo  ${LABEL[$i]}  $X3_X2    ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
                  echo  ${LABEL[$i]}  $X3_X3    ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
                  echo  ${LABEL[$i]}  $X3_X4    ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
                  echo  ${LABEL[$i]}  $X3_X5    ${Y[$i]}  ${Z[$i]} >> $file_X3_X5
                  echo  ${LABEL[$i]}  $X3_X6    ${Y[$i]}  ${Z[$i]} >> $file_X3_X6
                  echo  ${LABEL[$i]}  $X3       $Y1       ${Z[$i]} >> $file_X3_Y1
                  echo  ${LABEL[$i]}  $X3       $Y2       ${Z[$i]} >> $file_X3_Y2
                  echo  ${LABEL[$i]}  $X3       $Y3       ${Z[$i]} >> $file_X3_Y3
                  echo  ${LABEL[$i]}  $X3       $Y4       ${Z[$i]} >> $file_X3_Y4
                  echo  ${LABEL[$i]}  $X3       $Y5       ${Z[$i]} >> $file_X3_Y5
                  echo  ${LABEL[$i]}  $X3       $Y6       ${Z[$i]} >> $file_X3_Y6
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z1      >> $file_X3_Z1
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z2      >> $file_X3_Z2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z3      >> $file_X3_Z3
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z4      >> $file_X3_Z4
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z5      >> $file_X3_Z5
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z6      >> $file_X3_Z6
                  echo  ${LABEL[$i]}  $X4_X1    ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
                  echo  ${LABEL[$i]}  $X4_X2    ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
                  echo  ${LABEL[$i]}  $X4_X3    ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
                  echo  ${LABEL[$i]}  $X4_X4    ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
                  echo  ${LABEL[$i]}  $X4_X5    ${Y[$i]}  ${Z[$i]} >> $file_X4_X5
                  echo  ${LABEL[$i]}  $X4_X6    ${Y[$i]}  ${Z[$i]} >> $file_X4_X6
                  echo  ${LABEL[$i]}  $X4       $Y1       ${Z[$i]} >> $file_X4_Y1
                  echo  ${LABEL[$i]}  $X4       $Y2       ${Z[$i]} >> $file_X4_Y2
                  echo  ${LABEL[$i]}  $X4       $Y3       ${Z[$i]} >> $file_X4_Y3
                  echo  ${LABEL[$i]}  $X4       $Y4       ${Z[$i]} >> $file_X4_Y4
                  echo  ${LABEL[$i]}  $X4       $Y5       ${Z[$i]} >> $file_X4_Y5
                  echo  ${LABEL[$i]}  $X4       $Y6       ${Z[$i]} >> $file_X4_Y6
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z1      >> $file_X4_Z1
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z2      >> $file_X4_Z2
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z3      >> $file_X4_Z3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z4      >> $file_X4_Z4
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z5      >> $file_X4_Z5
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z6      >> $file_X4_Z6
                  echo  ${LABEL[$i]}  $X5_X1    ${Y[$i]}  ${Z[$i]} >> $file_X5_X1
                  echo  ${LABEL[$i]}  $X5_X2    ${Y[$i]}  ${Z[$i]} >> $file_X5_X2
                  echo  ${LABEL[$i]}  $X5_X3    ${Y[$i]}  ${Z[$i]} >> $file_X5_X3
                  echo  ${LABEL[$i]}  $X5_X4    ${Y[$i]}  ${Z[$i]} >> $file_X5_X4
                  echo  ${LABEL[$i]}  $X5_X5    ${Y[$i]}  ${Z[$i]} >> $file_X5_X5
                  echo  ${LABEL[$i]}  $X5_X6    ${Y[$i]}  ${Z[$i]} >> $file_X5_X6
                  echo  ${LABEL[$i]}  $X5       $Y1       ${Z[$i]} >> $file_X5_Y1
                  echo  ${LABEL[$i]}  $X5       $Y2       ${Z[$i]} >> $file_X5_Y2
                  echo  ${LABEL[$i]}  $X5       $Y3       ${Z[$i]} >> $file_X5_Y3
                  echo  ${LABEL[$i]}  $X5       $Y4       ${Z[$i]} >> $file_X5_Y4
                  echo  ${LABEL[$i]}  $X5       $Y5       ${Z[$i]} >> $file_X5_Y5
                  echo  ${LABEL[$i]}  $X5       $Y6       ${Z[$i]} >> $file_X5_Y6
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z1      >> $file_X5_Z1
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z2      >> $file_X5_Z2
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z3      >> $file_X5_Z3
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z4      >> $file_X5_Z4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z5      >> $file_X5_Z5
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z6      >> $file_X5_Z6
                  echo  ${LABEL[$i]}  $X6_X1    ${Y[$i]}  ${Z[$i]} >> $file_X6_X1
                  echo  ${LABEL[$i]}  $X6_X2    ${Y[$i]}  ${Z[$i]} >> $file_X6_X2
                  echo  ${LABEL[$i]}  $X6_X3    ${Y[$i]}  ${Z[$i]} >> $file_X6_X3
                  echo  ${LABEL[$i]}  $X6_X4    ${Y[$i]}  ${Z[$i]} >> $file_X6_X4
                  echo  ${LABEL[$i]}  $X6_X5    ${Y[$i]}  ${Z[$i]} >> $file_X6_X5
                  echo  ${LABEL[$i]}  $X6_X6    ${Y[$i]}  ${Z[$i]} >> $file_X6_X6
                  echo  ${LABEL[$i]}  $X6       $Y1       ${Z[$i]} >> $file_X6_Y1
                  echo  ${LABEL[$i]}  $X6       $Y2       ${Z[$i]} >> $file_X6_Y2
                  echo  ${LABEL[$i]}  $X6       $Y3       ${Z[$i]} >> $file_X6_Y3
                  echo  ${LABEL[$i]}  $X6       $Y4       ${Z[$i]} >> $file_X6_Y4
                  echo  ${LABEL[$i]}  $X6       $Y5       ${Z[$i]} >> $file_X6_Y5
                  echo  ${LABEL[$i]}  $X6       $Y6       ${Z[$i]} >> $file_X6_Y6
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z1      >> $file_X6_Z1
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z2      >> $file_X6_Z2
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z3      >> $file_X6_Z3
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z4      >> $file_X6_Z4
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z5      >> $file_X6_Z5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z6      >> $file_X6_Z6
                  echo  ${LABEL[$i]}  $X1       $Y1       ${Z[$i]} >> $file_Y1_X1
                  echo  ${LABEL[$i]}  $X2       $Y1       ${Z[$i]} >> $file_Y1_X2
                  echo  ${LABEL[$i]}  $X3       $Y1       ${Z[$i]} >> $file_Y1_X3
                  echo  ${LABEL[$i]}  $X4       $Y1       ${Z[$i]} >> $file_Y1_X4
                  echo  ${LABEL[$i]}  $X5       $Y1       ${Z[$i]} >> $file_Y1_X5
                  echo  ${LABEL[$i]}  $X6       $Y1       ${Z[$i]} >> $file_Y1_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y1    ${Z[$i]} >> $file_Y1_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y2    ${Z[$i]} >> $file_Y1_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y3    ${Z[$i]} >> $file_Y1_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y4    ${Z[$i]} >> $file_Y1_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y5    ${Z[$i]} >> $file_Y1_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y6    ${Z[$i]} >> $file_Y1_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z1      >> $file_Y1_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z2      >> $file_Y1_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z3      >> $file_Y1_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z4      >> $file_Y1_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z5      >> $file_Y1_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z6      >> $file_Y1_Z6
                  echo  ${LABEL[$i]}  $X1       $Y2       ${Z[$i]} >> $file_Y2_X1
                  echo  ${LABEL[$i]}  $X2       $Y2       ${Z[$i]} >> $file_Y2_X2
                  echo  ${LABEL[$i]}  $X3       $Y2       ${Z[$i]} >> $file_Y2_X3
                  echo  ${LABEL[$i]}  $X4       $Y2       ${Z[$i]} >> $file_Y2_X4
                  echo  ${LABEL[$i]}  $X5       $Y2       ${Z[$i]} >> $file_Y2_X5
                  echo  ${LABEL[$i]}  $X6       $Y2       ${Z[$i]} >> $file_Y2_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y1    ${Z[$i]} >> $file_Y2_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y2    ${Z[$i]} >> $file_Y2_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y3    ${Z[$i]} >> $file_Y2_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y4    ${Z[$i]} >> $file_Y2_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y5    ${Z[$i]} >> $file_Y2_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y6    ${Z[$i]} >> $file_Y2_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z1      >> $file_Y2_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z2      >> $file_Y2_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z3      >> $file_Y2_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z4      >> $file_Y2_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z5      >> $file_Y2_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z6      >> $file_Y2_Z6
                  echo  ${LABEL[$i]}  $X1       $Y3       ${Z[$i]} >> $file_Y3_X1
                  echo  ${LABEL[$i]}  $X2       $Y3       ${Z[$i]} >> $file_Y3_X2
                  echo  ${LABEL[$i]}  $X3       $Y3       ${Z[$i]} >> $file_Y3_X3
                  echo  ${LABEL[$i]}  $X4       $Y3       ${Z[$i]} >> $file_Y3_X4
                  echo  ${LABEL[$i]}  $X5       $Y3       ${Z[$i]} >> $file_Y3_X5
                  echo  ${LABEL[$i]}  $X6       $Y3       ${Z[$i]} >> $file_Y3_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y1    ${Z[$i]} >> $file_Y3_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y2    ${Z[$i]} >> $file_Y3_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y3    ${Z[$i]} >> $file_Y3_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y4    ${Z[$i]} >> $file_Y3_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y5    ${Z[$i]} >> $file_Y3_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y6    ${Z[$i]} >> $file_Y3_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z1      >> $file_Y3_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z2      >> $file_Y3_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z3      >> $file_Y3_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z4      >> $file_Y3_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z5      >> $file_Y3_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z6      >> $file_Y3_Z6
                  echo  ${LABEL[$i]}  $X1       $Y4       ${Z[$i]} >> $file_Y4_X1
                  echo  ${LABEL[$i]}  $X2       $Y4       ${Z[$i]} >> $file_Y4_X2
                  echo  ${LABEL[$i]}  $X3       $Y4       ${Z[$i]} >> $file_Y4_X3
                  echo  ${LABEL[$i]}  $X4       $Y4       ${Z[$i]} >> $file_Y4_X4
                  echo  ${LABEL[$i]}  $X5       $Y4       ${Z[$i]} >> $file_Y4_X5
                  echo  ${LABEL[$i]}  $X6       $Y4       ${Z[$i]} >> $file_Y4_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y1    ${Z[$i]} >> $file_Y4_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y2    ${Z[$i]} >> $file_Y4_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y3    ${Z[$i]} >> $file_Y4_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y4    ${Z[$i]} >> $file_Y4_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y5    ${Z[$i]} >> $file_Y4_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y6    ${Z[$i]} >> $file_Y4_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z1      >> $file_Y4_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z2      >> $file_Y4_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z3      >> $file_Y4_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z4      >> $file_Y4_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z5      >> $file_Y4_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z6      >> $file_Y4_Z6
                  echo  ${LABEL[$i]}  $X1       $Y5       ${Z[$i]} >> $file_Y5_X1
                  echo  ${LABEL[$i]}  $X2       $Y5       ${Z[$i]} >> $file_Y5_X2
                  echo  ${LABEL[$i]}  $X3       $Y5       ${Z[$i]} >> $file_Y5_X3
                  echo  ${LABEL[$i]}  $X4       $Y5       ${Z[$i]} >> $file_Y5_X4
                  echo  ${LABEL[$i]}  $X5       $Y5       ${Z[$i]} >> $file_Y5_X5
                  echo  ${LABEL[$i]}  $X6       $Y5       ${Z[$i]} >> $file_Y5_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y1    ${Z[$i]} >> $file_Y5_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y2    ${Z[$i]} >> $file_Y5_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y3    ${Z[$i]} >> $file_Y5_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y4    ${Z[$i]} >> $file_Y5_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y5    ${Z[$i]} >> $file_Y5_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y6    ${Z[$i]} >> $file_Y5_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z1      >> $file_Y5_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z2      >> $file_Y5_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z3      >> $file_Y5_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z4      >> $file_Y5_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z5      >> $file_Y5_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z6      >> $file_Y5_Z6
                  echo  ${LABEL[$i]}  $X1       $Y6       ${Z[$i]} >> $file_Y6_X1
                  echo  ${LABEL[$i]}  $X2       $Y6       ${Z[$i]} >> $file_Y6_X2
                  echo  ${LABEL[$i]}  $X3       $Y6       ${Z[$i]} >> $file_Y6_X3
                  echo  ${LABEL[$i]}  $X4       $Y6       ${Z[$i]} >> $file_Y6_X4
                  echo  ${LABEL[$i]}  $X5       $Y6       ${Z[$i]} >> $file_Y6_X5
                  echo  ${LABEL[$i]}  $X6       $Y6       ${Z[$i]} >> $file_Y6_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y1    ${Z[$i]} >> $file_Y6_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y2    ${Z[$i]} >> $file_Y6_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y3    ${Z[$i]} >> $file_Y6_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y4    ${Z[$i]} >> $file_Y6_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y5    ${Z[$i]} >> $file_Y6_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y6    ${Z[$i]} >> $file_Y6_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z1      >> $file_Y6_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z2      >> $file_Y6_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z3      >> $file_Y6_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z4      >> $file_Y6_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z5      >> $file_Y6_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z6      >> $file_Y6_Z6
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z1      >> $file_Z1_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z1      >> $file_Z1_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z1      >> $file_Z1_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z1      >> $file_Z1_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z1      >> $file_Z1_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z1      >> $file_Z1_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z1      >> $file_Z1_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z1      >> $file_Z1_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z1      >> $file_Z1_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z1      >> $file_Z1_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z1      >> $file_Z1_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z1      >> $file_Z1_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z1   >> $file_Z1_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z2   >> $file_Z1_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z3   >> $file_Z1_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z4   >> $file_Z1_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z5   >> $file_Z1_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z6   >> $file_Z1_Z6
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z2      >> $file_Z2_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z2      >> $file_Z2_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z2      >> $file_Z2_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z2      >> $file_Z2_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z2      >> $file_Z2_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z2      >> $file_Z2_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z2      >> $file_Z2_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z2      >> $file_Z2_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z2      >> $file_Z2_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z2      >> $file_Z2_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z2      >> $file_Z2_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z2      >> $file_Z2_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z1   >> $file_Z2_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z2   >> $file_Z2_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z3   >> $file_Z2_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z4   >> $file_Z2_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z5   >> $file_Z2_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z6   >> $file_Z2_Z6
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z3      >> $file_Z3_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z3      >> $file_Z3_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z3      >> $file_Z3_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z3      >> $file_Z3_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z3      >> $file_Z3_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z3      >> $file_Z3_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z3      >> $file_Z3_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z3      >> $file_Z3_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z3      >> $file_Z3_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z3      >> $file_Z3_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z3      >> $file_Z3_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z3      >> $file_Z3_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z1   >> $file_Z3_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z2   >> $file_Z3_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z3   >> $file_Z3_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z4   >> $file_Z3_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z5   >> $file_Z3_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z6   >> $file_Z3_Z6
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z4      >> $file_Z4_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z4      >> $file_Z4_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z4      >> $file_Z4_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z4      >> $file_Z4_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z4      >> $file_Z4_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z4      >> $file_Z4_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z4      >> $file_Z4_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z4      >> $file_Z4_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z4      >> $file_Z4_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z4      >> $file_Z4_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z4      >> $file_Z4_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z4      >> $file_Z4_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z1   >> $file_Z4_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z2   >> $file_Z4_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z3   >> $file_Z4_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z4   >> $file_Z4_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z5   >> $file_Z4_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z6   >> $file_Z4_Z6
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z5      >> $file_Z5_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z5      >> $file_Z5_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z5      >> $file_Z5_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z5      >> $file_Z5_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z5      >> $file_Z5_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z5      >> $file_Z5_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z5      >> $file_Z5_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z5      >> $file_Z5_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z5      >> $file_Z5_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z5      >> $file_Z5_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z5      >> $file_Z5_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z5      >> $file_Z5_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z1   >> $file_Z5_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z2   >> $file_Z5_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z3   >> $file_Z5_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z4   >> $file_Z5_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z5   >> $file_Z5_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z6   >> $file_Z5_Z6
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z6      >> $file_Z6_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z6      >> $file_Z6_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z6      >> $file_Z6_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z6      >> $file_Z6_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z6      >> $file_Z6_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z6      >> $file_Z6_X6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z6      >> $file_Z6_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z6      >> $file_Z6_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z6      >> $file_Z6_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z6      >> $file_Z6_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z6      >> $file_Z6_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z6      >> $file_Z6_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z1   >> $file_Z6_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z2   >> $file_Z6_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z3   >> $file_Z6_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z4   >> $file_Z6_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z5   >> $file_Z6_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z6   >> $file_Z6_Z6
              else
                  if [[ $index_atom1_to_distort -eq $ind ]]; then
                      # calculate
                      X1=$(echo " ${X[$i]} - 3 * $iDIST" | bc -l )
                      X2=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
                      X3=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
                      X4=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
                      X5=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
                      X6=$(echo " ${X[$i]} + 3 * $iDIST" | bc -l )
                      Y1=$(echo " ${Y[$i]} - 3 * $iDIST" | bc -l )
                      Y2=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
                      Y3=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
                      Y4=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
                      Y5=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
                      Y6=$(echo " ${Y[$i]} + 3 * $iDIST" | bc -l )
                      Z1=$(echo " ${Z[$i]} - 3 * $iDIST" | bc -l )
                      Z2=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
                      Z3=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
                      Z4=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
                      Z5=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
                      Z6=$(echo " ${Z[$i]} + 3 * $iDIST" | bc -l )
                      # write              X          Y          Z           file
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X5
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y3
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y5
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z3
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z5
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z6
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X5
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X6
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y3
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y4
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y5
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y6
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z3
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z4
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z5
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z6
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X5
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X6
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y1
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y3
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y4
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y5
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y6
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z1
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z3
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z4
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z5
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z6
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X5
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X6
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y1
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y2
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y4
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y5
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y6
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z1
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z2
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z4
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z5
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z6
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X1
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X2
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X3
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X5
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X6
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y1
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y2
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y3
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y5
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y6
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z1
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z2
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z3
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z5
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z6
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X1
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X2
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X3
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X4
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X6
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y1
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y2
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y3
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y4
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y6
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z1
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z2
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z3
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z4
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z6

                  fi
                  if [[ $index_atom2_to_distort -eq $ind ]]; then
                      # calculate
                      X1=$(echo " ${X[$i]} - 3 * $iDIST" | bc -l )
                      X2=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
                      X3=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
                      X4=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
                      X5=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
                      X6=$(echo " ${X[$i]} + 3 * $iDIST" | bc -l )
                      Y1=$(echo " ${Y[$i]} - 3 * $iDIST" | bc -l )
                      Y2=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
                      Y3=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
                      Y4=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
                      Y5=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
                      Y6=$(echo " ${Y[$i]} + 3 * $iDIST" | bc -l )
                      Z1=$(echo " ${Z[$i]} - 3 * $iDIST" | bc -l )
                      Z2=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
                      Z3=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
                      Z4=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
                      Z5=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
                      Z6=$(echo " ${Z[$i]} + 3 * $iDIST" | bc -l )
                      # write              X          Y          Z           file
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X1_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X1_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X1_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X1_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X1_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X1_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X2_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X2_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X2_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X2_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X2_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X2_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X3_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X3_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X3_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X3_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X3_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X3_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X4_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X4_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X4_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X4_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X4_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X4_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X5_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X5_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X5_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X5_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X5_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X5_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X5_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X5_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X5_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X5_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X5_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X5_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X5_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X5_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X5_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X5_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X5_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X6_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X6_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X6_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X6_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X6_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X6_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X6_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X6_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X6_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X6_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X6_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X6_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X6_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X6_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X6_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X6_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X6_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y1_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y1_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y1_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y1_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y2_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y2_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y2_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y2_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y3_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y3_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y3_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y3_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y4_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y4_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y4_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y4_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y5_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y5_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y5_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y5_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y5_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y5_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y5_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y5_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y5_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y5_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y5_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y6_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y6_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y6_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y6_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y6_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y6_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y6_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y6_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y6_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y6_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y6_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z1_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z1_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z1_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z1_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z2_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z2_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z2_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z2_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z3_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z3_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z3_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z3_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z4_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z4_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z4_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z4_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z5_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z5_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z5_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z5_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z5_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z5_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z5_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z5_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z5_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z5_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z5_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z6_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z6_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z6_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z6_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z6_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z6_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z6_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z6_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z6_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z6_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z6_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z6
                  fi
              fi

          elif [[ $npoints -eq 8 ]] ; then

              if [[ $index_atom1_to_distort -eq $index_atom2_to_distort ]] ; then
                  # calculate
                  X1=$(echo " ${X[$i]} - 4 * $iDIST" | bc -l )
                  X2=$(echo " ${X[$i]} - 3 * $iDIST" | bc -l )
                  X3=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
                  X4=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
                  X5=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
                  X6=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
                  X7=$(echo " ${X[$i]} + 3 * $iDIST" | bc -l )
                  X8=$(echo " ${X[$i]} + 4 * $iDIST" | bc -l )
                  Y1=$(echo " ${Y[$i]} - 4 * $iDIST" | bc -l )
                  Y2=$(echo " ${Y[$i]} - 3 * $iDIST" | bc -l )
                  Y3=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
                  Y4=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
                  Y5=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
                  Y6=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
                  Y7=$(echo " ${Y[$i]} + 3 * $iDIST" | bc -l )
                  Y8=$(echo " ${Y[$i]} + 4 * $iDIST" | bc -l )
                  Z1=$(echo " ${Z[$i]} - 4 * $iDIST" | bc -l )
                  Z2=$(echo " ${Z[$i]} - 3 * $iDIST" | bc -l )
                  Z3=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
                  Z4=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
                  Z5=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
                  Z6=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
                  Z7=$(echo " ${Z[$i]} + 3 * $iDIST" | bc -l )
                  Z8=$(echo " ${Z[$i]} + 4 * $iDIST" | bc -l )

                  X1_X1=$(echo " ${X[$i]} - 4 * $iDIST - 4 * $iDIST" | bc -l )
                  X1_X2=$(echo " ${X[$i]} - 4 * $iDIST - 3 * $iDIST" | bc -l )
                  X1_X3=$(echo " ${X[$i]} - 4 * $iDIST - 2 * $iDIST" | bc -l )
                  X1_X4=$(echo " ${X[$i]} - 4 * $iDIST - 1 * $iDIST" | bc -l )
                  X1_X5=$(echo " ${X[$i]} - 4 * $iDIST + 1 * $iDIST" | bc -l )
                  X1_X6=$(echo " ${X[$i]} - 4 * $iDIST + 2 * $iDIST" | bc -l )
                  X1_X7=$(echo " ${X[$i]} - 4 * $iDIST + 3 * $iDIST" | bc -l )
                  X1_X8=$(echo " ${X[$i]} - 4 * $iDIST + 4 * $iDIST" | bc -l )
                  X2_X1=$(echo " ${X[$i]} - 3 * $iDIST - 4 * $iDIST" | bc -l )
                  X2_X2=$(echo " ${X[$i]} - 3 * $iDIST - 3 * $iDIST" | bc -l )
                  X2_X3=$(echo " ${X[$i]} - 3 * $iDIST - 2 * $iDIST" | bc -l )
                  X2_X4=$(echo " ${X[$i]} - 3 * $iDIST - 1 * $iDIST" | bc -l )
                  X2_X5=$(echo " ${X[$i]} - 3 * $iDIST + 1 * $iDIST" | bc -l )
                  X2_X6=$(echo " ${X[$i]} - 3 * $iDIST + 2 * $iDIST" | bc -l )
                  X2_X7=$(echo " ${X[$i]} - 3 * $iDIST + 3 * $iDIST" | bc -l )
                  X2_X8=$(echo " ${X[$i]} - 3 * $iDIST + 4 * $iDIST" | bc -l )
                  X3_X1=$(echo " ${X[$i]} - 2 * $iDIST - 4 * $iDIST" | bc -l )
                  X3_X2=$(echo " ${X[$i]} - 2 * $iDIST - 3 * $iDIST" | bc -l )
                  X3_X3=$(echo " ${X[$i]} - 2 * $iDIST - 2 * $iDIST" | bc -l )
                  X3_X4=$(echo " ${X[$i]} - 2 * $iDIST - 1 * $iDIST" | bc -l )
                  X3_X5=$(echo " ${X[$i]} - 2 * $iDIST + 1 * $iDIST" | bc -l )
                  X3_X6=$(echo " ${X[$i]} - 2 * $iDIST + 2 * $iDIST" | bc -l )
                  X3_X7=$(echo " ${X[$i]} - 2 * $iDIST + 3 * $iDIST" | bc -l )
                  X3_X8=$(echo " ${X[$i]} - 2 * $iDIST + 4 * $iDIST" | bc -l )
                  X4_X1=$(echo " ${X[$i]} - 1 * $iDIST - 4 * $iDIST" | bc -l )
                  X4_X2=$(echo " ${X[$i]} - 1 * $iDIST - 3 * $iDIST" | bc -l )
                  X4_X3=$(echo " ${X[$i]} - 1 * $iDIST - 2 * $iDIST" | bc -l )
                  X4_X4=$(echo " ${X[$i]} - 1 * $iDIST - 1 * $iDIST" | bc -l )
                  X4_X5=$(echo " ${X[$i]} - 1 * $iDIST + 1 * $iDIST" | bc -l )
                  X4_X6=$(echo " ${X[$i]} - 1 * $iDIST + 2 * $iDIST" | bc -l )
                  X4_X7=$(echo " ${X[$i]} - 1 * $iDIST + 3 * $iDIST" | bc -l )
                  X4_X8=$(echo " ${X[$i]} - 1 * $iDIST + 4 * $iDIST" | bc -l )
                  X5_X1=$(echo " ${X[$i]} + 1 * $iDIST - 4 * $iDIST" | bc -l )
                  X5_X2=$(echo " ${X[$i]} + 1 * $iDIST - 3 * $iDIST" | bc -l )
                  X5_X3=$(echo " ${X[$i]} + 1 * $iDIST - 2 * $iDIST" | bc -l )
                  X5_X4=$(echo " ${X[$i]} + 1 * $iDIST - 1 * $iDIST" | bc -l )
                  X5_X5=$(echo " ${X[$i]} + 1 * $iDIST + 1 * $iDIST" | bc -l )
                  X5_X6=$(echo " ${X[$i]} + 1 * $iDIST + 2 * $iDIST" | bc -l )
                  X5_X7=$(echo " ${X[$i]} + 1 * $iDIST + 3 * $iDIST" | bc -l )
                  X5_X8=$(echo " ${X[$i]} + 1 * $iDIST + 4 * $iDIST" | bc -l )
                  X6_X1=$(echo " ${X[$i]} + 2 * $iDIST - 4 * $iDIST" | bc -l )
                  X6_X2=$(echo " ${X[$i]} + 2 * $iDIST - 3 * $iDIST" | bc -l )
                  X6_X3=$(echo " ${X[$i]} + 2 * $iDIST - 2 * $iDIST" | bc -l )
                  X6_X4=$(echo " ${X[$i]} + 2 * $iDIST - 1 * $iDIST" | bc -l )
                  X6_X5=$(echo " ${X[$i]} + 2 * $iDIST + 1 * $iDIST" | bc -l )
                  X6_X6=$(echo " ${X[$i]} + 2 * $iDIST + 2 * $iDIST" | bc -l )
                  X6_X7=$(echo " ${X[$i]} + 2 * $iDIST + 3 * $iDIST" | bc -l )
                  X6_X8=$(echo " ${X[$i]} + 2 * $iDIST + 4 * $iDIST" | bc -l )
                  X7_X1=$(echo " ${X[$i]} + 3 * $iDIST - 4 * $iDIST" | bc -l )
                  X7_X2=$(echo " ${X[$i]} + 3 * $iDIST - 3 * $iDIST" | bc -l )
                  X7_X3=$(echo " ${X[$i]} + 3 * $iDIST - 2 * $iDIST" | bc -l )
                  X7_X4=$(echo " ${X[$i]} + 3 * $iDIST - 1 * $iDIST" | bc -l )
                  X7_X5=$(echo " ${X[$i]} + 3 * $iDIST + 1 * $iDIST" | bc -l )
                  X7_X6=$(echo " ${X[$i]} + 3 * $iDIST + 2 * $iDIST" | bc -l )
                  X7_X7=$(echo " ${X[$i]} + 3 * $iDIST + 3 * $iDIST" | bc -l )
                  X7_X8=$(echo " ${X[$i]} + 3 * $iDIST + 4 * $iDIST" | bc -l )
                  X8_X1=$(echo " ${X[$i]} + 4 * $iDIST - 4 * $iDIST" | bc -l )
                  X8_X2=$(echo " ${X[$i]} + 4 * $iDIST - 3 * $iDIST" | bc -l )
                  X8_X3=$(echo " ${X[$i]} + 4 * $iDIST - 2 * $iDIST" | bc -l )
                  X8_X4=$(echo " ${X[$i]} + 4 * $iDIST - 1 * $iDIST" | bc -l )
                  X8_X5=$(echo " ${X[$i]} + 4 * $iDIST + 1 * $iDIST" | bc -l )
                  X8_X6=$(echo " ${X[$i]} + 4 * $iDIST + 2 * $iDIST" | bc -l )
                  X8_X7=$(echo " ${X[$i]} + 4 * $iDIST + 3 * $iDIST" | bc -l )
                  X8_X8=$(echo " ${X[$i]} + 4 * $iDIST + 4 * $iDIST" | bc -l )
                  Y1_Y1=$(echo " ${Y[$i]} - 4 * $iDIST - 4 * $iDIST" | bc -l )
                  Y1_Y2=$(echo " ${Y[$i]} - 4 * $iDIST - 3 * $iDIST" | bc -l )
                  Y1_Y3=$(echo " ${Y[$i]} - 4 * $iDIST - 2 * $iDIST" | bc -l )
                  Y1_Y4=$(echo " ${Y[$i]} - 4 * $iDIST - 1 * $iDIST" | bc -l )
                  Y1_Y5=$(echo " ${Y[$i]} - 4 * $iDIST + 1 * $iDIST" | bc -l )
                  Y1_Y6=$(echo " ${Y[$i]} - 4 * $iDIST + 2 * $iDIST" | bc -l )
                  Y1_Y7=$(echo " ${Y[$i]} - 4 * $iDIST + 3 * $iDIST" | bc -l )
                  Y1_Y8=$(echo " ${Y[$i]} - 4 * $iDIST + 4 * $iDIST" | bc -l )
                  Y2_Y1=$(echo " ${Y[$i]} - 3 * $iDIST - 4 * $iDIST" | bc -l )
                  Y2_Y2=$(echo " ${Y[$i]} - 3 * $iDIST - 3 * $iDIST" | bc -l )
                  Y2_Y3=$(echo " ${Y[$i]} - 3 * $iDIST - 2 * $iDIST" | bc -l )
                  Y2_Y4=$(echo " ${Y[$i]} - 3 * $iDIST - 1 * $iDIST" | bc -l )
                  Y2_Y5=$(echo " ${Y[$i]} - 3 * $iDIST + 1 * $iDIST" | bc -l )
                  Y2_Y6=$(echo " ${Y[$i]} - 3 * $iDIST + 2 * $iDIST" | bc -l )
                  Y2_Y7=$(echo " ${Y[$i]} - 3 * $iDIST + 3 * $iDIST" | bc -l )
                  Y2_Y8=$(echo " ${Y[$i]} - 3 * $iDIST + 4 * $iDIST" | bc -l )
                  Y3_Y1=$(echo " ${Y[$i]} - 2 * $iDIST - 4 * $iDIST" | bc -l )
                  Y3_Y2=$(echo " ${Y[$i]} - 2 * $iDIST - 3 * $iDIST" | bc -l )
                  Y3_Y3=$(echo " ${Y[$i]} - 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Y3_Y4=$(echo " ${Y[$i]} - 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Y3_Y5=$(echo " ${Y[$i]} - 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Y3_Y6=$(echo " ${Y[$i]} - 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Y3_Y7=$(echo " ${Y[$i]} - 2 * $iDIST + 3 * $iDIST" | bc -l )
                  Y3_Y8=$(echo " ${Y[$i]} - 2 * $iDIST + 4 * $iDIST" | bc -l )
                  Y4_Y1=$(echo " ${Y[$i]} - 1 * $iDIST - 4 * $iDIST" | bc -l )
                  Y4_Y2=$(echo " ${Y[$i]} - 1 * $iDIST - 3 * $iDIST" | bc -l )
                  Y4_Y3=$(echo " ${Y[$i]} - 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Y4_Y4=$(echo " ${Y[$i]} - 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Y4_Y5=$(echo " ${Y[$i]} - 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Y4_Y6=$(echo " ${Y[$i]} - 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Y4_Y7=$(echo " ${Y[$i]} - 1 * $iDIST + 3 * $iDIST" | bc -l )
                  Y4_Y8=$(echo " ${Y[$i]} - 1 * $iDIST + 4 * $iDIST" | bc -l )
                  Y5_Y1=$(echo " ${Y[$i]} + 1 * $iDIST - 4 * $iDIST" | bc -l )
                  Y5_Y2=$(echo " ${Y[$i]} + 1 * $iDIST - 3 * $iDIST" | bc -l )
                  Y5_Y3=$(echo " ${Y[$i]} + 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Y5_Y4=$(echo " ${Y[$i]} + 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Y5_Y5=$(echo " ${Y[$i]} + 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Y5_Y6=$(echo " ${Y[$i]} + 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Y5_Y7=$(echo " ${Y[$i]} + 1 * $iDIST + 3 * $iDIST" | bc -l )
                  Y5_Y8=$(echo " ${Y[$i]} + 1 * $iDIST + 4 * $iDIST" | bc -l )
                  Y6_Y1=$(echo " ${Y[$i]} + 2 * $iDIST - 4 * $iDIST" | bc -l )
                  Y6_Y2=$(echo " ${Y[$i]} + 2 * $iDIST - 3 * $iDIST" | bc -l )
                  Y6_Y3=$(echo " ${Y[$i]} + 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Y6_Y4=$(echo " ${Y[$i]} + 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Y6_Y5=$(echo " ${Y[$i]} + 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Y6_Y6=$(echo " ${Y[$i]} + 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Y6_Y7=$(echo " ${Y[$i]} + 2 * $iDIST + 3 * $iDIST" | bc -l )
                  Y6_Y8=$(echo " ${Y[$i]} + 2 * $iDIST + 4 * $iDIST" | bc -l )
                  Y7_Y1=$(echo " ${Y[$i]} + 3 * $iDIST - 4 * $iDIST" | bc -l )
                  Y7_Y2=$(echo " ${Y[$i]} + 3 * $iDIST - 3 * $iDIST" | bc -l )
                  Y7_Y3=$(echo " ${Y[$i]} + 3 * $iDIST - 2 * $iDIST" | bc -l )
                  Y7_Y4=$(echo " ${Y[$i]} + 3 * $iDIST - 1 * $iDIST" | bc -l )
                  Y7_Y5=$(echo " ${Y[$i]} + 3 * $iDIST + 1 * $iDIST" | bc -l )
                  Y7_Y6=$(echo " ${Y[$i]} + 3 * $iDIST + 2 * $iDIST" | bc -l )
                  Y7_Y7=$(echo " ${Y[$i]} + 3 * $iDIST + 3 * $iDIST" | bc -l )
                  Y7_Y8=$(echo " ${Y[$i]} + 3 * $iDIST + 4 * $iDIST" | bc -l )
                  Y8_Y1=$(echo " ${Y[$i]} + 4 * $iDIST - 4 * $iDIST" | bc -l )
                  Y8_Y2=$(echo " ${Y[$i]} + 4 * $iDIST - 3 * $iDIST" | bc -l )
                  Y8_Y3=$(echo " ${Y[$i]} + 4 * $iDIST - 2 * $iDIST" | bc -l )
                  Y8_Y4=$(echo " ${Y[$i]} + 4 * $iDIST - 1 * $iDIST" | bc -l )
                  Y8_Y5=$(echo " ${Y[$i]} + 4 * $iDIST + 1 * $iDIST" | bc -l )
                  Y8_Y6=$(echo " ${Y[$i]} + 4 * $iDIST + 2 * $iDIST" | bc -l )
                  Y8_Y7=$(echo " ${Y[$i]} + 4 * $iDIST + 3 * $iDIST" | bc -l )
                  Y8_Y8=$(echo " ${Y[$i]} + 4 * $iDIST + 4 * $iDIST" | bc -l )
                  Z1_Z1=$(echo " ${Z[$i]} - 4 * $iDIST - 4 * $iDIST" | bc -l )
                  Z1_Z2=$(echo " ${Z[$i]} - 4 * $iDIST - 3 * $iDIST" | bc -l )
                  Z1_Z3=$(echo " ${Z[$i]} - 4 * $iDIST - 2 * $iDIST" | bc -l )
                  Z1_Z4=$(echo " ${Z[$i]} - 4 * $iDIST - 1 * $iDIST" | bc -l )
                  Z1_Z5=$(echo " ${Z[$i]} - 4 * $iDIST + 1 * $iDIST" | bc -l )
                  Z1_Z6=$(echo " ${Z[$i]} - 4 * $iDIST + 2 * $iDIST" | bc -l )
                  Z1_Z7=$(echo " ${Z[$i]} - 4 * $iDIST + 3 * $iDIST" | bc -l )
                  Z1_Z8=$(echo " ${Z[$i]} - 4 * $iDIST + 4 * $iDIST" | bc -l )
                  Z2_Z1=$(echo " ${Z[$i]} - 3 * $iDIST - 4 * $iDIST" | bc -l )
                  Z2_Z2=$(echo " ${Z[$i]} - 3 * $iDIST - 3 * $iDIST" | bc -l )
                  Z2_Z3=$(echo " ${Z[$i]} - 3 * $iDIST - 2 * $iDIST" | bc -l )
                  Z2_Z4=$(echo " ${Z[$i]} - 3 * $iDIST - 1 * $iDIST" | bc -l )
                  Z2_Z5=$(echo " ${Z[$i]} - 3 * $iDIST + 1 * $iDIST" | bc -l )
                  Z2_Z6=$(echo " ${Z[$i]} - 3 * $iDIST + 2 * $iDIST" | bc -l )
                  Z2_Z7=$(echo " ${Z[$i]} - 3 * $iDIST + 3 * $iDIST" | bc -l )
                  Z2_Z8=$(echo " ${Z[$i]} - 3 * $iDIST + 4 * $iDIST" | bc -l )
                  Z3_Z1=$(echo " ${Z[$i]} - 2 * $iDIST - 4 * $iDIST" | bc -l )
                  Z3_Z2=$(echo " ${Z[$i]} - 2 * $iDIST - 3 * $iDIST" | bc -l )
                  Z3_Z3=$(echo " ${Z[$i]} - 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Z3_Z4=$(echo " ${Z[$i]} - 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Z3_Z5=$(echo " ${Z[$i]} - 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Z3_Z6=$(echo " ${Z[$i]} - 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Z3_Z7=$(echo " ${Z[$i]} - 2 * $iDIST + 3 * $iDIST" | bc -l )
                  Z3_Z8=$(echo " ${Z[$i]} - 2 * $iDIST + 4 * $iDIST" | bc -l )
                  Z4_Z1=$(echo " ${Z[$i]} - 1 * $iDIST - 4 * $iDIST" | bc -l )
                  Z4_Z2=$(echo " ${Z[$i]} - 1 * $iDIST - 3 * $iDIST" | bc -l )
                  Z4_Z3=$(echo " ${Z[$i]} - 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Z4_Z4=$(echo " ${Z[$i]} - 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Z4_Z5=$(echo " ${Z[$i]} - 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Z4_Z6=$(echo " ${Z[$i]} - 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Z4_Z7=$(echo " ${Z[$i]} - 1 * $iDIST + 3 * $iDIST" | bc -l )
                  Z4_Z8=$(echo " ${Z[$i]} - 1 * $iDIST + 4 * $iDIST" | bc -l )
                  Z5_Z1=$(echo " ${Z[$i]} + 1 * $iDIST - 4 * $iDIST" | bc -l )
                  Z5_Z2=$(echo " ${Z[$i]} + 1 * $iDIST - 3 * $iDIST" | bc -l )
                  Z5_Z3=$(echo " ${Z[$i]} + 1 * $iDIST - 2 * $iDIST" | bc -l )
                  Z5_Z4=$(echo " ${Z[$i]} + 1 * $iDIST - 1 * $iDIST" | bc -l )
                  Z5_Z5=$(echo " ${Z[$i]} + 1 * $iDIST + 1 * $iDIST" | bc -l )
                  Z5_Z6=$(echo " ${Z[$i]} + 1 * $iDIST + 2 * $iDIST" | bc -l )
                  Z5_Z7=$(echo " ${Z[$i]} + 1 * $iDIST + 3 * $iDIST" | bc -l )
                  Z5_Z8=$(echo " ${Z[$i]} + 1 * $iDIST + 4 * $iDIST" | bc -l )
                  Z6_Z1=$(echo " ${Z[$i]} + 2 * $iDIST - 4 * $iDIST" | bc -l )
                  Z6_Z2=$(echo " ${Z[$i]} + 2 * $iDIST - 3 * $iDIST" | bc -l )
                  Z6_Z3=$(echo " ${Z[$i]} + 2 * $iDIST - 2 * $iDIST" | bc -l )
                  Z6_Z4=$(echo " ${Z[$i]} + 2 * $iDIST - 1 * $iDIST" | bc -l )
                  Z6_Z5=$(echo " ${Z[$i]} + 2 * $iDIST + 1 * $iDIST" | bc -l )
                  Z6_Z6=$(echo " ${Z[$i]} + 2 * $iDIST + 2 * $iDIST" | bc -l )
                  Z6_Z7=$(echo " ${Z[$i]} + 2 * $iDIST + 3 * $iDIST" | bc -l )
                  Z6_Z8=$(echo " ${Z[$i]} + 2 * $iDIST + 4 * $iDIST" | bc -l )
                  Z7_Z1=$(echo " ${Z[$i]} + 3 * $iDIST - 4 * $iDIST" | bc -l )
                  Z7_Z2=$(echo " ${Z[$i]} + 3 * $iDIST - 3 * $iDIST" | bc -l )
                  Z7_Z3=$(echo " ${Z[$i]} + 3 * $iDIST - 2 * $iDIST" | bc -l )
                  Z7_Z4=$(echo " ${Z[$i]} + 3 * $iDIST - 1 * $iDIST" | bc -l )
                  Z7_Z5=$(echo " ${Z[$i]} + 3 * $iDIST + 1 * $iDIST" | bc -l )
                  Z7_Z6=$(echo " ${Z[$i]} + 3 * $iDIST + 2 * $iDIST" | bc -l )
                  Z7_Z7=$(echo " ${Z[$i]} + 3 * $iDIST + 3 * $iDIST" | bc -l )
                  Z7_Z8=$(echo " ${Z[$i]} + 3 * $iDIST + 4 * $iDIST" | bc -l )
                  Z8_Z1=$(echo " ${Z[$i]} + 4 * $iDIST - 4 * $iDIST" | bc -l )
                  Z8_Z2=$(echo " ${Z[$i]} + 4 * $iDIST - 3 * $iDIST" | bc -l )
                  Z8_Z3=$(echo " ${Z[$i]} + 4 * $iDIST - 2 * $iDIST" | bc -l )
                  Z8_Z4=$(echo " ${Z[$i]} + 4 * $iDIST - 1 * $iDIST" | bc -l )
                  Z8_Z5=$(echo " ${Z[$i]} + 4 * $iDIST + 1 * $iDIST" | bc -l )
                  Z8_Z6=$(echo " ${Z[$i]} + 4 * $iDIST + 2 * $iDIST" | bc -l )
                  Z8_Z7=$(echo " ${Z[$i]} + 4 * $iDIST + 3 * $iDIST" | bc -l )
                  Z8_Z8=$(echo " ${Z[$i]} + 4 * $iDIST + 4 * $iDIST" | bc -l )

                  # write              X          Y          Z           file
                  echo  ${LABEL[$i]}  $X1_X1    ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                  echo  ${LABEL[$i]}  $X1_X2    ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                  echo  ${LABEL[$i]}  $X1_X3    ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
                  echo  ${LABEL[$i]}  $X1_X4    ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
                  echo  ${LABEL[$i]}  $X1_X5    ${Y[$i]}  ${Z[$i]} >> $file_X1_X5
                  echo  ${LABEL[$i]}  $X1_X6    ${Y[$i]}  ${Z[$i]} >> $file_X1_X6
                  echo  ${LABEL[$i]}  $X1_X7    ${Y[$i]}  ${Z[$i]} >> $file_X1_X7
                  echo  ${LABEL[$i]}  $X1_X8    ${Y[$i]}  ${Z[$i]} >> $file_X1_X8
                  echo  ${LABEL[$i]}  $X1       $Y1       ${Z[$i]} >> $file_X1_Y1
                  echo  ${LABEL[$i]}  $X1       $Y2       ${Z[$i]} >> $file_X1_Y2
                  echo  ${LABEL[$i]}  $X1       $Y3       ${Z[$i]} >> $file_X1_Y3
                  echo  ${LABEL[$i]}  $X1       $Y4       ${Z[$i]} >> $file_X1_Y4
                  echo  ${LABEL[$i]}  $X1       $Y5       ${Z[$i]} >> $file_X1_Y5
                  echo  ${LABEL[$i]}  $X1       $Y6       ${Z[$i]} >> $file_X1_Y6
                  echo  ${LABEL[$i]}  $X1       $Y7       ${Z[$i]} >> $file_X1_Y7
                  echo  ${LABEL[$i]}  $X1       $Y8       ${Z[$i]} >> $file_X1_Y8
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z1      >> $file_X1_Z1
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z2      >> $file_X1_Z2
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z3      >> $file_X1_Z3
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z4      >> $file_X1_Z4
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z5      >> $file_X1_Z5
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z6      >> $file_X1_Z6
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z7      >> $file_X1_Z7
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z8      >> $file_X1_Z8
                  echo  ${LABEL[$i]}  $X2_X1    ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                  echo  ${LABEL[$i]}  $X2_X2    ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                  echo  ${LABEL[$i]}  $X2_X3    ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
                  echo  ${LABEL[$i]}  $X2_X4    ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
                  echo  ${LABEL[$i]}  $X2_X5    ${Y[$i]}  ${Z[$i]} >> $file_X2_X5
                  echo  ${LABEL[$i]}  $X2_X6    ${Y[$i]}  ${Z[$i]} >> $file_X2_X6
                  echo  ${LABEL[$i]}  $X2_X7    ${Y[$i]}  ${Z[$i]} >> $file_X2_X7
                  echo  ${LABEL[$i]}  $X2_X8    ${Y[$i]}  ${Z[$i]} >> $file_X2_X8
                  echo  ${LABEL[$i]}  $X2       $Y1       ${Z[$i]} >> $file_X2_Y1
                  echo  ${LABEL[$i]}  $X2       $Y2       ${Z[$i]} >> $file_X2_Y2
                  echo  ${LABEL[$i]}  $X2       $Y3       ${Z[$i]} >> $file_X2_Y3
                  echo  ${LABEL[$i]}  $X2       $Y4       ${Z[$i]} >> $file_X2_Y4
                  echo  ${LABEL[$i]}  $X2       $Y5       ${Z[$i]} >> $file_X2_Y5
                  echo  ${LABEL[$i]}  $X2       $Y6       ${Z[$i]} >> $file_X2_Y6
                  echo  ${LABEL[$i]}  $X2       $Y7       ${Z[$i]} >> $file_X2_Y7
                  echo  ${LABEL[$i]}  $X2       $Y8       ${Z[$i]} >> $file_X2_Y8
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z1      >> $file_X2_Z1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z2      >> $file_X2_Z2
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z3      >> $file_X2_Z3
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z4      >> $file_X2_Z4
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z5      >> $file_X2_Z5
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z6      >> $file_X2_Z6
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z7      >> $file_X2_Z7
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z8      >> $file_X2_Z8
                  echo  ${LABEL[$i]}  $X3_X1    ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
                  echo  ${LABEL[$i]}  $X3_X2    ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
                  echo  ${LABEL[$i]}  $X3_X3    ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
                  echo  ${LABEL[$i]}  $X3_X4    ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
                  echo  ${LABEL[$i]}  $X3_X5    ${Y[$i]}  ${Z[$i]} >> $file_X3_X5
                  echo  ${LABEL[$i]}  $X3_X6    ${Y[$i]}  ${Z[$i]} >> $file_X3_X6
                  echo  ${LABEL[$i]}  $X3_X7    ${Y[$i]}  ${Z[$i]} >> $file_X3_X7
                  echo  ${LABEL[$i]}  $X3_X8    ${Y[$i]}  ${Z[$i]} >> $file_X3_X8
                  echo  ${LABEL[$i]}  $X3       $Y1       ${Z[$i]} >> $file_X3_Y1
                  echo  ${LABEL[$i]}  $X3       $Y2       ${Z[$i]} >> $file_X3_Y2
                  echo  ${LABEL[$i]}  $X3       $Y3       ${Z[$i]} >> $file_X3_Y3
                  echo  ${LABEL[$i]}  $X3       $Y4       ${Z[$i]} >> $file_X3_Y4
                  echo  ${LABEL[$i]}  $X3       $Y5       ${Z[$i]} >> $file_X3_Y5
                  echo  ${LABEL[$i]}  $X3       $Y6       ${Z[$i]} >> $file_X3_Y6
                  echo  ${LABEL[$i]}  $X3       $Y7       ${Z[$i]} >> $file_X3_Y7
                  echo  ${LABEL[$i]}  $X3       $Y8       ${Z[$i]} >> $file_X3_Y8
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z1      >> $file_X3_Z1
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z2      >> $file_X3_Z2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z3      >> $file_X3_Z3
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z4      >> $file_X3_Z4
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z5      >> $file_X3_Z5
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z6      >> $file_X3_Z6
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z7      >> $file_X3_Z7
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z8      >> $file_X3_Z8
                  echo  ${LABEL[$i]}  $X4_X1    ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
                  echo  ${LABEL[$i]}  $X4_X2    ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
                  echo  ${LABEL[$i]}  $X4_X3    ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
                  echo  ${LABEL[$i]}  $X4_X4    ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
                  echo  ${LABEL[$i]}  $X4_X5    ${Y[$i]}  ${Z[$i]} >> $file_X4_X5
                  echo  ${LABEL[$i]}  $X4_X6    ${Y[$i]}  ${Z[$i]} >> $file_X4_X6
                  echo  ${LABEL[$i]}  $X4_X7    ${Y[$i]}  ${Z[$i]} >> $file_X4_X7
                  echo  ${LABEL[$i]}  $X4_X8    ${Y[$i]}  ${Z[$i]} >> $file_X4_X8
                  echo  ${LABEL[$i]}  $X4       $Y1       ${Z[$i]} >> $file_X4_Y1
                  echo  ${LABEL[$i]}  $X4       $Y2       ${Z[$i]} >> $file_X4_Y2
                  echo  ${LABEL[$i]}  $X4       $Y3       ${Z[$i]} >> $file_X4_Y3
                  echo  ${LABEL[$i]}  $X4       $Y4       ${Z[$i]} >> $file_X4_Y4
                  echo  ${LABEL[$i]}  $X4       $Y5       ${Z[$i]} >> $file_X4_Y5
                  echo  ${LABEL[$i]}  $X4       $Y6       ${Z[$i]} >> $file_X4_Y6
                  echo  ${LABEL[$i]}  $X4       $Y7       ${Z[$i]} >> $file_X4_Y7
                  echo  ${LABEL[$i]}  $X4       $Y8       ${Z[$i]} >> $file_X4_Y8
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z1      >> $file_X4_Z1
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z2      >> $file_X4_Z2
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z3      >> $file_X4_Z3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z4      >> $file_X4_Z4
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z5      >> $file_X4_Z5
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z6      >> $file_X4_Z6
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z7      >> $file_X4_Z7
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z8      >> $file_X4_Z8
                  echo  ${LABEL[$i]}  $X5_X1    ${Y[$i]}  ${Z[$i]} >> $file_X5_X1
                  echo  ${LABEL[$i]}  $X5_X2    ${Y[$i]}  ${Z[$i]} >> $file_X5_X2
                  echo  ${LABEL[$i]}  $X5_X3    ${Y[$i]}  ${Z[$i]} >> $file_X5_X3
                  echo  ${LABEL[$i]}  $X5_X4    ${Y[$i]}  ${Z[$i]} >> $file_X5_X4
                  echo  ${LABEL[$i]}  $X5_X5    ${Y[$i]}  ${Z[$i]} >> $file_X5_X5
                  echo  ${LABEL[$i]}  $X5_X6    ${Y[$i]}  ${Z[$i]} >> $file_X5_X6
                  echo  ${LABEL[$i]}  $X5_X7    ${Y[$i]}  ${Z[$i]} >> $file_X5_X7
                  echo  ${LABEL[$i]}  $X5_X8    ${Y[$i]}  ${Z[$i]} >> $file_X5_X8
                  echo  ${LABEL[$i]}  $X5       $Y1       ${Z[$i]} >> $file_X5_Y1
                  echo  ${LABEL[$i]}  $X5       $Y2       ${Z[$i]} >> $file_X5_Y2
                  echo  ${LABEL[$i]}  $X5       $Y3       ${Z[$i]} >> $file_X5_Y3
                  echo  ${LABEL[$i]}  $X5       $Y4       ${Z[$i]} >> $file_X5_Y4
                  echo  ${LABEL[$i]}  $X5       $Y5       ${Z[$i]} >> $file_X5_Y5
                  echo  ${LABEL[$i]}  $X5       $Y6       ${Z[$i]} >> $file_X5_Y6
                  echo  ${LABEL[$i]}  $X5       $Y7       ${Z[$i]} >> $file_X5_Y7
                  echo  ${LABEL[$i]}  $X5       $Y8       ${Z[$i]} >> $file_X5_Y8
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z1      >> $file_X5_Z1
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z2      >> $file_X5_Z2
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z3      >> $file_X5_Z3
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z4      >> $file_X5_Z4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z5      >> $file_X5_Z5
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z6      >> $file_X5_Z6
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z7      >> $file_X5_Z7
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z8      >> $file_X5_Z8
                  echo  ${LABEL[$i]}  $X6_X1    ${Y[$i]}  ${Z[$i]} >> $file_X6_X1
                  echo  ${LABEL[$i]}  $X6_X2    ${Y[$i]}  ${Z[$i]} >> $file_X6_X2
                  echo  ${LABEL[$i]}  $X6_X3    ${Y[$i]}  ${Z[$i]} >> $file_X6_X3
                  echo  ${LABEL[$i]}  $X6_X4    ${Y[$i]}  ${Z[$i]} >> $file_X6_X4
                  echo  ${LABEL[$i]}  $X6_X5    ${Y[$i]}  ${Z[$i]} >> $file_X6_X5
                  echo  ${LABEL[$i]}  $X6_X6    ${Y[$i]}  ${Z[$i]} >> $file_X6_X6
                  echo  ${LABEL[$i]}  $X6_X7    ${Y[$i]}  ${Z[$i]} >> $file_X6_X7
                  echo  ${LABEL[$i]}  $X6_X8    ${Y[$i]}  ${Z[$i]} >> $file_X6_X8
                  echo  ${LABEL[$i]}  $X6       $Y1       ${Z[$i]} >> $file_X6_Y1
                  echo  ${LABEL[$i]}  $X6       $Y2       ${Z[$i]} >> $file_X6_Y2
                  echo  ${LABEL[$i]}  $X6       $Y3       ${Z[$i]} >> $file_X6_Y3
                  echo  ${LABEL[$i]}  $X6       $Y4       ${Z[$i]} >> $file_X6_Y4
                  echo  ${LABEL[$i]}  $X6       $Y5       ${Z[$i]} >> $file_X6_Y5
                  echo  ${LABEL[$i]}  $X6       $Y6       ${Z[$i]} >> $file_X6_Y6
                  echo  ${LABEL[$i]}  $X6       $Y7       ${Z[$i]} >> $file_X6_Y7
                  echo  ${LABEL[$i]}  $X6       $Y8       ${Z[$i]} >> $file_X6_Y8
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z1      >> $file_X6_Z1
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z2      >> $file_X6_Z2
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z3      >> $file_X6_Z3
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z4      >> $file_X6_Z4
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z5      >> $file_X6_Z5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z6      >> $file_X6_Z6
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z7      >> $file_X6_Z7
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z8      >> $file_X6_Z8
                  echo  ${LABEL[$i]}  $X7_X1    ${Y[$i]}  ${Z[$i]} >> $file_X7_X1
                  echo  ${LABEL[$i]}  $X7_X2    ${Y[$i]}  ${Z[$i]} >> $file_X7_X2
                  echo  ${LABEL[$i]}  $X7_X3    ${Y[$i]}  ${Z[$i]} >> $file_X7_X3
                  echo  ${LABEL[$i]}  $X7_X4    ${Y[$i]}  ${Z[$i]} >> $file_X7_X4
                  echo  ${LABEL[$i]}  $X7_X5    ${Y[$i]}  ${Z[$i]} >> $file_X7_X5
                  echo  ${LABEL[$i]}  $X7_X6    ${Y[$i]}  ${Z[$i]} >> $file_X7_X6
                  echo  ${LABEL[$i]}  $X7_X7    ${Y[$i]}  ${Z[$i]} >> $file_X7_X7
                  echo  ${LABEL[$i]}  $X7_X8    ${Y[$i]}  ${Z[$i]} >> $file_X7_X8
                  echo  ${LABEL[$i]}  $X7       $Y1       ${Z[$i]} >> $file_X7_Y1
                  echo  ${LABEL[$i]}  $X7       $Y2       ${Z[$i]} >> $file_X7_Y2
                  echo  ${LABEL[$i]}  $X7       $Y3       ${Z[$i]} >> $file_X7_Y3
                  echo  ${LABEL[$i]}  $X7       $Y4       ${Z[$i]} >> $file_X7_Y4
                  echo  ${LABEL[$i]}  $X7       $Y5       ${Z[$i]} >> $file_X7_Y5
                  echo  ${LABEL[$i]}  $X7       $Y6       ${Z[$i]} >> $file_X7_Y6
                  echo  ${LABEL[$i]}  $X7       $Y7       ${Z[$i]} >> $file_X7_Y7
                  echo  ${LABEL[$i]}  $X7       $Y8       ${Z[$i]} >> $file_X7_Y8
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z1      >> $file_X7_Z1
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z2      >> $file_X7_Z2
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z3      >> $file_X7_Z3
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z4      >> $file_X7_Z4
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z5      >> $file_X7_Z5
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z6      >> $file_X7_Z6
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z7      >> $file_X7_Z7
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z8      >> $file_X7_Z8
                  echo  ${LABEL[$i]}  $X8_X1    ${Y[$i]}  ${Z[$i]} >> $file_X8_X1
                  echo  ${LABEL[$i]}  $X8_X2    ${Y[$i]}  ${Z[$i]} >> $file_X8_X2
                  echo  ${LABEL[$i]}  $X8_X3    ${Y[$i]}  ${Z[$i]} >> $file_X8_X3
                  echo  ${LABEL[$i]}  $X8_X4    ${Y[$i]}  ${Z[$i]} >> $file_X8_X4
                  echo  ${LABEL[$i]}  $X8_X5    ${Y[$i]}  ${Z[$i]} >> $file_X8_X5
                  echo  ${LABEL[$i]}  $X8_X6    ${Y[$i]}  ${Z[$i]} >> $file_X8_X6
                  echo  ${LABEL[$i]}  $X8_X7    ${Y[$i]}  ${Z[$i]} >> $file_X8_X7
                  echo  ${LABEL[$i]}  $X8_X8    ${Y[$i]}  ${Z[$i]} >> $file_X8_X8
                  echo  ${LABEL[$i]}  $X8       $Y1       ${Z[$i]} >> $file_X8_Y1
                  echo  ${LABEL[$i]}  $X8       $Y2       ${Z[$i]} >> $file_X8_Y2
                  echo  ${LABEL[$i]}  $X8       $Y3       ${Z[$i]} >> $file_X8_Y3
                  echo  ${LABEL[$i]}  $X8       $Y4       ${Z[$i]} >> $file_X8_Y4
                  echo  ${LABEL[$i]}  $X8       $Y5       ${Z[$i]} >> $file_X8_Y5
                  echo  ${LABEL[$i]}  $X8       $Y6       ${Z[$i]} >> $file_X8_Y6
                  echo  ${LABEL[$i]}  $X8       $Y7       ${Z[$i]} >> $file_X8_Y7
                  echo  ${LABEL[$i]}  $X8       $Y8       ${Z[$i]} >> $file_X8_Y8
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z1      >> $file_X8_Z1
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z2      >> $file_X8_Z2
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z3      >> $file_X8_Z3
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z4      >> $file_X8_Z4
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z5      >> $file_X8_Z5
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z6      >> $file_X8_Z6
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z7      >> $file_X8_Z7
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z8      >> $file_X8_Z8
                  echo  ${LABEL[$i]}  $X1       $Y1       ${Z[$i]} >> $file_Y1_X1
                  echo  ${LABEL[$i]}  $X2       $Y1       ${Z[$i]} >> $file_Y1_X2
                  echo  ${LABEL[$i]}  $X3       $Y1       ${Z[$i]} >> $file_Y1_X3
                  echo  ${LABEL[$i]}  $X4       $Y1       ${Z[$i]} >> $file_Y1_X4
                  echo  ${LABEL[$i]}  $X5       $Y1       ${Z[$i]} >> $file_Y1_X5
                  echo  ${LABEL[$i]}  $X6       $Y1       ${Z[$i]} >> $file_Y1_X6
                  echo  ${LABEL[$i]}  $X7       $Y1       ${Z[$i]} >> $file_Y1_X7
                  echo  ${LABEL[$i]}  $X8       $Y1       ${Z[$i]} >> $file_Y1_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y1    ${Z[$i]} >> $file_Y1_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y2    ${Z[$i]} >> $file_Y1_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y3    ${Z[$i]} >> $file_Y1_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y4    ${Z[$i]} >> $file_Y1_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y5    ${Z[$i]} >> $file_Y1_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y6    ${Z[$i]} >> $file_Y1_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y7    ${Z[$i]} >> $file_Y1_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1_Y8    ${Z[$i]} >> $file_Y1_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z1      >> $file_Y1_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z2      >> $file_Y1_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z3      >> $file_Y1_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z4      >> $file_Y1_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z5      >> $file_Y1_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z6      >> $file_Y1_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z7      >> $file_Y1_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z8      >> $file_Y1_Z8
                  echo  ${LABEL[$i]}  $X1       $Y2       ${Z[$i]} >> $file_Y2_X1
                  echo  ${LABEL[$i]}  $X2       $Y2       ${Z[$i]} >> $file_Y2_X2
                  echo  ${LABEL[$i]}  $X3       $Y2       ${Z[$i]} >> $file_Y2_X3
                  echo  ${LABEL[$i]}  $X4       $Y2       ${Z[$i]} >> $file_Y2_X4
                  echo  ${LABEL[$i]}  $X5       $Y2       ${Z[$i]} >> $file_Y2_X5
                  echo  ${LABEL[$i]}  $X6       $Y2       ${Z[$i]} >> $file_Y2_X6
                  echo  ${LABEL[$i]}  $X7       $Y2       ${Z[$i]} >> $file_Y2_X7
                  echo  ${LABEL[$i]}  $X8       $Y2       ${Z[$i]} >> $file_Y2_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y1    ${Z[$i]} >> $file_Y2_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y2    ${Z[$i]} >> $file_Y2_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y3    ${Z[$i]} >> $file_Y2_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y4    ${Z[$i]} >> $file_Y2_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y5    ${Z[$i]} >> $file_Y2_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y6    ${Z[$i]} >> $file_Y2_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y7    ${Z[$i]} >> $file_Y2_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2_Y8    ${Z[$i]} >> $file_Y2_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z1      >> $file_Y2_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z2      >> $file_Y2_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z3      >> $file_Y2_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z4      >> $file_Y2_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z5      >> $file_Y2_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z6      >> $file_Y2_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z7      >> $file_Y2_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z8      >> $file_Y2_Z8
                  echo  ${LABEL[$i]}  $X1       $Y3       ${Z[$i]} >> $file_Y3_X1
                  echo  ${LABEL[$i]}  $X2       $Y3       ${Z[$i]} >> $file_Y3_X2
                  echo  ${LABEL[$i]}  $X3       $Y3       ${Z[$i]} >> $file_Y3_X3
                  echo  ${LABEL[$i]}  $X4       $Y3       ${Z[$i]} >> $file_Y3_X4
                  echo  ${LABEL[$i]}  $X5       $Y3       ${Z[$i]} >> $file_Y3_X5
                  echo  ${LABEL[$i]}  $X6       $Y3       ${Z[$i]} >> $file_Y3_X6
                  echo  ${LABEL[$i]}  $X7       $Y3       ${Z[$i]} >> $file_Y3_X7
                  echo  ${LABEL[$i]}  $X8       $Y3       ${Z[$i]} >> $file_Y3_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y1    ${Z[$i]} >> $file_Y3_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y2    ${Z[$i]} >> $file_Y3_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y3    ${Z[$i]} >> $file_Y3_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y4    ${Z[$i]} >> $file_Y3_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y5    ${Z[$i]} >> $file_Y3_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y6    ${Z[$i]} >> $file_Y3_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y7    ${Z[$i]} >> $file_Y3_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3_Y8    ${Z[$i]} >> $file_Y3_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z1      >> $file_Y3_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z2      >> $file_Y3_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z3      >> $file_Y3_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z4      >> $file_Y3_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z5      >> $file_Y3_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z6      >> $file_Y3_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z7      >> $file_Y3_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z8      >> $file_Y3_Z8
                  echo  ${LABEL[$i]}  $X1       $Y4       ${Z[$i]} >> $file_Y4_X1
                  echo  ${LABEL[$i]}  $X2       $Y4       ${Z[$i]} >> $file_Y4_X2
                  echo  ${LABEL[$i]}  $X3       $Y4       ${Z[$i]} >> $file_Y4_X3
                  echo  ${LABEL[$i]}  $X4       $Y4       ${Z[$i]} >> $file_Y4_X4
                  echo  ${LABEL[$i]}  $X5       $Y4       ${Z[$i]} >> $file_Y4_X5
                  echo  ${LABEL[$i]}  $X6       $Y4       ${Z[$i]} >> $file_Y4_X6
                  echo  ${LABEL[$i]}  $X7       $Y4       ${Z[$i]} >> $file_Y4_X7
                  echo  ${LABEL[$i]}  $X8       $Y4       ${Z[$i]} >> $file_Y4_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y1    ${Z[$i]} >> $file_Y4_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y2    ${Z[$i]} >> $file_Y4_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y3    ${Z[$i]} >> $file_Y4_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y4    ${Z[$i]} >> $file_Y4_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y5    ${Z[$i]} >> $file_Y4_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y6    ${Z[$i]} >> $file_Y4_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y7    ${Z[$i]} >> $file_Y4_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4_Y8    ${Z[$i]} >> $file_Y4_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z1      >> $file_Y4_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z2      >> $file_Y4_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z3      >> $file_Y4_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z4      >> $file_Y4_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z5      >> $file_Y4_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z6      >> $file_Y4_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z7      >> $file_Y4_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z8      >> $file_Y4_Z8
                  echo  ${LABEL[$i]}  $X1       $Y5       ${Z[$i]} >> $file_Y5_X1
                  echo  ${LABEL[$i]}  $X2       $Y5       ${Z[$i]} >> $file_Y5_X2
                  echo  ${LABEL[$i]}  $X3       $Y5       ${Z[$i]} >> $file_Y5_X3
                  echo  ${LABEL[$i]}  $X4       $Y5       ${Z[$i]} >> $file_Y5_X4
                  echo  ${LABEL[$i]}  $X5       $Y5       ${Z[$i]} >> $file_Y5_X5
                  echo  ${LABEL[$i]}  $X6       $Y5       ${Z[$i]} >> $file_Y5_X6
                  echo  ${LABEL[$i]}  $X7       $Y5       ${Z[$i]} >> $file_Y5_X7
                  echo  ${LABEL[$i]}  $X8       $Y5       ${Z[$i]} >> $file_Y5_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y1    ${Z[$i]} >> $file_Y5_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y2    ${Z[$i]} >> $file_Y5_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y3    ${Z[$i]} >> $file_Y5_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y4    ${Z[$i]} >> $file_Y5_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y5    ${Z[$i]} >> $file_Y5_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y6    ${Z[$i]} >> $file_Y5_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y7    ${Z[$i]} >> $file_Y5_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5_Y8    ${Z[$i]} >> $file_Y5_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z1      >> $file_Y5_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z2      >> $file_Y5_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z3      >> $file_Y5_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z4      >> $file_Y5_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z5      >> $file_Y5_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z6      >> $file_Y5_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z7      >> $file_Y5_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z8      >> $file_Y5_Z8
                  echo  ${LABEL[$i]}  $X1       $Y6       ${Z[$i]} >> $file_Y6_X1
                  echo  ${LABEL[$i]}  $X2       $Y6       ${Z[$i]} >> $file_Y6_X2
                  echo  ${LABEL[$i]}  $X3       $Y6       ${Z[$i]} >> $file_Y6_X3
                  echo  ${LABEL[$i]}  $X4       $Y6       ${Z[$i]} >> $file_Y6_X4
                  echo  ${LABEL[$i]}  $X5       $Y6       ${Z[$i]} >> $file_Y6_X5
                  echo  ${LABEL[$i]}  $X6       $Y6       ${Z[$i]} >> $file_Y6_X6
                  echo  ${LABEL[$i]}  $X7       $Y6       ${Z[$i]} >> $file_Y6_X7
                  echo  ${LABEL[$i]}  $X8       $Y6       ${Z[$i]} >> $file_Y6_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y1    ${Z[$i]} >> $file_Y6_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y2    ${Z[$i]} >> $file_Y6_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y3    ${Z[$i]} >> $file_Y6_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y4    ${Z[$i]} >> $file_Y6_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y5    ${Z[$i]} >> $file_Y6_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y6    ${Z[$i]} >> $file_Y6_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y7    ${Z[$i]} >> $file_Y6_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6_Y8    ${Z[$i]} >> $file_Y6_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z1      >> $file_Y6_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z2      >> $file_Y6_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z3      >> $file_Y6_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z4      >> $file_Y6_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z5      >> $file_Y6_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z6      >> $file_Y6_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z7      >> $file_Y6_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z8      >> $file_Y6_Z8
                  echo  ${LABEL[$i]}  $X1       $Y7       ${Z[$i]} >> $file_Y7_X1
                  echo  ${LABEL[$i]}  $X2       $Y7       ${Z[$i]} >> $file_Y7_X2
                  echo  ${LABEL[$i]}  $X3       $Y7       ${Z[$i]} >> $file_Y7_X3
                  echo  ${LABEL[$i]}  $X4       $Y7       ${Z[$i]} >> $file_Y7_X4
                  echo  ${LABEL[$i]}  $X5       $Y7       ${Z[$i]} >> $file_Y7_X5
                  echo  ${LABEL[$i]}  $X6       $Y7       ${Z[$i]} >> $file_Y7_X6
                  echo  ${LABEL[$i]}  $X7       $Y7       ${Z[$i]} >> $file_Y7_X7
                  echo  ${LABEL[$i]}  $X8       $Y7       ${Z[$i]} >> $file_Y7_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7_Y1    ${Z[$i]} >> $file_Y7_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7_Y2    ${Z[$i]} >> $file_Y7_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7_Y3    ${Z[$i]} >> $file_Y7_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7_Y4    ${Z[$i]} >> $file_Y7_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7_Y5    ${Z[$i]} >> $file_Y7_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7_Y6    ${Z[$i]} >> $file_Y7_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7_Y7    ${Z[$i]} >> $file_Y7_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7_Y8    ${Z[$i]} >> $file_Y7_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z1      >> $file_Y7_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z2      >> $file_Y7_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z3      >> $file_Y7_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z4      >> $file_Y7_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z5      >> $file_Y7_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z6      >> $file_Y7_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z7      >> $file_Y7_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z8      >> $file_Y7_Z8
                  echo  ${LABEL[$i]}  $X1       $Y8       ${Z[$i]} >> $file_Y8_X1
                  echo  ${LABEL[$i]}  $X2       $Y8       ${Z[$i]} >> $file_Y8_X2
                  echo  ${LABEL[$i]}  $X3       $Y8       ${Z[$i]} >> $file_Y8_X3
                  echo  ${LABEL[$i]}  $X4       $Y8       ${Z[$i]} >> $file_Y8_X4
                  echo  ${LABEL[$i]}  $X5       $Y8       ${Z[$i]} >> $file_Y8_X5
                  echo  ${LABEL[$i]}  $X6       $Y8       ${Z[$i]} >> $file_Y8_X6
                  echo  ${LABEL[$i]}  $X7       $Y8       ${Z[$i]} >> $file_Y8_X7
                  echo  ${LABEL[$i]}  $X8       $Y8       ${Z[$i]} >> $file_Y8_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8_Y1    ${Z[$i]} >> $file_Y8_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8_Y2    ${Z[$i]} >> $file_Y8_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8_Y3    ${Z[$i]} >> $file_Y8_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8_Y4    ${Z[$i]} >> $file_Y8_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8_Y5    ${Z[$i]} >> $file_Y8_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8_Y6    ${Z[$i]} >> $file_Y8_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8_Y7    ${Z[$i]} >> $file_Y8_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8_Y8    ${Z[$i]} >> $file_Y8_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z1      >> $file_Y8_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z2      >> $file_Y8_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z3      >> $file_Y8_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z4      >> $file_Y8_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z5      >> $file_Y8_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z6      >> $file_Y8_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z7      >> $file_Y8_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z8      >> $file_Y8_Z8
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z1      >> $file_Z1_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z1      >> $file_Z1_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z1      >> $file_Z1_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z1      >> $file_Z1_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z1      >> $file_Z1_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z1      >> $file_Z1_X6
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z1      >> $file_Z1_X7
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z1      >> $file_Z1_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z1   >> $file_Z1_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z2   >> $file_Z1_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z3   >> $file_Z1_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z4   >> $file_Z1_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z5   >> $file_Z1_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z6   >> $file_Z1_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z7   >> $file_Z1_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1_Z8   >> $file_Z1_Z8
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z2      >> $file_Z2_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z2      >> $file_Z2_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z2      >> $file_Z2_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z2      >> $file_Z2_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z2      >> $file_Z2_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z2      >> $file_Z2_X6
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z2      >> $file_Z2_X7
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z2      >> $file_Z2_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z2      >> $file_Z2_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z2      >> $file_Z2_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z2      >> $file_Z2_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z2      >> $file_Z2_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z2      >> $file_Z2_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z2      >> $file_Z2_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z2      >> $file_Z2_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z2      >> $file_Z2_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z1   >> $file_Z2_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z2   >> $file_Z2_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z3   >> $file_Z2_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z4   >> $file_Z2_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z5   >> $file_Z2_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z6   >> $file_Z2_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z7   >> $file_Z2_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2_Z8   >> $file_Z2_Z8
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z3      >> $file_Z3_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z3      >> $file_Z3_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z3      >> $file_Z3_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z3      >> $file_Z3_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z3      >> $file_Z3_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z3      >> $file_Z3_X6
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z3      >> $file_Z3_X7
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z3      >> $file_Z3_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z3      >> $file_Z3_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z3      >> $file_Z3_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z3      >> $file_Z3_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z3      >> $file_Z3_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z3      >> $file_Z3_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z3      >> $file_Z3_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z3      >> $file_Z3_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z3      >> $file_Z3_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z1   >> $file_Z3_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z2   >> $file_Z3_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z3   >> $file_Z3_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z4   >> $file_Z3_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z5   >> $file_Z3_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z6   >> $file_Z3_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z7   >> $file_Z3_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3_Z8   >> $file_Z3_Z8
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z4      >> $file_Z4_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z4      >> $file_Z4_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z4      >> $file_Z4_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z4      >> $file_Z4_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z4      >> $file_Z4_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z4      >> $file_Z4_X6
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z4      >> $file_Z4_X7
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z4      >> $file_Z4_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z4      >> $file_Z4_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z4      >> $file_Z4_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z4      >> $file_Z4_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z4      >> $file_Z4_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z4      >> $file_Z4_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z4      >> $file_Z4_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z4      >> $file_Z4_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z4      >> $file_Z4_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z1   >> $file_Z4_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z2   >> $file_Z4_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z3   >> $file_Z4_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z4   >> $file_Z4_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z5   >> $file_Z4_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z6   >> $file_Z4_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z7   >> $file_Z4_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4_Z8   >> $file_Z4_Z8
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z5      >> $file_Z5_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z5      >> $file_Z5_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z5      >> $file_Z5_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z5      >> $file_Z5_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z5      >> $file_Z5_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z5      >> $file_Z5_X6
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z5      >> $file_Z5_X7
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z5      >> $file_Z5_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z5      >> $file_Z5_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z5      >> $file_Z5_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z5      >> $file_Z5_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z5      >> $file_Z5_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z5      >> $file_Z5_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z5      >> $file_Z5_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z5      >> $file_Z5_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z5      >> $file_Z5_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z1   >> $file_Z5_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z2   >> $file_Z5_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z3   >> $file_Z5_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z4   >> $file_Z5_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z5   >> $file_Z5_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z6   >> $file_Z5_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z7   >> $file_Z5_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5_Z8   >> $file_Z5_Z8
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z6      >> $file_Z6_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z6      >> $file_Z6_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z6      >> $file_Z6_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z6      >> $file_Z6_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z6      >> $file_Z6_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z6      >> $file_Z6_X6
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z6      >> $file_Z6_X7
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z6      >> $file_Z6_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z6      >> $file_Z6_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z6      >> $file_Z6_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z6      >> $file_Z6_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z6      >> $file_Z6_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z6      >> $file_Z6_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z6      >> $file_Z6_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z6      >> $file_Z6_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z6      >> $file_Z6_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z1   >> $file_Z6_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z2   >> $file_Z6_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z3   >> $file_Z6_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z4   >> $file_Z6_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z5   >> $file_Z6_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z6   >> $file_Z6_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z7   >> $file_Z6_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6_Z8   >> $file_Z6_Z8
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z7      >> $file_Z7_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z7      >> $file_Z7_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z7      >> $file_Z7_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z7      >> $file_Z7_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z7      >> $file_Z7_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z7      >> $file_Z7_X6
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z7      >> $file_Z7_X7
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z7      >> $file_Z7_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z7      >> $file_Z7_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z7      >> $file_Z7_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z7      >> $file_Z7_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z7      >> $file_Z7_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z7      >> $file_Z7_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z7      >> $file_Z7_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z7      >> $file_Z7_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z7      >> $file_Z7_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7_Z1   >> $file_Z7_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7_Z2   >> $file_Z7_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7_Z3   >> $file_Z7_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7_Z4   >> $file_Z7_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7_Z5   >> $file_Z7_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7_Z6   >> $file_Z7_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7_Z7   >> $file_Z7_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7_Z8   >> $file_Z7_Z8
                  echo  ${LABEL[$i]}  $X1       ${Y[$i]}  $Z8      >> $file_Z8_X1
                  echo  ${LABEL[$i]}  $X2       ${Y[$i]}  $Z8      >> $file_Z8_X2
                  echo  ${LABEL[$i]}  $X3       ${Y[$i]}  $Z8      >> $file_Z8_X3
                  echo  ${LABEL[$i]}  $X4       ${Y[$i]}  $Z8      >> $file_Z8_X4
                  echo  ${LABEL[$i]}  $X5       ${Y[$i]}  $Z8      >> $file_Z8_X5
                  echo  ${LABEL[$i]}  $X6       ${Y[$i]}  $Z8      >> $file_Z8_X6
                  echo  ${LABEL[$i]}  $X7       ${Y[$i]}  $Z8      >> $file_Z8_X7
                  echo  ${LABEL[$i]}  $X8       ${Y[$i]}  $Z8      >> $file_Z8_X8
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y1       $Z8      >> $file_Z8_Y1
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y2       $Z8      >> $file_Z8_Y2
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y3       $Z8      >> $file_Z8_Y3
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y4       $Z8      >> $file_Z8_Y4
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y5       $Z8      >> $file_Z8_Y5
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y6       $Z8      >> $file_Z8_Y6
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y7       $Z8      >> $file_Z8_Y7
                  echo  ${LABEL[$i]}  ${X[$i]}  $Y8       $Z8      >> $file_Z8_Y8
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8_Z1   >> $file_Z8_Z1
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8_Z2   >> $file_Z8_Z2
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8_Z3   >> $file_Z8_Z3
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8_Z4   >> $file_Z8_Z4
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8_Z5   >> $file_Z8_Z5
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8_Z6   >> $file_Z8_Z6
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8_Z7   >> $file_Z8_Z7
                  echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8_Z8   >> $file_Z8_Z8

              else

                  if [[ $index_atom1_to_distort -eq $ind ]]; then
                      # calculate
                      X1=$(echo " ${X[$i]} - 4 * $iDIST" | bc -l )
                      X2=$(echo " ${X[$i]} - 3 * $iDIST" | bc -l )
                      X3=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
                      X4=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
                      X5=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
                      X6=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
                      X7=$(echo " ${X[$i]} + 3 * $iDIST" | bc -l )
                      X8=$(echo " ${X[$i]} + 4 * $iDIST" | bc -l )
                      Y1=$(echo " ${Y[$i]} - 4 * $iDIST" | bc -l )
                      Y2=$(echo " ${Y[$i]} - 3 * $iDIST" | bc -l )
                      Y3=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
                      Y4=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
                      Y5=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
                      Y6=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
                      Y6=$(echo " ${Y[$i]} + 3 * $iDIST" | bc -l )
                      Y6=$(echo " ${Y[$i]} + 4 * $iDIST" | bc -l )
                      Z1=$(echo " ${Z[$i]} - 4 * $iDIST" | bc -l )
                      Z2=$(echo " ${Z[$i]} - 3 * $iDIST" | bc -l )
                      Z3=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
                      Z4=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
                      Z5=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
                      Z6=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
                      Z6=$(echo " ${Z[$i]} + 3 * $iDIST" | bc -l )
                      Z6=$(echo " ${Z[$i]} + 4 * $iDIST" | bc -l )

                      # write              X          Y          Z           file
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X5
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X7
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y3
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y5
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y7
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Y8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z1
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z2
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z3
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z4
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z5
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z6
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z7
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_Z8
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X5
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X6
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X7
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X8
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y3
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y4
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y5
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y6
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y7
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Y8
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z2
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z3
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z4
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z5
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z6
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z7
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_Z8
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X5
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X6
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X7
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X8
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y1
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y3
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y4
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y5
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y6
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y7
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Y8
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z1
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z3
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z4
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z5
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z6
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z7
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_Z8
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X5
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X6
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X7
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X8
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y1
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y2
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y4
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y5
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y6
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y7
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Y8
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z1
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z2
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z4
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z5
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z6
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z7
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_Z8
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X1
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X2
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X3
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X5
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X6
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X7
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X8
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y1
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y2
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y3
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y5
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y6
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y7
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Y8
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z1
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z2
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z3
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z5
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z6
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z7
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_Z8
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X1
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X2
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X3
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X4
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X6
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X7
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X8
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y1
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y2
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y3
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y4
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y6
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y7
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Y8
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z1
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z2
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z3
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z4
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z6
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z7
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_Z8
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_X1
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_X2
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_X3
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_X4
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_X5
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_X7
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_X8
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Y1
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Y2
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Y3
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Y4
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Y5
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Y6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Y7
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Y8
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Z1
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Z2
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Z3
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Z4
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Z5
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Z6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Z7
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_Z8
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_X1
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_X2
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_X3
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_X4
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_X5
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_X6
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_X8
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Y1
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Y2
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Y3
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Y4
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Y5
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Y6
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Y7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Y8
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Z1
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Z2
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Z3
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Z4
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Z5
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Z6
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Z7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Z8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_X1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_X2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_X3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_X4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_X5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_X6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_X7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Z8

                  fi
                  if [[ $index_atom2_to_distort -eq $ind ]]; then
                      # calculate
                      X1=$(echo " ${X[$i]} - 4 * $iDIST" | bc -l )
                      X2=$(echo " ${X[$i]} - 3 * $iDIST" | bc -l )
                      X3=$(echo " ${X[$i]} - 2 * $iDIST" | bc -l )
                      X4=$(echo " ${X[$i]} - 1 * $iDIST" | bc -l )
                      X5=$(echo " ${X[$i]} + 1 * $iDIST" | bc -l )
                      X6=$(echo " ${X[$i]} + 2 * $iDIST" | bc -l )
                      X7=$(echo " ${X[$i]} + 3 * $iDIST" | bc -l )
                      X8=$(echo " ${X[$i]} + 4 * $iDIST" | bc -l )
                      Y1=$(echo " ${Y[$i]} - 4 * $iDIST" | bc -l )
                      Y2=$(echo " ${Y[$i]} - 3 * $iDIST" | bc -l )
                      Y3=$(echo " ${Y[$i]} - 2 * $iDIST" | bc -l )
                      Y4=$(echo " ${Y[$i]} - 1 * $iDIST" | bc -l )
                      Y5=$(echo " ${Y[$i]} + 1 * $iDIST" | bc -l )
                      Y6=$(echo " ${Y[$i]} + 2 * $iDIST" | bc -l )
                      Y6=$(echo " ${Y[$i]} + 3 * $iDIST" | bc -l )
                      Y6=$(echo " ${Y[$i]} + 4 * $iDIST" | bc -l )
                      Z1=$(echo " ${Z[$i]} - 4 * $iDIST" | bc -l )
                      Z2=$(echo " ${Z[$i]} - 3 * $iDIST" | bc -l )
                      Z3=$(echo " ${Z[$i]} - 2 * $iDIST" | bc -l )
                      Z4=$(echo " ${Z[$i]} - 1 * $iDIST" | bc -l )
                      Z5=$(echo " ${Z[$i]} + 1 * $iDIST" | bc -l )
                      Z6=$(echo " ${Z[$i]} + 2 * $iDIST" | bc -l )
                      Z6=$(echo " ${Z[$i]} + 3 * $iDIST" | bc -l )
                      Z6=$(echo " ${Z[$i]} + 4 * $iDIST" | bc -l )
                      # write              X          Y          Z           file
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X1_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X1_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X1_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X1_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X1_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X1_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_X1_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_X1_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X1_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X1_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_X1_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_X1_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X2_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X2_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X2_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X2_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X2_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X2_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_X2_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_X2_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X2_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X2_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_X2_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_X2_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X3_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X3_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X3_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X3_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X3_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X3_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_X3_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_X3_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X3_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X3_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_X3_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_X3_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X4_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X4_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X4_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X4_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X4_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X4_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_X4_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_X4_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X4_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X4_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_X4_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_X4_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X5_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X5_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X5_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X5_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X5_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X5_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X5_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X5_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X5_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X5_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X5_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X5_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X5_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X5_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_X5_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_X5_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X5_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X5_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X5_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X5_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X5_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X5_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_X5_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_X5_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X6_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X6_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X6_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X6_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X6_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X6_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X6_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X6_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X6_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X6_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X6_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X6_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X6_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X6_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_X6_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_X6_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X6_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X6_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X6_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X6_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X6_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X6_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_X6_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_X6_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X7_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X7_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X7_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X7_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X7_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X7_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X7_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X7_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X7_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X7_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X7_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X7_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X7_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X7_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_X7_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_X7_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X7_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X7_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X7_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X7_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X7_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X7_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_X7_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_X7_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_X8_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_X8_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_X8_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_X8_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_X8_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_X8_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_X8_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_X8_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_X8_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_X8_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_X8_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_X8_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_X8_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_X8_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_X8_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_X8_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_X8_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_X8_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_X8_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_X8_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_X8_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_X8_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_X8_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_X8_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Y1_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y1_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y1_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y1_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y1_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y1_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y1_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Y1_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Y1_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Y2_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y2_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y2_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y2_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y2_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y2_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y2_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Y2_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Y2_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Y3_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y3_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y3_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y3_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y3_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y3_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y3_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Y3_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Y3_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Y4_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y4_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y4_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y4_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y4_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y4_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y4_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Y4_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Y4_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Y5_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y5_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y5_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y5_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y5_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y5_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y5_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y5_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y5_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y5_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y5_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y5_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y5_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y5_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y5_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Y5_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Y5_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Y6_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y6_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y6_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y6_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y6_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y6_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y6_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y6_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y6_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y6_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y6_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y6_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y6_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y6_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y6_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Y6_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Y6_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y7_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y7_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y7_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y7_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y7_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y7_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Y7_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Y7_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y7_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y7_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y7_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y7_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y7_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y7_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y7_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y7_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y7_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y7_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y7_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y7_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y7_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y7_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Y7_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Y7_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Y8_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Y8_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Y8_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Y8_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Y8_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Y8_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Y8_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Y8_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Y8_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Y8_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Y8_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Y8_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Y8_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Y8_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Y8_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Y8_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Y8_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Y8_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Y8_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Y8_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Y8_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Y8_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Y8_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Y8_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Z1_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z1_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z1_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z1_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z1_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z1_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z1_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Z1_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Z1_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z1_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z1_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z1_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z1_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z1_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z1_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z1_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z1_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Z2_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z2_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z2_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z2_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z2_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z2_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z2_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Z2_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Z2_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z2_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z2_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z2_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z2_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z2_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z2_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z2_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z2_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Z3_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z3_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z3_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z3_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z3_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z3_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z3_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Z3_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Z3_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z3_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z3_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z3_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z3_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z3_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z3_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z3_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z3_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Z4_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z4_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z4_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z4_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z4_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z4_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z4_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Z4_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Z4_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z4_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z4_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z4_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z4_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z4_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z4_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z4_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z4_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Z5_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z5_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z5_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z5_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z5_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z5_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z5_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Z5_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Z5_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z5_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z5_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z5_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z5_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z5_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z5_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z5_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z5_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Z6_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z6_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z6_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z6_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z6_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z6_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z6_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Z6_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Z6_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z6_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z6_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z6_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z6_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z6_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z6_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z6_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z6_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z7_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z7_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z7_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z7_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z7_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z7_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Z7_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Z7_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z7_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z7_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z7_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z7_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z7_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z7_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Z7_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Z7_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z7_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z7_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z7_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z7_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z7_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z7_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z7_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z7_Z8
                      echo  ${LABEL[$i]}  $X1       ${Y[$i]}  ${Z[$i]} >> $file_Z8_X1
                      echo  ${LABEL[$i]}  $X2       ${Y[$i]}  ${Z[$i]} >> $file_Z8_X2
                      echo  ${LABEL[$i]}  $X3       ${Y[$i]}  ${Z[$i]} >> $file_Z8_X3
                      echo  ${LABEL[$i]}  $X4       ${Y[$i]}  ${Z[$i]} >> $file_Z8_X4
                      echo  ${LABEL[$i]}  $X5       ${Y[$i]}  ${Z[$i]} >> $file_Z8_X5
                      echo  ${LABEL[$i]}  $X6       ${Y[$i]}  ${Z[$i]} >> $file_Z8_X6
                      echo  ${LABEL[$i]}  $X7       ${Y[$i]}  ${Z[$i]} >> $file_Z8_X7
                      echo  ${LABEL[$i]}  $X8       ${Y[$i]}  ${Z[$i]} >> $file_Z8_X8
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y1       ${Z[$i]} >> $file_Z8_Y1
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y2       ${Z[$i]} >> $file_Z8_Y2
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y3       ${Z[$i]} >> $file_Z8_Y3
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y4       ${Z[$i]} >> $file_Z8_Y4
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y5       ${Z[$i]} >> $file_Z8_Y5
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y6       ${Z[$i]} >> $file_Z8_Y6
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y7       ${Z[$i]} >> $file_Z8_Y7
                      echo  ${LABEL[$i]}  ${X[$i]}  $Y8       ${Z[$i]} >> $file_Z8_Y8
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z1      >> $file_Z8_Z1
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z2      >> $file_Z8_Z2
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z3      >> $file_Z8_Z3
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z4      >> $file_Z8_Z4
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z5      >> $file_Z8_Z5
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z6      >> $file_Z8_Z6
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z7      >> $file_Z8_Z7
                      echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  $Z8      >> $file_Z8_Z8
                  fi
              fi
          fi

       else
          # this section concerns all other atom which are not be distorted

          if [[ $npoints -eq 2 ]] ; then
              # write              X          Y          Z           file
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z2

          elif [[ $npoints -eq 4 ]] ; then
              # write              X          Y          Z           file
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z4

          elif [[ $npoints -eq 6 ]] ; then
              # write              X          Y          Z           file
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z6

          elif [[ $npoints -eq 8 ]] ; then
              # write              X          Y          Z           file
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X1_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X2_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X3_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X4_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X5_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X6_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X7_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_X8_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y1_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y2_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y3_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y4_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y5_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y6_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y7_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Y8_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z1_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z2_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z3_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z4_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z5_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z6_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z7_Z8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_X1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_X2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_X3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_X4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_X5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_X6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_X7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_X8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Y1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Y2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Y3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Y4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Y5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Y6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Y7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Y8
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Z1
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Z2
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Z3
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Z4
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Z5
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Z6
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Z7
              echo  ${LABEL[$i]}  ${X[$i]}  ${Y[$i]}  ${Z[$i]} >> $file_Z8_Z8

          fi
       fi

       let ind=$ind+1

    done
}

