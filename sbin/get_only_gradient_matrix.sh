#!/bin/bash

get_only_gradient_matrix() {

local ms=$1
local nroots=$2
local natoms=$3

declare -a Xmatrix=()
declare -a Ymatrix=()
declare -a Zmatrix=()
declare -a LABEL=( )
declare -a X=( )
declare -a Y=( )
declare -a Z=( )

local index=0;
local nrows=$nroots
local atom=0;

local READFILE=$CurrDir/"$Project"_alaska_ms"$ms"_init_grad.out
local WRITEFILE=$CurrDir/"$Project"_alaska_ms"$ms"_gradient_only.out

if [ ! -f $READFILE  ]; then
   echo get_full_gradient_matrix::  FILE=$READFILE does NOT exist.
   exit 9
fi
if [ -f $WRITEFILE ]; then rm -rf $WRITEFILE; fi;

# read the ALASKA_ININTIAL_GREP file
while read -a line; do
    COLS=${#line[@]}
    if [ ! -z "${line[0]}" ] ; then
        LABEL[$index]=${line[0]}
        X[$index]=${line[1]}
        Y[$index]=${line[2]}
        Z[$index]=${line[3]}
        ((index++))
    fi
done < $READFILE

echo get_only_gradient_matrix:: Number of atoms    $natoms #>> $WRITEFILE
echo get_only_gradient_matrix:: Number of roots    $nroots #>> $WRITEFILE
echo get_only_gradient_matrix:: Spin multiplicity  $ms     #>> $WRITEFILE
echo get_only_gradient_matrix:: Index              $index  #>> $WRITEFILE

for ((atom=0;atom<$natoms;atom++)) ; do

    xindex=$atom
    echo ${LABEL[$xindex]} X direction     >> $WRITEFILE

    for r1 in ${roots_in_ALASKA[@]}; do

        Xmatrix[$r1]=${X[$xindex]}
        Xau=`echo ${X[$xindex]} | sed 's/[eE]+*/\*10\^/'`
        Xcm=`echo "$Xau * 219474.7 / 0.529177210903" | bc -l` 
#echo "${LABEL[$xindex]}   1   $r1   $r1    ${Xmatrix[$r1]}    $Xcm "                   >> $WRITEFILE
        printf "%s %5d %5d %5d %22.14e %22.14f\n"  "${LABEL[$xindex]}" 1 "$r1" "$r1" "${Xmatrix[$r1]}" "$Xcm" >> $WRITEFILE
        xindex=`expr $xindex + $natoms`
    done
    echo ------------------------------    >> $WRITEFILE

    yindex=$atom
    echo ${LABEL[$yindex]} Y direction     >> $WRITEFILE

    for r1 in ${roots_in_ALASKA[@]}; do

        Ymatrix[$r1]=${Y[$yindex]}
        Yau=`echo ${Y[$yindex]} | sed 's/[eE]+*/\*10\^/'`
        Ycm=`echo "$Yau * 219474.7 / 0.529177210903" | bc -l` 
#echo "${LABEL[$yindex]}   2   $r1   $r1    ${Ymatrix[$r1]}    $Ycm "                   >> $WRITEFILE
        printf "%s %5d %5d %5d %22.14e %22.14f\n"  "${LABEL[$yindex]}" 2 "$r1" "$r1" "${Ymatrix[$r1]}" "$Ycm" >> $WRITEFILE
        yindex=`expr $yindex + $natoms`

    done
    echo ------------------------------    >> $WRITEFILE

    zindex=$atom
    echo ${LABEL[$zindex]} Z direction     >> $WRITEFILE

    for r1 in ${roots_in_ALASKA[@]}; do

        Zmatrix[$r1]=${Z[$zindex]}
        Zau=`echo ${Z[$zindex]} | sed 's/[eE]+*/\*10\^/'`
        Zcm=`echo "$Zau * 219474.7 / 0.529177210903" | bc -l` 
#echo "${LABEL[$zindex]}   3   $r1   $r1    ${Zmatrix[$r1]}    $Zcm "                   >> $WRITEFILE
        printf "%s %5d %5d %5d %22.14e %22.14f\n"  "${LABEL[$zindex]}" 3 "$r1" "$r1" "${Zmatrix[$r1]}" "$Zcm" >> $WRITEFILE
        zindex=`expr $zindex + $natoms`

    done
    echo ------------------------------    >> $WRITEFILE

done

}
