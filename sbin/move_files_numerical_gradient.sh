#!/bin/bash

function move_files_numerical_gradient() {
  # this is the function template for running MOLCAS for one single set of parameters
  # feel free to modify as you wish, to include or exclude any computation steps
  local Complex=$1
  local bas1=$2
  local bas2=$3
  local me=$4
  local el=$5
  local orb=$6
  local npoints_done=$7   # 2-points, 4-points, 7-points
  local npoints_new=$8    # 2-points, 4-points, 7-points
  local active_space="$el"in"$orb"

  echo "inside move_files_numerical_gradient::         name=" $name
  echo "inside move_files_numerical_gradient::         bas1=" $bas1
  echo "inside move_files_numerical_gradient::         bas2=" $bas2
  echo "inside move_files_numerical_gradient::           me=" $me
  echo "inside move_files_numerical_gradient:: npoints_done=" $npoints_done
  echo "inside move_files_numerical_gradient:: npoints_new =" $npoints_new

  # check previous runs:
    #--------------------------------------------------------------------------------





 #--------------------------------------------------------------------------------
 if [ -f $CurrDir/$Complex.xyz ]; then
    echo File $CurrDir/$Complex.xyz was found. Proceed to process it.
    #dos2unix $CurrDir/$Complex.xyz

    # get the number of atoms from the input XYZ file
    natoms=$(head -n 1 $Complex.xyz)
    echo "gradient_by_shifting_atoms::   natoms="$natoms

    tail -n +3 $CurrDir/$Complex.xyz > $CurrDir/coords.tmp12
    cat $CurrDir/coords.tmp12

    tail -n +3 $CurrDir/$Complex.xyz | grep -v '^$' > $CurrDir/coords.tmp
 else
    echo File $CurrDir/$Complex.xyz was NOT found. ERROR. Stop Now.
    exit 9
 fi

 cat  $CurrDir/coords.tmp
 #--------------------------------------------------------------------------------
 # get the coordinates from XYZ file
 declare -a LABEL=( )
 declare -a X=( )
 declare -a Y=( )
 declare -a Z=( )
 declare -a XYZ_files=()

 echo "Coords read from the XYZ file"
 index=0;
 while read -a line; do
    echo 'line:' ${line[0]}  ${line[1]}  ${line[2]}  ${line[3]}
    COLS=${#line[@]}
    echo COLS=$COLS
    echo index=$index
    #LABEL[$index]=${line[0]}
    LABEL[$index]=$( echo "${line[0]}" | tr '[:lower:]' '[:upper:]' ) # make uppercase
    X[$index]=${line[1]}
    Y[$index]=${line[2]}
    Z[$index]=${line[3]}
    echo 'final:' ${LABEL[$index]} ${X[$index]} ${Y[$index]} ${Z[$index]}
    ((index++))
 done < $CurrDir/coords.tmp

 rm -rf $CurrDir/coords.tmp
 rm -rf $CurrDir/coords.tmp12
 #--------------------------------------------------------------------------------



 indxATOM=0
 for iat in ${LABEL[@]} ; do
    #---------------------------------------------------------
    if   [[ ( $npoints_done -eq 2 ) && ( $npoints_new -eq 4 ) ]] ; then
       echo "moving files from 2 to 4"

       for iaxis in X Y Z; do
         P="$Complex"_dist_"$iat""$indxATOM"_"$iaxis"
         S="$bas1"_"$bas2"_cas_"$active_space"
         # SEWARD
         move $CurrDir/"$P"_2_"$S"_seward.out                              $CurrDir/"$P"_3_"$S"_seward.out
         move $CurrDir/"$P"_1_"$S"_seward.out                              $CurrDir/"$P"_2_"$S"_seward.out

         # RASSCF
         for i in ${!spinMS[*]}; do
           ms=${spinMS[$i]}
           move $CurrDir/"$P"_2_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_3_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_1_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_2_"$S"_rasscf_ms"$ms".out
           #
           move $FileDir/"$P"_2_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_3_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_2_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_3_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_2_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_3_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_2_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_3_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_1_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_2_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_1_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_2_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_1_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_2_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_1_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_2_"$S"_ms"$ms".rasscf.h5
         done
         # RASSI
         move $CurrDir/"$P"_2_"$S"_rassi_cas.out                           $CurrDir/"$P"_3_"$S"_rassi_cas.out
         move $CurrDir/"$P"_1_"$S"_rassi_cas.out                           $CurrDir/"$P"_2_"$S"_rassi_cas.out
         #
         move $FileDir/"$P"_2_"$S"_rassi_cas.h5                            $FileDir/"$P"_3_"$S"_rassi_cas.h5
         move $FileDir/"$P"_2_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_3_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_1_"$S"_rassi_cas.h5                            $FileDir/"$P"_2_"$S"_rassi_cas.h5
         move $FileDir/"$P"_1_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_2_"$S"_rassi_cas.RunFile

         # SINGLE_ANISO
         move $CurrDir/"$P"_2_"$S"_aniso_cas.out                           $CurrDir/"$P"_3_"$S"_aniso_cas.out
         move $CurrDir/"$P"_1_"$S"_aniso_cas.out                           $CurrDir/"$P"_2_"$S"_aniso_cas.out
         #
         move $FileDir/"$P"_2_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_3_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas.aniso                         $FileDir/"$P"_3_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_1_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_2_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas.aniso                         $FileDir/"$P"_2_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_2_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_2_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_2_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.png

         wait
       done


    #---------------------------------------------------------
    elif [[ ( $npoints_done -eq 2 ) && ( $npoints_new -eq 6 ) ]] ; then
       echo "moving files from 2 to 6"

       for iaxis in X Y Z; do
         P="$Complex"_dist_"$iat""$indxATOM"_"$iaxis"
         S="$bas1"_"$bas2"_cas_"$active_space"
         # SEWARD
         move $CurrDir/"$P"_2_"$S"_seward.out                              $CurrDir/"$P"_4_"$S"_seward.out
         move $CurrDir/"$P"_1_"$S"_seward.out                              $CurrDir/"$P"_3_"$S"_seward.out

         # RASSCF
         for i in ${!spinMS[*]}; do
           ms=${spinMS[$i]}
           move $CurrDir/"$P"_2_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_4_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_1_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_3_"$S"_rasscf_ms"$ms".out
           #
           move $FileDir/"$P"_2_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_4_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_2_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_4_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_2_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_4_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_2_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_4_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_1_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_3_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_1_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_3_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_1_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_3_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_1_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_3_"$S"_ms"$ms".rasscf.h5
         done
         # RASSI
         move $CurrDir/"$P"_2_"$S"_rassi_cas.out                           $CurrDir/"$P"_4_"$S"_rassi_cas.out
         move $CurrDir/"$P"_1_"$S"_rassi_cas.out                           $CurrDir/"$P"_3_"$S"_rassi_cas.out
         #
         move $FileDir/"$P"_2_"$S"_rassi_cas.h5                            $FileDir/"$P"_4_"$S"_rassi_cas.h5
         move $FileDir/"$P"_2_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_4_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_1_"$S"_rassi_cas.h5                            $FileDir/"$P"_3_"$S"_rassi_cas.h5
         move $FileDir/"$P"_1_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_3_"$S"_rassi_cas.RunFile

         # SINGLE_ANISO
         move $CurrDir/"$P"_2_"$S"_aniso_cas.out                           $CurrDir/"$P"_4_"$S"_aniso_cas.out
         move $CurrDir/"$P"_1_"$S"_aniso_cas.out                           $CurrDir/"$P"_3_"$S"_aniso_cas.out
         #
         move $FileDir/"$P"_2_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_4_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas.aniso                         $FileDir/"$P"_4_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_1_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_3_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas.aniso                         $FileDir/"$P"_3_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.png

         wait
       done


    #---------------------------------------------------------
    elif [[ ( $npoints_done -eq 2 ) && ( $npoints_new -eq 8 ) ]] ; then
       echo "moving files from 2 to 8"

       for iaxis in X Y Z; do
         P="$Complex"_dist_"$iat""$indxATOM"_"$iaxis"
         S="$bas1"_"$bas2"_cas_"$active_space"
         # SEWARD
         move $CurrDir/"$P"_2_"$S"_seward.out                              $CurrDir/"$P"_5_"$S"_seward.out
         move $CurrDir/"$P"_1_"$S"_seward.out                              $CurrDir/"$P"_4_"$S"_seward.out

         # RASSCF
         for i in ${!spinMS[*]}; do
           ms=${spinMS[$i]}
           move $CurrDir/"$P"_2_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_5_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_1_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_4_"$S"_rasscf_ms"$ms".out
           #
           move $FileDir/"$P"_2_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_5_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_2_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_5_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_2_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_5_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_2_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_5_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_1_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_4_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_1_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_4_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_1_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_4_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_1_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_4_"$S"_ms"$ms".rasscf.h5
         done
         # RASSI
         move $CurrDir/"$P"_2_"$S"_rassi_cas.out                           $CurrDir/"$P"_5_"$S"_rassi_cas.out
         move $CurrDir/"$P"_1_"$S"_rassi_cas.out                           $CurrDir/"$P"_4_"$S"_rassi_cas.out
         #
         move $FileDir/"$P"_2_"$S"_rassi_cas.h5                            $FileDir/"$P"_5_"$S"_rassi_cas.h5
         move $FileDir/"$P"_2_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_5_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_1_"$S"_rassi_cas.h5                            $FileDir/"$P"_4_"$S"_rassi_cas.h5
         move $FileDir/"$P"_1_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_4_"$S"_rassi_cas.RunFile

         # SINGLE_ANISO
         move $CurrDir/"$P"_2_"$S"_aniso_cas.out                           $CurrDir/"$P"_5_"$S"_aniso_cas.out
         move $CurrDir/"$P"_1_"$S"_aniso_cas.out                           $CurrDir/"$P"_4_"$S"_aniso_cas.out
         #
         move $FileDir/"$P"_2_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_5_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas.aniso                         $FileDir/"$P"_5_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_1_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_4_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas.aniso                         $FileDir/"$P"_4_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.png

         wait
       done


    #---------------------------------------------------------
    elif [[ ( $npoints_done -eq 4 ) && ( $npoints_new -eq 6 ) ]] ; then
       echo "moving files from 4 to 6"

       for iaxis in X Y Z; do
         P="$Complex"_dist_"$iat""$indxATOM"_"$iaxis"
         S="$bas1"_"$bas2"_cas_"$active_space"
         # SEWARD
         move $CurrDir/"$P"_4_"$S"_seward.out                              $CurrDir/"$P"_5_"$S"_seward.out
         move $CurrDir/"$P"_3_"$S"_seward.out                              $CurrDir/"$P"_4_"$S"_seward.out
         move $CurrDir/"$P"_2_"$S"_seward.out                              $CurrDir/"$P"_3_"$S"_seward.out
         move $CurrDir/"$P"_1_"$S"_seward.out                              $CurrDir/"$P"_2_"$S"_seward.out

         # RASSCF
         for i in ${!spinMS[*]}; do
           ms=${spinMS[$i]}
           move $CurrDir/"$P"_4_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_5_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_3_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_4_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_2_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_3_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_1_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_2_"$S"_rasscf_ms"$ms".out
           #
           move $FileDir/"$P"_4_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_5_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_4_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_5_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_4_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_5_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_4_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_5_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_3_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_4_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_3_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_4_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_3_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_4_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_3_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_4_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_2_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_3_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_2_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_3_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_2_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_3_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_2_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_3_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_1_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_2_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_1_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_2_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_1_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_2_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_1_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_2_"$S"_ms"$ms".rasscf.h5
         done
         # RASSI
         move $CurrDir/"$P"_4_"$S"_rassi_cas.out                           $CurrDir/"$P"_5_"$S"_rassi_cas.out
         move $CurrDir/"$P"_3_"$S"_rassi_cas.out                           $CurrDir/"$P"_4_"$S"_rassi_cas.out
         move $CurrDir/"$P"_2_"$S"_rassi_cas.out                           $CurrDir/"$P"_3_"$S"_rassi_cas.out
         move $CurrDir/"$P"_1_"$S"_rassi_cas.out                           $CurrDir/"$P"_2_"$S"_rassi_cas.out
         #
         move $FileDir/"$P"_4_"$S"_rassi_cas.h5                            $FileDir/"$P"_5_"$S"_rassi_cas.h5
         move $FileDir/"$P"_4_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_5_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_3_"$S"_rassi_cas.h5                            $FileDir/"$P"_4_"$S"_rassi_cas.h5
         move $FileDir/"$P"_3_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_4_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_2_"$S"_rassi_cas.h5                            $FileDir/"$P"_3_"$S"_rassi_cas.h5
         move $FileDir/"$P"_2_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_3_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_1_"$S"_rassi_cas.h5                            $FileDir/"$P"_2_"$S"_rassi_cas.h5
         move $FileDir/"$P"_1_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_2_"$S"_rassi_cas.RunFile

         # SINGLE_ANISO
         move $CurrDir/"$P"_4_"$S"_aniso_cas.out                           $CurrDir/"$P"_5_"$S"_aniso_cas.out
         move $CurrDir/"$P"_3_"$S"_aniso_cas.out                           $CurrDir/"$P"_4_"$S"_aniso_cas.out
         move $CurrDir/"$P"_2_"$S"_aniso_cas.out                           $CurrDir/"$P"_3_"$S"_aniso_cas.out
         move $CurrDir/"$P"_1_"$S"_aniso_cas.out                           $CurrDir/"$P"_2_"$S"_aniso_cas.out
         #
         move $FileDir/"$P"_4_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_5_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_4_"$S"_aniso_cas.aniso                         $FileDir/"$P"_5_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_3_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_4_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_3_"$S"_aniso_cas.aniso                         $FileDir/"$P"_4_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_2_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_3_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas.aniso                         $FileDir/"$P"_3_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_1_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_2_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas.aniso                         $FileDir/"$P"_2_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_2_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_2_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_2_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.png

         wait
       done


    #---------------------------------------------------------
    elif [[ ( $npoints_done -eq 4 ) && ( $npoints_new -eq 8 ) ]] ; then
       echo "moving files from 4 to 8"

       for iaxis in X Y Z; do
         P="$Complex"_dist_"$iat""$indxATOM"_"$iaxis"
         S="$bas1"_"$bas2"_cas_"$active_space"
         # SEWARD
         move $CurrDir/"$P"_4_"$S"_seward.out                              $CurrDir/"$P"_6_"$S"_seward.out
         move $CurrDir/"$P"_3_"$S"_seward.out                              $CurrDir/"$P"_5_"$S"_seward.out
         move $CurrDir/"$P"_2_"$S"_seward.out                              $CurrDir/"$P"_4_"$S"_seward.out
         move $CurrDir/"$P"_1_"$S"_seward.out                              $CurrDir/"$P"_3_"$S"_seward.out

         # RASSCF
         for i in ${!spinMS[*]}; do
           ms=${spinMS[$i]}
           move $CurrDir/"$P"_4_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_6_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_3_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_5_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_2_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_4_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_1_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_3_"$S"_rasscf_ms"$ms".out
           #
           move $FileDir/"$P"_4_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_6_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_4_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_6_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_4_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_6_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_4_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_6_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_3_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_5_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_3_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_5_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_3_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_5_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_3_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_5_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_2_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_4_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_2_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_4_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_2_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_4_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_2_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_4_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_1_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_3_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_1_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_3_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_1_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_3_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_1_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_3_"$S"_ms"$ms".rasscf.h5
         done
         # RASSI
         move $CurrDir/"$P"_4_"$S"_rassi_cas.out                           $CurrDir/"$P"_6_"$S"_rassi_cas.out
         move $CurrDir/"$P"_3_"$S"_rassi_cas.out                           $CurrDir/"$P"_5_"$S"_rassi_cas.out
         move $CurrDir/"$P"_2_"$S"_rassi_cas.out                           $CurrDir/"$P"_4_"$S"_rassi_cas.out
         move $CurrDir/"$P"_1_"$S"_rassi_cas.out                           $CurrDir/"$P"_3_"$S"_rassi_cas.out
         #
         move $FileDir/"$P"_4_"$S"_rassi_cas.h5                            $FileDir/"$P"_6_"$S"_rassi_cas.h5
         move $FileDir/"$P"_4_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_6_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_3_"$S"_rassi_cas.h5                            $FileDir/"$P"_5_"$S"_rassi_cas.h5
         move $FileDir/"$P"_3_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_5_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_2_"$S"_rassi_cas.h5                            $FileDir/"$P"_4_"$S"_rassi_cas.h5
         move $FileDir/"$P"_2_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_4_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_1_"$S"_rassi_cas.h5                            $FileDir/"$P"_3_"$S"_rassi_cas.h5
         move $FileDir/"$P"_1_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_3_"$S"_rassi_cas.RunFile

         # SINGLE_ANISO
         move $CurrDir/"$P"_4_"$S"_aniso_cas.out                           $CurrDir/"$P"_6_"$S"_aniso_cas.out
         move $CurrDir/"$P"_3_"$S"_aniso_cas.out                           $CurrDir/"$P"_5_"$S"_aniso_cas.out
         move $CurrDir/"$P"_2_"$S"_aniso_cas.out                           $CurrDir/"$P"_4_"$S"_aniso_cas.out
         move $CurrDir/"$P"_1_"$S"_aniso_cas.out                           $CurrDir/"$P"_3_"$S"_aniso_cas.out
         #
         move $FileDir/"$P"_4_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_6_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_4_"$S"_aniso_cas.aniso                         $FileDir/"$P"_6_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_6_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_6_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_6_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_6_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_6_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_6_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_3_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_5_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_3_"$S"_aniso_cas.aniso                         $FileDir/"$P"_5_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_2_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_4_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas.aniso                         $FileDir/"$P"_4_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_1_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_3_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas.aniso                         $FileDir/"$P"_3_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.png

         wait
       done


    #---------------------------------------------------------
    elif [[ ( $npoints_done -eq 6 ) && ( $npoints_new -eq 8 ) ]] ; then
       echo "moving files from 6 to 8"

       for iaxis in X Y Z; do
         P="$Complex"_dist_"$iat""$indxATOM"_"$iaxis"
         S="$bas1"_"$bas2"_cas_"$active_space"
         # SEWARD
         move $CurrDir/"$P"_6_"$S"_seward.out                              $CurrDir/"$P"_7_"$S"_seward.out
         move $CurrDir/"$P"_5_"$S"_seward.out                              $CurrDir/"$P"_6_"$S"_seward.out
         move $CurrDir/"$P"_4_"$S"_seward.out                              $CurrDir/"$P"_5_"$S"_seward.out
         move $CurrDir/"$P"_3_"$S"_seward.out                              $CurrDir/"$P"_4_"$S"_seward.out
         move $CurrDir/"$P"_2_"$S"_seward.out                              $CurrDir/"$P"_3_"$S"_seward.out
         move $CurrDir/"$P"_1_"$S"_seward.out                              $CurrDir/"$P"_2_"$S"_seward.out

         # RASSCF
         for i in ${!spinMS[*]}; do
           ms=${spinMS[$i]}
           move $CurrDir/"$P"_6_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_7_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_5_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_6_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_4_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_5_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_3_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_4_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_2_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_3_"$S"_rasscf_ms"$ms".out
           move $CurrDir/"$P"_1_"$S"_rasscf_ms"$ms".out                    $CurrDir/"$P"_2_"$S"_rasscf_ms"$ms".out
           #
           move $FileDir/"$P"_6_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_7_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_6_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_7_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_6_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_7_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_6_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_7_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_5_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_6_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_5_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_6_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_5_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_6_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_5_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_6_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_4_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_5_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_4_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_5_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_4_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_5_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_4_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_5_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_3_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_4_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_3_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_4_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_3_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_4_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_3_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_4_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_2_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_3_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_2_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_3_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_2_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_3_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_2_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_3_"$S"_ms"$ms".rasscf.h5
           #
           move $FileDir/"$P"_1_"$S"_ms"$ms".JobIph                        $FileDir/"$P"_2_"$S"_ms"$ms".JobIph
           move $FileDir/"$P"_1_"$S"_ms"$ms".RunFile                       $FileDir/"$P"_2_"$S"_ms"$ms".RunFile
           move $FileDir/"$P"_1_"$S"_ms"$ms".RasOrb                        $FileDir/"$P"_2_"$S"_ms"$ms".RasOrb
           move $FileDir/"$P"_1_"$S"_ms"$ms".rasscf.h5                     $FileDir/"$P"_2_"$S"_ms"$ms".rasscf.h5
         done
         # RASSI
         move $CurrDir/"$P"_6_"$S"_rassi_cas.out                           $CurrDir/"$P"_7_"$S"_rassi_cas.out
         move $CurrDir/"$P"_5_"$S"_rassi_cas.out                           $CurrDir/"$P"_6_"$S"_rassi_cas.out
         move $CurrDir/"$P"_4_"$S"_rassi_cas.out                           $CurrDir/"$P"_5_"$S"_rassi_cas.out
         move $CurrDir/"$P"_3_"$S"_rassi_cas.out                           $CurrDir/"$P"_4_"$S"_rassi_cas.out
         move $CurrDir/"$P"_2_"$S"_rassi_cas.out                           $CurrDir/"$P"_3_"$S"_rassi_cas.out
         move $CurrDir/"$P"_1_"$S"_rassi_cas.out                           $CurrDir/"$P"_2_"$S"_rassi_cas.out
         #
         move $FileDir/"$P"_6_"$S"_rassi_cas.h5                            $FileDir/"$P"_7_"$S"_rassi_cas.h5
         move $FileDir/"$P"_6_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_7_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_5_"$S"_rassi_cas.h5                            $FileDir/"$P"_6_"$S"_rassi_cas.h5
         move $FileDir/"$P"_5_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_6_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_4_"$S"_rassi_cas.h5                            $FileDir/"$P"_5_"$S"_rassi_cas.h5
         move $FileDir/"$P"_4_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_5_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_3_"$S"_rassi_cas.h5                            $FileDir/"$P"_4_"$S"_rassi_cas.h5
         move $FileDir/"$P"_3_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_4_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_2_"$S"_rassi_cas.h5                            $FileDir/"$P"_3_"$S"_rassi_cas.h5
         move $FileDir/"$P"_2_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_3_"$S"_rassi_cas.RunFile
         move $FileDir/"$P"_1_"$S"_rassi_cas.h5                            $FileDir/"$P"_2_"$S"_rassi_cas.h5
         move $FileDir/"$P"_1_"$S"_rassi_cas.RunFile                       $FileDir/"$P"_2_"$S"_rassi_cas.RunFile

         # SINGLE_ANISO
         move $CurrDir/"$P"_6_"$S"_aniso_cas.out                           $CurrDir/"$P"_7_"$S"_aniso_cas.out
         move $CurrDir/"$P"_5_"$S"_aniso_cas.out                           $CurrDir/"$P"_6_"$S"_aniso_cas.out
         move $CurrDir/"$P"_4_"$S"_aniso_cas.out                           $CurrDir/"$P"_5_"$S"_aniso_cas.out
         move $CurrDir/"$P"_3_"$S"_aniso_cas.out                           $CurrDir/"$P"_4_"$S"_aniso_cas.out
         move $CurrDir/"$P"_2_"$S"_aniso_cas.out                           $CurrDir/"$P"_3_"$S"_aniso_cas.out
         move $CurrDir/"$P"_1_"$S"_aniso_cas.out                           $CurrDir/"$P"_2_"$S"_aniso_cas.out
         #
         move $FileDir/"$P"_6_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_7_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_6_"$S"_aniso_cas.aniso                         $FileDir/"$P"_7_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_7_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_7_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_7_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_7_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_6_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_7_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_6_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_7_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_6_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_7_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_6_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_7_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_6_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_7_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_6_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_7_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_7_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_7_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_7_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_7_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_7_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_7_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_5_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_6_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_5_"$S"_aniso_cas.aniso                         $FileDir/"$P"_6_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_6_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_5_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_6_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_5_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_6_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_5_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_6_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_6_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_6_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_6_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_6_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_4_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_5_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_4_"$S"_aniso_cas.aniso                         $FileDir/"$P"_5_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_5_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_5_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_5_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_5_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_3_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_4_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_3_"$S"_aniso_cas.aniso                         $FileDir/"$P"_4_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_4_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_4_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_4_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_4_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_2_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_3_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas.aniso                         $FileDir/"$P"_3_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_3_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_3_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_3_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_3_"$S"_aniso_cas_XT_with_field_M_over_H.png
         #
         move $FileDir/"$P"_1_"$S"_aniso_cas.old.aniso                     $FileDir/"$P"_2_"$S"_aniso_cas.old.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas.aniso                         $FileDir/"$P"_2_"$S"_aniso_cas.aniso
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_ENE.dat               $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_ENE.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER_TME.dat               $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER_TME.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.plt                   $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_BARRIER.png                   $FileDir/"$P"_2_"$S"_aniso_cas_BARRIER.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.dat                        $FileDir/"$P"_2_"$S"_aniso_cas_MH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.plt                        $FileDir/"$P"_2_"$S"_aniso_cas_MH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_MH.png                        $FileDir/"$P"_2_"$S"_aniso_cas_MH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.dat               $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.plt               $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_no_field.png               $FileDir/"$P"_2_"$S"_aniso_cas_XT_no_field.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat  $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt  $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_dM_over_dH.png  $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_dM_over_dH.png
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.dat    $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.dat
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.plt    $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.plt
         move $FileDir/"$P"_1_"$S"_aniso_cas_XT_with_field_M_over_H.png    $FileDir/"$P"_2_"$S"_aniso_cas_XT_with_field_M_over_H.png

         wait
       done


    #---------------------------------------------------------
    else
       echo "not clear the task"
    fi

    let indxATOM=$indxATOM+1

 done

}






#        indxATOM=0;
#        for iat in ${LABEL[@]} ; do
#
#          # delete the unnecessary large files. They might clutter the disc space
##           for iaxis in X Y Z; do
#               for idist in 1 2 3 4 5 6; do
#                   # RASSCF
##                   for i in ${!spinMS[*]}; do
#                       roots=${rootsCASSCF[$i]}
#                       ms=${spinMS[$i]}
#                       echo "calling run_rasscf for ms="$ms
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".RunFile
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".RasOrb
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".rasscf.h5
#                   done
#                   # RASSI
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.h5
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.RunFile
#                   # SINGLE_ANISO
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER_ENE.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER_TME.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.png
#                   # other files
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist".xyz
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist".txt
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
#               done
#           done
#
#
#           let indxATOM=$indxATOM+1
#        done




