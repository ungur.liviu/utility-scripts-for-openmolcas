#!/bin/bash


function hessian_by_shifting_atoms() {
     # this is the function template for running MOLCAS for one single set of parameters
     # feel free to modify as you wish, to include or exclude any computation steps
     local Complex=$1
     local bas1=$2
     local bas2=$3
     local me=$4
     local el=$5
     local orb=$6
     local npoints=$7   # 2-points, 4-points, 6-points, 8-points
     local active_space="$el"in"$orb"

     echo "inside hessian_by_shifting_atoms:: npoints=" $npoints


    if [ -f $CurrDir/$Complex.xyz ]; then
       echo File $CurrDir/$Complex.xyz was found. Proceed to process it.
       #dos2unix $CurrDir/$Complex.xyz

       # get the number of atoms from the input XYZ file
       natoms=$(head -n 1 $Complex.xyz)
       echo "hessian_by_shifting_atoms::   natoms="$natoms

       tail -n +3 $CurrDir/$Complex.xyz > $CurrDir/coords.tmp12
       cat $CurrDir/coords.tmp12

       tail -n +3 $CurrDir/$Complex.xyz | grep -v '^$' > $CurrDir/coords.tmp
    else
       echo File $CurrDir/$Complex.xyz was NOT found. ERROR. Stop Now.
       exit 9
    fi

    cat  $CurrDir/coords.tmp
    #--------------------------------------------------------------------------------
    # get the coordinates from XYZ file
    declare -a LABEL=( )
    declare -a X=( )
    declare -a Y=( )
    declare -a Z=( )
    declare -a XYZ_files=()

    echo "Coords read from the XYZ file"
    index=0;
    while read -a line; do
       echo 'line:' ${line[0]}  ${line[1]}  ${line[2]}  ${line[3]}
       COLS=${#line[@]}
       echo COLS=$COLS
       echo index=$index
       #LABEL[$index]=${line[0]}
       LABEL[$index]=$( echo "${line[0]}" | tr '[:lower:]' '[:upper:]' ) # make uppercase
       X[$index]=${line[1]}
       Y[$index]=${line[2]}
       Z[$index]=${line[3]}
       echo 'final:' ${LABEL[$index]} ${X[$index]} ${Y[$index]} ${Z[$index]}
       ((index++))
    done < $CurrDir/coords.tmp

    rm -rf $CurrDir/coords.tmp
    rm -rf $CurrDir/coords.tmp12

#--------------------------------------------------------------------------------
# create an array specifying the BAS1 and BAS2 for each atom, in the non-distorted
# geometry. This array will be further used by "my_molcas_run" for each of th distorted
# structures
    #--------------------------------------
    me=$( echo "$metal" | tr '[:lower:]' '[:upper:]' ) # make uppercase
    for ((i=0;i<$natoms;i++)); do
        label=${LABEL[$i]}
        echo label=$label
        if [ "${LABEL[$i]}" == "$me" ]; then
           x=${X[$i]}
           y=${Y[$i]}
           z=${Z[$i]}
        fi
    done
    echo X_me=$x  Y_me=$y  Z_me=$z
    #-------------------------------------
    echo "hessian_by_shifting_atoms::   bas1       ="$bas1
    echo "hessian_by_shifting_atoms::   bas2       ="$bas2
    echo "hessian_by_shifting_atoms::   cutoff_dist="$cutoff_dist
    declare -a BASIS_LBL=( );
    for ((atom1=0;atom1<$natoms;atom1++)); do
       for ((atom2=0;atom2<$natoms;atom2++)); do
          #index=$(bc -l <<<"$atom + 1")
          xdiff=$(bc -l <<<"${X[$atom]} - $x")
          ydiff=$(bc -l <<<"${Y[$atom]} - $y")
          zdiff=$(bc -l <<<"${Z[$atom]} - $z")
          distance=$(bc -l <<< "sqrt($xdiff*$xdiff + $ydiff*$ydiff + $zdiff*$zdiff)")
          if [[ 1 -eq "$( echo "${distance} < ${cutoff_dist}" | bc -l )" ]]; then
                echo atom distance bas1 = $atom $distance
                BASIS_LBL[$atom]=$bas1
          else
                echo atom distance bas2 = $atom $distance
                BASIS_LBL[$atom]=$bas2
          fi
       done
    done

    for ((atom=0;atom<$natoms;atom++)); do
        echo 'BASIS_LBL:' ${LABEL[$atom]}  $atom  ${BASIS_LBL[$atom]}
    done

#--------------------------------------------------------------------------------
# run_molcas for each individual atom displacements:
     dist="0.005"  # in Bohr, a.u.

     indxATOM1=0
     for iat in ${LABEL[@]} ; do
        indxATOM2=0
        for jat in ${LABEL[@]} ; do
           #---------------------------------------------------------
           if   [[ $npoints -eq 2 ]] ; then
              # standard numerical second derivatives
              # generate 6*6*natoms*natoms XYZ files
              echo "calling generate_distorted_xyz_hessian function, 2 points"
              generate_distorted_xyz_hessian $Complex $iat $indxATOM1 $jat $indxATOM2 $npoints $dist
           elif [[ $npoints -eq 4 ]] ; then
              # standard numerical derivative  4 points
              # generate 12*12*natoms*natoms XYZ files
              echo "calling generate_distorted_xyz_hessian function, 4 points"
              generate_distorted_xyz_hessian $Complex $iat $indxATOM1 $jat $indxATOM2 $npoints $dist
           elif [[ $npoints -eq 6 ]] ; then
              # standard numerical derivative  6 points
              # generate 18*18*natoms*natoms  XYZ files
              echo "calling generate_distorted_xyz_hessian function, 6 points"
              generate_distorted_xyz_hessian $Complex $iat $indxATOM1 $jat $indxATOM2 $npoints $dist
           elif [[ $npoints -eq 8 ]] ; then
              # standard numerical derivative  8 points
              # generate 24*24*natoms*natoms XYZ files
              echo "calling generate_distorted_xyz_hessian function, 8 points"
              generate_distorted_xyz_hessian $Complex $iat $indxATOM1 $jat $indxATOM2 $npoints $dist
           else
              echo "the npoints must be 2, 4, 6 or 8. Current value  npoints="$npoints
           fi
           let indxATOM2=$indxATOM2+1
        done
        let indxATOM1=$indxATOM1+1
     done


     exit

     #---------------------------------------------------------------------------
     # all XYZ containing distortions are generated. Now proceed to run them,
     # $number_of_procs_for_numerical_gradient  molcas calculations at a time.
     number_of_molcas_calculations=$( echo "3*$natoms*$npoints" | bc -l )
     echo "number_of_molcas_calculations="$number_of_molcas_calculations
     echo "number_of_procs_for_numerical_gradient="$number_of_procs_for_numerical_gradient

     if   [[ $npoints -eq 2 ]] ; then
        input_list_1=`ls "$Complex"_dist_*_1.xyz`
        input_list_2=`ls "$Complex"_dist_*_2.xyz`

        echo LIST-1 = $input_list_1
        echo " --- "
        echo LIST-2 = $input_list_2
        echo " --- "

        c=0;
        for i in $input_list_1  $input_list_2  ; do
          j=${i%.xyz}
          echo j=$j
          export do_restart='CIRESTART'
          export clean_workdir='true'

          for is in ${!spinMS[*]}; do
            ms=${spinMS[$is]}
                 link $FileDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph  $FileDir/"$j"_"$bas1"_"$bas2"_ms"$ms".JOBOLD
            echo LINK $FileDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph  $FileDir/"$j"_"$bas1"_"$bas2"_ms"$ms".JOBOLD
          done

          my_molcas_run  $j  $bas1  $bas2   $metal  $el $orb  &
          let c=$c+1;
          rem=$(( $c % $number_of_procs_for_numerical_gradient ))
          if [ "$rem" -eq 0 ]; then
             wait
          fi

        done
        #--------------------------------------
        wait
        sleep 2

        compute_numerical_gradient_SFS
        wait
        compute_numerical_gradient_SOS
        wait

        if [ "$metal" == "sc" ] || [ "$metal" == "ti" ] || [ "$metal" == "v"  ] || [ "$metal" == "cr" ] || [ "$metal" == "mn" ] || \
           [ "$metal" == "fe" ] || [ "$metal" == "co" ] || [ "$metal" == "ni" ] || [ "$metal" == "cu" ] || \
           [ "$metal" == "ce" ] || [ "$metal" == "pr" ] || [ "$metal" == "nd" ] || [ "$metal" == "pm" ] || [ "$metal" == "sm" ] || \
           [ "$metal" == "eu" ] || [ "$metal" == "gd" ] || [ "$metal" == "tb" ] || [ "$metal" == "dy" ] || [ "$metal" == "ho" ] || \
           [ "$metal" == "er" ] || [ "$metal" == "tm" ] || [ "$metal" == "yb" ] || \
           [ "$metal" == "pa" ] || [ "$metal" == "u"  ] || [ "$metal" == "np" ] || [ "$metal" == "pu" ] || [ "$metal" == "am" ] || \
           [ "$metal" == "cm" ] || [ "$metal" == "bk" ] || [ "$metal" == "cf" ] || [ "$metal" == "es" ] || [ "$metal" == "fm" ] || \
           [ "$metal" == "md" ] || [ "$metal" == "no" ] || [ "$metal" == "lr" ] ; then
           compute_numerical_gradient_L_Bkq
           wait
        fi
        if [ "$metal" == "ce" ] || [ "$metal" == "pr" ] || [ "$metal" == "nd" ] || [ "$metal" == "pm" ] || [ "$metal" == "sm" ] || \
           [ "$metal" == "eu" ] || [ "$metal" == "gd" ] || [ "$metal" == "tb" ] || [ "$metal" == "dy" ] || [ "$metal" == "ho" ] || \
           [ "$metal" == "er" ] || [ "$metal" == "tm" ] || [ "$metal" == "yb" ] || \
           [ "$metal" == "pa" ] || [ "$metal" == "u"  ] || [ "$metal" == "np" ] || [ "$metal" == "pu" ] || [ "$metal" == "am" ] || \
           [ "$metal" == "cm" ] || [ "$metal" == "bk" ] || [ "$metal" == "cf" ] || [ "$metal" == "es" ] || [ "$metal" == "fm" ] || \
           [ "$metal" == "md" ] || [ "$metal" == "no" ] || [ "$metal" == "lr" ] ; then
           compute_numerical_gradient_J_Bkq
           wait
        fi

        # get the gradient of D and E, if pseudospin > 1, multiplet 1


        if [ "$metal" == "ce" ] || [ "$metal" == "pr" ] || [ "$metal" == "nd" ] || [ "$metal" == "pm" ] || [ "$metal" == "sm" ] || \
           [ "$metal" == "eu" ] || [ "$metal" == "gd" ] || [ "$metal" == "tb" ] || [ "$metal" == "dy" ] || [ "$metal" == "ho" ] || \
           [ "$metal" == "er" ] || [ "$metal" == "tm" ] || [ "$metal" == "yb" ] || \
           [ "$metal" == "pa" ] || [ "$metal" == "u"  ] || [ "$metal" == "np" ] || [ "$metal" == "pu" ] || [ "$metal" == "am" ] || \
           [ "$metal" == "cm" ] || [ "$metal" == "bk" ] || [ "$metal" == "cf" ] || [ "$metal" == "es" ] || [ "$metal" == "fm" ] || \
           [ "$metal" == "md" ] || [ "$metal" == "no" ] || [ "$metal" == "lr" ] ; then
           wait
        fi

        metal_up=$( echo "$metal" | tr '[:lower:]' '[:upper:]' ) # make uppercase
        echo metal_up=$metal_up
        indxATOM=0
        for iat in ${LABEL[@]} ; do
            if [ "${LABEL[$iat]}" == "$metal_up" ] ; then
                file="$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out
            fi
            ((indxATOM++))
        done
        echo hessian_by_shifting_atoms:: file=$file

        termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$file" | colrm 1 86 | sed 's/)//' | tr -d ' '`
        echo hessian_by_shifting_atoms:: termS=$termS
        if [ "$termS" != "1/2" ] ; then
           echo calling compute_numerical_gradient_ZFS_D_E
           compute_numerical_gradient_ZFS_D_E
           compute_numerical_gradient_S_Bkq
        fi

#        indxATOM=0;
#        for iat in ${LABEL[@]} ; do
#           for iaxis in X Y Z; do
#               for idist in 1 2; do
#                   grep :: $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.out | grep ' RASSI State'    | colrm 1 42 >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist".txt
#                   grep :: $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.out | grep ' SO-RASSI State' | colrm 1 45 >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
#                   # grep CFP from SINGLE_ANISO output, Bkq, using Stevens parameterisation
#                   # keep the same order as in ANISO
#                   grep -A200 'CALCULATION OF CRYSTAL-FIELD PARAMETERS OF THE GROUND ATOMIC TERM' \
#                         $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A40 'Extended Stevens Operators' | grep -A40  '2 |  -2' | colrm 48 | grep -v '\-\-\-\-\-' | colrm 1 2 | colrm 3 4 | colrm 6 21 | sed '28,1000d' >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist"_CFP_L.txt
#
#
#                   for ik in $( seq 2 2 $kmax ) ; do       # list even numbers from 2 till $kmax
#                       for iq in  $( seq -$ik $ik ) ; do   # list all range from -$ik till +$ik, step 1
#                          echo ik= $ik    iq=$iq
#                           # grep Bkq from aniso outputs of all distortions  ${DISTORTIONS[@]}
#                           # compute the derivative by n-points stencil formula
#                           # save (append) the results in a file
#                       done
#                   done
#               done
#           done
#
#
#
#

#           # delete the unnecessary large files. They might clutter the disc space
#           for iaxis in X Y Z ; do
#               for idist in 1 2 ; do
#                   # RASSCF
#                   for i in ${!spinMS[*]}; do
#                       roots=${rootsCASSCF[$i]}
#                       ms=${spinMS[$i]}
#                       echo "calling run_rasscf for ms="$ms
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".RunFile
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".RasOrb
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".rasscf.h5
#                   done
#                   # RASSI
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.h5
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.RunFile
#                   # SINGLE_ANISO
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER_ENE.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER_TME.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.png
#                   # other files
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist".xyz
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist".txt
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
#               done
#           done
#
#           let indxATOM=$indxATOM+1 ;
#        done
#======================================================================================================









     elif [[ $npoints -eq 4 ]] ; then
        input_list_1=`ls "$Complex"_dist_*_1.xyz`
        input_list_2=`ls "$Complex"_dist_*_2.xyz`
        input_list_3=`ls "$Complex"_dist_*_3.xyz`
        input_list_4=`ls "$Complex"_dist_*_4.xyz`

        echo LIST-1 = $input_list_1
        echo " --- "
        echo LIST-2 = $input_list_2
        echo " --- "
        echo LIST-3 = $input_list_3
        echo " --- "
        echo LIST-4 = $input_list_4
        echo " --- "


        c=0;
        for i in $input_list_1 $input_list_4   $input_list_2 $input_list_3 ; do
          j=${i%.xyz}
          echo j=$j
          export do_restart='CIRESTART'
          export clean_workdir='true'

          for is in ${!spinMS[*]}; do
            ms=${spinMS[$is]}
                 link $FileDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph  $FileDir/"$j"_"$bas1"_"$bas2"_ms"$ms".JOBOLD
            echo LINK $FileDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph  $FileDir/"$j"_"$bas1"_"$bas2"_ms"$ms".JOBOLD
          done

          my_molcas_run  $j  $bas1  $bas2   $metal  $el $orb &
          let c=$c+1;
          rem=$(( $c % $number_of_procs_for_numerical_gradient ))
          if [ "$rem" -eq 0 ]; then
             wait
          fi

        done
        #--------------------------------------
        wait
        sleep 2

        compute_numerical_gradient_SFS
        wait
        compute_numerical_gradient_SOS
        wait
        if [ "$metal" == "sc" ] || [ "$metal" == "ti" ] || [ "$metal" == "v"  ] || [ "$metal" == "cr" ] || [ "$metal" == "mn" ] || \
           [ "$metal" == "fe" ] || [ "$metal" == "co" ] || [ "$metal" == "ni" ] || [ "$metal" == "cu" ] || \
           [ "$metal" == "ce" ] || [ "$metal" == "pr" ] || [ "$metal" == "nd" ] || [ "$metal" == "pm" ] || [ "$metal" == "sm" ] || \
           [ "$metal" == "eu" ] || [ "$metal" == "gd" ] || [ "$metal" == "tb" ] || [ "$metal" == "dy" ] || [ "$metal" == "ho" ] || \
           [ "$metal" == "er" ] || [ "$metal" == "tm" ] || [ "$metal" == "yb" ] || \
           [ "$metal" == "pa" ] || [ "$metal" == "u"  ] || [ "$metal" == "np" ] || [ "$metal" == "pu" ] || [ "$metal" == "am" ] || \
           [ "$metal" == "cm" ] || [ "$metal" == "bk" ] || [ "$metal" == "cf" ] || [ "$metal" == "es" ] || [ "$metal" == "fm" ] || \
           [ "$metal" == "md" ] || [ "$metal" == "no" ] || [ "$metal" == "lr" ] ; then
           compute_numerical_gradient_L_Bkq
           wait
        fi
        if [ "$metal" == "ce" ] || [ "$metal" == "pr" ] || [ "$metal" == "nd" ] || [ "$metal" == "pm" ] || [ "$metal" == "sm" ] || \
           [ "$metal" == "eu" ] || [ "$metal" == "gd" ] || [ "$metal" == "tb" ] || [ "$metal" == "dy" ] || [ "$metal" == "ho" ] || \
           [ "$metal" == "er" ] || [ "$metal" == "tm" ] || [ "$metal" == "yb" ] || \
           [ "$metal" == "pa" ] || [ "$metal" == "u"  ] || [ "$metal" == "np" ] || [ "$metal" == "pu" ] || [ "$metal" == "am" ] || \
           [ "$metal" == "cm" ] || [ "$metal" == "bk" ] || [ "$metal" == "cf" ] || [ "$metal" == "es" ] || [ "$metal" == "fm" ] || \
           [ "$metal" == "md" ] || [ "$metal" == "no" ] || [ "$metal" == "lr" ] ; then
           compute_numerical_gradient_J_Bkq
           wait
        fi
#        termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | colrm 1 86 | sed 's/)//' | tr -d ' '`
#        if [ "$termS" != "1/2" ] ; then
#           compute_numerical_gradient_ZFS_D_E
#        fi


        metal_up=$( echo "$metal" | tr '[:lower:]' '[:upper:]' ) # make uppercase
        echo metal_up=$metal_up
        indxATOM=0
        for iat in ${LABEL[@]} ; do
            if [ "${LABEL[$iat]}" == "$metal_up" ] ; then
                file="$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out
            fi
            ((indxATOM++))
        done
        echo hessian_by_shifting_atoms:: file=$file

        termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$file" | colrm 1 86 | sed 's/)//' | tr -d ' '`
        echo hessian_by_shifting_atoms:: termS=$termS
        if [ "$termS" != "1/2" ] ; then
           echo calling compute_numerical_gradient_ZFS_D_E
           compute_numerical_gradient_ZFS_D_E
           compute_numerical_gradient_S_Bkq
        fi


#        indxATOM=0;
#        for iat in ${LABEL[@]} ; do
#
#           # delete the unnecessary large files. They might clutter the disc space
#           for iaxis in X Y Z; do
#               for idist in 1 2 3 4; do
#                   # RASSCF
#                   for i in ${!spinMS[*]}; do
#                       roots=${rootsCASSCF[$i]}
#                       ms=${spinMS[$i]}
#                       echo "calling run_rasscf for ms="$ms
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".RunFile
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".RasOrb
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".rasscf.h5
#                   done
#                   # RASSI
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.h5
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.RunFile
#                   # SINGLE_ANISO
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER_ENE.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER_TME.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.png
#                   # other files
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist".xyz
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist".txt
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
#               done
#           done
#
#
#           let indxATOM=$indxATOM+1 ;
#        done





















     elif [[ $npoints -eq 6 ]] ; then
        input_list_1=`ls "$Complex"_dist_*_1.xyz`
        input_list_2=`ls "$Complex"_dist_*_2.xyz`
        input_list_3=`ls "$Complex"_dist_*_3.xyz`
        input_list_4=`ls "$Complex"_dist_*_4.xyz`
        input_list_5=`ls "$Complex"_dist_*_5.xyz`
        input_list_6=`ls "$Complex"_dist_*_6.xyz`

        echo LIST-1 = $input_list_1
        echo " --- "
        echo LIST-2 = $input_list_2
        echo " --- "
        echo LIST-3 = $input_list_3
        echo " --- "
        echo LIST-4 = $input_list_4
        echo " --- "
        echo LIST-5 = $input_list_5
        echo " --- "
        echo LIST-6 = $input_list_6
        echo " --- "

        c=0;
        for i in $input_list_1 $input_list_6   $input_list_2 $input_list_5   $input_list_3 $input_list_4 ; do
          j=${i%.xyz}
          echo j=$j
          export do_restart='CIRESTART'
          export clean_workdir='true'

          for is in ${!spinMS[*]}; do
            ms=${spinMS[$is]}
                 link $FileDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph  $FileDir/"$j"_"$bas1"_"$bas2"_ms"$ms".JOBOLD
            echo LINK $FileDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph  $FileDir/"$j"_"$bas1"_"$bas2"_ms"$ms".JOBOLD
          done

          my_molcas_run  $j  $bas1  $bas2   $metal  $el $orb &

          let c=$c+1;
          rem=$(( $c % $number_of_procs_for_numerical_gradient ))
          if [ "$rem" -eq 0 ]; then
             wait
          fi
        done
        #--------------------------------------
        wait
        sleep 2

        compute_numerical_gradient_SFS
        wait
        compute_numerical_gradient_SOS
        wait
        if [ "$metal" == "sc" ] || [ "$metal" == "ti" ] || [ "$metal" == "v"  ] || [ "$metal" == "cr" ] || [ "$metal" == "mn" ] || \
           [ "$metal" == "fe" ] || [ "$metal" == "co" ] || [ "$metal" == "ni" ] || [ "$metal" == "cu" ] || \
           [ "$metal" == "ce" ] || [ "$metal" == "pr" ] || [ "$metal" == "nd" ] || [ "$metal" == "pm" ] || [ "$metal" == "sm" ] || \
           [ "$metal" == "eu" ] || [ "$metal" == "gd" ] || [ "$metal" == "tb" ] || [ "$metal" == "dy" ] || [ "$metal" == "ho" ] || \
           [ "$metal" == "er" ] || [ "$metal" == "tm" ] || [ "$metal" == "yb" ] || \
           [ "$metal" == "pa" ] || [ "$metal" == "u"  ] || [ "$metal" == "np" ] || [ "$metal" == "pu" ] || [ "$metal" == "am" ] || \
           [ "$metal" == "cm" ] || [ "$metal" == "bk" ] || [ "$metal" == "cf" ] || [ "$metal" == "es" ] || [ "$metal" == "fm" ] || \
           [ "$metal" == "md" ] || [ "$metal" == "no" ] || [ "$metal" == "lr" ] ; then
           compute_numerical_gradient_L_Bkq
           wait
        fi
        if [ "$metal" == "ce" ] || [ "$metal" == "pr" ] || [ "$metal" == "nd" ] || [ "$metal" == "pm" ] || [ "$metal" == "sm" ] || \
           [ "$metal" == "eu" ] || [ "$metal" == "gd" ] || [ "$metal" == "tb" ] || [ "$metal" == "dy" ] || [ "$metal" == "ho" ] || \
           [ "$metal" == "er" ] || [ "$metal" == "tm" ] || [ "$metal" == "yb" ] || \
           [ "$metal" == "pa" ] || [ "$metal" == "u"  ] || [ "$metal" == "np" ] || [ "$metal" == "pu" ] || [ "$metal" == "am" ] || \
           [ "$metal" == "cm" ] || [ "$metal" == "bk" ] || [ "$metal" == "cf" ] || [ "$metal" == "es" ] || [ "$metal" == "fm" ] || \
           [ "$metal" == "md" ] || [ "$metal" == "no" ] || [ "$metal" == "lr" ] ; then
           compute_numerical_gradient_J_Bkq
           wait
        fi

        metal_up=$( echo "$metal" | tr '[:lower:]' '[:upper:]' ) # make uppercase
        echo metal_up=$metal_up
        indxATOM=0
        for iat in ${LABEL[@]} ; do
            if [ "${LABEL[$iat]}" == "$metal_up" ] ; then
                file="$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out
            fi
            ((indxATOM++))
        done
        echo hessian_by_shifting_atoms:: file=$file

        termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$file" | colrm 1 86 | sed 's/)//' | tr -d ' '`
        echo hessian_by_shifting_atoms:: termS=$termS
        if [ "$termS" != "1/2" ] ; then
           echo calling compute_numerical_gradient_ZFS_D_E
           compute_numerical_gradient_ZFS_D_E
           compute_numerical_gradient_S_Bkq
        fi
#        indxATOM=0;
#        for iat in ${LABEL[@]} ; do
#
#          # delete the unnecessary large files. They might clutter the disc space
##           for iaxis in X Y Z; do
#               for idist in 1 2 3 4 5 6; do
#                   # RASSCF
##                   for i in ${!spinMS[*]}; do
#                       roots=${rootsCASSCF[$i]}
#                       ms=${spinMS[$i]}
#                       echo "calling run_rasscf for ms="$ms
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".RunFile
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".RasOrb
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".rasscf.h5
#                   done
#                   # RASSI
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.h5
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.RunFile
#                   # SINGLE_ANISO
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER_ENE.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER_TME.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.png
#                   # other files
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist".xyz
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist".txt
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
#               done
#           done
#
#
#           let indxATOM=$indxATOM+1
#        done


     elif [[ $npoints -eq 8 ]] ; then
        input_list_1=`ls "$Complex"_dist_*_1.xyz`
        input_list_2=`ls "$Complex"_dist_*_2.xyz`
        input_list_3=`ls "$Complex"_dist_*_3.xyz`
        input_list_4=`ls "$Complex"_dist_*_4.xyz`
        input_list_5=`ls "$Complex"_dist_*_5.xyz`
        input_list_6=`ls "$Complex"_dist_*_6.xyz`
        input_list_7=`ls "$Complex"_dist_*_7.xyz`
        input_list_8=`ls "$Complex"_dist_*_8.xyz`

        echo LIST-1 = $input_list_1
        echo " --- "
        echo LIST-2 = $input_list_2
        echo " --- "
        echo LIST-3 = $input_list_3
        echo " --- "
        echo LIST-4 = $input_list_4
        echo " --- "
        echo LIST-5 = $input_list_5
        echo " --- "
        echo LIST-6 = $input_list_6
        echo " --- "
        echo LIST-7 = $input_list_7
        echo " --- "
        echo LIST-8 = $input_list_8
        echo " --- "

        c=0;
        for i in $input_list_1 $input_list_8   $input_list_2 $input_list_7   $input_list_3 $input_list_6   $input_list_4 $input_list_5  ; do
          j=${i%.xyz}
          echo j=$j
          export do_restart='CIRESTART'
          export clean_workdir='true'

          for is in ${!spinMS[*]}; do
            ms=${spinMS[$is]}
                 link $FileDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph  $FileDir/"$j"_"$bas1"_"$bas2"_ms"$ms".JOBOLD
            echo LINK $FileDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph  $FileDir/"$j"_"$bas1"_"$bas2"_ms"$ms".JOBOLD
          done

          my_molcas_run  $j  $bas1  $bas2   $metal  $el $orb &
          let c=$c+1;
          rem=$(( $c % $number_of_procs_for_numerical_gradient ))
          if [ "$rem" -eq 0 ]; then
             wait
          fi
        done
        #--------------------------------------
        wait
        sleep 2

        compute_numerical_gradient_SFS
        wait
        compute_numerical_gradient_SOS
        wait
        if [ "$metal" == "sc" ] || [ "$metal" == "ti" ] || [ "$metal" == "v"  ] || [ "$metal" == "cr" ] || [ "$metal" == "mn" ] || \
           [ "$metal" == "fe" ] || [ "$metal" == "co" ] || [ "$metal" == "ni" ] || [ "$metal" == "cu" ] || \
           [ "$metal" == "ce" ] || [ "$metal" == "pr" ] || [ "$metal" == "nd" ] || [ "$metal" == "pm" ] || [ "$metal" == "sm" ] || \
           [ "$metal" == "eu" ] || [ "$metal" == "gd" ] || [ "$metal" == "tb" ] || [ "$metal" == "dy" ] || [ "$metal" == "ho" ] || \
           [ "$metal" == "er" ] || [ "$metal" == "tm" ] || [ "$metal" == "yb" ] || \
           [ "$metal" == "pa" ] || [ "$metal" == "u"  ] || [ "$metal" == "np" ] || [ "$metal" == "pu" ] || [ "$metal" == "am" ] || \
           [ "$metal" == "cm" ] || [ "$metal" == "bk" ] || [ "$metal" == "cf" ] || [ "$metal" == "es" ] || [ "$metal" == "fm" ] || \
           [ "$metal" == "md" ] || [ "$metal" == "no" ] || [ "$metal" == "lr" ] ; then
           compute_numerical_gradient_L_Bkq
           wait
        fi
        if [ "$metal" == "ce" ] || [ "$metal" == "pr" ] || [ "$metal" == "nd" ] || [ "$metal" == "pm" ] || [ "$metal" == "sm" ] || \
           [ "$metal" == "eu" ] || [ "$metal" == "gd" ] || [ "$metal" == "tb" ] || [ "$metal" == "dy" ] || [ "$metal" == "ho" ] || \
           [ "$metal" == "er" ] || [ "$metal" == "tm" ] || [ "$metal" == "yb" ] || \
           [ "$metal" == "pa" ] || [ "$metal" == "u"  ] || [ "$metal" == "np" ] || [ "$metal" == "pu" ] || [ "$metal" == "am" ] || \
           [ "$metal" == "cm" ] || [ "$metal" == "bk" ] || [ "$metal" == "cf" ] || [ "$metal" == "es" ] || [ "$metal" == "fm" ] || \
           [ "$metal" == "md" ] || [ "$metal" == "no" ] || [ "$metal" == "lr" ] ; then
           compute_numerical_gradient_J_Bkq
           wait
        fi

        metal_up=$( echo "$metal" | tr '[:lower:]' '[:upper:]' ) # make uppercase
        echo metal_up=$metal_up
        indxATOM=0
        for iat in ${LABEL[@]} ; do
            if [ "${LABEL[$iat]}" == "$metal_up" ] ; then
                file="$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out
            fi
            ((indxATOM++))
        done
        echo hessian_by_shifting_atoms:: file=$file

        termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$file" | colrm 1 86 | sed 's/)//' | tr -d ' '`
        echo hessian_by_shifting_atoms:: termS=$termS
        if [ "$termS" != "1/2" ] ; then
           echo calling compute_numerical_gradient_ZFS_D_E
           compute_numerical_gradient_ZFS_D_E
           compute_numerical_gradient_S_Bkq
        fi
#        indxATOM=0;
#        for iat in ${LABEL[@]} ; do
#
#          # delete the unnecessary large files. They might clutter the disc space
##           for iaxis in X Y Z; do
#               for idist in 1 2 3 4 5 6; do
#                   # RASSCF
##                   for i in ${!spinMS[*]}; do
#                       roots=${rootsCASSCF[$i]}
#                       ms=${spinMS[$i]}
#                       echo "calling run_rasscf for ms="$ms
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".JobIph
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".RunFile
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".RasOrb
#                       delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_ms"$ms".rasscf.h5
#                   done
#                   # RASSI
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.h5
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rassi_cas.RunFile
#                   # SINGLE_ANISO
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER_ENE.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER_TME.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_BARRIER.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_MH.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_no_field.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_dM_over_dH.png
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.dat
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.plt
#                   delete $FileDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas_XT_with_field_M_over_H.png
#                   # other files
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist".xyz
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist".txt
#                   delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SOS_"$iaxis"_"$idist".txt
#               done
#           done
#
#
#           let indxATOM=$indxATOM+1
#        done

     fi

# -------------------------------------------------------------------------------





#---------------------------------------------------------------------------
#   #  now all distortions of "atom" along cartesian axis "iL" are known
#   #  grep Bkq

#   for ik in $( seq 2 2 $kmax ) ; do       # list even numbers from 2 till $kmax
#       for iq in  $( seq -$ik $ik ) ; do   # list all range from -$ik till +$ik, step 1
#           echo ik= $ik    iq=$iq
#           # grep Bkq from aniso outputs of all distortions  ${DISTORTIONS[@]}
#           # compute the derivative by n-points stencil formula
#           # save (append) the results in a file
#       done
#   done





#----------------------------------------------------------
      # amplitudes =  +0.1 * normal_mode_vector
#
#      for istep in $npoints ; do
#          generate_distorted_XYZ $istep $natoms $selected_mode normal_mode_X[@] normal_mode_Y[@] normal_mode_Z[@]
#      done

#----------------------------------------------------------


#      for istep in $npoints
#
#          export Project="$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_nm_"$selected_mode"_step_$istep
#          export RootDir=$ScratchDir/"$Complex"_"$bas1"_"$bas2"_"$active_space"_nm_"$selected_mode"_step_$istep
#          if [ ! -d "$RootDir" ]; then mkdir $RootDir ; fi
#
#          echo my_molcas_run::  me=$me
#
#
#          #--------------------------------------------------
#          # initial verification:
#
#          echo "my_molcas_run::  initial verification"
#          initial_verification  spinMS[@] rootsCASSCF[@] rootsCASPT2[@] rootsRASSI[@] groundL[@]
#
#          echo "my_molcas_run::  spinMS[]     =" ${spinMS[@]}
#          echo "my_molcas_run::  rootsCASSCF[]=" ${rootsCASSCF[@]}
#          echo "my_molcas_run::  rootsCASPT2[]=" ${rootsCASPT2[@]}
#          echo "my_molcas_run::  rootsRASSI[] =" ${rootsRASSI[@]}
#          echo "my_molcas_run::  groundL[]    =" ${groundL[@]}
#          #--------------------------------------------------
#          # run seward:
#
#          echo "calling run_seward"
#          run_seward  $bas1  $bas2  $me  $cutoff_dist &
#          #wait
#
#
##-------------------------------------------------------
##          CASSCF/RASSI SECTION
##-------------------------------------------------------
#          # run rasscf for all spin states in parallel
#
#          echo "calling run_rasscf for all spin multiplicities"
#          for i in ${!spinMS[*]}; do
#              roots=${rootsCASSCF[$i]}
#              ms=${spinMS[$i]}
#              echo "calling run_rasscf for ms="$ms
#              run_rasscf  $ms  $roots  $el  $orb  $bas1  $bas2 &
#          done
#          wait
#
#          #--------------------------------------------------
#          # run rassi
#          echo "calling run_rassi"
#          run_rassi   cas  spinMS[@]  rootsRASSI[@]
#
#          #--------------------------------------------------
#          # run rasscf for all spin states in parallel
#
#          echo "calling run_aniso"
#          run_aniso   cas  $me
#
#
#      done
#
#      #--------------------------------------------------
#      # post - processing this normal mode
#      # grep each parameter Bkq and plot them along the path...
#        for k in (2 4 6 8 10 12) do ;
#           for q in list ( -k, -k+1, ..., +k-1, +k ) do ;
#              1) grep  Bkq from the output of ANISO ...
#              2) make  a list   idist   Bkq(idist) => write into a file
#              3) get the  dBkq/dqi  from fitting the curve with a quadratic equation:
#                      Bkq(idist) = Ax^2 + Bx + C
#                      as the number (B).
#           done
#        done
#
#        Write the  dBkq/dqi for this normal mode in a file
#      #--------------------------------------------------
#

}

