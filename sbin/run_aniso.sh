#!/bin/bash

run_aniso () {
   local kind=$1
   local metal=`echo "$2" | tr A-Z a-z`
   echo $kind $metal $oxidation_state
   export WorkDir=$RootDir/aniso_"$kind"
   #  check_previous_runs
   local aniso_old_run=0
   if [ -f $CurrDir/"$Project"_aniso_"$kind".out ] && [ -f $FileDir/"$Project"_aniso_"$kind".aniso ] ; then
       aniso_old_run=`grep 'Stop Module:' $CurrDir/"$Project"_aniso_"$kind".out | grep 'single_aniso ' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
   fi

   if [ ! -f $CurrDir/"$Project"_aniso_"$kind".out ] || [ "$aniso_old_run" -ne 1 ]; then
      # clean up
      if [ -d $WorkDir ]; then
         rm -rf $WorkDir
         mkdir  $WorkDir
      else
         mkdir $WorkDir
      fi
      echo $RootDir/aniso_"$kind" was created or cleaned up.

      # just to make sure we are using a correct version of the RunFile:
      delete $FileDir/"$Project"_aniso_"$kind".aniso
      copy   $FileDir/"$Project"_rassi_"$kind".RunFile  $WorkDir/$Project.RunFile

      echo "&SINGLE_ANISO"                         >   $CurrDir/"$Project"_aniso_"$kind".inp
      echo "MLTP"                                  >>  $CurrDir/"$Project"_aniso_"$kind".inp
      echo ${#MLTP_in_ANISO[*]}                    >>  $CurrDir/"$Project"_aniso_"$kind".inp
      echo ` printf ' %s' "${MLTP_in_ANISO[@]}" `  >>  $CurrDir/"$Project"_aniso_"$kind".inp

      if [ "${#MLTP_in_ANISO[*]}" -ne "1" ]; then
          echo "UBAR"                              >>  $CurrDir/"$Project"_aniso_"$kind".inp
      else
          echo "UBAR is deactivated because NMLTP=1"
      fi
      echo "PLOT"                                  >>  $CurrDir/"$Project"_aniso_"$kind".inp
      echo "TINT"                                  >>  $CurrDir/"$Project"_aniso_"$kind".inp
      echo "0 300 301"                             >>  $CurrDir/"$Project"_aniso_"$kind".inp
      echo "XFIE"                                  >>  $CurrDir/"$Project"_aniso_"$kind".inp
      echo "0.50"                                  >>  $CurrDir/"$Project"_aniso_"$kind".inp
      if [ "$oxidation_state" -eq "2" ]; then
          if [ "$metal" == "sc" ] || [ "$metal" == "ti" ] || [ "$metal" == "v"  ] || [ "$metal" == "cr" ] || [ "$metal" == "mn" ] || \
             [ "$metal" == "fe" ] || [ "$metal" == "co" ] || [ "$metal" == "ni" ] || [ "$metal" == "cu" ] ; then
             echo "CRYS"                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo $metal                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo $oxidation_state                 >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "QUAX"                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "3"                              >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "1.00  0.00  0.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "0.00  1.00  0.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "0.00  0.00  1.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
          fi
      elif [ "$oxidation_state" -eq "3" ]; then
          if [ "$metal" == "ce" ] || [ "$metal" == "pr" ] || [ "$metal" == "nd" ] || [ "$metal" == "pm" ] || [ "$metal" == "sm" ] || \
             [ "$metal" == "eu" ] || [ "$metal" == "gd" ] || [ "$metal" == "tb" ] || [ "$metal" == "dy" ] || [ "$metal" == "ho" ] || \
             [ "$metal" == "er" ] || [ "$metal" == "tm" ] || [ "$metal" == "yb" ] ; then
             echo "CRYS"                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo $metal                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "QUAX"                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "3"                              >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "1.00  0.00  0.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "0.00  1.00  0.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "0.00  0.00  1.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
          fi
          if [ "$metal" == "sc" ] || [ "$metal" == "ti" ] || [ "$metal" == "v"  ] || [ "$metal" == "cr" ] || [ "$metal" == "mn" ] || \
             [ "$metal" == "fe" ] || [ "$metal" == "co" ] || [ "$metal" == "ni" ] || [ "$metal" == "cu" ] ; then
             echo "CRYS"                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo $metal                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo $oxidation_state                 >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "QUAX"                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "3"                              >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "1.00  0.00  0.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "0.00  1.00  0.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "0.00  0.00  1.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
          fi
      elif [ "$oxidation_state" -eq "4" ]; then
          if [ "$metal" == "pa" ] || [ "$metal" == "u"  ] || [ "$metal" == "np" ] || [ "$metal" == "pu" ] || [ "$metal" == "am" ] || \
             [ "$metal" == "cm" ] || [ "$metal" == "bk" ] || [ "$metal" == "cf" ] || [ "$metal" == "es" ] || [ "$metal" == "fm" ] || \
             [ "$metal" == "md" ] || [ "$metal" == "no" ] || [ "$metal" == "lr" ] ; then
             echo "CRYS"                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "$metal"                         >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "$oxidation_state"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "QUAX"                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "3"                              >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "1.00  0.00  0.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "0.00  1.00  0.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
             echo "0.00  0.00  1.00"               >>  $CurrDir/"$Project"_aniso_"$kind".inp
           fi
      fi
      if [ "$magnetism" == "yes" ]; then
         echo "HINT"                               >>  $CurrDir/"$Project"_aniso_"$kind".inp
         echo "0 7.0 71"                           >>  $CurrDir/"$Project"_aniso_"$kind".inp
         echo "TMAG"                               >>  $CurrDir/"$Project"_aniso_"$kind".inp
         echo "5    2.0 3.0 5.0 8.0 10.0"          >>  $CurrDir/"$Project"_aniso_"$kind".inp
#echo "20  0.5 1.0 1.5 1.8 2.0 2.2 2.5 3.0 3.5 4.0 5.0 6.0 7.0 8.0 9.0 10.0 15.0 20.0 30.0 40.0"  >>  $CurrDir/"$Project"_aniso_"$kind".inp
         echo "MAVE"                               >>  $CurrDir/"$Project"_aniso_"$kind".inp
         echo "1 8"                                >>  $CurrDir/"$Project"_aniso_"$kind".inp
      fi
      echo "End Of Input"                          >> $CurrDir/"$Project"_aniso_"$kind".inp

      # run_molcas in its own subfolder
      link $RootDir/"$Project".OrdInt  $WorkDir/"$Project".OrdInt
      link $RootDir/"$Project".OneInt  $WorkDir/"$Project".OneInt
      link $RootDir/"$Project".ChDiag  $WorkDir/"$Project".ChDiag
      link $RootDir/"$Project".ChMap   $WorkDir/"$Project".ChMap
      link $RootDir/"$Project".ChRst   $WorkDir/"$Project".ChRst
      link $RootDir/"$Project".ChRed   $WorkDir/"$Project".ChRed
      link $RootDir/"$Project".ChSel   $WorkDir/"$Project".ChSel
      link $RootDir/"$Project".ChVec   $WorkDir/"$Project".ChVec
      link $RootDir/"$Project".ChFV    $WorkDir/"$Project".ChFV
      link $RootDir/"$Project".ChRstL  $WorkDir/"$Project".ChRstL
      link $RootDir/"$Project".ChRdL   $WorkDir/"$Project".ChRdL
      link $RootDir/"$Project".ChVcl   $WorkDir/"$Project".ChVcl
      link $RootDir/"$Project".Vtmp    $WorkDir/"$Project".Vtmp
      link $RootDir/"$Project".LDFC    $WorkDir/"$Project".LDFC
      link $RootDir/"$Project".LDFAP   $WorkDir/"$Project".LDFAP
      link $RootDir/"$Project".OneRel  $WorkDir/"$Project".OneRel
      link $RootDir/"$Project".BasInt  $WorkDir/"$Project".BasInt
      link $RootDir/"$Project".SymInfo $WorkDir/"$Project".SymInfo
      link $RootDir/"$Project".QVec    $WorkDir/"$Project".QVec
      link $RootDir/"$Project".ChVec1  $WorkDir/"$Project".ChVec1

      if [ "$cluster" == "NSCC" ] ; then
          if [ -f $CurrDir/"$Project"_aniso_"$kind" ] ; then rm -rf $CurrDir/"$Project"_aniso_"$kind".sh ; fi
          echo "#!/bin/sh"                               > $CurrDir/"$Project"_aniso_"$kind".sh
          echo "#PBS -q $queue"                         >> $CurrDir/"$Project"_aniso_"$kind".sh
          if [ ! -z "$ProjectID" ] ; then
            echo "#PBS -P $ProjectID"                   >> $CurrDir/"$Project"_aniso_"$kind".sh
          else
            echo "#PBS -P Personal"                     >> $CurrDir/"$Project"_aniso_"$kind".sh
          fi
          echo "#PBS -l walltime=$walltime:00:00"       >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "#PBS -l select=$no_of_cores:ncpus=$no_of_ncpus:mpiprocs=$no_of_mpiprocs:mem=$memory" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "#PBS -j oe"                             >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "#PBS -N "$Project"_aniso_"$kind""       >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo ""                                       >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "module load python/3.5.1"               >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "module load hdf5/1.8.16/xe_2016/serial" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo ""                                       >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "export CurrDir=$CurrDir"                >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "export Project=$Project"                >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "export FileDir=$FileDir"                >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "export WorkDir=$WorkDir"                >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "export MOLCAS=$MOLCAS"                  >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "export MOLCAS_MEM=$MOLCAS_MEM"          >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo ""                                       >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ ! -d $WorkDir ]"                   >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "then"                                   >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "   mkdir -p $WorkDir"                   >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "fi"                                     >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "cd $WorkDir"                            >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo ""                                       >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".OrdInt  ]; then ln -fs $RootDir/"$Project".OrdInt  $WorkDir/"$Project".OrdInt  ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".OneInt  ]; then ln -fs $RootDir/"$Project".OneInt  $WorkDir/"$Project".OneInt  ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChDiag  ]; then ln -fs $RootDir/"$Project".ChDiag  $WorkDir/"$Project".ChDiag  ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChMap   ]; then ln -fs $RootDir/"$Project".ChMap   $WorkDir/"$Project".ChMap   ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChRst   ]; then ln -fs $RootDir/"$Project".ChRst   $WorkDir/"$Project".ChRst   ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChRed   ]; then ln -fs $RootDir/"$Project".ChRed   $WorkDir/"$Project".ChRed   ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChSel   ]; then ln -fs $RootDir/"$Project".ChSel   $WorkDir/"$Project".ChSel   ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChVec   ]; then ln -fs $RootDir/"$Project".ChVec   $WorkDir/"$Project".ChVec   ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChFV    ]; then ln -fs $RootDir/"$Project".ChFV    $WorkDir/"$Project".ChFV    ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChRstL  ]; then ln -fs $RootDir/"$Project".ChRstL  $WorkDir/"$Project".ChRstL  ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChRdL   ]; then ln -fs $RootDir/"$Project".ChRdL   $WorkDir/"$Project".ChRdL   ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChVcL   ]; then ln -fs $RootDir/"$Project".ChVcl   $WorkDir/"$Project".ChVcl   ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".Vtmp    ]; then ln -fs $RootDir/"$Project".Vtmp    $WorkDir/"$Project".Vtmp    ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".LDFC    ]; then ln -fs $RootDir/"$Project".LDFC    $WorkDir/"$Project".LDFC    ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".LDFAP   ]; then ln -fs $RootDir/"$Project".LDFAP   $WorkDir/"$Project".LDFAP   ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".OneRel  ]; then ln -fs $RootDir/"$Project".OneRel  $WorkDir/"$Project".OneRel  ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".BasInt  ]; then ln -fs $RootDir/"$Project".BasInt  $WorkDir/"$Project".BasInt  ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".SymInfo ]; then ln -fs $RootDir/"$Project".SymInfo $WorkDir/"$Project".SymInfo ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".QVec    ]; then ln -fs $RootDir/"$Project".QVec    $WorkDir/"$Project".QVec    ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "if [ -f $RootDir/"$Project".ChVec1  ]; then ln -fs $RootDir/"$Project".ChVec1  $WorkDir/"$Project".ChVec1  ; fi" >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "cp $RootDir/$Project.RunFile  $WorkDir/$Project.RunFile"                                                         >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo ""                                                                                                                >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "pymolcas $CurrDir/"$Project"_aniso_"$kind".inp > $CurrDir/"$Project"_aniso_"$kind".out 2> $CurrDir/"$Project"_aniso_"$kind".err " >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo ""                                       >> $CurrDir/"$Project"_aniso_"$kind".sh
          echo "exit $?"                                >> $CurrDir/"$Project"_aniso_"$kind".sh
      else
          cd $WorkDir
          pymolcas $CurrDir/"$Project"_aniso_"$kind".inp  > \
                   $CurrDir/"$Project"_aniso_"$kind".out  2> \
                   $CurrDir/"$Project"_aniso_"$kind".err

          cat $CurrDir/"$Project"_aniso_"$kind".err
          cd  $CurrDir

          echo "MOLCAS/ANISO return code is:" $?
          if [ $? -eq 0 ]; then
              # a bit of saving & clean-up
              delete $CurrDir/"$Project"_aniso_"$kind".inp
              delete $CurrDir/"$Project"_aniso_"$kind".xml
              copy $WorkDir/"$Project".aniso                        $FileDir/"$Project"_aniso_"$kind".aniso
              copy $WorkDir/"$Project".old.aniso                    $FileDir/"$Project"_aniso_"$kind".old.aniso
              copy $WorkDir/"$Project".BARRIER_ENE.dat              $FileDir/"$Project"_aniso_"$kind"_BARRIER_ENE.dat
              copy $WorkDir/"$Project".BARRIER_TME.dat              $FileDir/"$Project"_aniso_"$kind"_BARRIER_TME.dat
              copy $WorkDir/"$Project".BARRIER.plt                  $FileDir/"$Project"_aniso_"$kind"_BARRIER.plt
              copy $WorkDir/"$Project".BARRIER.png                  $FileDir/"$Project"_aniso_"$kind"_BARRIER.png
              copy $WorkDir/"$Project".BARRIER.eps                  $FileDir/"$Project"_aniso_"$kind"_BARRIER.eps
              copy $WorkDir/"$Project".XT_no_field.dat              $FileDir/"$Project"_aniso_"$kind"_XT_no_field.dat
              copy $WorkDir/"$Project".XT_no_field.plt              $FileDir/"$Project"_aniso_"$kind"_XT_no_field.plt
              copy $WorkDir/"$Project".XT_no_field.png              $FileDir/"$Project"_aniso_"$kind"_XT_no_field.png
              copy $WorkDir/"$Project".XT_no_field.eps              $FileDir/"$Project"_aniso_"$kind"_XT_no_field.eps
              copy $WorkDir/"$Project".XT_with_field_M_over_H.dat   $FileDir/"$Project"_aniso_"$kind"_XT_with_field_M_over_H.dat
              copy $WorkDir/"$Project".XT_with_field_M_over_H.plt   $FileDir/"$Project"_aniso_"$kind"_XT_with_field_M_over_H.plt
              copy $WorkDir/"$Project".XT_with_field_M_over_H.png   $FileDir/"$Project"_aniso_"$kind"_XT_with_field_M_over_H.png
              copy $WorkDir/"$Project".XT_with_field_M_over_H.eps   $FileDir/"$Project"_aniso_"$kind"_XT_with_field_M_over_H.eps
              copy $WorkDir/"$Project".XT_with_field_dM_over_dH.dat $FileDir/"$Project"_aniso_"$kind"_XT_with_field_dM_over_dH.dat
              copy $WorkDir/"$Project".XT_with_field_dM_over_dH.plt $FileDir/"$Project"_aniso_"$kind"_XT_with_field_dM_over_dH.plt
              copy $WorkDir/"$Project".XT_with_field_dM_over_dH.png $FileDir/"$Project"_aniso_"$kind"_XT_with_field_dM_over_dH.png
              copy $WorkDir/"$Project".XT_with_field_dM_over_dH.eps $FileDir/"$Project"_aniso_"$kind"_XT_with_field_dM_over_dH.eps
              copy $WorkDir/"$Project".MH.dat                       $FileDir/"$Project"_aniso_"$kind"_MH.dat
              copy $WorkDir/"$Project".MH.plt                       $FileDir/"$Project"_aniso_"$kind"_MH.plt
              copy $WorkDir/"$Project".MH.png                       $FileDir/"$Project"_aniso_"$kind"_MH.png
              copy $WorkDir/"$Project".MH.eps                       $FileDir/"$Project"_aniso_"$kind"_MH.eps
              rm -rf $WorkDir
          else
              echo "SINGLE_ANISO did not finish well. Please check.   "
              echo "No important files (like ANISOINPUT) were saved into $FileDir"
              return $?
          fi
      fi

   else
      echo The "$Project"_aniso_"$kind".out is present in the "$CurrDir". Nothing to do.
      echo If not satisfied, remove the following files or folders and resubmit
      echo $CurrDir/"$Project"_aniso_"$kind".out
   fi
}



