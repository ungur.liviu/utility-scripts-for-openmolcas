#!/bin/bash

function compute_numerical_gradient_SFS() {

    echo compute_numerical_gradient_SFS::      CurrDir=$CurrDir
    echo compute_numerical_gradient_SFS::      FileFir=$FileDir
    echo compute_numerical_gradient_SFS::      Complex=$Complex
    echo compute_numerical_gradient_SFS::       LABELS=${LABEL[@]}
    echo compute_numerical_gradient_SFS::         bas1=$bas1
    echo compute_numerical_gradient_SFS::         bas2=$bas2
    echo compute_numerical_gradient_SFS:: active_space=$active_space
    echo compute_numerical_gradient_SFS::      npoints=$npoints
    echo compute_numerical_gradient_SFS::      spin_ms=${spinMS[@]}

    cd $CurrDir

    for i in ${!spinMS[*]}; do
         ms=${spinMS[$i]}
         echo compute_numerical_gradient_SFS::            i=$i   ms=$ms
    done
#----------------------------------------------------------------------------
if   [[ $npoints -eq 2 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for is in ${!spinMS[*]}; do
    ms=${spinMS[$is]}

    for iaxis in X Y Z; do
      for idist in $(seq 1 $npoints ); do
        grep :: $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rasscf_ms"$ms".out | grep 'RASSCF root number'    | colrm 1 42 >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist"_ms"$ms".txt
      done
    done

    unset ESFS_X_1 ESFS_X_2
    unset ESFS_Y_1 ESFS_Y_2
    unset ESFS_Z_1 ESFS_Z_2

    declare -a ESFS_X_1=( ) ; declare -a ESFS_Y_1=( ) ; declare -a ESFS_Z_1=( )
    declare -a ESFS_X_2=( ) ; declare -a ESFS_Y_2=( ) ; declare -a ESFS_Z_2=( )

    index=0;
    while read -a line; do
       ESFS_X_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_2_ms"$ms".txt

    if [ "$is" -eq "0" ] && [ "$indxATOM" -eq "0" ] ; then
      #-----------------------------------------------------------------------------------------------------------------------------
      echo "SPIN-FREE NUMERICAL GRADIENT COMPUTED BY 2-POINT ATOM DISTORTION"    > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "FORMULA USED:                    "                                  >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "           -ENERGY(1) +ENERGY(2) "                                  >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "gradient = --------------------- "                                  >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "                    2*delta      "                                  >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo " where:                          "                                  >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         delta     = $dist Bohr  "                                  >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(1) = ENERGY( -delta ) "                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(1) = ENERGY( +delta ) "                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo " "                                                                  >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "SPIN-FREE STATES"                                                   >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "  Atom  | SPIN | root |      SFS gradient-X     SFS gradient-Y     SFS gradient-Z   |       SFS gradient-X         SFS gradient-Y         SFS gradient-Z   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "Lbl. Nr.|  ms  |      |         Ha/Bohr            Ha/Bohr           Ha/Bohr        |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
    fi

    iroot=0;
    for i in $(seq 1 ${rootsCASSCF[$is]} ); do
      Xau=`echo "( - ${ESFS_X_1[$iroot]} + ${ESFS_X_2[$iroot]} ) / ( 2* $dist ) " | bc -l`
      Yau=`echo "( - ${ESFS_Y_1[$iroot]} + ${ESFS_Y_2[$iroot]} ) / ( 2* $dist ) " | bc -l`
      Zau=`echo "( - ${ESFS_Z_1[$iroot]} + ${ESFS_Z_2[$iroot]} ) / ( 2* $dist ) " | bc -l`
      Xcm=`echo "$Xau * 219474.7 / 0.529177210903" | bc -l`
      Ycm=`echo "$Yau * 219474.7 / 0.529177210903" | bc -l`
      Zcm=`echo "$Zau * 219474.7 / 0.529177210903" | bc -l`
      printf "%2s%4d %s %3d %s %3d %s    %18.14f %18.14f %18.14f %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |" "$ms" " |" "$i" " |"  $Xau $Yau $Zau "|" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      ((iroot++))
    done
    echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt

    # delete the unnecessary large files. They might clutter the disc space
    for iaxis in X Y Z ; do
      for idist in $(seq 1 $npoints ); do
        delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist"_ms"$ms".txt
      done
    done
  #end  of the main loop over spin multiplicities
  done
  let indxATOM=$indxATOM+1 ;
#end  of the main loop over atoms
done





#----------------------------------------------------------------------------
elif   [[ $npoints -eq 4 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for is in ${!spinMS[*]}; do
    ms=${spinMS[$is]}

    for iaxis in X Y Z ; do
      for idist in $(seq 1 $npoints ); do
          grep :: $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rasscf_ms"$ms".out | grep 'RASSCF root number'    | colrm 1 42 >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist"_ms"$ms".txt
      done
    done

    unset ESFS_X_1 ESFS_X_2 ESFS_X_3 ESFS_X_4
    unset ESFS_Y_1 ESFS_Y_2 ESFS_Y_3 ESFS_Y_4
    unset ESFS_Z_1 ESFS_Z_2 ESFS_Z_3 ESFS_Z_4

    declare -a ESFS_X_1=( ) ; declare -a ESFS_Y_1=( ) ; declare -a ESFS_Z_1=( )
    declare -a ESFS_X_2=( ) ; declare -a ESFS_Y_2=( ) ; declare -a ESFS_Z_2=( )
    declare -a ESFS_X_3=( ) ; declare -a ESFS_Y_3=( ) ; declare -a ESFS_Z_3=( )
    declare -a ESFS_X_4=( ) ; declare -a ESFS_Y_4=( ) ; declare -a ESFS_Z_4=( )

    index=0;
    while read -a line; do
       ESFS_X_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_3[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_3_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_4[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_4_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_3[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_3_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_4[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_4_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_3[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_3_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_4[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_4_ms"$ms".txt

    if [ "$is" -eq "0" ] && [ "$indxATOM" -eq "0" ] ; then
      #-----------------------------------------------------------------------------------------------------------------------------
      echo "NUMERICAL GRADIENT COMPUTED BY 4-POINT ATOM DISTORTION"        > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "FORMULA USED:                                               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "            ENERGY(1) -8*ENERGY(2) +8*ENERGY(3) - ENERGY(4) " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "gradient = ------------------------------------------------ " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "                              12*delta                      " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo " where:                                 "                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         delta     = $dist Bohr         "                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(1) = ENERGY( -2*delta ) "                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(2) = ENERGY( -  delta ) "                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(3) = ENERGY( +  delta ) "                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(4) = ENERGY( +2*delta ) "                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo " "                                                            >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "SPIN-FREE STATES"                                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "  Atom  | SPIN | root |      SFS gradient-X     SFS gradient-Y     SFS gradient-Z   |       SFS gradient-X         SFS gradient-Y         SFS gradient-Z   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "Lbl. Nr.|  ms  |      |         Ha/Bohr            Ha/Bohr           Ha/Bohr        |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
    fi

    iroot=0;
    for i in $(seq 1 ${rootsCASSCF[$is]} ); do
      Xau=`echo "( ${ESFS_X_1[$iroot]} -8* ${ESFS_X_2[$iroot]} +8* ${ESFS_X_3[$iroot]} - ${ESFS_X_4[$iroot]} ) / ( 12* $dist ) " | bc -l`
      Yau=`echo "( ${ESFS_Y_1[$iroot]} -8* ${ESFS_Y_2[$iroot]} +8* ${ESFS_Y_3[$iroot]} - ${ESFS_Y_4[$iroot]} ) / ( 12* $dist ) " | bc -l`
      Zau=`echo "( ${ESFS_Z_1[$iroot]} -8* ${ESFS_Z_2[$iroot]} +8* ${ESFS_Z_3[$iroot]} - ${ESFS_Z_4[$iroot]} ) / ( 12* $dist ) " | bc -l`
      Xcm=`echo "$Xau * 219474.7 / 0.529177210903" | bc -l`
      Ycm=`echo "$Yau * 219474.7 / 0.529177210903" | bc -l`
      Zcm=`echo "$Zau * 219474.7 / 0.529177210903" | bc -l`
      printf "%2s%4d %s %3d %s %3d %s    %18.14f %18.14f %18.14f %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |" "$ms" " |" "$i" " |"  $Xau $Yau $Zau "|" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      ((iroot++))
    done
    echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt

    # delete the unnecessary large files. They might clutter the disc space
    for iaxis in X Y Z ; do
      for idist in $(seq 1 $npoints ); do
        delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist"_ms"$ms".txt
      done
    done
#end  of the main loop over spin multiplicities
  done
#end  of the main loop over atoms
  let indxATOM=$indxATOM+1 ;
done




#----------------------------------------------------------------------------
elif   [[ $npoints -eq 6 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for is in ${!spinMS[*]}; do
    ms=${spinMS[$is]}

    for iaxis in X Y Z ; do
      for idist in $(seq 1 $npoints ); do
        grep :: $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rasscf_ms"$ms".out | grep 'RASSCF root number'    | colrm 1 42 >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist"_ms"$ms".txt
      done
    done

    unset ESFS_X_1 ESFS_X_2 ESFS_X_3 ESFS_X_4 ESFS_X_5 ESFS_X_6
    unset ESFS_Y_1 ESFS_Y_2 ESFS_Y_3 ESFS_Y_4 ESFS_Y_5 ESFS_Y_6
    unset ESFS_Z_1 ESFS_Z_2 ESFS_Z_3 ESFS_Z_4 ESFS_Z_5 ESFS_Z_6

    declare -a ESFS_X_1=( ) ; declare -a ESFS_Y_1=( ) ; declare -a ESFS_Z_1=( )
    declare -a ESFS_X_2=( ) ; declare -a ESFS_Y_2=( ) ; declare -a ESFS_Z_2=( )
    declare -a ESFS_X_3=( ) ; declare -a ESFS_Y_3=( ) ; declare -a ESFS_Z_3=( )
    declare -a ESFS_X_4=( ) ; declare -a ESFS_Y_4=( ) ; declare -a ESFS_Z_4=( )
    declare -a ESFS_X_5=( ) ; declare -a ESFS_Y_5=( ) ; declare -a ESFS_Z_5=( )
    declare -a ESFS_X_6=( ) ; declare -a ESFS_Y_6=( ) ; declare -a ESFS_Z_6=( )

    index=0;
    while read -a line; do
       ESFS_X_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_3[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_3_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_4[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_4_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_5[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_5_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_6[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_6_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_3[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_3_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_4[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_4_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_5[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_5_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_6[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_6_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_3[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_3_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_4[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_4_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_5[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_5_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_6[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_6_ms"$ms".txt

    if [ "$is" -eq "0" ] && [ "$indxATOM" -eq "0" ] ; then
      #-----------------------------------------------------------------------------------------------------------------------------
      echo "NUMERICAL GRADIENT COMPUTED BY 6-POINT ATOM DISTORTION "                                 > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "FORMULA USED:                                          "                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "           -ENERGY(1) +9*ENERGY(2) -45*ENERGY(3) +45*ENERGY(4) -9*ENERGY(5) +ENERGY(6)" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "gradient = ---------------------------------------------------------------------------" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "                                            60*delta                                  " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo " where:                                 "                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         delta     = $dist Bohr         "                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(1) = ENERGY( -3*delta ) "                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(2) = ENERGY( -2*delta ) "                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(3) = ENERGY( -  delta ) "                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(4) = ENERGY( +  delta ) "                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(5) = ENERGY( +2*delta ) "                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(6) = ENERGY( +3*delta ) "                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo " "                                                                                      >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "SPIN-FREE STATES"                                                                       >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "  Atom  | SPIN | root |      SFS gradient-X     SFS gradient-Y     SFS gradient-Z   |       SFS gradient-X         SFS gradient-Y         SFS gradient-Z   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "Lbl. Nr.|  ms  |      |         Ha/Bohr            Ha/Bohr           Ha/Bohr        |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
    fi

    iroot=0;
    for i in $(seq 1 ${rootsCASSCF[$is]} ); do
      Xau=`echo "( - ${ESFS_X_1[$iroot]} +9* ${ESFS_X_2[$iroot]} -45* ${ESFS_X_3[$iroot]} +45* ${ESFS_X_4[$iroot]} -9* ${ESFS_X_5[$iroot]} + ${ESFS_X_6[$iroot]} ) / ( 60* $dist ) " | bc -l`
      Yau=`echo "( - ${ESFS_Y_1[$iroot]} +9* ${ESFS_Y_2[$iroot]} -45* ${ESFS_Y_3[$iroot]} +45* ${ESFS_Y_4[$iroot]} -9* ${ESFS_Y_5[$iroot]} + ${ESFS_Y_6[$iroot]} ) / ( 60* $dist ) " | bc -l`
      Zau=`echo "( - ${ESFS_Z_1[$iroot]} +9* ${ESFS_Z_2[$iroot]} -45* ${ESFS_Z_3[$iroot]} +45* ${ESFS_Z_4[$iroot]} -9* ${ESFS_Z_5[$iroot]} + ${ESFS_Z_6[$iroot]} ) / ( 60* $dist ) " | bc -l`
      Xcm=`echo "$Xau * 219474.7 / 0.529177210903" | bc -l`
      Ycm=`echo "$Yau * 219474.7 / 0.529177210903" | bc -l`
      Zcm=`echo "$Zau * 219474.7 / 0.529177210903" | bc -l`
      printf "%2s%4d %s %3d %s %3d %s    %18.14f %18.14f %18.14f %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |" "$ms" " |" "$i" " |"  $Xau $Yau $Zau "|" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      ((iroot++))
    done
    echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt

    # delete the unnecessary large files. They might clutter the disc space
    for iaxis in X Y Z ; do
      for idist in $(seq 1 $npoints ); do
        delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist"_ms"$ms".txt
      done
    done
  #end  of the main loop over spin multiplicities
  done
  let indxATOM=$indxATOM+1 ;
#end the main loop over atoms
done




#----------------------------------------------------------------------------
elif   [[ $npoints -eq 8 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for is in ${!spinMS[*]}; do
    ms=${spinMS[$is]}

    for iaxis in X Y Z ; do
      for idist in $(seq 1 $npoints ); do
        grep :: $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_rasscf_ms"$ms".out | grep 'RASSCF root number'    | colrm 1 42 >> $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist"_ms"$ms".txt
      done
    done

    unset ESFS_X_1 ESFS_X_2 ESFS_X_3 ESFS_X_4 ESFS_X_5 ESFS_X_6 ESFS_X_7 ESFS_X_8
    unset ESFS_Y_1 ESFS_Y_2 ESFS_Y_3 ESFS_Y_4 ESFS_Y_5 ESFS_Y_6 ESFS_Y_7 ESFS_Y_8
    unset ESFS_Z_1 ESFS_Z_2 ESFS_Z_3 ESFS_Z_4 ESFS_Z_5 ESFS_Z_6 ESFS_Z_7 ESFS_Z_8

    declare -a ESFS_X_1=( ) ; declare -a ESFS_Y_1=( ) ; declare -a ESFS_Z_1=( )
    declare -a ESFS_X_2=( ) ; declare -a ESFS_Y_2=( ) ; declare -a ESFS_Z_2=( )
    declare -a ESFS_X_3=( ) ; declare -a ESFS_Y_3=( ) ; declare -a ESFS_Z_3=( )
    declare -a ESFS_X_4=( ) ; declare -a ESFS_Y_4=( ) ; declare -a ESFS_Z_4=( )
    declare -a ESFS_X_5=( ) ; declare -a ESFS_Y_5=( ) ; declare -a ESFS_Z_5=( )
    declare -a ESFS_X_6=( ) ; declare -a ESFS_Y_6=( ) ; declare -a ESFS_Z_6=( )
    declare -a ESFS_X_7=( ) ; declare -a ESFS_Y_7=( ) ; declare -a ESFS_Z_7=( )
    declare -a ESFS_X_8=( ) ; declare -a ESFS_Y_8=( ) ; declare -a ESFS_Z_8=( )

    index=0;
    while read -a line; do
       ESFS_X_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_3[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_3_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_4[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_4_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_5[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_5_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_6[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_6_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_7[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_7_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_X_8[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_X_8_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_3[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_3_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_4[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_4_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_5[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_5_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_6[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_6_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_7[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_7_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Y_8[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Y_8_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_1[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_1_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_2[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_2_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_3[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_3_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_4[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_4_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_5[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_5_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_6[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_6_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_7[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_7_ms"$ms".txt
    index=0;
    while read -a line; do
       ESFS_Z_8[$index]=${line[0]}
       ((index++))
    done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_Z_8_ms"$ms".txt

    if [ "$is" -eq "0" ] && [ "$indxATOM" -eq "0" ] ; then
      #-----------------------------------------------------------------------------------------------------------------------------
      echo "NUMERICAL GRADIENT COMPUTED BY 8-POINT ATOM DISTORTION "                                      > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "FORMULA USED:                                          "                                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "           3*ENERGY(1) -32*ENERGY(2) +168*ENERGY(3) -672*ENERGY(4) +672*ENERGY(5) -168*ENERGY(6) +32*ENERGY(7) -3*ENERGY(8)" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "gradient = ----------------------------------------------------------------------------------------------------------------" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "                                                              840*delta                                                    " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo " where:                                 "                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         delta     = $dist Bohr         "                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(1) = ENERGY( -4*delta ) "                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(2) = ENERGY( -3*delta ) "                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(3) = ENERGY( -2*delta ) "                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(4) = ENERGY( -  delta ) "                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(5) = ENERGY( +  delta ) "                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(6) = ENERGY( +2*delta ) "                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(7) = ENERGY( +3*delta ) "                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "         ENERGY(8) = ENERGY( +4*delta ) "                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo " "                                                                                           >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "SPIN-FREE STATES"                                                                            >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "  Atom  | SPIN | root |      SFS gradient-X     SFS gradient-Y     SFS gradient-Z   |       SFS gradient-X         SFS gradient-Y         SFS gradient-Z   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "Lbl. Nr.|  ms  |      |         Ha/Bohr            Ha/Bohr           Ha/Bohr        |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
    fi

    iroot=0;
    for i in $(seq 1 ${rootsCASSCF[$is]} ); do
      Xau=`echo "( 3* ${ESFS_X_1[$iroot]} -32* ${ESFS_X_2[$iroot]} +168* ${ESFS_X_3[$iroot]} -672* ${ESFS_X_4[$iroot]} +672* ${ESFS_X_5[$iroot]} -168* ${ESFS_X_6[$iroot]} +32* ${ESFS_X_7[$iroot]} -3* ${ESFS_X_8[$iroot]} ) / ( 840* $dist ) " | bc -l`
      Yau=`echo "( 3* ${ESFS_Y_1[$iroot]} -32* ${ESFS_Y_2[$iroot]} +168* ${ESFS_Y_3[$iroot]} -672* ${ESFS_Y_4[$iroot]} +672* ${ESFS_Y_5[$iroot]} -168* ${ESFS_Y_6[$iroot]} +32* ${ESFS_Y_7[$iroot]} -3* ${ESFS_Y_8[$iroot]} ) / ( 840* $dist ) " | bc -l`
      Zau=`echo "( 3* ${ESFS_Z_1[$iroot]} -32* ${ESFS_Z_2[$iroot]} +168* ${ESFS_Z_3[$iroot]} -672* ${ESFS_Z_4[$iroot]} +672* ${ESFS_Z_5[$iroot]} -168* ${ESFS_Z_6[$iroot]} +32* ${ESFS_Z_7[$iroot]} -3* ${ESFS_Z_8[$iroot]} ) / ( 840* $dist ) " | bc -l`
      Xcm=`echo "$Xau * 219474.7 / 0.529177210903" | bc -l`
      Ycm=`echo "$Yau * 219474.7 / 0.529177210903" | bc -l`
      Zcm=`echo "$Zau * 219474.7 / 0.529177210903" | bc -l`
      printf "%2s%4d %s %3d %s %3d %s    %18.14f %18.14f %18.14f %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |" "$ms" " |" "$i" " |"  $Xau $Yau $Zau "|" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt
      ((iroot++))
    done
    echo "--------|------|------|-------------------------------------------------------------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_SFS"$npoints"_gradient.txt

    # delete the unnecessary large files. They might clutter the disc space
    for iaxis in X Y Z ; do
      for idist in $(seq 1 $npoints ); do
        delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_SFS_"$iaxis"_"$idist"_ms"$ms".txt
      done
    done
  #end  of the main loop over spin multiplicities
  done
  let indxATOM=$indxATOM+1 ;
#end the main loop over atoms
done



#----------------------------------------------------------------------------
else
    echo "not yet implemented"
#----------------------------------------------------------------------------
#elif   [[ $npoints -eq 12 ]] ; then
    echo "not yet implemented"
#----------------------------------------------------------------------------
fi


}




