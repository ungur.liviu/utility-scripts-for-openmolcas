

run_final_caspt2 () {
  local nroots=$1
  local ms=$2
  local type=$3

  export WorkDir=$RootDir/caspt2_ms"$ms"
  mkdir $WorkDir

  #check on previous runs
  local old_run=0
  if [ -f $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".out ]; then
      old_run=`grep 'Stop Module:' $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".out | grep 'caspt2' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l`
  fi

  if [ ! -f $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".out ] || [ "$old_run" -ne 1 ] ; then

      # generate a specific input for this state
      echo "&CASPT2"                                                       > $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
#      if [ "$ms" == ${spinMS[0]} ] || ( [ "$same_multiplet" == "true" ] && [[ "$ms" == ${spinMS[1]} ]] ) ; then
#          echo "XMUL                                                    " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
# #         echo "${#groundL[*]}                                          " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
#          icount=1
#          for i in ${!groundL[*]}; do
#              echo ${groundL[$i]}  $(seq $icount $nroots)                 >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
#
#              let icount=$icount+${groundL[$i]}
#          done
#      else
#          echo "MULTI                                                   " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
#          echo "${#roots_in_CASPT2[*]} $(seq ${#roots_in_CASPT2[*]})    " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
#      fi

      # generate a specific input for this state
      if   [[ "$type" == "ss" ]]; then
           #----------------------------------------------
           echo "MULTI"                                                   >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
           echo $nroots  $(seq 1 $nroots )                                >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp

      elif [[ "$type" == "ms" ]]; then
           #----------------------------------------------
           echo "MULTI"                                                   >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
           echo $nroots  $(seq 1 $nroots )                                >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp

      elif [[ "$type" == "xms" ]]; then
           #----------------------------------------------
           echo "XMUL"                                                    >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
           echo $nroots  $(seq 1 $nroots )                                >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp

      elif [[ "$type" == "dw" ]]; then
           #----------------------------------------------
           echo "DWMS"                                                    >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
           echo $nroots  $(seq 1 $nroots )                                >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
           echo "DWMS"                                                    >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
           echo $dwms_zeta                                                >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
           echo "DWTY"                                                    >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
           echo $dwms_type                                                >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp

      elif [[ "$type" == "rcp" ]]; then
           #----------------------------------------------
           echo "RMUL"                                                    >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
           echo $nroots  $(seq 1 $nroots )                                >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp

      else
          echo run_one_single_state_caspt2::  variable type=$type is undefined. This function does not know it.
          return 999
      fi


      echo "EFFE                                                        " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
      cat $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_full.txt        >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
      echo "IMAG                                                        " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
      echo "  0.10                                                      " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
      echo "End Of Input                                                " >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp

      # run_molcas in its own subfolder
      cd $WorkDir
      link $RootDir/"$Project".OrdInt          $WorkDir/"$Project".OrdInt
      link $RootDir/"$Project".OneInt          $WorkDir/"$Project".OneInt
      link $RootDir/"$Project".ChDiag          $WorkDir/"$Project".ChDiag
      link $RootDir/"$Project".ChMap           $WorkDir/"$Project".ChMap
      link $RootDir/"$Project".ChRst           $WorkDir/"$Project".ChRst
      link $RootDir/"$Project".ChRed           $WorkDir/"$Project".ChRed
      link $RootDir/"$Project".ChSel           $WorkDir/"$Project".ChSel
      link $RootDir/"$Project".ChVec           $WorkDir/"$Project".ChVec
      link $RootDir/"$Project".ChFV            $WorkDir/"$Project".ChFV
      link $RootDir/"$Project".ChRstL          $WorkDir/"$Project".ChRstL
      link $RootDir/"$Project".ChRdL           $WorkDir/"$Project".ChRdL
      link $RootDir/"$Project".ChVcl           $WorkDir/"$Project".ChVcl
      link $RootDir/"$Project".Vtmp            $WorkDir/"$Project".Vtmp
      link $RootDir/"$Project".LDFC            $WorkDir/"$Project".LDFC
      link $RootDir/"$Project".LDFAP           $WorkDir/"$Project".LDFAP
      link $RootDir/"$Project".OneRel          $WorkDir/"$Project".OneRel
      link $RootDir/"$Project".BasInt          $WorkDir/"$Project".BasInt
      link $RootDir/"$Project".SymInfo         $WorkDir/"$Project".SymInfo
      link $RootDir/"$Project".QVec            $WorkDir/"$Project".QVec
      link $RootDir/"$Project".ChVec1          $WorkDir/"$Project".ChVec1
      link $RootDir/"$Project".RunFile         $WorkDir/"$Project".RunFile
      link $FileDir/"$Project"_ms"$ms".JobIph  $WorkDir/"$Project".JobIph

      # fold lines since CASPT2 works only with 120 lines
      fold -w 120 -s $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp  >  $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp2
      mv $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp2 $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp

      pymolcas $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp  >  \
               $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".out  2> \
               $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".err

      cd $CurrDir
      if [ $? -eq 0 ]; then
          delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".inp
          delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".err
          delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".xml
          copy $WorkDir/$Project.JobMix     $FileDir/"$Project"_"$type"_ms"$ms".JobMix
          copy $WorkDir/$Project.caspt2.h5  $FileDir/"$Project"_"$type"_ms"$ms".caspt2.h5
      fi
#rm -rf $WorkDir
#echo DELETE $WorkDir
  else
      echo The "$Project"_caspt2_ms"$ms".out is present in the "$CurrDir". Nothing to do.
  fi
}
