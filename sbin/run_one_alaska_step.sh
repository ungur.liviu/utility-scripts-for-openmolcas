run_one_alaska_step() {
   local ms=$1
   local r1=$2
   local r2=$3
   local nroots=$4
   export WorkDir=$RootDir/alaska_ms"$ms"_state_"$r1"_"$r2"

  #check on previous runs
  local old_run=0
  if [ -f $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".out ]; then
      old_run=`grep 'Stop Module:' $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".out | grep 'alaska' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l`
  fi

  if [ ! -f $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".out ] || [ "$old_run" -ne 1 ] ; then
      mkdir $WorkDir
      # generate a specific input for this state
      echo "&MCLR"                                                      >  $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "ITER"                                                       >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "200"                                                        >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
#      echo "EXPD"                                                       >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
#      echo "$nroots"                                                    >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "PRINT"                                                      >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "3"                                                          >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "THREshold"                                                  >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "1.0d-7"                                                     >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      if [ "$r1" -eq "$r2" ] ; then
         echo "SALA"                                                    >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
         echo "$r1"                                                     >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      else
         echo "NAC"                                                     >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
         echo "$r1"  "$r2"                                              >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      fi
      echo "TWOStep"                                                    >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "SECOND"                                                     >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "End Of Input"                                               >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2

      echo "&ALASKA"                                                    >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "SHOW"                                                       >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "VERBOSE"                                                    >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "CUTOFF"                                                     >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "1.0d-14"                                                    >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      echo "PNEW"                                                       >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      if [ "$r1" -eq "$r2" ] ; then
         echo "ROOT"                                                    >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
         echo "$r1"                                                     >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      else
         echo "NAC"                                                     >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
         echo "$r1"  "$r2"                                              >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
      fi
      echo "End Of Input"                                               >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2

      fold -w 120 -s $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2 > $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp

      # run_molcas in its own subfolder
      if [ "$cluster" == "NSCC" ] ; then
          if [ -f $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2" ] ; then rm -rf $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2" ; fi
          echo "#!/bin/sh" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "#PBS -q $queue" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          if [ ! -z "$ProjectID" ] ; then
            echo "#PBS -P $ProjectID" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          else
            echo "#PBS -P Personal" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          fi
          echo "#PBS -l walltime=$walltime:00:00" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "#PBS -l select=$no_of_cores:ncpus=$no_of_ncpus:mpiprocs=$no_of_mpiprocs:mem=$memory" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "#PBS -j oe" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "#PBS -N "$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "module load python/3.5.1" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "module load hdf5/1.8.16/xe_2016/serial" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "export CurrDir=$CurrDir" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "export Project=$Project" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "export FileDir=$FileDir" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "export WorkDir=$WorkDir" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "export MOLCAS=$MOLCAS" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "export MOLCAS_MEM=$MOLCAS_MEM" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ ! -d $WorkDir ]" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "then" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "   mkdir -p $WorkDir" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "cd $WorkDir" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".OrdInt  ]; then ln -fs $RootDir/"$Project".OrdInt  $WorkDir/"$Project".OrdInt  ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".OneInt  ]; then ln -fs $RootDir/"$Project".OneInt  $WorkDir/"$Project".OneInt  ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChDiag  ]; then ln -fs $RootDir/"$Project".ChDiag  $WorkDir/"$Project".ChDiag  ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChMap   ]; then ln -fs $RootDir/"$Project".ChMap   $WorkDir/"$Project".ChMap   ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChRst   ]; then ln -fs $RootDir/"$Project".ChRst   $WorkDir/"$Project".ChRst   ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChRed   ]; then ln -fs $RootDir/"$Project".ChRed   $WorkDir/"$Project".ChRed   ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChSel   ]; then ln -fs $RootDir/"$Project".ChSel   $WorkDir/"$Project".ChSel   ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChVec   ]; then ln -fs $RootDir/"$Project".ChVec   $WorkDir/"$Project".ChVec   ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChFV    ]; then ln -fs $RootDir/"$Project".ChFV    $WorkDir/"$Project".ChFV    ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChRstL  ]; then ln -fs $RootDir/"$Project".ChRstL  $WorkDir/"$Project".ChRstL  ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChRdL   ]; then ln -fs $RootDir/"$Project".ChRdL   $WorkDir/"$Project".ChRdL   ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChVcL   ]; then ln -fs $RootDir/"$Project".ChVcl   $WorkDir/"$Project".ChVcl   ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".Vtmp    ]; then ln -fs $RootDir/"$Project".Vtmp    $WorkDir/"$Project".Vtmp    ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".LDFC    ]; then ln -fs $RootDir/"$Project".LDFC    $WorkDir/"$Project".LDFC    ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".LDFAP   ]; then ln -fs $RootDir/"$Project".LDFAP   $WorkDir/"$Project".LDFAP   ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".OneRel  ]; then ln -fs $RootDir/"$Project".OneRel  $WorkDir/"$Project".OneRel  ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".BasInt  ]; then ln -fs $RootDir/"$Project".BasInt  $WorkDir/"$Project".BasInt  ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".SymInfo ]; then ln -fs $RootDir/"$Project".SymInfo $WorkDir/"$Project".SymInfo ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".QVec    ]; then ln -fs $RootDir/"$Project".QVec    $WorkDir/"$Project".QVec    ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "if [ -f $RootDir/"$Project".ChVec1  ]; then ln -fs $RootDir/"$Project".ChVec1  $WorkDir/"$Project".ChVec1  ; fi" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "cp     $RootDir/"$Project".RunFile         $WorkDir/"$Project".RunFile" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "ln -fs $FileDir/"$Project"_ms"$ms".JobIph  $WorkDir/"$Project".JobIph"  >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "pymolcas $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp > $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".out 2> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".err " >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
          echo "exit $?" >> $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2"
      else
          cd $WorkDir
          link $RootDir/"$Project".OrdInt               $WorkDir/"$Project".OrdInt
          link $RootDir/"$Project".OneInt               $WorkDir/"$Project".OneInt
          link $RootDir/"$Project".ChDiag               $WorkDir/"$Project".ChDiag
          link $RootDir/"$Project".ChMap                $WorkDir/"$Project".ChMap
          link $RootDir/"$Project".ChRst                $WorkDir/"$Project".ChRst
          link $RootDir/"$Project".ChRed                $WorkDir/"$Project".ChRed
          link $RootDir/"$Project".ChSel                $WorkDir/"$Project".ChSel
          link $RootDir/"$Project".ChVec                $WorkDir/"$Project".ChVec
          link $RootDir/"$Project".ChFV11               $WorkDir/"$Project".ChFV11
          link $RootDir/"$Project".ChRstL               $WorkDir/"$Project".ChRstL
          link $RootDir/"$Project".ChRdL                $WorkDir/"$Project".ChRdL
          link $RootDir/"$Project".ChVcl                $WorkDir/"$Project".ChVcl
          link $RootDir/"$Project".Vtmp                 $WorkDir/"$Project".Vtmp
          link $RootDir/"$Project".LDFC                 $WorkDir/"$Project".LDFC
          link $RootDir/"$Project".LDFAP                $WorkDir/"$Project".LDFAP
          link $RootDir/"$Project".OneRel               $WorkDir/"$Project".OneRel
          link $RootDir/"$Project".BasInt               $WorkDir/"$Project".BasInt
          link $RootDir/"$Project".SymInfo              $WorkDir/"$Project".SymInfo
          link $RootDir/"$Project".QVec00               $WorkDir/"$Project".QVec00
          link $RootDir/"$Project".ChVec1               $WorkDir/"$Project".ChVec1
          link $RootDir/"$Project"_mclr_ms"$ms".ChFV11  $WorkDir/"$Project".ChFV11
          link $RootDir/"$Project"_mclr_ms"$ms".Qdat    $WorkDir/"$Project".Qdat
          link $RootDir/"$Project"_mclr_ms"$ms".Resp    $WorkDir/"$Project".Resp
          link $RootDir/"$Project"_mclr_ms"$ms".tramo   $WorkDir/"$Project".tramo
          link $FileDir/"$Project"_ms"$ms".JobIph       $WorkDir/"$Project".JobIph
          copy $FileDir/"$Project"_mclr_ms"$ms".RunFile $WorkDir/"$Project".RunFile

          pymolcas $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp  >  \
                   $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".out  2> \
                   $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".err

          cd $CurrDir
          if [ $? -eq 0 ]; then
              delete $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp2
              delete $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".inp
              delete $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".err
              delete $CurrDir/"$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".xml
              rm -rf $WorkDir
              echo DELETE  $WorkDir
          fi
    fi
  else
      echo The file "$Project"_alaska_ms"$ms"_state_"$r1"_"$r2".out is present in the "$CurrDir". Nothing to do.
  fi
}
