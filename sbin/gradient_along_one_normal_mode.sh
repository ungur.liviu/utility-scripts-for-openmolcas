function gradient_along_one_normal_mode(){
     # this is the function template for running MOLCAS for one single set of parameters
     # feel free to modify as you wish, to include or exclude any computation steps
     local bas1=$1
     local bas2=$2
     local me=$3
     local el=$4
     local orb=$5
     local selected_mode=$6
     local npoints=$7
     local amplitude_scan=$8
     local active_space="$el"in"$orb"

     # read Hessian and fetch the normal mode vector corresponding to index $6
     # length =  3*natoms
     #
     if [ -f $CurrDir/$Complex.xyz ]; then
        echo File $CurrDir/$Complex.xyz was found. Proceed to process it.
        dos2unix $CurrDir/$Complex.xyz
        # get the number of atoms from the input XYZ file
        natoms=$(head -n 1 $Complex.xyz)
        echo "run_seward::   natoms="$natoms
     #
     echo 'natoms = ', $natoms

#     if [ -f $CurrDir/$Complex.hess ]; then
#        read_orca_hess_file  $natoms  $selected_mode  normal_mode_X[@] normal_mode_Y[@] normal_mode_Z[@]
#     else
#        echo The HESSIAN file "$Complex.hess" does not exist. We cannot compute the gradient.
#        exit 9
#     fi
#     # loop

#----------------------------------------------------------
      # amplitudes =  +0.1 * normal_mode_vector
#
#      for istep in $npoints ; do
#          generate_distorted_XYZ $istep $natoms $selected_mode normal_mode_X[@] normal_mode_Y[@] normal_mode_Z[@]
#      done

#----------------------------------------------------------


#      for istep in $npoints
#
#          export Project="$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_nm_"$selected_mode"_step_$istep
#          export RootDir=$ScratchDir/"$Complex"_"$bas1"_"$bas2"_"$active_space"_nm_"$selected_mode"_step_$istep
#          if [ ! -d "$RootDir" ]; then mkdir $RootDir ; fi
#
#          echo my_molcas_run::  me=$me
#
#
#          #--------------------------------------------------
#          # initial verification:
#
#          echo "my_molcas_run::  initial verification"
#          initial_verification  spinMS[@] rootsCASSCF[@] rootsCASPT2[@] rootsRASSI[@] groundL[@]
#
#          echo "my_molcas_run::  spinMS[]     =" ${spinMS[@]}
#          echo "my_molcas_run::  rootsCASSCF[]=" ${rootsCASSCF[@]}
#          echo "my_molcas_run::  rootsCASPT2[]=" ${rootsCASPT2[@]}
#          echo "my_molcas_run::  rootsRASSI[] =" ${rootsRASSI[@]}
#          echo "my_molcas_run::  groundL[]    =" ${groundL[@]}
#          #--------------------------------------------------
#          # run seward:
#
#          echo "calling run_seward"
#          run_seward  $bas1  $bas2  $me  $cutoff_dist &
#          #wait
#
#
##-------------------------------------------------------
##          CASSCF/RASSI SECTION
##-------------------------------------------------------
#          # run rasscf for all spin states in parallel
#
#          echo "calling run_rasscf for all spin multiplicities"
#          for i in ${!spinMS[*]}; do
#              roots=${rootsCASSCF[$i]}
#              ms=${spinMS[$i]}
#              echo "calling run_rasscf for ms="$ms
#              run_rasscf  $ms  $roots  $el  $orb  $bas1  $bas2 &
#          done
#          wait
#
#          #--------------------------------------------------
#          # run rassi
#          echo "calling run_rassi"
#          run_rassi   cas  spinMS[@]  rootsRASSI[@]
#
#          #--------------------------------------------------
#          # run rasscf for all spin states in parallel
#
#          echo "calling run_aniso"
#          run_aniso   cas  $me
#
#
#      done
#
#      #--------------------------------------------------
#      # post - processing this normal mode
#      # grep each parameter Bkq and plot them along the path...
#        for k in (2 4 6 8 10 12) do ;
#           for q in list ( -k, -k+1, ..., +k-1, +k ) do ;
#              1) grep  Bkq from the output of ANISO ...
#              2) make  a list   idist   Bkq(idist) => write into a file
#              3) get the  dBkq/dqi  from fitting the curve with a quadratic equation:
#                      Bkq(idist) = Ax^2 + Bx + C
#                      as the number (B).
#           done
#        done
#
#        Write the  dBkq/dqi for this normal mode in a file
#      #--------------------------------------------------
#
}

