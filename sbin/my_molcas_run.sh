#!/bin/bash
#
# The follwing variables must be defined at the moment of calling this function
#              Complex
#              active_space, active_electrons, active_orbitals
#              ScratchDir
#              spinMS[] rootsCASSCF[] rootsCASPT2[] rootsRASSI[] groundL[] distance[]
#              cutoff_dist
#              natoms
#
#  This is just one template of a sequence of MOLCAS calculations which I usually use.
#  All calculations are done for the same active space.
#



my_molcas_run() {
     # this is the function template for running MOLCAS for one single set of parameters
     # feel free to modify as you wish, to include or exclude any computation steps
     local Complex=$1
     local bas1=$2
     local bas2=$3
     local me=$4
     local el=$5
     local orb=$6
     local inv=$7
     local inversion=$( echo "$7" | tr '[:lower:]' '[:upper:]' ) # make uppercase
     local active_space="$el"in"$orb"


     export Project="$Complex"_"$bas1"_"$bas2"_cas_"$active_space"
     export RootDir=$ScratchDir/"$Complex"_"$bas1"_"$bas2"_"$active_space"
     if [ ! -d "$RootDir" ]; then mkdir $RootDir ; fi

     echo my_molcas_run::  me=$me

     #--------------------------------------------------
     # initial verification:

     echo "my_molcas_run::  initial verification"
     initial_verification  spinMS[@] rootsCASSCF[@] rootsCASPT2[@] rootsRASSI[@] groundL[@]

     echo "my_molcas_run::  spinMS[]     =" ${spinMS[@]}
     echo "my_molcas_run::  rootsCASSCF[]=" ${rootsCASSCF[@]}
     echo "my_molcas_run::  rootsCASPT2[]=" ${rootsCASPT2[@]}
     echo "my_molcas_run::  rootsRASSI[] =" ${rootsRASSI[@]}
     echo "my_molcas_run::  groundL[]    =" ${groundL[@]}
     if [ ${#BASIS_LBL[@]} -eq 0 ]; then
           # array is empty
           echo "my_molcas_run::  BASIS_LBL[]  = is not defined"
     else
           # array is NOT empty
           echo "my_molcas_run::  BASIS_LBL[]  = is defined"
           echo "my_molcas_run::  natoms       = " $natoms
        for ((atom=0;atom<$natoms;atom++)); do
           echo "my_molcas_run::  BASIS_LBL[]  =" ${LABEL[$atom]}  $atom  ${BASIS_LBL[$atom]}
        done
     fi

     #--------------------------------------------------
     # run seward:

     echo my_molcas_run:: calling run_seward
     echo my_molcas_run:: total_charge=$total_charge
     echo my_molcas_run:: inversion=$inversion
     if [ "$inversion" == "TRUE" ] || [ "$inversion" == "YES" ]; then
        run_seward_inversion $Complex  $bas1  $bas2  $me  $cutoff_dist &
     else
        run_seward $Complex  $bas1  $bas2  $me  $cutoff_dist &
     fi
     wait

     echo my_molcas_run:: total number of electrons=$total_Nel

#-------------------------------
#      export active_electrons=7
#      inactive_Nel=$(bc -l <<<"$total_Nel - $active_electrons ")
#      if [ $(( $inactive_Nel % 2 )) -eq 0 ]; then
#         printf '%s\n' "Number $inactive_Nel is Even", odd spin multiplicity is OK
#      else
#         printf '%s\n' "Number $inactive_Nel is Odd", even spin multiplicity is OK
#      fi
#-------------------------------
#
#      echo 'total charge='  $total_charge
#      inactive_orbitals=$(bc -l <<<" ( $total_Z - $total_charge - $active_electrons ) / 2")
#      echo 'calculated number of inactive orbitals:' $inactive_orbitals


#--------------------------------------------------
#     CASSCF/RASSI SECTION
#--------------------------------------------------
     # run rasscf for all spin states in parallel

     echo "calling run_rasscf for all spin multiplicities"
     for i in ${!spinMS[*]}; do
         roots=${rootsCASSCF[$i]}
         ms=${spinMS[$i]}
         echo "calling run_rasscf for ms="$ms
         run_rasscf $Complex  $ms  $roots  $el  $orb  $bas1  $bas2 &
     done
     wait

     #--------------------------------------------------
     # run rassi
     echo "calling run_rassi"
     run_rassi   cas  spinMS[@]  rootsRASSI[@]

     #--------------------------------------------------
     # run rasscf for all spin states in parallel

     echo "calling run_aniso"
     run_aniso   cas  $me

     #--------------------------------------------------


#--------------------------------------------------
#     CASPT2/RASSI SECTION
#--------------------------------------------------
     # run caspt2 for all spin states in parallel...
     # here the parallelisation is done "by root", therefore there is no & symbol
     if [ -z $IVO ] && [ "$do_caspt2" == "true" ] ; then
         for i in ${!spinMS[*]}; do
             roots=${rootsCASPT2[$i]}
             ms=${spinMS[$i]}
             echo "calling run_caspt2 for ms="$ms" roots="$roots
             run_caspt2 $ms $number_of_procs_for_caspt2 $roots groundL[@]  $run_caspt2_on_separate_nodes
         done
         wait

         #--------------------------------------------------
         # run rassi
         echo "calling run_rassi"
         run_rassi   "$PT2_TYPE"_pt2  spinMS[@]  rootsRASSI[@]

         #--------------------------------------------------
         # run rasscf for all spin states in parallel

         echo "calling run_aniso"
         run_aniso   "$PT2_TYPE"_pt2  $me

         #--------------------------------------------------

     else
         echo 'It looks like either you are using the IVO option for RASSCF, which is not compatible with CASPT2 & MRCI'
         echo 'or the CASPT2 calculation is not requested'
     fi
#--------------------------------------------------
#    # clean up the WorkDir if rquested
     if [ "$clean_workdir" == "true" ] ; then
        rm -rf $RootDir
        echo RootDir was adeleted
     else
        echo RootDir was not deleted
     fi

}
