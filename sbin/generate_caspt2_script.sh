
#================================================================================================
generate_single_caspt2_script() {
  local ms=$1
  local root=$2
  local nroots=$3

  export WorkDir=$RootDir/caspt2_ms"$ms"_state_"$root"

  if [ -f $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".sh ] ; then rm -rf $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".sh ; fi

  #check on previous runs
  local old_run=0
  if [ -f $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".out ]; then
      old_run=`grep 'Stop Module:' $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".out | grep 'caspt2' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l`
  fi

# generate the script
  if [ ! -f $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".out ] || [ "$old_run" -ne 1 ] ; then
      echo "#!/bin/sh                                                                   " >  $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "#PBS -q $queue                                                              " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      if [ ! -z "$ProjectID" ] ; then
        echo "#PBS -P $ProjectID" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      else
        echo "#PBS -P Personal" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      fi
      echo "#PBS -l walltime=$walltime:00:00                                            " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "#PBS -l select=$no_of_cores:ncpus=$no_of_ncpus:mpiprocs=$no_of_mpiprocs:mem=$memory" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "#PBS -j oe                                                                  " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "#PBS -N "$Project"_caspt2_ms"$ms"_state_"$root"                             " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "                                                                            " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "module load python/3.5.1                                                    " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "module load hdf5/1.8.16/xe_2016/serial                                      " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "                                                                            " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "export CurrDir=$CurrDir                                                     " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "export Project=$Project                                                     " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "export FileDir=$FileDir                                                     " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "export RootDir=$RootDir                                                     " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "export RootProject=$RootProject                                             " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "export WorkDir=$WorkDir                                                     " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "export MOLCAS=$MOLCAS                                                       " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "export MOLCAS_MEM=$MOLCAS_MEM                                               " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "                                                                            " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if  [ -d $WorkDir ]; then rm -rf $WorkDir; fi                               " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "                                                                            " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "mkdir $WorkDir                                                              " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "cd $WorkDir                                                                 " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "                                                                            " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".OrdInt  ]; then ln -fs $RootDir/"$Project".OrdInt  $WorkDir/"$Project".OrdInt  ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".OneInt  ]; then ln -fs $RootDir/"$Project".OneInt  $WorkDir/"$Project".OneInt  ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChDiag  ]; then ln -fs $RootDir/"$Project".ChDiag  $WorkDir/"$Project".ChDiag  ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChMap   ]; then ln -fs $RootDir/"$Project".ChMap   $WorkDir/"$Project".ChMap   ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChRst   ]; then ln -fs $RootDir/"$Project".ChRst   $WorkDir/"$Project".ChRst   ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChRed   ]; then ln -fs $RootDir/"$Project".ChRed   $WorkDir/"$Project".ChRed   ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChSel   ]; then ln -fs $RootDir/"$Project".ChSel   $WorkDir/"$Project".ChSel   ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChVec   ]; then ln -fs $RootDir/"$Project".ChVec   $WorkDir/"$Project".ChVec   ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChFV    ]; then ln -fs $RootDir/"$Project".ChFV    $WorkDir/"$Project".ChFV    ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChRstL  ]; then ln -fs $RootDir/"$Project".ChRstL  $WorkDir/"$Project".ChRstL  ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChRdL   ]; then ln -fs $RootDir/"$Project".ChRdL   $WorkDir/"$Project".ChRdL   ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChVcL   ]; then ln -fs $RootDir/"$Project".ChVcl   $WorkDir/"$Project".ChVcl   ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".Vtmp    ]; then ln -fs $RootDir/"$Project".Vtmp    $WorkDir/"$Project".Vtmp    ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".LDFC    ]; then ln -fs $RootDir/"$Project".LDFC    $WorkDir/"$Project".LDFC    ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".LDFAP   ]; then ln -fs $RootDir/"$Project".LDFAP   $WorkDir/"$Project".LDFAP   ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".OneRel  ]; then ln -fs $RootDir/"$Project".OneRel  $WorkDir/"$Project".OneRel  ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".BasInt  ]; then ln -fs $RootDir/"$Project".BasInt  $WorkDir/"$Project".BasInt  ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".SymInfo ]; then ln -fs $RootDir/"$Project".SymInfo $WorkDir/"$Project".SymInfo ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".QVec    ]; then ln -fs $RootDir/"$Project".QVec    $WorkDir/"$Project".QVec    ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "if [ -f $RootDir/"$Project".ChVec1  ]; then ln -fs $RootDir/"$Project".ChVec1  $WorkDir/"$Project".ChVec1  ; fi" >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "ln -fs  $RootDir/"$Project".RunFile           $WorkDir/$Project.RunFile                                        " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "ln -fs  $FileDir/"$Project"_ms"$ms".JobIph    $WorkDir/$Project.JobIph                                         " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "                                                                                                               " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "pymolcas $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp > $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".out  2> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".err" '\'   >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "                                                                                                               " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "  if [ \$? -eq 0 ] ; then                                                                                      " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "      cd     \$CurrDir                                                                                         " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "      rm -rf \$WorkDir                                                                                         " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "      rm -rf \$CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp                                             " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "      rm -rf \$CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".err                                             " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "  fi                                                                                                           " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
      echo "exit \$?                                                                                                       " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root"
  else
      echo The file "$Project"_caspt2_ms"$ms"_state_"$root".out is present in the "$CurrDir". Nothing to do.
      delete $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp
      delete $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".err
      delete $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".xml
  fi


# generate the input
  if [ ! -f $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".out ] || [ "$old_run" -ne 1 ] ; then
      # generate a specific input for this state
      echo "&CASPT2                                                                        "  > $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      if [ "$ms" == ${spinMS[0]} ] || ( [ "$same_multiplet" == "true" ] && [[ "$ms" == ${spinMS[1]} ]] ) ; then
          echo "XMUL                                                                       " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
#          echo "${#groundL[*]}                                                             " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
          icount=1
          for i in ${!groundL[*]}; do
              echo ${groundL[$i]}  $(seq $icount $nroots)                                    >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
              let icount=$icount+${groundL[$i]}
          done
      else
          echo "MULTI                                                                      " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
          echo "${#roots_in_CASPT2[*]} $(seq ${#roots_in_CASPT2[*]})                       " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      fi
      echo "ONLY                                                                           " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo "  $root                                                                        " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo "IMAG                                                                           " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo "  0.10                                                                         " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo "IPEA                                                                           " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo "  0.0                                                                          " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo "NOMIX                                                                          " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo "NOORB                                                                          " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo "MAXITER                                                                        " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo " 100                                                                           " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo "THREsholds                                                                     " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      echo "1.0d-9  1.0d-07                                                                " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      if [ "$ms" -ne ${spinMS[0]} ] && ( [ "$NOMULT" == "true" ] || [ "$NOMULT" == "TRUE" ] ) ; then
         echo "NOMULT                                                                      " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
      fi
      echo "End Of Input                                                                   " >> $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2

      fold -w 120 -s $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2 > $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp
      rm -rf  $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp2
  else
      echo The file "$Project"_caspt2_ms"$ms"_state_"$root".out is present in the "$CurrDir". Nothing to do.
      delete $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".inp
      delete $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".err
      delete $CurrDir/"$Project"_caspt2_ms"$ms"_state_"$root".xml
  fi
}


