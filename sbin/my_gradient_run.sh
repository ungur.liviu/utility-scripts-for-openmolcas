#!/bin/bash

my_gradient_run() {
     # this is the function template for running MOLCAS for one single set of parameters
     # feel free to modify as you wish, to include or exclude any computation steps
     local Complex=$1
     local bas1=$2
     local bas2=$3
     local me=$4
     local el=$5
     local orb=$6
     local only_gradient=$7
     local active_space="$el"in"$orb"


     export Project="$Complex"_"$bas1"_"$bas2"_cas_"$active_space"
     export RootDir=$ScratchDir/"$Complex"_"$bas1"_"$bas2"_"$active_space"
     if [ ! -d "$RootDir" ]; then mkdir $RootDir ; fi

     echo my_molcas_run::  me=$me

     #--------------------------------------------------
     # initial verification:

     echo "my_molcas_run::  initial verification"
     initial_verification  spinMS[@] rootsCASSCF[@] rootsCASPT2[@] rootsRASSI[@] groundL[@]

     echo "my_molcas_run::  spinMS[]     =" ${spinMS[@]}
     echo "my_molcas_run::  rootsCASSCF[]=" ${rootsCASSCF[@]}
     echo "my_molcas_run::  rootsCASPT2[]=" ${rootsCASPT2[@]}
     echo "my_molcas_run::  rootsRASSI[] =" ${rootsRASSI[@]}
     echo "my_molcas_run::  groundL[]    =" ${groundL[@]}
     #--------------------------------------------------
     # run seward:

     echo "calling run_seward"
     echo echo MOLCAS_MEM=$MOLCAS_MEM
     run_seward $Complex  $bas1  $bas2  $me  $cutoff_dist &
     wait


#--------------------------------------------------
#     CASSCF/RASSI SECTION
#--------------------------------------------------
     # run rasscf for all spin states in parallel

     echo "calling run_rasscf for all spin multiplicities"
     for i in ${!spinMS[*]}; do
         roots=${rootsCASSCF[$i]}
         ms=${spinMS[$i]}
         echo "calling run_rasscf for ms="$ms
         run_rasscf $Complex  $ms  $roots  $el  $orb  $bas1  $bas2 &
     done
     wait

     #--------------------------------------------------
     # run alaska for all spin states...
     for i in ${!spinMS[*]}; do
         roots=${rootsCASSCF[$i]}
         ms=${spinMS[$i]}

         #check if the "first MCLR step" finished OK
         local first_mclr_rc=0
         if [ -f $CurrDir/"$Project"_mclr_ms"$ms"_first_run.out ] && [ -f $RootDir/"$Project"_mclr_ms"$ms".tramo ] ; then
             first_mclr_rc=`grep 'Stop Module:' $CurrDir/"$Project"_mclr_ms"$ms"_first_run.out | grep 'mclr ' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l`
         fi
         echo 'first_mclr_rc =' $first_mclr_rc

         if [ "$first_mclr_rc" -eq 0 ] ; then
            run_mclr_first $ms  $roots
            wait
         fi

         if [ "$only_gradient" -eq "1" ]; then
            run_alaska_only_gradient    $ms $number_of_procs_for_alaska  $roots  $run_alaska_on_separate_nodes  $natoms
            wait
         else
            run_alaska_gradient_and_nac $ms $number_of_procs_for_alaska  $roots  $run_alaska_on_separate_nodes  $natoms
            wait
         fi
         
     done
     wait
}
