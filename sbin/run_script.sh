#!/bin/bash

Complex="YbF"                                #"define here your complex"
# input parameters:  plese set up here your variables for the calculation:
CleanWorkDir="no"                            #  set it to "yes" in case you wish that $WorkDir to be cleaned-up (i.e. to erase everything)
metal="Yb"                                   #  define here the chemical symbol of the main metal in your computed system (where your active orbitals are localized)
oxidation_state="3"                          #  defines the oxidation state (valence) of the main metal site
basis_sets_close_atoms=("vdz") # "vtzp")              #  define here the basis set for close atoms for you wish to loop over...
basis_sets_distant_atoms=("mb")              #  define here the basis set for distnt atoms for you wish to loop over...
frozen_orbitals="0"
inactive_orbitals="32"                       #  define the number of inactive orbitals
magnetism="yes"                               #  defines if the M=f(H,T) is to be computed for single_aniso
gradient_calculation="no"
number_of_procs_for_caspt2="8"              #  defines the numbe of available processors (i.e. cores) to run the calculations in parallel
                                             #  if this line is commented, the script will use all available cores
number_of_procs_for_caspt2="3"               #  defines the numbe of available processors (i.e. cores) to run the calculations in parallel
number_of_procs_for_alaska="5"               #  defines the numbe of available processors (i.e. cores) to run the calculations in parallel
run_caspt2_on_separate_nodes="no"  # "no =  is also when this string is undefined"
run_alaska_on_separate_nodes="no"  # "no =  is also when this string is undefined"
numerical_gradient="no"
cluster="NSCC"   # define here the name of the cluster and generate a new function which will produce a simple submitting script (only the PBS commands) for your caspt2 run
                 # so far only NSCC and HPC are defined
#------------------------------------------------------------------------------#
export CurrDir=`pwd`   #!$PBS_O_WORKDIR                #  defines the path to the folder where this calculation is submitted from
export FileDir=$CurrDir/trash                #  defines the path to the folder where all calculations extra files will be stored
export RootProject=$Complex                        #  user-defined name for the entire project ...
export ScratchDir=/home/scratch/liviu/$Project   # scratch folder where all the claculations will take place
#export ScratchDir=/scratch/users/nus/e0196782/molcas/$Project   # scratch folder where all the claculations will take place
#export MOLCAS=~/molcas/molcas-extra-gcc_mkl_fast   # path to a molcas installation
export MOLCAS=/opt/OpenMolcas_22Feb2021_hdf5_mkl_serial
#export MOLCAS=/home/users/nus/e0196782/software/OpenMolcas_23Jul2020_hdf5_mkl_serial
#export MOLCAS_MEM=10000
#=============================================================================================a
.  ~/bin/run_openmolcas_util
# initalize default values...
initialize_system $metal $oxidation_state

# user-defined values for spin multiplicities and  number of states
# the dafalut will include ALL possible states for the given d^N or  f^N configuration of the defined
# metal ion in its oxidation state
ras1_o="0"
ras3_o="0"
holes_ras1="0"
electrons_ras3="0"
cutoff_dist="3.0"
natoms="2"

#active_electrons=(9)
#active_orbitals=(7)
#spinMS=(8 6 4 2)
#rootsCASSCF=(1 48 129)
#rootsCASPT2=(35) #21 128 130)
#rootsRASSI=( ) #21 128 130)
#groundL=(11 7 3)
active_space="$active_electrons"in"$active_orbitals"

NOMULT="false"
do_caspt2="true"
do_restart="EXPBAS"
#same_multiplet="true"
#supsym="1;  16   29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 "

queue="normal"
walltime="24"
no_of_cores="1"
no_of_ncpus="12"
no_of_mpiprocs="12"
memory="40GB"
MOLCAS_MEM="8000"

#=============================================================================================
#                   MAIN PART   define the major loops ove various variables
#--------------------------------------------------------------------------------------------
cd $CurrDir
if [ ! -d "$FileDir" ]; then mkdir $FileDir ; fi

export Complex=$Complex

if [ "$numerical_gradient" -eq "yes" ]; then
   gradient_by_shifting_atoms   $Complex  mb  mb  $metal $active_electrons  $active_orbitals  &
fi

#------------------------------------------------------------
# run MB basis first, then project orbitals to a larger basis
export d="0"
my_molcas_run $Complex mb  mb  $metal $d &
wait
# prepare the files for EXPBAS:
copy $FileDir/"$Complex"_mb_mb_cas_"$active_space"_ms"${spinMS[0]}".RasOrb   $FileDir/"$Complex".INPORB
copy $FileDir/"$Complex"_mb_mb_cas_"$active_space"_ms"${spinMS[0]}".RunFile  $FileDir/"$Complex".RUNFIL1

#run larger basis calculations:
my_molcas_run  $Complex  vdzp  vdz   $metal  $d  &
my_molcas_run  $Complex  vtzp  vdzp  $metal  $d  &
my_molcas_run  $Complex  vqzp  vdzp  $metal  $d  &
wait
#--------------------------------------------------------------------------------------------


# clean-up all error files, in case all calculations finished ok
if [ $? -eq 0 ]; then
   rm -rf $CurrDir/*.err
   rm -rf $CurrDir/*.xml
   rm -rf $CurrDir/*.status
fi
exit 0

