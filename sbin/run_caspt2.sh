#!/bin/bash


run_caspt2(){
   local ms=$1
   local nproc=$2
   local nroots=$3
   #declare -a roots_in_CASPT2=(`echo $(seq $nroots)`)
   declare -a multiplets_in_XMUL=("${!4}")
   local roots_in_CASPT2=(`echo $(seq 1 $nroots)`)
   local separate_scripts=$5
   #  three  steps:
   #   -- run individual caspt2 calcualtions on each root, in batches over n_procs
   #   -- grep the HEFF and assemble the final caspt2 input
   #   -- run the final caspt2 and save JOBMIX file in $FileDir

   local rasscf_rc=0
   rasscf_rc=`grep 'Stop Module:' $CurrDir/"$Project"_rasscf_ms"$ms".out | grep 'rasscf' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
   echo      `grep 'Stop Module:' $CurrDir/"$Project"_rasscf_ms"$ms".out | grep 'rasscf' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
   echo      'rasscf_rc =' $rasscf_rc

#   if [ "$rasscf_rc" -eq 1 ] && [ -f $FileDir/"$Project"_ms"$ms".JobIph ] && [ -f $FileDir/"$Project"_ms"$ms".RunFile ] ; then
       #check on previous runs
       local caspt2_old_run=0
       if [ -f $CurrDir/"$Project"_"$PT2_TYPE"_caspt2_ms"$ms"_all_roots.out ] && [ -f $CurrDir/"$Project"_"$PT2_TYPE"_caspt2_ms"$ms".out ]; then
           caspt2_old_run=`grep 'Stop Module:' $CurrDir/"$Project"_"$PT2_TYPE"_caspt2_ms"$ms"_all_roots.out  $CurrDir/"$Project"_"$PT2_TYPE"_caspt2_ms"$ms".out | grep 'caspt2 ' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l`
       fi
       nval=$(( $nroots + 1 )); echo nval = $nval

       if [ ! -f $CurrDir/"$Project"_"$PT2_TYPE"_caspt2_ms"$ms".out ] || [ "$caspt2_old_run" -ne "$nval" ] || [ ! -f $FileDir/"$Project"_"$PT2_TYPE"_ms"$ms".JobMix ] ; then

           delete $RootDir/"$Project".JobIph
           copy $FileDir/"$Project"_ms"$ms".JobIph  $RootDir/$Project.JobIph
           copy $FileDir/"$Project"_ms"$ms".RunFile $RootDir/$Project.RunFile

           #check if it is the ground ms and if we have distributed L groups:
           if [ "$ms" == ${spinMS[0]} ] || ( [ "$same_multiplet" == "true" ] && [[ "$ms" == ${spinMS[1]} ]] ) ; then
               echo CASPT2::  ms=$ms is the ground spin defined in spinMS[] data array

               icountL=1; icount_root=0;
               previous_roots=0 ; indexL=0
               for iL in ${!groundL[@]}; do
                  let istart_state=$previous_roots+1
                  let iend_state=$istart_state+${groundL[$iL]}-1
                  echo CASPT2::  ms=$ms, iL=$iL compute all roots of this group start_root=$istart_state  end_state=$iend_state

                  icount_root_for_indexing_ONLY=0
                  for root in  $(seq $istart_state $iend_state) ; do
                      echo CASPT2::  run one single caspt2 for root  $root
                      let icount_root_for_indexing_ONLY=$icount_root_for_indexing_ONLY+1;
                      run_one_single_state_caspt2  $ms $root  ${groundL[$iL]}  $istart_state  $iend_state $PT2_TYPE  $icount_root_for_indexing_ONLY  &
                      let icount_root=$icount_root+1;
                      rem=$(( $icount_root % $nproc ))
                      if [ "$rem" -eq 0 ]; then
                           echo "Waiting a for the first batch of caspt2 calculations to finish: icount=" $icount_root
                           wait
                           sleep 1
                      fi
                  done

                  let previous_roots=$previous_roots+${groundL[$iL]};
               done
           else
               echo CASPT2::  ms=$ms is NOT the ground spin defined in spinMS[] data array

                  icount_root=0 ; istart_state=1; iend_state=$nroots ;
                  for root in  $(seq 1 $nroots ) ; do
                      echo CASPT2::  run one single caspt2 for  ms=$ms  root=$root
                      run_one_single_state_caspt2  $ms  $root  $nroots  $istart_state  $iend_state $PT2_TYPE  $root &
                      let icount_root=$icount_root+1;
                      rem=$(( $icount_root % $nproc ))
                      if [ "$rem" -eq 0 ]; then
                           echo "Waiting a for the first batch of caspt2 calculations to finish: icount=" $icount_root
                           wait
                           sleep 1
                      fi
                  done
           fi

           wait



           # group single-state caspt2 jobs according to allowed number of runs/node in scripts
           # and submit them

           #if [ "$separate_scripts" == "yes" ]; then
              #join_and_submit_individual_scripts
              #echo "all single state CASPT2 calculations wer submitted on separate jobs / nodes"
              #echo "Wait until all of them are finished and resumbit the initial script for "
              #echo "further execution & and data processing"
           #fi
           #wait

           #=========================
           # check if all single-root outputs are present in $CurrDir. If yes, then execute the following two commands:
           for root in ${roots_in_CASPT2[@]}; do
               let  final_code[$root]=0
           done
           echo " final_code array:" ${final_code[@]}
           check=0
           for root in ${roots_in_CASPT2[@]}; do
               if [ -f $CurrDir/"$Project"_"$PT2_TYPE"_caspt2_ms"$ms"_state_"$root".out ] ; then
                   final_code[$root]=`grep 'Stop Module:' $CurrDir/"$Project"_"$PT2_TYPE"_caspt2_ms"$ms"_state_"$root".out | grep 'caspt2' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l`
                   let check=$check+1
               fi
           done
           echo "Number of single-state CASPT2 calculations which finished OK: " $check
           echo "Final code " ${final_code[@]}


           if [ "$check" -eq ${#roots_in_CASPT2[@]} ]; then
              grep_and_join_heff $nroots $ms $PT2_TYPE
              wait
              sleep 1
              run_final_caspt2 $nroots $ms $PT2_TYPE
              wait
              sleep 1
           else
              echo "It seems that not all single-state CASPT2 calculations for MS="$ms" have successfully finished."
              for root in ${roots_in_CASPT2[@]}; do
                  if [ "${final_code[$root]}" -ne 1 ]; then
                      echo " --->  Single-state CASPT2 calculation for MS="$ms" and ROOT="$root" did not finish well."
                  fi
              done
              echo "please check again and eventually resubmit the original script"
           fi

       else
          echo "It looks like you had done this calculation. If not satisified, please remove the following files and re-submit:"
          echo "$CurrDir/"$Project"_"$PT2_TYPE"_caspt2_ms"$ms".out"
          echo "$CurrDir/"$Project"_"$PT2_TYPE"_caspt2_ms"$ms"_all_roots.out"
          echo "$FileDir/"$Project"_"$PT2_TYPE"_ms"$ms".JobMix"
       fi
 #  else
 #     echo "RASSCF calculation is probably not finished correctly, OR the required input files were not found. Please check."
 #     echo "The following files are needed: "
 #     echo "$FileDir/"$Project"_ms"$ms".JobIph"
 #     echo "$FileDir/"$Project"_ms"$ms".RunFile"
 #  fi
}

