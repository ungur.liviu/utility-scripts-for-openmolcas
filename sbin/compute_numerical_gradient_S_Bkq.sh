#!/bin/bash

function compute_numerical_gradient_S_Bkq() {

echo compute_numerical_gradient_S_Bkq::      CurrDir=$CurrDir
echo compute_numerical_gradient_S_Bkq::      FileFir=$FileDir
echo compute_numerical_gradient_S_Bkq::      Complex=$Complex
echo compute_numerical_gradient_S_Bkq::       LABELS=${LABEL[@]}
echo compute_numerical_gradient_S_Bkq::         bas1=$bas1
echo compute_numerical_gradient_S_Bkq::         bas2=$bas2
echo compute_numerical_gradient_S_Bkq:: active_space=$active_space
echo compute_numerical_gradient_S_Bkq::      npoints=$npoints

cd $CurrDir


#----------------------------------------------------------------------------
if   [[ $npoints -eq 2 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for iaxis in X Y Z; do
    for idist in $(seq 1 $npoints ); do
      termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | colrm 1 86 | sed 's/)//' | sed 's/\ //g' `
      dimS=`echo " 2 * $termS + 1 " | bc -l`
      grep  -A350 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A125 'Ab Initio Calculated Zero-Field Splitting Matrix' | grep -A4 '2 |  -2 | ' | sed 's/|//g' | colrm 11 21 > $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_"$iaxis"_"$idist".txt
    done
  done

  unset S_Bkq_X_1k S_Bkq_X_1q S_Bkq_X_1    S_Bkq_X_2k S_Bkq_X_2q S_Bkq_X_2
  unset S_Bkq_Y_1k S_Bkq_Y_1q S_Bkq_Y_1    S_Bkq_Y_2k S_Bkq_Y_2q S_Bkq_Y_2
  unset S_Bkq_Z_1k S_Bkq_Z_1q S_Bkq_Z_1    S_Bkq_Z_2k S_Bkq_Z_2q S_Bkq_Z_2

  declare -a S_Bkq_X_1k=( ) ; declare -a S_Bkq_X_1q=( ) ; declare -a S_Bkq_X_1=( ) ;
  declare -a S_Bkq_X_2k=( ) ; declare -a S_Bkq_X_2q=( ) ; declare -a S_Bkq_X_2=( ) ;
  declare -a S_Bkq_Y_1k=( ) ; declare -a S_Bkq_Y_1q=( ) ; declare -a S_Bkq_Y_1=( ) ;
  declare -a S_Bkq_Y_2k=( ) ; declare -a S_Bkq_Y_2q=( ) ; declare -a S_Bkq_Y_2=( ) ;
  declare -a S_Bkq_Z_1k=( ) ; declare -a S_Bkq_Z_1q=( ) ; declare -a S_Bkq_Z_1=( ) ;
  declare -a S_Bkq_Z_2k=( ) ; declare -a S_Bkq_Z_2q=( ) ; declare -a S_Bkq_Z_2=( ) ;

  index=0;
  while read -a line; do
    S_Bkq_X_1k[$index]=${line[0]}
    S_Bkq_X_1q[$index]=${line[1]}
    S_Bkq_X_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_1.txt
  index=0;
  while read -a line; do
    S_Bkq_X_2k[$index]=${line[0]}
    S_Bkq_X_2q[$index]=${line[1]}
    S_Bkq_X_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_2.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_1k[$index]=${line[0]}
    S_Bkq_Y_1q[$index]=${line[1]}
    S_Bkq_Y_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_1.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_2k[$index]=${line[0]}
    S_Bkq_Y_2q[$index]=${line[1]}
    S_Bkq_Y_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_2.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_1k[$index]=${line[0]}
    S_Bkq_Z_1q[$index]=${line[1]}
    S_Bkq_Z_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_1.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_2k[$index]=${line[0]}
    S_Bkq_Z_2q[$index]=${line[1]}
    S_Bkq_Z_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_2.txt
  S_Bkqroots=$index
  echo S_Bkqroots=$S_Bkqroots

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "GRADIENT OF ZFS (rank 2) Bkq PARAMETERS COMPUTED BY 2-POINT ATOM DISTORTION"                      > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "FORMULA USED:              "                                                                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "           -Bkq(1) +Bkq(2) "                                                                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "gradient = --------------- "                                                                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                2*delta    "                                                                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo " where:                    "                                                                     >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         delta  = $dist Bohr "                                                                   >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(1) = Bkq( -delta ) "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(2) = Bkq( +delta ) "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo " PseudoSpin S = $termS "                                                                         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    printf "%s %d\n" " Dimension 2S+1 =" "$dimS"                                                           >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                                                                                               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "The Crystal-Field Hamiltonian:                                                                 " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   Hcf = SUM_{k,q} * [ Bkq * O(k,q) ];                                                         " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "where:                                                                                         " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   O(k,q) =  Extended Stevens Operators (ESO)as defined in:                                    " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "          1. Rudowicz, C.; J.Phys.C: Solid State Phys.,18(1985) 1415-1430.                     " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "          2. Implemented in the "EasySpin" function in MATLAB, www.easyspin.org.               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   k - the rqnk of ITO of ZFS = 2                                                              " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   q - the component of the ITO, = -k, -k+1, ... 0, 1, ... k;                                  " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                                                                                               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "  Atom  | rank | proj. |      S_Bkq gradient-X       S_Bkq gradient-Y       S_Bkq gradient-Z  |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "Lbl. Nr.|  k   |   q   |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
  fi

  iroot=0 ;
  for i in $(seq 1 $S_Bkqroots ) ; do
    kL=${S_Bkq_X_1k[$iroot]}
    qL=${S_Bkq_X_1q[$iroot]}
    X1=`echo ${S_Bkq_X_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X2=`echo ${S_Bkq_X_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y1=`echo ${S_Bkq_Y_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y2=`echo ${S_Bkq_Y_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z1=`echo ${S_Bkq_Z_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z2=`echo ${S_Bkq_Z_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Xcm=`echo " ( - $X1 + $X2 ) / ( 2* $dist * 0.529177210903 ) " | bc -l`
    Ycm=`echo " ( - $Y1 + $Y2 ) / ( 2* $dist * 0.529177210903 ) " | bc -l`
    Zcm=`echo " ( - $Z1 + $Z2 ) / ( 2* $dist * 0.529177210903 ) " | bc -l`
    printf "%2s%4d %s %3d %s %3d %s %22.14e %22.14e %22.14e %s\n"  "$iat" "$indxATOM" " |" "$kL" " |" "$qL" "  |" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    ((iroot++))
  done
  echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt

  # delete the unnecessary large files. They might clutter the disc space
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_"$iaxis"_"$idist".txt
    done
  done
  let indxATOM=$indxATOM+1 ;
  #end  of the main loop over atoms
done





#----------------------------------------------------------------------------
elif   [[ $npoints -eq 4 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | colrm 1 86 | sed 's/)//'`
      dimS=`echo " 2 * $termS + 1 " | bc -l`
      grep  -A350 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A125 'Ab Initio Calculated Zero-Field Splitting Matrix' | grep -A4 '2 |  -2 | ' | sed 's/|//g' | colrm 11 21 > $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_"$iaxis"_"$idist".txt
    done
  done

  unset S_Bkq_X_1k S_Bkq_X_1q S_Bkq_X_1    S_Bkq_X_2k S_Bkq_X_2q S_Bkq_X_2     S_Bkq_X_3k S_Bkq_X_3q S_Bkq_X_3    S_Bkq_X_4k S_Bkq_X_4q S_Bkq_X_4
  unset S_Bkq_Y_1k S_Bkq_Y_1q S_Bkq_Y_1    S_Bkq_Y_2k S_Bkq_Y_2q S_Bkq_Y_2     S_Bkq_Y_3k S_Bkq_Y_3q S_Bkq_Y_3    S_Bkq_Y_4k S_Bkq_Y_4q S_Bkq_Y_4
  unset S_Bkq_Z_1k S_Bkq_Z_1q S_Bkq_Z_1    S_Bkq_Z_2k S_Bkq_Z_2q S_Bkq_Z_2     S_Bkq_Z_3k S_Bkq_Z_3q S_Bkq_Z_3    S_Bkq_Z_4k S_Bkq_Z_4q S_Bkq_Z_4

  declare -a S_Bkq_X_1k=( ) ; declare -a S_Bkq_X_1q=( ) ; declare -a S_Bkq_X_1=( ) ;
  declare -a S_Bkq_X_2k=( ) ; declare -a S_Bkq_X_2q=( ) ; declare -a S_Bkq_X_2=( ) ;
  declare -a S_Bkq_X_3k=( ) ; declare -a S_Bkq_X_3q=( ) ; declare -a S_Bkq_X_3=( ) ;
  declare -a S_Bkq_X_4k=( ) ; declare -a S_Bkq_X_4q=( ) ; declare -a S_Bkq_X_4=( ) ;
  declare -a S_Bkq_Y_1k=( ) ; declare -a S_Bkq_Y_1q=( ) ; declare -a S_Bkq_Y_1=( ) ;
  declare -a S_Bkq_Y_2k=( ) ; declare -a S_Bkq_Y_2q=( ) ; declare -a S_Bkq_Y_2=( ) ;
  declare -a S_Bkq_Y_3k=( ) ; declare -a S_Bkq_Y_3q=( ) ; declare -a S_Bkq_Y_3=( ) ;
  declare -a S_Bkq_Y_4k=( ) ; declare -a S_Bkq_Y_4q=( ) ; declare -a S_Bkq_Y_4=( ) ;
  declare -a S_Bkq_Z_1k=( ) ; declare -a S_Bkq_Z_1q=( ) ; declare -a S_Bkq_Z_1=( ) ;
  declare -a S_Bkq_Z_2k=( ) ; declare -a S_Bkq_Z_2q=( ) ; declare -a S_Bkq_Z_2=( ) ;
  declare -a S_Bkq_Z_3k=( ) ; declare -a S_Bkq_Z_3q=( ) ; declare -a S_Bkq_Z_3=( ) ;
  declare -a S_Bkq_Z_4k=( ) ; declare -a S_Bkq_Z_4q=( ) ; declare -a S_Bkq_Z_4=( ) ;

  index=0;
  while read -a line; do
    S_Bkq_X_1k[$index]=${line[0]}
    S_Bkq_X_1q[$index]=${line[1]}
    S_Bkq_X_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_1.txt
  index=0;
  while read -a line; do
    S_Bkq_X_2k[$index]=${line[0]}
    S_Bkq_X_2q[$index]=${line[1]}
    S_Bkq_X_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_2.txt
  index=0;
  while read -a line; do
    S_Bkq_X_3k[$index]=${line[0]}
    S_Bkq_X_3q[$index]=${line[1]}
    S_Bkq_X_3[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_3.txt
  index=0;
  while read -a line; do
    S_Bkq_X_4k[$index]=${line[0]}
    S_Bkq_X_4q[$index]=${line[1]}
    S_Bkq_X_4[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_4.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_1k[$index]=${line[0]}
    S_Bkq_Y_1q[$index]=${line[1]}
    S_Bkq_Y_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_1.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_2k[$index]=${line[0]}
    S_Bkq_Y_2q[$index]=${line[1]}
    S_Bkq_Y_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_2.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_3k[$index]=${line[0]}
    S_Bkq_Y_3q[$index]=${line[1]}
    S_Bkq_Y_3[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_3.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_4k[$index]=${line[0]}
    S_Bkq_Y_4q[$index]=${line[1]}
    S_Bkq_Y_4[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_4.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_1k[$index]=${line[0]}
    S_Bkq_Z_1q[$index]=${line[1]}
    S_Bkq_Z_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_1.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_2k[$index]=${line[0]}
    S_Bkq_Z_2q[$index]=${line[1]}
    S_Bkq_Z_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_2.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_3k[$index]=${line[0]}
    S_Bkq_Z_3q[$index]=${line[1]}
    S_Bkq_Z_3[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_3.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_4k[$index]=${line[0]}
    S_Bkq_Z_4q[$index]=${line[1]}
    S_Bkq_Z_4[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_4.txt
  S_Bkqroots=$index
  echo S_Bkqroots=$S_Bkqroots

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "GRADIENT OF S-Bkq PARAMETERS COMPUTED BY 4-POINT ATOM DISTORTION"                                 > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "FORMULA USED:                            "                                                       >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "           Bkq(1) -8*Bkq(2) +8*Bkq(3) -Bkq(4)"                                                   >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "gradient = ----------------------------------"                                                   >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                       12*delta "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo " where:                             "                                                            >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         delta  = $dist Bohr     "                                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(1) = Bkq( -2*delta ) "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(2) = Bkq( -  delta ) "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(3) = Bkq( +  delta ) "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(4) = Bkq( +2*delta ) "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo " PseudoSpin S = $termS "                                                                         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    printf "%s %d\n" " Dimension 2S+1 =" "$dimS"                                                           >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                                                                                               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "The Crystal-Field Hamiltonian:                                                                 " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   Hcf = SUM_{k,q} * [ Bkq * O(k,q) ];                                                         " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "where:                                                                                         " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   O(k,q) =  Extended Stevens Operators (ESO)as defined in:                                    " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "          1. Rudowicz, C.; J.Phys.C: Solid State Phys.,18(1985) 1415-1430.                     " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "          2. Implemented in the "EasySpin" function in MATLAB, www.easyspin.org.               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   k - the rank of the ITO, = 2, 4, 6, 8, 10, 12.                                              " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   q - the component of the ITO, = -k, -k+1, ... 0, 1, ... k;                                  " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                                                                                               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "  Atom  | rank | proj. |      S_Bkq gradient-X       S_Bkq gradient-Y       S_Bkq gradient-Z  |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "Lbl. Nr.|  k   |   q   |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
  fi

  iroot=0;
  for i in $(seq 1 $S_Bkqroots ); do
    kL=${S_Bkq_X_1k[$iroot]}
    qL=${S_Bkq_X_1q[$iroot]}
    X1=`echo ${S_Bkq_X_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X2=`echo ${S_Bkq_X_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X3=`echo ${S_Bkq_X_3[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X4=`echo ${S_Bkq_X_4[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y1=`echo ${S_Bkq_Y_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y2=`echo ${S_Bkq_Y_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y3=`echo ${S_Bkq_Y_3[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y4=`echo ${S_Bkq_Y_4[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z1=`echo ${S_Bkq_Z_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z2=`echo ${S_Bkq_Z_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z3=`echo ${S_Bkq_Z_3[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z4=`echo ${S_Bkq_Z_4[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Xcm=`echo " ( $X1 -8* $X2 +8* $X3 - $X4 ) / ( 12* $dist * 0.529177210903 ) " | bc -l`
    Ycm=`echo " ( $Y1 -8* $Y2 +8* $Y3 - $Y4 ) / ( 12* $dist * 0.529177210903 ) " | bc -l`
    Zcm=`echo " ( $Z1 -8* $Z2 +8* $Z3 - $Z4 ) / ( 12* $dist * 0.529177210903 ) " | bc -l`
    printf "%2s%4d %s %3d %s %3d %s %22.14e %22.14e %22.14e %s\n"  "$iat" "$indxATOM" " |" "$kL" " |" "$qL" "  |" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    ((iroot++))
  done
  echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt

  # delete the unnecessary large files. They might clutter the disc space
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_"$iaxis"_"$idist".txt
    done
  done
  #end  of the main loop over atoms
  let indxATOM=$indxATOM+1 ;
done



#----------------------------------------------------------------------------
elif   [[ $npoints -eq 6 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | colrm 1 86 | sed 's/)//'`
      dimS=`echo " 2 * $termS + 1 " | bc -l`
      grep  -A350 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A125 'Ab Initio Calculated Zero-Field Splitting Matrix' | grep -A4 '2 |  -2 | ' | sed 's/|//g' | colrm 11 21 > $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_"$iaxis"_"$idist".txt
    done
  done

  unset S_Bkq_X_1k S_Bkq_X_1q S_Bkq_X_1    S_Bkq_X_2k S_Bkq_X_2q S_Bkq_X_2     S_Bkq_X_3k S_Bkq_X_3q S_Bkq_X_3    S_Bkq_X_4k S_Bkq_X_4q S_Bkq_X_4    S_Bkq_X_5k S_Bkq_X_5q S_Bkq_X_5    S_Bkq_X_6k S_Bkq_X_6q S_Bkq_X_6
  unset S_Bkq_Y_1k S_Bkq_Y_1q S_Bkq_Y_1    S_Bkq_Y_2k S_Bkq_Y_2q S_Bkq_Y_2     S_Bkq_Y_3k S_Bkq_Y_3q S_Bkq_Y_3    S_Bkq_Y_4k S_Bkq_Y_4q S_Bkq_Y_4    S_Bkq_Y_5k S_Bkq_Y_5q S_Bkq_Y_5    S_Bkq_Y_6k S_Bkq_Y_6q S_Bkq_Y_6
  unset S_Bkq_Z_1k S_Bkq_Z_1q S_Bkq_Z_1    S_Bkq_Z_2k S_Bkq_Z_2q S_Bkq_Z_2     S_Bkq_Z_3k S_Bkq_Z_3q S_Bkq_Z_3    S_Bkq_Z_4k S_Bkq_Z_4q S_Bkq_Z_4    S_Bkq_Z_5k S_Bkq_Z_5q S_Bkq_Z_5    S_Bkq_Z_6k S_Bkq_Z_6q S_Bkq_Z_6

  declare -a S_Bkq_X_1k=( ) ; declare -a S_Bkq_X_1q=( ) ; declare -a S_Bkq_X_1=( ) ;
  declare -a S_Bkq_X_2k=( ) ; declare -a S_Bkq_X_2q=( ) ; declare -a S_Bkq_X_2=( ) ;
  declare -a S_Bkq_X_3k=( ) ; declare -a S_Bkq_X_3q=( ) ; declare -a S_Bkq_X_3=( ) ;
  declare -a S_Bkq_X_4k=( ) ; declare -a S_Bkq_X_4q=( ) ; declare -a S_Bkq_X_4=( ) ;
  declare -a S_Bkq_X_5k=( ) ; declare -a S_Bkq_X_5q=( ) ; declare -a S_Bkq_X_5=( ) ;
  declare -a S_Bkq_X_6k=( ) ; declare -a S_Bkq_X_6q=( ) ; declare -a S_Bkq_X_6=( ) ;
  declare -a S_Bkq_Y_1k=( ) ; declare -a S_Bkq_Y_1q=( ) ; declare -a S_Bkq_Y_1=( ) ;
  declare -a S_Bkq_Y_2k=( ) ; declare -a S_Bkq_Y_2q=( ) ; declare -a S_Bkq_Y_2=( ) ;
  declare -a S_Bkq_Y_3k=( ) ; declare -a S_Bkq_Y_3q=( ) ; declare -a S_Bkq_Y_3=( ) ;
  declare -a S_Bkq_Y_4k=( ) ; declare -a S_Bkq_Y_4q=( ) ; declare -a S_Bkq_Y_4=( ) ;
  declare -a S_Bkq_Y_5k=( ) ; declare -a S_Bkq_Y_5q=( ) ; declare -a S_Bkq_Y_5=( ) ;
  declare -a S_Bkq_Y_6k=( ) ; declare -a S_Bkq_Y_6q=( ) ; declare -a S_Bkq_Y_6=( ) ;
  declare -a S_Bkq_Z_1k=( ) ; declare -a S_Bkq_Z_1q=( ) ; declare -a S_Bkq_Z_1=( ) ;
  declare -a S_Bkq_Z_2k=( ) ; declare -a S_Bkq_Z_2q=( ) ; declare -a S_Bkq_Z_2=( ) ;
  declare -a S_Bkq_Z_3k=( ) ; declare -a S_Bkq_Z_3q=( ) ; declare -a S_Bkq_Z_3=( ) ;
  declare -a S_Bkq_Z_4k=( ) ; declare -a S_Bkq_Z_4q=( ) ; declare -a S_Bkq_Z_4=( ) ;
  declare -a S_Bkq_Z_5k=( ) ; declare -a S_Bkq_Z_5q=( ) ; declare -a S_Bkq_Z_5=( ) ;
  declare -a S_Bkq_Z_6k=( ) ; declare -a S_Bkq_Z_6q=( ) ; declare -a S_Bkq_Z_6=( ) ;


  index=0;
  while read -a line; do
    S_Bkq_X_1k[$index]=${line[0]}
    S_Bkq_X_1q[$index]=${line[1]}
    S_Bkq_X_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_1.txt
  index=0;
  while read -a line; do
    S_Bkq_X_2k[$index]=${line[0]}
    S_Bkq_X_2q[$index]=${line[1]}
    S_Bkq_X_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_2.txt
  index=0;
  while read -a line; do
    S_Bkq_X_3k[$index]=${line[0]}
    S_Bkq_X_3q[$index]=${line[1]}
    S_Bkq_X_3[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_3.txt
  index=0;
  while read -a line; do
    S_Bkq_X_4k[$index]=${line[0]}
    S_Bkq_X_4q[$index]=${line[1]}
    S_Bkq_X_4[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_4.txt
  index=0;
  while read -a line; do
    S_Bkq_X_5k[$index]=${line[0]}
    S_Bkq_X_5q[$index]=${line[1]}
    S_Bkq_X_5[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_5.txt
  index=0;
  while read -a line; do
    S_Bkq_X_6k[$index]=${line[0]}
    S_Bkq_X_6q[$index]=${line[1]}
    S_Bkq_X_6[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_6.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_1k[$index]=${line[0]}
    S_Bkq_Y_1q[$index]=${line[1]}
    S_Bkq_Y_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_1.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_2k[$index]=${line[0]}
    S_Bkq_Y_2q[$index]=${line[1]}
    S_Bkq_Y_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_2.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_3k[$index]=${line[0]}
    S_Bkq_Y_3q[$index]=${line[1]}
    S_Bkq_Y_3[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_3.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_4k[$index]=${line[0]}
    S_Bkq_Y_4q[$index]=${line[1]}
    S_Bkq_Y_4[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_4.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_5k[$index]=${line[0]}
    S_Bkq_Y_5q[$index]=${line[1]}
    S_Bkq_Y_5[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_5.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_6k[$index]=${line[0]}
    S_Bkq_Y_6q[$index]=${line[1]}
    S_Bkq_Y_6[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_6.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_1k[$index]=${line[0]}
    S_Bkq_Z_1q[$index]=${line[1]}
    S_Bkq_Z_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_1.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_2k[$index]=${line[0]}
    S_Bkq_Z_2q[$index]=${line[1]}
    S_Bkq_Z_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_2.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_3k[$index]=${line[0]}
    S_Bkq_Z_3q[$index]=${line[1]}
    S_Bkq_Z_3[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_3.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_4k[$index]=${line[0]}
    S_Bkq_Z_4q[$index]=${line[1]}
    S_Bkq_Z_4[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_4.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_5k[$index]=${line[0]}
    S_Bkq_Z_5q[$index]=${line[1]}
    S_Bkq_Z_5[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_5.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_6k[$index]=${line[0]}
    S_Bkq_Z_6q[$index]=${line[1]}
    S_Bkq_Z_6[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_6.txt
  S_Bkqroots=$index

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "GRADIENT OF S-Bkq PARAMETERS COMPUTED BY 6-POINT ATOM DISTORTION"                                 > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "FORMULA USED:                                          "                                         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "           -Bkq(1) +9*Bkq(2) -45*Bkq(3) +45*Bkq(4) -9*Bkq(5) +Bkq(6) "                           >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "gradient = --------------------------------------------------------- "                           >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                                  60*delta                           "                           >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo " where:                            "                                                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         delta  = $dist Bohr       "                                                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(1) = Bkq( -3*delta )  "                                                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(2) = Bkq( -2*delta )  "                                                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(3) = Bkq( -  delta )  "                                                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(4) = Bkq( +  delta )  "                                                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(5) = Bkq( +2*delta )  "                                                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(6) = Bkq( +3*delta )  "                                                             >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo " PseudoSpin S = $termS "                                                                         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    printf "%s %d\n" " Dimension 2S+1 =" "$dimS"                                                           >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                                                                                               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "The Crystal-Field Hamiltonian:                                                                 " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   Hcf = SUM_{k,q} * [ Bkq * O(k,q) ];                                                         " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "where:                                                                                         " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   O(k,q) =  Extended Stevens Operators (ESO)as defined in:                                    " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "          1. Rudowicz, C.; J.Phys.C: Solid State Phys.,18(1985) 1415-1430.                     " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "          2. Implemented in the "EasySpin" function in MATLAB, www.easyspin.org.               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   k - the rank of the ITO, = 2, 4, 6, 8, 10, 12.                                              " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   q - the component of the ITO, = -k, -k+1, ... 0, 1, ... k;                                  " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                                                                                               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "  Atom  | rank | proj. |      S_Bkq gradient-X       S_Bkq gradient-Y       S_Bkq gradient-Z  |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "Lbl. Nr.|  k   |   q   |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
  fi

  iroot=0;
  for i in $(seq 1 $S_Bkqroots ); do
    kL=${S_Bkq_X_1k[$iroot]}
    qL=${S_Bkq_X_1q[$iroot]}
    X1=`echo ${S_Bkq_X_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X2=`echo ${S_Bkq_X_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X3=`echo ${S_Bkq_X_3[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X4=`echo ${S_Bkq_X_4[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X5=`echo ${S_Bkq_X_5[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X6=`echo ${S_Bkq_X_6[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y1=`echo ${S_Bkq_Y_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y2=`echo ${S_Bkq_Y_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y3=`echo ${S_Bkq_Y_3[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y4=`echo ${S_Bkq_Y_4[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y5=`echo ${S_Bkq_Y_5[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y6=`echo ${S_Bkq_Y_6[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z1=`echo ${S_Bkq_Z_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z2=`echo ${S_Bkq_Z_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z3=`echo ${S_Bkq_Z_3[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z4=`echo ${S_Bkq_Z_4[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z5=`echo ${S_Bkq_Z_5[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z6=`echo ${S_Bkq_Z_6[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Xcm=`echo " ( - $X1 +9* $X2 -45* $X3 +45* $X4 -9* $X5 + $X6 ) / ( 60* $dist * 0.529177210903 ) " | bc -l`
    Ycm=`echo " ( - $Y1 +9* $Y2 -45* $Y3 +45* $Y4 -9* $Y5 + $Y6 ) / ( 60* $dist * 0.529177210903 ) " | bc -l`
    Zcm=`echo " ( - $Z1 +9* $Z2 -45* $Z3 +45* $Z4 -9* $Z5 + $Z6 ) / ( 60* $dist * 0.529177210903 ) " | bc -l`
    printf "%2s%4d %s %3d %s %3d %s %22.14e %22.14e %22.14e %s\n"  "$iat" "$indxATOM" " |" "$kL" " |" "$qL" "  |" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    ((iroot++))
  done
  echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt

  # delete the unnecessary large files. They might clutter the disc space
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_"$iaxis"_"$idist".txt
    done
  done
  #end  of the main loop over atoms
  let indxATOM=$indxATOM+1 ;
done



#----------------------------------------------------------------------------
elif   [[ $npoints -eq 8 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | colrm 1 86 | sed 's/)//'`
      dimS=`echo " 2 * $termS + 1 " | bc -l`
      grep  -A350 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_"$iaxis"_"$idist"_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A125 'Ab Initio Calculated Zero-Field Splitting Matrix' | grep -A4 '2 |  -2 | ' | sed 's/|//g' | colrm 11 21 > $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_"$iaxis"_"$idist".txt
    done
  done

  unset S_Bkq_X_1k S_Bkq_X_1q S_Bkq_X_1    S_Bkq_X_2k S_Bkq_X_2q S_Bkq_X_2     S_Bkq_X_3k S_Bkq_X_3q S_Bkq_X_3    S_Bkq_X_4k S_Bkq_X_4q S_Bkq_X_4    S_Bkq_X_5k S_Bkq_X_5q S_Bkq_X_5    S_Bkq_X_6k S_Bkq_X_6q S_Bkq_X_6    S_Bkq_X_7k S_Bkq_X_7q S_Bkq_X_7    S_Bkq_X_8k S_Bkq_X_8q S_Bkq_X_8
  unset S_Bkq_Y_1k S_Bkq_Y_1q S_Bkq_Y_1    S_Bkq_Y_2k S_Bkq_Y_2q S_Bkq_Y_2     S_Bkq_Y_3k S_Bkq_Y_3q S_Bkq_Y_3    S_Bkq_Y_4k S_Bkq_Y_4q S_Bkq_Y_4    S_Bkq_Y_5k S_Bkq_Y_5q S_Bkq_Y_5    S_Bkq_Y_6k S_Bkq_Y_6q S_Bkq_Y_6    S_Bkq_Y_7k S_Bkq_Y_7q S_Bkq_Y_7    S_Bkq_Y_8k S_Bkq_Y_8q S_Bkq_Y_8
  unset S_Bkq_Z_1k S_Bkq_Z_1q S_Bkq_Z_1    S_Bkq_Z_2k S_Bkq_Z_2q S_Bkq_Z_2     S_Bkq_Z_3k S_Bkq_Z_3q S_Bkq_Z_3    S_Bkq_Z_4k S_Bkq_Z_4q S_Bkq_Z_4    S_Bkq_Z_5k S_Bkq_Z_5q S_Bkq_Z_5    S_Bkq_Z_6k S_Bkq_Z_6q S_Bkq_Z_6    S_Bkq_Z_7k S_Bkq_Z_7q S_Bkq_Z_7    S_Bkq_Z_8k S_Bkq_Z_8q S_Bkq_Z_8

  declare -a S_Bkq_X_1k=( ) ; declare -a S_Bkq_X_1q=( ) ; declare -a S_Bkq_X_1=( ) ;
  declare -a S_Bkq_X_2k=( ) ; declare -a S_Bkq_X_2q=( ) ; declare -a S_Bkq_X_2=( ) ;
  declare -a S_Bkq_X_3k=( ) ; declare -a S_Bkq_X_3q=( ) ; declare -a S_Bkq_X_3=( ) ;
  declare -a S_Bkq_X_4k=( ) ; declare -a S_Bkq_X_4q=( ) ; declare -a S_Bkq_X_4=( ) ;
  declare -a S_Bkq_X_5k=( ) ; declare -a S_Bkq_X_5q=( ) ; declare -a S_Bkq_X_5=( ) ;
  declare -a S_Bkq_X_6k=( ) ; declare -a S_Bkq_X_6q=( ) ; declare -a S_Bkq_X_6=( ) ;
  declare -a S_Bkq_X_7k=( ) ; declare -a S_Bkq_X_7q=( ) ; declare -a S_Bkq_X_7=( ) ;
  declare -a S_Bkq_X_8k=( ) ; declare -a S_Bkq_X_8q=( ) ; declare -a S_Bkq_X_8=( ) ;
  declare -a S_Bkq_Y_1k=( ) ; declare -a S_Bkq_Y_1q=( ) ; declare -a S_Bkq_Y_1=( ) ;
  declare -a S_Bkq_Y_2k=( ) ; declare -a S_Bkq_Y_2q=( ) ; declare -a S_Bkq_Y_2=( ) ;
  declare -a S_Bkq_Y_3k=( ) ; declare -a S_Bkq_Y_3q=( ) ; declare -a S_Bkq_Y_3=( ) ;
  declare -a S_Bkq_Y_4k=( ) ; declare -a S_Bkq_Y_4q=( ) ; declare -a S_Bkq_Y_4=( ) ;
  declare -a S_Bkq_Y_5k=( ) ; declare -a S_Bkq_Y_5q=( ) ; declare -a S_Bkq_Y_5=( ) ;
  declare -a S_Bkq_Y_6k=( ) ; declare -a S_Bkq_Y_6q=( ) ; declare -a S_Bkq_Y_6=( ) ;
  declare -a S_Bkq_Y_7k=( ) ; declare -a S_Bkq_Y_7q=( ) ; declare -a S_Bkq_Y_7=( ) ;
  declare -a S_Bkq_Y_8k=( ) ; declare -a S_Bkq_Y_8q=( ) ; declare -a S_Bkq_Y_8=( ) ;
  declare -a S_Bkq_Z_1k=( ) ; declare -a S_Bkq_Z_1q=( ) ; declare -a S_Bkq_Z_1=( ) ;
  declare -a S_Bkq_Z_2k=( ) ; declare -a S_Bkq_Z_2q=( ) ; declare -a S_Bkq_Z_2=( ) ;
  declare -a S_Bkq_Z_3k=( ) ; declare -a S_Bkq_Z_3q=( ) ; declare -a S_Bkq_Z_3=( ) ;
  declare -a S_Bkq_Z_4k=( ) ; declare -a S_Bkq_Z_4q=( ) ; declare -a S_Bkq_Z_4=( ) ;
  declare -a S_Bkq_Z_5k=( ) ; declare -a S_Bkq_Z_5q=( ) ; declare -a S_Bkq_Z_5=( ) ;
  declare -a S_Bkq_Z_6k=( ) ; declare -a S_Bkq_Z_6q=( ) ; declare -a S_Bkq_Z_6=( ) ;
  declare -a S_Bkq_Z_7k=( ) ; declare -a S_Bkq_Z_7q=( ) ; declare -a S_Bkq_Z_7=( ) ;
  declare -a S_Bkq_Z_8k=( ) ; declare -a S_Bkq_Z_8q=( ) ; declare -a S_Bkq_Z_8=( ) ;


  index=0;
  while read -a line; do
    S_Bkq_X_1k[$index]=${line[0]}
    S_Bkq_X_1q[$index]=${line[1]}
    S_Bkq_X_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_1.txt
  index=0;
  while read -a line; do
    S_Bkq_X_2k[$index]=${line[0]}
    S_Bkq_X_2q[$index]=${line[1]}
    S_Bkq_X_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_2.txt
  index=0;
  while read -a line; do
    S_Bkq_X_3k[$index]=${line[0]}
    S_Bkq_X_3q[$index]=${line[1]}
    S_Bkq_X_3[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_3.txt
  index=0;
  while read -a line; do
    S_Bkq_X_4k[$index]=${line[0]}
    S_Bkq_X_4q[$index]=${line[1]}
    S_Bkq_X_4[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_4.txt
  index=0;
  while read -a line; do
    S_Bkq_X_5k[$index]=${line[0]}
    S_Bkq_X_5q[$index]=${line[1]}
    S_Bkq_X_5[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_5.txt
  index=0;
  while read -a line; do
    S_Bkq_X_6k[$index]=${line[0]}
    S_Bkq_X_6q[$index]=${line[1]}
    S_Bkq_X_6[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_6.txt
  index=0;
  while read -a line; do
    S_Bkq_X_7k[$index]=${line[0]}
    S_Bkq_X_7q[$index]=${line[1]}
    S_Bkq_X_7[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_7.txt
  index=0;
  while read -a line; do
    S_Bkq_X_8k[$index]=${line[0]}
    S_Bkq_X_8q[$index]=${line[1]}
    S_Bkq_X_8[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_X_8.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_1k[$index]=${line[0]}
    S_Bkq_Y_1q[$index]=${line[1]}
    S_Bkq_Y_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_1.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_2k[$index]=${line[0]}
    S_Bkq_Y_2q[$index]=${line[1]}
    S_Bkq_Y_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_2.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_3k[$index]=${line[0]}
    S_Bkq_Y_3q[$index]=${line[1]}
    S_Bkq_Y_3[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_3.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_4k[$index]=${line[0]}
    S_Bkq_Y_4q[$index]=${line[1]}
    S_Bkq_Y_4[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_4.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_5k[$index]=${line[0]}
    S_Bkq_Y_5q[$index]=${line[1]}
    S_Bkq_Y_5[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_5.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_6k[$index]=${line[0]}
    S_Bkq_Y_6q[$index]=${line[1]}
    S_Bkq_Y_6[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_6.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_7k[$index]=${line[0]}
    S_Bkq_Y_7q[$index]=${line[1]}
    S_Bkq_Y_7[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_7.txt
  index=0;
  while read -a line; do
    S_Bkq_Y_8k[$index]=${line[0]}
    S_Bkq_Y_8q[$index]=${line[1]}
    S_Bkq_Y_8[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Y_8.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_1k[$index]=${line[0]}
    S_Bkq_Z_1q[$index]=${line[1]}
    S_Bkq_Z_1[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_1.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_2k[$index]=${line[0]}
    S_Bkq_Z_2q[$index]=${line[1]}
    S_Bkq_Z_2[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_2.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_3k[$index]=${line[0]}
    S_Bkq_Z_3q[$index]=${line[1]}
    S_Bkq_Z_3[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_3.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_4k[$index]=${line[0]}
    S_Bkq_Z_4q[$index]=${line[1]}
    S_Bkq_Z_4[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_4.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_5k[$index]=${line[0]}
    S_Bkq_Z_5q[$index]=${line[1]}
    S_Bkq_Z_5[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_5.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_6k[$index]=${line[0]}
    S_Bkq_Z_6q[$index]=${line[1]}
    S_Bkq_Z_6[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_6.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_7k[$index]=${line[0]}
    S_Bkq_Z_7q[$index]=${line[1]}
    S_Bkq_Z_7[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_7.txt
  index=0;
  while read -a line; do
    S_Bkq_Z_8k[$index]=${line[0]}
    S_Bkq_Z_8q[$index]=${line[1]}
    S_Bkq_Z_8[$index]=${line[2]}
    ((index++))
  done < $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_Z_8.txt
  S_Bkqroots=$index

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "GRADIENT OF S-Bkq PARAMETERS COMPUTED BY 8-POINT ATOM DISTORTION"                                    > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "FORMULA USED:                                          "                                            >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "           3*Bkq(1) -32*Bkq(2) +168*Bkq(3) -672*Bkq(4) +672*Bkq(5) -168*Bkq(6) +32*Bkq(7) -3*Bkq(8)">> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "gradient = ----------------------------------------------------------------------------------------">> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                                                   840*delta"                                       >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo " where:                            "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         delta  = $dist Bohr       "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(1) = Bkq( -4*delta )  "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(2) = Bkq( -3*delta )  "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(3) = Bkq( -2*delta )  "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(4) = Bkq( -  delta )  "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(5) = Bkq( +  delta )  "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(6) = Bkq( +2*delta )  "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(7) = Bkq( +3*delta )  "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "         Bkq(8) = Bkq( +4*delta )  "                                                                >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo " PseudoSpin S = $termS "                                                                         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    printf "%s %d\n" " Dimension 2S+1 =" "$dimS"                                                           >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                                                                                               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "The Crystal-Field Hamiltonian:                                                                 " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   Hcf = SUM_{k,q} * [ Bkq * O(k,q) ];                                                         " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "where:                                                                                         " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   O(k,q) =  Extended Stevens Operators (ESO)as defined in:                                    " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "          1. Rudowicz, C.; J.Phys.C: Solid State Phys.,18(1985) 1415-1430.                     " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "          2. Implemented in the "EasySpin" function in MATLAB, www.easyspin.org.               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   k - the rank of the ITO, = 2, 4, 6, 8, 10, 12.                                              " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "   q - the component of the ITO, = -k, -k+1, ... 0, 1, ... k;                                  " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "                                                                                               " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "  Atom  | rank | proj. |      S_Bkq gradient-X       S_Bkq gradient-Y       S_Bkq gradient-Z  |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "Lbl. Nr.|  k   |   q   |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    echo "--------|------|-------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
  fi

  iroot=0;
  for i in $(seq 1 $S_Bkqroots ); do
    kL=${S_Bkq_X_1k[$iroot]}
    qL=${S_Bkq_X_1q[$iroot]}
    X1=`echo ${S_Bkq_X_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X2=`echo ${S_Bkq_X_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X3=`echo ${S_Bkq_X_3[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X4=`echo ${S_Bkq_X_4[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X5=`echo ${S_Bkq_X_5[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X6=`echo ${S_Bkq_X_6[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X7=`echo ${S_Bkq_X_7[$iroot]} | sed 's/[eE]+*/*10^/g'`
    X8=`echo ${S_Bkq_X_8[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y1=`echo ${S_Bkq_Y_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y2=`echo ${S_Bkq_Y_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y3=`echo ${S_Bkq_Y_3[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y4=`echo ${S_Bkq_Y_4[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y5=`echo ${S_Bkq_Y_5[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y6=`echo ${S_Bkq_Y_6[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y7=`echo ${S_Bkq_Y_7[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Y8=`echo ${S_Bkq_Y_8[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z1=`echo ${S_Bkq_Z_1[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z2=`echo ${S_Bkq_Z_2[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z3=`echo ${S_Bkq_Z_3[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z4=`echo ${S_Bkq_Z_4[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z5=`echo ${S_Bkq_Z_5[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z6=`echo ${S_Bkq_Z_6[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z7=`echo ${S_Bkq_Z_7[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Z8=`echo ${S_Bkq_Z_8[$iroot]} | sed 's/[eE]+*/*10^/g'`
    Xcm=`echo " ( 3* $X1 -32* $X2 +168* $X3 -672* $X4 +672* $X5 -168* $X6 +32* $X7 -3* $X8 ) / ( 840* $dist * 0.529177210903 ) " | bc -l`
    Ycm=`echo " ( 3* $Y1 -32* $Y2 +168* $Y3 -672* $Y4 +672* $Y5 -168* $Y6 +32* $Y7 -3* $Y8 ) / ( 840* $dist * 0.529177210903 ) " | bc -l`
    Zcm=`echo " ( 3* $Z1 -32* $Z2 +168* $Z3 -672* $Z4 +672* $Z5 -168* $Z6 +32* $Z7 -3* $Z8 ) / ( 840* $dist * 0.529177210903 ) " | bc -l`
    printf "%2s%4d %s %3d %s %3d %s %22.14e %22.14e %22.14e %s\n"  "$iat" "$indxATOM" " |" "$kL" " |" "$qL" "  |" $Xcm $Ycm $Zcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt
    ((iroot++))
  done
  echo "--------|------|-------|----------------------------------------------------------------------|"    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_S_Bkq"$npoints"_gradient.txt

  # delete the unnecessary large files. They might clutter the disc space
  for iaxis in X Y Z ; do
    for idist in $(seq 1 $npoints ); do
      delete $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_S_Bkq_"$iaxis"_"$idist".txt
    done
  done
  #end  of the main loop over atoms
  let indxATOM=$indxATOM+1 ;
done





#----------------------------------------------------------------------------
#elif   [[ $npoints -eq 8 ]] ; then
else
    echo "not yet implemented"
#----------------------------------------------------------------------------
#elif   [[ $npoints -eq 10 ]] ; then
    echo "not yet implemented"
#----------------------------------------------------------------------------
#elif   [[ $npoints -eq 12 ]] ; then
    echo "not yet implemented"
#----------------------------------------------------------------------------
fi


}

