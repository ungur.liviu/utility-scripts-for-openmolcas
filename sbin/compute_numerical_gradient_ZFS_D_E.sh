#!/bin/bash
#        compute_numerical_gradient_ZFS_D_E
#        compute_numerical_gradient_J_D


function compute_numerical_gradient_ZFS_D_E() {

echo compute_numerical_gradient_ZFS_D_E::      CurrDir=$CurrDir
echo compute_numerical_gradient_ZFS_D_E::      FileFir=$FileDir
echo compute_numerical_gradient_ZFS_D_E::      Complex=$Complex
echo compute_numerical_gradient_ZFS_D_E::       LABELS=${LABEL[@]}
echo compute_numerical_gradient_ZFS_D_E::         bas1=$bas1
echo compute_numerical_gradient_ZFS_D_E::         bas2=$bas2
echo compute_numerical_gradient_ZFS_D_E:: active_space=$active_space
echo compute_numerical_gradient_ZFS_D_E::      npoints=$npoints

cd $CurrDir


#----------------------------------------------------------------------------
if   [[ $npoints -eq 2 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do

  unset D_X1 D_X2 D_Y1 D_Y2 D_Z1 D_Z2
  unset E_X1 E_X2 E_Y1 E_Y2 E_Z1 E_Z2

  termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | colrm 1 86 | sed 's/)//'`

  D_X1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`

  E_X1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "GRADIENT OF ZFS PARAMETERS D and E COMPUTED BY 2-POINT ATOM DISTORTION"                        > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "FORMULA USED:            "                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "             -D(1) +D(2) "                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "gradient D = ----------- "                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "               2*delta   "                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo " where:                  "                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         delta = $dist Bohr "                                                                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(1) = D( -delta ) "                                                                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(2) = D( +delta ) "                                                                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo " PseudoSpin S = $termS "                                                                      >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "                                                                                            " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "--------|-----------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "  Atom  |    ZFS    |          gradient-X             gradient-Y             gradient-Z    |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "Lbl. Nr.| parameter |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "--------|-----------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  fi

  DXcm=`echo " ( - $D_X1 + $D_X2 ) / ( 2* $dist * 0.529177210903 ) " | bc -l`
  DYcm=`echo " ( - $D_Y1 + $D_Y2 ) / ( 2* $dist * 0.529177210903 ) " | bc -l`
  DZcm=`echo " ( - $D_Z1 + $D_Z2 ) / ( 2* $dist * 0.529177210903 ) " | bc -l`

  EXcm=`echo " ( - $E_X1 + $E_X2 ) / ( 2* $dist * 0.529177210903 ) " | bc -l`
  EYcm=`echo " ( - $E_Y1 + $E_Y2 ) / ( 2* $dist * 0.529177210903 ) " | bc -l`
  EZcm=`echo " ( - $E_Z1 + $E_Z2 ) / ( 2* $dist * 0.529177210903 ) " | bc -l`
  printf "%2s%4d %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |     D     |"  $DXcm $DYcm $DZcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  printf "%2s%4d %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |     E     |"  $EXcm $EYcm $EZcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  echo "--------|-----------|----------------------------------------------------------------------|"         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt

  let indxATOM=$indxATOM+1 ;
  #end  of the main loop over atoms
done





#----------------------------------------------------------------------------
elif   [[ $npoints -eq 4 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do

  unset D_X1 D_X2 D_X3 D_X4   D_Y1 D_Y2 D_Y3 D_Y4   D_Z1 D_Z2 D_Z3 D_Z4
  unset E_X1 E_X2 E_X3 E_X4   E_Y1 E_Y2 E_Y3 E_Y4   E_Z1 E_Z2 E_Z3 E_Z4
  termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | colrm 1 86 | sed 's/)//'`

  D_X1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`

  E_X1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`

  echo E_Z4=$E_Z4
  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "GRADIENT OF ZFS PARAMETERS D and E COMPUTED BY 4-POINT ATOM DISTORTION"                        > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "FORMULA USED:            "                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "           D(1) -8*D(2) +8*D(3) -D(4)"                                                        >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "gradient = --------------------------"                                                        >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "                   12*delta          "                                                        >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo " where:                  "                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         delta = $dist Bohr "                                                                 >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(1) = D( -2*delta ) "                                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(2) = D( -  delta ) "                                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(3) = D( +  delta ) "                                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(4) = D( +2*delta ) "                                                               >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo " PseudoSpin S = $termS "                                                                      >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "                                                                                            " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "--------|-----------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "  Atom  |    ZFS    |          gradient-X             gradient-Y             gradient-Z    |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "Lbl. Nr.| parameter |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "--------|-----------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  fi

  DXcm=`echo " ( $D_X1 -8* $D_X2 +8* $D_X3 - $D_X4) / ( 12* $dist * 0.529177210903 ) " | bc -l`
  DYcm=`echo " ( $D_Y1 -8* $D_Y2 +8* $D_Y3 - $D_Y4) / ( 12* $dist * 0.529177210903 ) " | bc -l`
  DZcm=`echo " ( $D_Z1 -8* $D_Z2 +8* $D_Z3 - $D_Z4) / ( 12* $dist * 0.529177210903 ) " | bc -l`

  EXcm=`echo " ( $E_X1 -8* $E_X2 +8* $E_X3 - $E_X4) / ( 12* $dist * 0.529177210903 ) " | bc -l`
  EYcm=`echo " ( $E_Y1 -8* $E_Y2 +8* $E_Y3 - $E_Y4) / ( 12* $dist * 0.529177210903 ) " | bc -l`
  EZcm=`echo " ( $E_Z1 -8* $E_Z2 +8* $E_Z3 - $E_Z4) / ( 12* $dist * 0.529177210903 ) " | bc -l`
  printf "%2s%4d %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |     D     |"  $DXcm $DYcm $DZcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  printf "%2s%4d %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |     E     |"  $EXcm $EYcm $EZcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  echo "--------|-----------|----------------------------------------------------------------------|"         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt

  let indxATOM=$indxATOM+1 ;
  #end  of the main loop over atoms
done



#----------------------------------------------------------------------------
elif   [[ $npoints -eq 6 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do

  unset D_X1 D_X2 D_X3 D_X4 D_X5 D_X6   D_Y1 D_Y2 D_Y3 D_Y4 D_Y5 D_Y6   D_Z1 D_Z2 D_Z3 D_Z4 D_Z5 D_Z6
  unset E_X1 E_X2 E_X3 E_X4 E_X5 E_X6   E_Y1 E_Y2 E_Y3 E_Y4 E_Y5 E_Y6   E_Z1 E_Z2 E_Z3 E_Z4 E_Z5 E_Z6
  termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | colrm 1 86 | sed 's/)//'`


  D_X1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`

  E_X1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "GRADIENT OF ZFS PARAMETERS D and E COMPUTED BY 6-POINT ATOM DISTORTION"                        > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "FORMULA USED:            "                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "           -D(1) +9*D(2) -45*D(3) +45*D(4) -9*D(5) +D(6) "                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "gradient = --------------------------------------------- "                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "                             60*delta                    "                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo " where:                  "                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         delta  = $dist Bohr       "                                                          >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(1) = D( -3*delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(2) = D( -2*delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(3) = D( -  delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(4) = D( +  delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(5) = D( +2*delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(6) = D( +3*delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo " PseudoSpin S = $termS "                                                                      >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "                                                                                            " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "--------|-----------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "  Atom  |    ZFS    |          gradient-X             gradient-Y             gradient-Z    |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "Lbl. Nr.| parameter |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "--------|-----------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  fi

  DXcm=`echo " ( - $D_X1 +9* $D_X2 -45* $D_X3 +45* $D_X4 -9* $D_X5 + $D_X6 ) / ( 60* $dist * 0.529177210903 ) " | bc -l`
  DYcm=`echo " ( - $D_Y1 +9* $D_Y2 -45* $D_Y3 +45* $D_Y4 -9* $D_Y5 + $D_Y6 ) / ( 60* $dist * 0.529177210903 ) " | bc -l`
  DZcm=`echo " ( - $D_Z1 +9* $D_Z2 -45* $D_Z3 +45* $D_Z4 -9* $D_Z5 + $D_Z6 ) / ( 60* $dist * 0.529177210903 ) " | bc -l`

  EXcm=`echo " ( - $E_X1 +9* $E_X2 -45* $E_X3 +45* $E_X4 -9* $E_X5 + $E_X6 ) / ( 60* $dist * 0.529177210903 ) " | bc -l`
  EYcm=`echo " ( - $E_Y1 +9* $E_Y2 -45* $E_Y3 +45* $E_Y4 -9* $E_Y5 + $E_Y6 ) / ( 60* $dist * 0.529177210903 ) " | bc -l`
  EZcm=`echo " ( - $E_Z1 +9* $E_Z2 -45* $E_Z3 +45* $E_Z4 -9* $E_Z5 + $E_Z6 ) / ( 60* $dist * 0.529177210903 ) " | bc -l`
  printf "%2s%4d %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |     D     |"  $DXcm $DYcm $DZcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  printf "%2s%4d %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |     E     |"  $EXcm $EYcm $EZcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  echo "--------|-----------|----------------------------------------------------------------------|"         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt

  let indxATOM=$indxATOM+1 ;
  #end  of the main loop over atoms
done



#----------------------------------------------------------------------------
elif   [[ $npoints -eq 8 ]] ; then
#----------------------------------------------------------------------------
indxATOM=0;
for iat in ${LABEL[@]} ; do

  unset D_X1 D_X2 D_X3 D_X4 D_X5 D_X6 D_X7 D_X8   D_Y1 D_Y2 D_Y3 D_Y4 D_Y5 D_Y6 D_Y7 D_Y8   D_Z1 D_Z2 D_Z3 D_Z4 D_Z5 D_Z6 D_Z7 D_Z8
  unset E_X1 E_X2 E_X3 E_X4 E_X5 E_X6 E_X7 E_X8   E_Y1 E_Y2 E_Y3 E_Y4 E_Y5 E_Y6 E_Y7 E_Y8   E_Z1 E_Z2 E_Z3 E_Z4 E_Z5 E_Z6 E_Z7 E_Z8
  termS=`grep 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1'  $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | colrm 1 86 | sed 's/)//'`

  D_X1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X7=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_7_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_X8=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_8_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y7=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_7_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Y8=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_8_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z7=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_7_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  D_Z8=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_8_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'D =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`

  E_X1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X7=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_7_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_X8=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_X_8_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y7=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_7_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Y8=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Y_8_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z1=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_1_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z2=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_2_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z3=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_3_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z4=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_4_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z5=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_5_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z6=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_6_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z7=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_7_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`
  E_Z8=`grep -A250 'CALCULATION OF PSEUDOSPIN HAMILTONIAN TENSORS FOR THE MULTIPLET 1' $CurrDir/"$Complex"_dist_"$iat""$indxATOM"_Z_8_"$bas1"_"$bas2"_cas_"$active_space"_aniso_cas.out | grep -A120  'DECOMPOSITION OF THE ZERO-FIELD SPLITTING' | grep -A2 'Anisotropy parameters' | grep 'E =' | grep -v 'Anisotropy parameters' |  sed 's/\ //g' | colrm 1 2`

  #-----------------------------------------------------------------------------------------------------------------------------
  if [ "$indxATOM" -eq "0" ] ; then
    echo "GRADIENT OF ZFS PARAMETERS D and E COMPUTED BY 6-POINT ATOM DISTORTION"                        > $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "FORMULA USED:            "                                                                    >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "           3*D(1) -32*D(2) +168*D(3) -672*D(4) +672*D(5) -168*D(6) +32*D(7) -3*D(8)"          >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "gradient = ------------------------------------------------------------------------"          >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "                                          840*delta"                                          >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo " where:                            "                                                          >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         delta  = $dist Bohr       "                                                          >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(1) = D( -4*delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(2) = D( -3*delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(3) = D( -2*delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(4) = D( -  delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(5) = D( +  delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(6) = D( +2*delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(7) = D( +3*delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "         D(8) = D( +4*delta )  "                                                              >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo " PseudoSpin S = $termS "                                                                      >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "                                                                                            " >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "--------|-----------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "  Atom  |    ZFS    |          gradient-X             gradient-Y             gradient-Z    |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "Lbl. Nr.| parameter |        cm-1/Angstrom          cm-1/Angstrom          cm-1/Angstrom   |" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
    echo "--------|-----------|----------------------------------------------------------------------|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  fi

  DXcm=`echo " ( 3* $D_X1 -32* $D_X2 +168* $D_X3 -672* $D_X4 +672* $D_X5 -168* $D_X6 +32* $D_X7 -3* $D_X8 ) / ( 840* $dist * 0.529177210903 ) " | bc -l`
  DYcm=`echo " ( 3* $D_Y1 -32* $D_Y2 +168* $D_Y3 -672* $D_Y4 +672* $D_Y5 -168* $D_Y6 +32* $D_Y7 -3* $D_Y8 ) / ( 840* $dist * 0.529177210903 ) " | bc -l`
  DZcm=`echo " ( 3* $D_Z1 -32* $D_Z2 +168* $D_Z3 -672* $D_Z4 +672* $D_Z5 -168* $D_Z6 +32* $D_Z7 -3* $D_Z8 ) / ( 840* $dist * 0.529177210903 ) " | bc -l`

  EXcm=`echo " ( 3* $E_X1 -32* $E_X2 +168* $E_X3 -672* $E_X4 +672* $E_X5 -168* $E_X6 +32* $E_X7 -3* $E_X8 ) / ( 840* $dist * 0.529177210903 ) " | bc -l`
  EYcm=`echo " ( 3* $E_Y1 -32* $E_Y2 +168* $E_Y3 -672* $E_Y4 +672* $E_Y5 -168* $E_Y6 +32* $E_Y7 -3* $E_Y8 ) / ( 840* $dist * 0.529177210903 ) " | bc -l`
  EZcm=`echo " ( 3* $E_Z1 -32* $E_Z2 +168* $E_Z3 -672* $E_Z4 +672* $E_Z5 -168* $E_Z6 +32* $E_Z7 -3* $E_Z8 ) / ( 840* $dist * 0.529177210903 ) " | bc -l`

  printf "%2s%4d %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |     D     |"  $DXcm $DYcm $DZcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  printf "%2s%4d %s %22.14f %22.14f %22.14f %s\n"  "$iat" "$indxATOM" " |     E     |"  $EXcm $EYcm $EZcm "|" >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt
  echo "--------|-----------|----------------------------------------------------------------------|"         >> $CurrDir/"$Complex"_"$bas1"_"$bas2"_cas_"$active_space"_ZFS"$npoints"_gradient.txt

  let indxATOM=$indxATOM+1 ;
  #end  of the main loop over atoms
done


#----------------------------------------------------------------------------
#elif   [[ $npoints -eq 8 ]] ; then
else
    echo "not yet implemented"
#----------------------------------------------------------------------------
#elif   [[ $npoints -eq 10 ]] ; then
    echo "not yet implemented"
#----------------------------------------------------------------------------
#elif   [[ $npoints -eq 12 ]] ; then
    echo "not yet implemented"
#----------------------------------------------------------------------------
fi


}




