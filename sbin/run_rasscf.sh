#!/bin/bash

#------------------------------------------
run_rasscf(){
#   run_rasscf $Complex  $ms  $roots  $el  $orb  $bas1  $bas2 &

   local Complex=$1
   local ms=$2
   local roots=$3
   local active_e=$4
   local active_o=$5
   local bas1=$6
   local bas2=$7
   local active_space="$active_e"in"$active_o"
   local active_o2=`echo "$active_o-$ras1_o-$ras3_o" | bc -l`

   export WorkDir=$RootDir/rasscf_ms"$ms"

   echo run_rasscf::     Complex=$Complex
   echo run_rasscf::          ms=$ms
   echo run_rasscf::       roots=$roots
   echo run_rasscf::    active_e=$active_e
   echo run_rasscf::    active_o=$active_o
   echo run_rasscf::        bas1=$bas1
   echo run_rasscf::        bas2=$bas2
   echo run_rasscf::active_space=$active_space
   echo run_rasscf::     FileDir=$FileDir
   echo run_rasscf::  do_restart=$do_restart

   #  check_previous_runs
   local old_run=0
   local restart_rasscf=' '
   if [ -f $CurrDir/"$Project"_rasscf_ms"$ms".out ]  ; then
      old_run=`grep 'Stop Module:' $CurrDir/"$Project"_rasscf_ms"$ms".out | grep 'rasscf' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
   fi
   echo run_rasscf:: $Project ms=$ms active_space=$active_o2 old_run=$old_run

   if [ ! -f $CurrDir/"$Project"_rasscf_ms"$ms".out ] || [ "$old_run" -ne 1 ] || \
      [ ! -f $FileDir/"$Project"_ms"$ms".JobIph     ] ||                         \
      [ ! -f $FileDir/"$Project"_ms"$ms".RasOrb     ] ; then
      echo "run_rasscf:: $Project ms=$ms It looks like we need a new RASSCF calculation for this case."
      if [ ! -d "$WorkDir" ]; then  mkdir $WorkDir ; else rm -rf $WorkDir/* ; fi
      # run_molcas in its own subfolder
      cd $WorkDir
      delete $CurrDir/"$Project"_rasscf_ms"$ms".inp
      if [ "$CleanWorkDir" == "yes" ] || [ "$CleanWorkDir" == "Yes" ] || [ "$CleanWorkDir" == "YES" ] ; then
          delete $FileDir/"$Project"_ms"$ms".RasOrb
          delete $FileDir/"$Project"_ms"$ms".JobIph
          delete $FileDir/"$Project"_ms"$ms".RunFile
          delete $FileDir/"$Project"_ms"$ms".rasscf.h5
          delete $FileDir/"$Project"_ms"$ms".molden
      fi

      # check if the minimal basis calculation is present in $FileDir
      if [ -f  $FileDir/"$Complex".INPORB  ] && [ -f $FileDir/"$Complex".RUNFIL1 ] && [ "$do_restart" = "EXPBAS" ] ;  then
          copy $FileDir/"$Complex".INPORB   $WorkDir/INPORB
          copy $FileDir/"$Complex".RUNFIL1  $WorkDir/RUNFIL1
          copy $RootDir/"$Project".RunFile  $WorkDir/RUNFIL2

          echo "&EXPBAS"                       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo ">>COPY $Project.ExpOrb INPORB" >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "&RASSCF"                       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "LUMORB"                        >> $CurrDir/"$Project"_rasscf_ms"$ms".inp

          echo Restarting RASSCF using the $Project.ExpOrb orbitals

      # check if the minimal basis calculation is present in $FileDir
      elif [ -f  $FileDir/"$Complex"_ms"$ms".INPORB  ] && [ -f $FileDir/"$Complex"_ms"$ms".RUNFIL1 ] && [ "$do_restart" = "EXPBAS_FULL" ] ; then
          copy $FileDir/"$Complex"_ms"$ms".INPORB   $WorkDir/INPORB
          copy $FileDir/"$Complex"_ms"$ms".RUNFIL1  $WorkDir/RUNFIL1
          copy $RootDir/"$Project".RunFile          $WorkDir/RUNFIL2

          echo "&EXPBAS"                       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo ">>COPY $Project.ExpOrb INPORB" >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "&RASSCF"                       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "LUMORB"                        >> $CurrDir/"$Project"_rasscf_ms"$ms".inp

          echo Restarting RASSCF using the $Project.ExpOrb orbitals

      elif [ -f $FileDir/"$Complex"_"$bas1"_"$bas2".INPORB ] && [ "$do_restart" = "INPORB" ] ; then
          copy  $FileDir/"$Complex"_"$bas1"_"$bas2".INPORB  $WorkDir/INPORB
          restart_rasscf='LUMORB'
          echo "&RASSCF"                       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "LUMORB"                        >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
#         echo "TYPEindex"                     >> $CurrDir/"$Project"_rasscf_ms"$ms".inp

      elif [ -f $FileDir/"$Complex"_"$bas1"_"$bas2"_ms"$ms".JOBOLD ] && [ "$do_restart" = "CIRESTART" ] ; then
          copy  $FileDir/"$Complex"_"$bas1"_"$bas2"_ms"$ms".JOBOLD  $WorkDir/JOBOLD
          echo
          restart_rasscf='CIRESTART'
          echo "&RASSCF"                       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "JOBIPH"                        >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "CIRESTART"                     >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
#         echo "TYPEindex"                     >> $CurrDir/"$Project"_rasscf_ms"$ms".inp

      elif [ -f $FileDir/"$Complex"_"$bas1"_"$bas2"_ms"$ms".INPORB ] && [ "$do_restart" = "INPORB_MS" ] ; then
          copy  $FileDir/"$Complex"_"$bas1"_"$bas2"_ms"$ms".INPORB  $WorkDir/INPORB
          restart_rasscf='LUMORB'
          echo "&RASSCF"                       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "LUMORB"                        >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
#         echo "TYPEindex"                     >> $CurrDir/"$Project"_rasscf_ms"$ms".inp

      elif [ -f $WorkDir/$Project.RasOrb ] && [ "$do_restart" = "CONTINUE" ] ; then
          copy  $WorkDir/$Project.RasOrb  $WorkDir/INPORB
          restart_rasscf='LUMORB'
          echo "&RASSCF"                       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "LUMORB"                        >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
#         echo "TYPEindex"                     >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
      else
          echo "&RASSCF"                       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
      fi

      cat <<EOF >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
SPIN
$ms
NACTEL
$active_e $holes_ras1 $electrons_ras3
FROZ
$frozen_orbitals
INAC
$inactive_orbitals
RAS1
$ras1_o
RAS2
$active_o2
RAS3
$ras3_o
CIROOT
$roots $roots 1
ITERations
200 20
CIMX
20
PRSD
PRSP
PRWF
0.001
THRS
  1.0e-09 1.0e-05 1.0e-05
ORBL
ALL
ORBA
FULL
EOF
      if [ ! -z $CIONLY ]; then
          echo "CIONLY"       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
      fi
      if [ ! -z $IVO ]; then
          echo "IVO "         >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
      fi
      if [ ! -z "$supsym" ]; then
          echo "SUPSYM"       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo $supsym        >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
      fi
      if [ ! -z "$CANONICAL" ]; then
          echo "OUTO "         >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "CANONICAL "    >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
      fi
      if [ -f $FileDir/"$Project"_ms"$ms".RasOrb ] && [ "$do_restart" != "INPORB" ]; then
          echo "LUMORB"       >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          echo "End Of Input" >> $CurrDir/"$Project"_rasscf_ms"$ms".inp
          copy $FileDir/"$Project"_ms"$ms".RasOrb $WorkDir/INPORB
      fi

      echo "End Of Input" >> $CurrDir/"$Project"_rasscf_ms"$ms".inp

      mv $CurrDir/"$Project"_rasscf_ms"$ms".inp $CurrDir/"$Project"_rasscf_ms"$ms".inp2
      fold -w 180 -s $CurrDir/"$Project"_rasscf_ms"$ms".inp2  > $CurrDir/"$Project"_rasscf_ms"$ms".inp
      delete $CurrDir/"$Project"_rasscf_ms"$ms".inp2

# run RASSCF -----------------
      link $RootDir/"$Project".OrdInt  $WorkDir/"$Project".OrdInt
      link $RootDir/"$Project".OneInt  $WorkDir/"$Project".OneInt
      link $RootDir/"$Project".ChDiag  $WorkDir/"$Project".ChDiag
      link $RootDir/"$Project".ChMap   $WorkDir/"$Project".ChMap
      link $RootDir/"$Project".ChRst   $WorkDir/"$Project".ChRst
      link $RootDir/"$Project".ChRed   $WorkDir/"$Project".ChRed
      link $RootDir/"$Project".ChSel   $WorkDir/"$Project".ChSel
      link $RootDir/"$Project".ChVec   $WorkDir/"$Project".ChVec
      link $RootDir/"$Project".ChFV    $WorkDir/"$Project".ChFV
      link $RootDir/"$Project".ChRstL  $WorkDir/"$Project".ChRstL
      link $RootDir/"$Project".ChRdL   $WorkDir/"$Project".ChRdL
      link $RootDir/"$Project".ChVcl   $WorkDir/"$Project".ChVcl
      link $RootDir/"$Project".Vtmp    $WorkDir/"$Project".Vtmp
      link $RootDir/"$Project".LDFC    $WorkDir/"$Project".LDFC
      link $RootDir/"$Project".LDFAP   $WorkDir/"$Project".LDFAP
      link $RootDir/"$Project".OneRel  $WorkDir/"$Project".OneRel
      link $RootDir/"$Project".BasInt  $WorkDir/"$Project".BasInt
      link $RootDir/"$Project".SymInfo $WorkDir/"$Project".SymInfo
      link $RootDir/"$Project".QVec    $WorkDir/"$Project".QVec
      link $RootDir/"$Project".ChVec1  $WorkDir/"$Project".ChVec1

      copy $RootDir/"$Project".RunFile $WorkDir/"$Project".RunFile

      if [ "$cluster" == "NSCC" ] ; then
          if [ -f $CurrDir/"$Project"_rasscf_ms"$ms" ] ; then rm -rf $CurrDir/"$Project"_rasscf_ms"$ms" ; fi
          echo "#!/bin/sh"                               > $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "#PBS -q $queue"                         >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          if [ ! -z "$ProjectID" ] ; then
             echo "#PBS -P $ProjectID"                  >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          else
            echo "#PBS -P Personal"                     >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          fi
          echo "#PBS -l walltime=$walltime:00:00"       >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "#PBS -l select=$no_of_cores:ncpus=$no_of_ncpus:mpiprocs=$no_of_mpiprocs:mem=$memory" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "#PBS -j oe"                             >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "#PBS -N "$Project"_rasscf_ms"$ms""      >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo ""                                       >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "module load python/3.5.1"               >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "module load hdf5/1.8.16/xe_2016/serial" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo ""                                       >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "export RootDir=$RootDir"                >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "export CurrDir=$CurrDir"                >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "export Project=$Project"                >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "export FileDir=$FileDir"                >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "export WorkDir=$WorkDir"                >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "export MOLCAS=$MOLCAS"                  >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "export MOLCAS_MEM=$MOLCAS_MEM"          >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo ""                                       >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ ! -d \$WorkDir ]"                  >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "then"                                   >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "   mkdir -p \$WorkDir"                  >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "fi"                                     >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "cd \$WorkDir"                           >> $CurrDir/"$Project"_"$prog"
          echo ""                                       >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".OrdInt  ]; then ln -fs \$RootDir/"\$Project".OrdInt  \$WorkDir/"\$Project".OrdInt  ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".OneInt  ]; then ln -fs \$RootDir/"\$Project".OneInt  \$WorkDir/"\$Project".OneInt  ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChDiag  ]; then ln -fs \$RootDir/"\$Project".ChDiag  \$WorkDir/"\$Project".ChDiag  ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChMap   ]; then ln -fs \$RootDir/"\$Project".ChMap   \$WorkDir/"\$Project".ChMap   ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChRst   ]; then ln -fs \$RootDir/"\$Project".ChRst   \$WorkDir/"\$Project".ChRst   ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChRed   ]; then ln -fs \$RootDir/"\$Project".ChRed   \$WorkDir/"\$Project".ChRed   ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChSel   ]; then ln -fs \$RootDir/"\$Project".ChSel   \$WorkDir/"\$Project".ChSel   ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChVec   ]; then ln -fs \$RootDir/"\$Project".ChVec   \$WorkDir/"\$Project".ChVec   ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChFV    ]; then ln -fs \$RootDir/"\$Project".ChFV    \$WorkDir/"\$Project".ChFV    ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChRstL  ]; then ln -fs \$RootDir/"\$Project".ChRstL  \$WorkDir/"\$Project".ChRstL  ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChRdL   ]; then ln -fs \$RootDir/"\$Project".ChRdL   \$WorkDir/"\$Project".ChRdL   ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChVcL   ]; then ln -fs \$RootDir/"\$Project".ChVcl   \$WorkDir/"\$Project".ChVcl   ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".Vtmp    ]; then ln -fs \$RootDir/"\$Project".Vtmp    \$WorkDir/"\$Project".Vtmp    ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".LDFC    ]; then ln -fs \$RootDir/"\$Project".LDFC    \$WorkDir/"\$Project".LDFC    ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".LDFAP   ]; then ln -fs \$RootDir/"\$Project".LDFAP   \$WorkDir/"\$Project".LDFAP   ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".OneRel  ]; then ln -fs \$RootDir/"\$Project".OneRel  \$WorkDir/"\$Project".OneRel  ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".BasInt  ]; then ln -fs \$RootDir/"\$Project".BasInt  \$WorkDir/"\$Project".BasInt  ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".SymInfo ]; then ln -fs \$RootDir/"\$Project".SymInfo \$WorkDir/"\$Project".SymInfo ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".QVec    ]; then ln -fs \$RootDir/"\$Project".QVec    \$WorkDir/"\$Project".QVec    ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$RootDir/"\$Project".ChVec1  ]; then ln -fs \$RootDir/"\$Project".ChVec1  \$WorkDir/"\$Project".ChVec1  ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "cp \$RootDir/"\$Project".RunFile  \$WorkDir/\$Project.RunFile" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo ""                                                              >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "pymolcas \$CurrDir/"\$Project"_rasscf_ms"$ms".inp >"\          >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "         \$CurrDir/"\$Project"_rasscf_ms"$ms".out 2>"\         >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "         \$CurrDir/"\$Project"_rasscf_ms"$ms".err "            >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo ""                                       >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$WorkDir/"\$Project".RasOrb    ]; then cp  \$WorkDir/"\$Project".RasOrb     \$FileDir/"\$Project"_ms"$ms".RasOrb    ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$WorkDir/"\$Project".JobIph    ]; then cp  \$WorkDir/"\$Project".JobIph     \$FileDir/"\$Project"_ms"$ms".JobIph    ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$WorkDir/"\$Project".RunFile   ]; then cp  \$WorkDir/"\$Project".RunFile    \$FileDir/"\$Project"_ms"$ms".RunFile   ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$WorkDir/"\$Project".rasscf.h5 ]; then cp  \$WorkDir/"\$Project".rasscf.h5  \$FileDir/"\$Project"_ms"$ms".rasscf.h5 ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "if [ -f \$WorkDir/"\$Project".molden    ]; then cp  \$WorkDir/"\$Project".molden     \$FileDir/"\$Project"_ms"$ms".molden    ; fi" >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo ""                                       >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
          echo "exit $?"                                >> $CurrDir/"$Project"_rasscf_ms"$ms".sh
      else
          pymolcas $CurrDir/"$Project"_rasscf_ms"$ms".inp  >  \
                   $CurrDir/"$Project"_rasscf_ms"$ms".out  2> \
                   $CurrDir/"$Project"_rasscf_ms"$ms".err

    # end run RASSCF -----------------
          local rasscf_return_code=0
          rasscf_return_code=`grep 'Stop Module:' $CurrDir/"$Project"_rasscf_ms"$ms".out | grep 'rasscf' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
          echo RASSCF_return_code= $rasscf_return_code

          if [ $? -eq 0 ] && [ "$rasscf_return_code" -eq 1 ]; then
             copy  $WorkDir/"$Project".RasOrb     $FileDir/"$Project"_ms"$ms".RasOrb
             copy  $WorkDir/"$Project".JobIph     $FileDir/"$Project"_ms"$ms".JobIph
             copy  $WorkDir/"$Project".RunFile    $FileDir/"$Project"_ms"$ms".RunFile
             copy  $WorkDir/"$Project".rasscf.h5  $FileDir/"$Project"_ms"$ms".rasscf.h5
             copy  $WorkDir/"$Project".molden     $FileDir/"$Project"_ms"$ms".molden
             # a bit of clean-up
             rm -rf $WorkDir
             delete $CurrDir/"$Project"_rasscf_ms"$ms".inp
             delete $CurrDir/"$Project"_rasscf_ms"$ms".xml
             return 0
          else
             echo RASSCF did not finish well. Please check.
             echo $WorkDir  is not deleted, for future re-use.
             echo No important files "(like *.JobIph, *.RasOrb, *.rasscf.h5)" were saved into $FileDir
             return $?
          fi
      fi

   else
      echo The "$Project"_rasscf_ms"$ms".out is present in the "$CurrDir". Nothing to do.
      echo "If not satisfied, remove the following files or folders and resubmit"
      echo $CurrDir/"$Project"_rasscf_ms"$ms".out
      echo $WorkDir
   fi
}


