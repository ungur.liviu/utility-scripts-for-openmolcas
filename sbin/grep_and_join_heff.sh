#!/bin/bash


grep_and_join_heff(){

   local nroots=$1
   local ms=$2
   local type=$3

   let nlines=$nroots+4
   if [ $NOMULT == 'true' ] || [ $NOMULT == 'TRUE' ] || [[ $type == "ss" ]]; then
   # grep the total energy and print the diagonal model Hamiltonian
       for ((i=1;i<=$nroots;i++)); do
           if [ -f $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt ]; then delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt ; fi ;
           grep "::" $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$i".out | grep 'CASPT2' | colrm 1 38 >& $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_energy_"$i".txt
           for ((j=1;j<=$nroots;j++)); do
              if [[ $i == $j ]]; then
                 cat $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_energy_"$j".txt >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt
              else
                 echo "0.00000000000000E+00 "                                   >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt
              fi
           done
           delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_energy_"$i".txt
       done
   else
   # grep for the HEFF: this belongs to the L terms now...
       if [[ $type == "xms" ]] || [[ $type == "ms" ]] || [[ $type == "rcp" ]]  ; then

           if [[ $ms == ${spinMS[0]} ]] || ( [ "$same_multiplet" == "true" ] && [[ "$ms" == ${spinMS[1]} ]] ) ; then

               icountL=1; icount_root=0;
               previous_roots=0 ; indexL=0
               for iL in ${!groundL[@]}; do
                  let istart_state=$previous_roots+1
                  let iend_state=$istart_state+${groundL[$iL]}-1
                  echo GREP_HEFF_XMS_CASPT2::  ms=$ms, iL=$iL compute all roots of this group start_root=$istart_state  end_state=$iend_state

                  let nlines=${groundL[$iL]}+3 ; echo "GREP_HEFF_XMS_CASPT2:: nlines=" $nlines ;

                  for i in  $(seq $istart_state $iend_state) ; do
                      echo CASPT2::  run one single caspt2 for root  $root
#                      let icount_root_for_indexing_ONLY=$icount_root_for_indexing_ONLY+1;
                      echo GREP_HEFF_XMS_CASPT2::  previous_roots=$previous_roots
                      rm $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtA
                      rm $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtB
                      rm $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtC
                      if [ -f $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt ]; then delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt ; fi ;

                      # add 0.0 for all previous roots:
                      if [[ $previous_roots > 0 ]] ; then
                         for j in $(seq 1 $previous_roots ) ; do
                             echo "0.00000000000000E+00 "  >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtA
                         done
                      fi

                      # extract the effective Hamiltonian from ROOT $i output:
                      grep -A$nlines 'Hamiltonian Effective Couplings'    $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$i".out \
                             | grep ' <  ' | grep -A${groundL[$iL]} " $istart_state |"  | colrm 1 10 >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtB

                      # add 0.0 for all other roots
                      let zeros=$nroots-${groundL[$iL]}-$previous_roots ;  echo GREP_HEFF_XMS_CASPT2::  zeros=$zeros
                      if [[ $zeros > 0 ]] ; then
                         for j in $(seq 1 $zeros ) ; do
                             echo "0.00000000000000E+00 "  >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtC
                         done
                      fi

                      cat $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtA $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtB $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtC >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt
                      rm $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtA
                      rm $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtB
                      rm $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txtC
                  done

                  let previous_roots=$previous_roots+${groundL[$iL]};
               done

           else

             # grep the HEFF for non-ground state MS
                  let nlines=$nroots+4;
                  for i in  $(seq 1 $nroots ) ; do
                      echo CASPT2::  run one single caspt2 for  ms=$ms  root=$root
                      if [ -f $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt ]; then delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt ; fi ;

                      # extract the effective Hamiltonian from ROOT $i output:
                      grep -A$nlines 'Hamiltonian Effective Couplings'    $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$i".out \
                             | grep ' <  ' | grep -A$nroots " $istart_state |"  | colrm 1 10 >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt
                  done



           fi
               # up till here we have two HEFFs for each L term. Now we need to make one big matrix containing ALL $nroots for this $ms.

       else
           for ((i=1;i<=$nroots;i++)); do
               if [ -f $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt ]; then delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt ; fi ;
               grep -A$nlines 'Hamiltonian Effective Couplings'    $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$i".out \
                      | grep -A$nroots ' <    1 |' | colrm 1 10 >& $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt
           done
       fi
   fi





   #--------------------------
   # join and transpose the heff:
   if [ -e $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp1.txt ]; then  rm -rf  $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp1.txt ; fi
   if [ -e $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp2.txt ]; then  rm -rf  $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp2.txt ; fi
   if [ -e $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_full.txt ]; then  rm -rf  $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_full.txt ; fi
   for ((i=1;i<=$nroots;i++)) ; do
       XYZ=($(cat        $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt ))
       echo ${XYZ[@]} >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp1.txt
   done
   declare -a array=( )
   read -a line < $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp1.txt
   COLS=${#line[@]}

   echo GREP_HEFF_XMS_CASPT2::  COLS=$COLS
   index=0;
   while read -a line ; do
       for (( COUNTER=0; COUNTER<${#line[@]}; COUNTER++ )) ;  do
           array[$index]=${line[$COUNTER]}
           ((index++))
       done
   done < $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp1.txt


   echo $nroots >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp2.txt
   for ((ROW=0;ROW<COLS;ROW++)) ; do
     for (( COUNTER = ROW; COUNTER < ${#array[@]}; COUNTER += COLS )); do
          printf "%s\t" "${array[$COUNTER]}"  >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp2.txt
     done
     printf "\n"  >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp2.txt
   done


   # fold lines since CASPT2 works only with 120 lines

   test_file $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp2.txt
   fold -w 120 -s $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp2.txt  >  $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_full.txt
   test_file $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_full.txt



   touch $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".tmp
   for ((i=1;i<=$nroots;i++)) ; do
       cat $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".tmp   $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$i".out >> $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".tmp1
       mv  $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".tmp1  $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".tmp
   done
   mv $CurrDir/"$Project"_"$type"_caspt2_ms"$ms".tmp $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_all_roots.out

#   # clean-up
   for ((i=1;i<=$nroots;i++)); do
      delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_"$i".txt
#      delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_state_"$i".out
   done
   delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp1.txt
   delete $CurrDir/"$Project"_"$type"_caspt2_ms"$ms"_heff_tmp2.txt
}

