
run_mclr_first(){
   local ms=$1
   local nroots=$2
   # check if the previous rasscf calculation was ok
   local rasscf_rc=0
   export WorkDir=$RootDir/mclr_ms"$ms"_first_run
#export WorkDir=$RootDir

   rasscf_rc=`grep 'Stop Module:' $CurrDir/"$Project"_rasscf_ms"$ms".out | grep 'rasscf' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
   echo      `grep 'Stop Module:' $CurrDir/"$Project"_rasscf_ms"$ms".out | grep 'rasscf' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
   echo 'rasscf_rc =' $rasscf_rc

   local mclr_old_run=0
   if [ -f $CurrDir/"$Project"_mclr_ms"$ms"_first_run.out ]; then
       mclr_old_run=`grep 'Stop Module:' $CurrDir/"$Project"_mclr_ms"$ms"_first_run.out | grep 'mclr ' | grep 'rc=_RC_ALL_IS_WELL_' | wc -l`
   fi
   echo Inside run_mclr_first:::   mclr_old_run=$mclr_old_run


   if [ "$mclr_old_run" -ne 1 ] ; then
      delete $FileDir/"$Project"_mclr_ms"$ms".RunFile
      delete $RootDir/"$Project"_mclr_ms"$ms".tramo
#     delete $FileDir/"$Project"_mclr_ms"$ms".ChFV11

      if [ "$rasscf_rc" -eq 1 ] && [ -f $FileDir/"$Project"_ms"$ms".JobIph ] && [ -f $FileDir/"$Project"_ms"$ms".RunFile ] ; then
         delete $WorkDir/$Project.JobIph
         copy   $FileDir/"$Project"_ms"$ms".JobIph   $WorkDir/$Project.JobIph
         copy   $FileDir/"$Project"_ms"$ms".RunFile  $WorkDir/$Project.RunFile

#if [ ! -f $CurrDir/"$Project"_mclr_ms"$ms"_first_run.out ] && [ "$mclr_old_run" -ne 1 ] ; then
            mkdir $WorkDir

            # generate a specific input for this calculation
            echo "&MCLR"           >  $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "ITER"            >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "200"             >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "EXPD"            >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "$nroots"         >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "PRINT"           >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "3"               >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "THREshold"       >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "1.0d-10"         >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "SALA"            >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "1"               >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "TWOStep"         >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "FIRST"           >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
            echo "End Of Input"    >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2

            fold -w 120 -s $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2 > $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp

            # run_molcas in its own subfolder
          if [ "$cluster" == "NSCC" ] ; then
              if [ -f $CurrDir/"$Project"_mclr_ms"$ms"_first_run ] ; then rm -rf $CurrDir/"$Project"_mclr_ms"$ms"_first_run ; fi
              echo "#!/bin/sh"                                                                                                       >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "#PBS -q $queue"                                                                                                  >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              if [ ! -z "$ProjectID" ] ; then
                echo "#PBS -P $ProjectID"                                                                                            >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              else
                echo "#PBS -P Personal"                                                                                              >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              fi
              echo "#PBS -l walltime=$walltime:00:00"                                                                                >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "#PBS -l select=$no_of_cores:ncpus=$no_of_ncpus:mpiprocs=$no_of_mpiprocs:mem=$memory"                             >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "#PBS -j oe"                                                                                                      >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "#PBS -N "$Project"_mclr_ms"$ms"_first_run"                                                                       >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo ""                                                                                                                >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "module load python/3.5.1"                                                                                        >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "module load hdf5/1.8.16/xe_2016/serial"                                                                          >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo ""                                                                                                                >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "export CurrDir=$CurrDir"                                                                                         >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "export Project=$Project"                                                                                         >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "export FileDir=$FileDir"                                                                                         >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "export WorkDir=$WorkDir"                                                                                         >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "export MOLCAS=$MOLCAS"                                                                                           >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "export MOLCAS_MEM=$MOLCAS_MEM"                                                                                   >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo ""                                                                                                                >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ ! -d $WorkDir ]"                                                                                            >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "then"                                                                                                            >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "   mkdir -p $WorkDir"                                                                                            >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "fi"                                                                                                              >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "cd $WorkDir"                                                                                                     >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo ""                                                                                                                >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".OrdInt  ]; then ln -fs $RootDir/"$Project".OrdInt  $WorkDir/"$Project".OrdInt  ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".OneInt  ]; then ln -fs $RootDir/"$Project".OneInt  $WorkDir/"$Project".OneInt  ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChDiag  ]; then ln -fs $RootDir/"$Project".ChDiag  $WorkDir/"$Project".ChDiag  ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChMap   ]; then ln -fs $RootDir/"$Project".ChMap   $WorkDir/"$Project".ChMap   ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChRst   ]; then ln -fs $RootDir/"$Project".ChRst   $WorkDir/"$Project".ChRst   ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChRed   ]; then ln -fs $RootDir/"$Project".ChRed   $WorkDir/"$Project".ChRed   ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChSel   ]; then ln -fs $RootDir/"$Project".ChSel   $WorkDir/"$Project".ChSel   ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChVec   ]; then ln -fs $RootDir/"$Project".ChVec   $WorkDir/"$Project".ChVec   ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChFV    ]; then ln -fs $RootDir/"$Project".ChFV    $WorkDir/"$Project".ChFV    ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChRstL  ]; then ln -fs $RootDir/"$Project".ChRstL  $WorkDir/"$Project".ChRstL  ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChRdL   ]; then ln -fs $RootDir/"$Project".ChRdL   $WorkDir/"$Project".ChRdL   ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChVcL   ]; then ln -fs $RootDir/"$Project".ChVcl   $WorkDir/"$Project".ChVcl   ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".Vtmp    ]; then ln -fs $RootDir/"$Project".Vtmp    $WorkDir/"$Project".Vtmp    ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".LDFC    ]; then ln -fs $RootDir/"$Project".LDFC    $WorkDir/"$Project".LDFC    ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".LDFAP   ]; then ln -fs $RootDir/"$Project".LDFAP   $WorkDir/"$Project".LDFAP   ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".OneRel  ]; then ln -fs $RootDir/"$Project".OneRel  $WorkDir/"$Project".OneRel  ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".BasInt  ]; then ln -fs $RootDir/"$Project".BasInt  $WorkDir/"$Project".BasInt  ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".SymInfo ]; then ln -fs $RootDir/"$Project".SymInfo $WorkDir/"$Project".SymInfo ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".QVec    ]; then ln -fs $RootDir/"$Project".QVec    $WorkDir/"$Project".QVec    ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "if [ -f $RootDir/"$Project".ChVec1  ]; then ln -fs $RootDir/"$Project".ChVec1  $WorkDir/"$Project".ChVec1  ; fi" >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "cp     $RootDir/"$Project".RunFile         $WorkDir/"$Project".RunFile"                                          >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "ln -fs $FileDir/"$Project"_ms"$ms".JobIph  $WorkDir/"$Project".JobIph"                                           >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo ""                                                                                                                >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "pymolcas $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp > $CurrDir/"$Project"_mclr_ms"$ms"_first_run.out 2> $CurrDir/"$Project"_mclr_ms"$ms"_first_run.err " >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo ""                                                                                                                >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
              echo "exit $?"                                                                                                         >> $CurrDir/"$Project"_mclr_ms"$ms"_first_run
          else
              cd $WorkDir
              # link useful files
              link $RootDir/"$Project".OrdInt  $WorkDir/"$Project".OrdInt
              link $RootDir/"$Project".OneInt  $WorkDir/"$Project".OneInt
              link $RootDir/"$Project".ChDiag  $WorkDir/"$Project".ChDiag
              link $RootDir/"$Project".ChMap   $WorkDir/"$Project".ChMap
              link $RootDir/"$Project".ChRst   $WorkDir/"$Project".ChRst
              link $RootDir/"$Project".ChRed   $WorkDir/"$Project".ChRed
              link $RootDir/"$Project".ChSel   $WorkDir/"$Project".ChSel
              link $RootDir/"$Project".ChVec   $WorkDir/"$Project".ChVec
              link $RootDir/"$Project".ChFV    $WorkDir/"$Project".ChFV
              link $RootDir/"$Project".ChRstL  $WorkDir/"$Project".ChRstL
              link $RootDir/"$Project".ChRdL   $WorkDir/"$Project".ChRdL
              link $RootDir/"$Project".ChVcl   $WorkDir/"$Project".ChVcl
              link $RootDir/"$Project".Vtmp    $WorkDir/"$Project".Vtmp
              link $RootDir/"$Project".LDFC    $WorkDir/"$Project".LDFC
              link $RootDir/"$Project".LDFAP   $WorkDir/"$Project".LDFAP
              link $RootDir/"$Project".OneRel  $WorkDir/"$Project".OneRel
              link $RootDir/"$Project".BasInt  $WorkDir/"$Project".BasInt
              link $RootDir/"$Project".SymInfo $WorkDir/"$Project".SymInfo
              link $RootDir/"$Project".QVec    $WorkDir/"$Project".QVec
              link $RootDir/"$Project".ChVec1  $WorkDir/"$Project".ChVec1

              copy $FileDir/"$Project"_ms"$ms".RunFile      $WorkDir/"$Project".RunFile
              link $FileDir/"$Project"_ms"$ms".JobIph       $WorkDir/"$Project".JobIph

            echo Inside run_mclr_first:::   CALLING PYMOLCAS FOR FIRST MCLR

            pymolcas $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp  >  \
                     $CurrDir/"$Project"_mclr_ms"$ms"_first_run.out  2> \
                     $CurrDir/"$Project"_mclr_ms"$ms"_first_run.err

            cd $CurrDir
            if [ $? -eq 0 ]; then
                copy    $WorkDir/"$Project".RunFile  $FileDir/"$Project"_mclr_ms"$ms".RunFile
                move    $WorkDir/"$Project".Qdat     $RootDir/"$Project"_mclr_ms"$ms".Qdat
                move    $WorkDir/"$Project".Resp     $RootDir/"$Project"_mclr_ms"$ms".Resp
                move    $WorkDir/"$Project".tramo    $RootDir/"$Project"_mclr_ms"$ms".tramo
                move    $WorkDir/"$Project".ChFV11   $RootDir/"$Project"_mclr_ms"$ms".ChFV11
                delete  $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp2
                delete  $CurrDir/"$Project"_mclr_ms"$ms"_first_run.inp
                rm -rf $WorkDir
                echo DELETE  $WorkDir
            fi
        fi
#     else
#            echo The file "$Project"_mclr_ms"$ms"_first_run.out is present in the "$CurrDir". Nothing to do.
#     fi
      fi
   fi
}
