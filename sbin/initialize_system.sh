#!/bin/bash



initialize_system() {
metal=`echo "$1" | tr A-Z a-z`
oxidation_state=$2

echo inside initialize_system function: metal=$metal oxidation_state=$oxidation_state

if [ "$oxidation_state" == "4" ]; then

# 3-rd row transition metals
   if   [ "$metal" == "ti" ]; then
       #  3d^0 configuration
       spinMS=(1);
       rootsCASSCF=(1);
       rootsCASPT2=(1);
       rootsRASSI=(1);
       groundL=(1);
       groundLstr=("1S");
       ActiveElectrons=(0);
       SS_ActiveOrbitals=(0);
       DS_ActiveOrbitals=(0);
       MLTP_in_ANISO=(1);

   elif  [ "$metal" == "v" ]; then
       #  3d^1 configuration
       spinMS=(2);
       rootsCASSCF=(5);
       rootsCASPT2=(5);
       rootsRASSI=(5);
       groundL=(5);
       groundLstr=("2D");
       ActiveElectrons=(1);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(2 2 2 2 2);

   elif [ "$metal" == "cr" ]; then
       #  3d^2 configuration
       spinMS=(3 1);
       rootsCASSCF=(10 15);
       rootsCASPT2=(10 15);
       rootsRASSI=(10 15);
       groundL=(7 3);
       groundLstr=("3F" "3P");
       ActiveElectrons=(2);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(3);

   elif [ "$metal" == "mn" ]; then
       #  3d^3 configuration
       spinMS=(4 2);
       rootsCASSCF=(10 40);
       rootsCASPT2=(10 40);
       rootsRASSI=(10 40);
       groundL=(7 3);
       groundLstr=("4F" "4P");
       ActiveElectrons=(3);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "fe" ]; then
       #  3d^4 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(5 45 50);
       rootsCASPT2=(5 45 50);
       rootsRASSI=(5 45 50);
       groundL=(5);
       groundLstr=("5D");
       ActiveElectrons=(4);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(5);

   elif [ "$metal" == "co" ]; then
       #  3d^5 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(1 24 75);
       rootsCASPT2=(1 24 75);
       rootsRASSI=(1 24 75);
       groundL=(1);
       groundLstr=("6S");
       ActiveElectrons=(5);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(6);

   elif [ "$metal" == "ni" ]; then
       #  3d^6 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(5 45 50);
       rootsCASPT2=(5 45 50);
       rootsRASSI=(5 45 50);
       groundL=(5);
       groundLstr=("5D");
       ActiveElectrons=(6);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(5);

   elif [ "$metal" == "cu" ]; then
       #  3d^7 configuration
       spinMS=(4 2);
       rootsCASSCF=(10 40);
       rootsCASPT2=(10 40);
       rootsRASSI=(10 40);
       groundL=(7 3);
       groundLstr=("4F" "4P");
       ActiveElectrons=(7);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(5);

#lanthanides +4:
   elif [ "$metal" == "ce" ]; then
       # Ce(IV): 4f^0 configuration
       spinMS=(1);
       rootsCASSCF=(1);
       rootsCASPT2=(1);
       rootsRASSI=(1);
       groundL=(1);
       groundLstr=("1S");
       ActiveElectrons=(0);
       SS_ActiveOrbitals=(0);
       DS_ActiveOrbitals=(0);
       MLTP_in_ANISO=(1);

   elif [ "$metal" == "pr" ]; then
       # Pr(IV): 4f^1 configuration
       spinMS=(2);
       rootsCASSCF=(7);
       rootsCASPT2=(7);
       rootsRASSI=(7);
       groundL=(7);
       groundLstr=("2F");
       ActiveElectrons=(1);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2);

   elif [ "$metal" == "nd" ]; then
       # Nd(IV): 4f^2 configuration
       spinMS=(3 1);
       rootsCASSCF=(21 28);
       rootsCASPT2=(21 28);
       rootsRASSI=(21 28);
       groundL=(11 7 3);
       groundLstr=("3H" "3F" "3P");
       ActiveElectrons=(2);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2);

   elif [ "$metal" == "pm" ]; then
       # Pm(IV): 4f^3 configuration
       spinMS=(4 2);
       rootsCASSCF=(35 112);
       rootsCASPT2=(35 112);
       rootsRASSI=(35 112);
       groundL=(13 8 9 5);
       groundLstr=("4I" "4FS" "4G" "4D");
       ActiveElectrons=(3);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "sm" ]; then
       # Sm(IV): 4f^4 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(35 210 196);
       rootsCASPT2=(35 210 196);
       rootsRASSI=(35 210 196);
       groundL=(13 8 9 5);
       groundLstr=("5I" "5FS" "5G" "5D");
       ActiveElectrons=(4);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "eu" ]; then
       # Eu(IV): 4f^5 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(21 224 490);
       rootsCASPT2=(21 224 490);
       rootsRASSI=(21 224 490);
       groundL=(11 7 3);
       groundLstr=("6H" "6F" "6P");
       ActiveElectrons=(5);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "gd" ]; then
       # Gd(IV): 4f^6 configuration
       spinMS=(7 5 3 1);
       rootsCASSCF=(7 140 588 490);
       rootsCASPT2=(7 140 588 490);
       rootsRASSI=(7 140 588 490);
       groundL=(7);
       groundLstr=("7F");
       ActiveElectrons=(6);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);


   elif [ "$metal" == "tb" ]; then
       # Tb(IV):  4f^7 configuration
       spinMS=(8 6 4 2);
       rootsCASSCF=(1 48 392 784);
       rootsCASPT2=(1 48 392 784);
       rootsRASSI=(1 48 392 784);
       groundL=(1);
       groundLstr=("8S");
       ActiveElectrons=(7);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(8);

   elif [ "$metal" == "dy" ]; then
       # Dy(IV):  4f^8 configuration
       spinMS=(7 5 3 1);
       rootsCASSCF=(7 140 588 490);
       rootsCASPT2=(7 140 588 490);
       rootsRASSI=(7 140 588 490);
       groundL=(7);
       groundLstr=("7F");
       ActiveElectrons=(8);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 1);

   elif [ "$metal" == "ho" ]; then
       # Ho(IV):  4f^9 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(21 224 490);
       rootsCASPT2=(21 224 490);
       rootsRASSI=(21 224 490);
       groundL=(11 7 3);
       groundLstr=("6H" "6F" "6P");
       ActiveElectrons=(9);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "er" ]; then
       # Er(IV):  4f^10 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(35 210 196);
       rootsCASPT2=(35 210 196);
       rootsRASSI=(35 210 196);
       groundL=(13 8 9 5);
       groundLstr=("5I" "5FS" "5G" "5D");
       ActiveElectrons=(10);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 1);

   elif [ "$metal" == "tm" ]; then
       # Tm(IV):  4f^11 configuration
       spinMS=(4 2);
       rootsCASSCF=(35 112);
       rootsCASPT2=(35 112);
       rootsRASSI=(35 112);
       groundL=(13 8 9 5);
       groundLstr=("4I" "4FS" "4G" "4D");
       ActiveElectrons=(11);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "yb" ]; then
       # Yb(IV):  4f^12 configuration
       spinMS=(3 1);
       rootsCASSCF=(21 28);
       rootsCASPT2=(21 28);
       rootsRASSI=(21 28);
       groundL=(11 7 3);
       groundLstr=("3H 3F 3P");
       ActiveElectrons=(12);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "lu" ]; then
       # Lu(IV):  4f^13 configuration
       spinMS=(2);
       rootsCASSCF=(7);
       rootsCASPT2=(7);
       rootsRASSI=(7);
       groundL=(7);
       groundLstr=("2F");
       ActiveElectrons=(13);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2);



#actinides +4:
   elif [ "$metal" == "th" ]; then
       # Th(IV): 4f^0 configuration
       spinMS=(1);
       rootsCASSCF=(1);
       rootsCASPT2=(1);
       rootsRASSI=(1);
       groundL=(1);
       groundLstr=("1S");
       ActiveElectrons=(0);
       SS_ActiveOrbitals=(0);
       DS_ActiveOrbitals=(0);
       MLTP_in_ANISO=(1);

   elif [ "$metal" == "pa" ]; then
       # Pa(IV): 4f^1 configuration
       spinMS=(2);
       rootsCASSCF=(7);
       rootsCASPT2=(7);
       rootsRASSI=(7);
       groundL=(7);
       groundLstr=("2F");
       ActiveElectrons=(1);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2);

   elif [ "$metal" == "u" ]; then
       # U(IV): 4f^2 configuration
       spinMS=(3 1);
       rootsCASSCF=(21 28);
       rootsCASPT2=(21 28);
       rootsRASSI=(21 28);
       groundL=(11 7 3);
       groundLstr=("3H" "3F" "3P");
       ActiveElectrons=(2);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2);

   elif [ "$metal" == "np" ]; then
       # Np(IV): 4f^3 configuration
       spinMS=(4 2);
       rootsCASSCF=(35 112);
       rootsCASPT2=(35 112);
       rootsRASSI=(35 112);
       groundL=(13 8 9 5);
       groundLstr=("4I" "4FS" "4G" "4D");
       ActiveElectrons=(3);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "pu" ]; then
       # Pu(IV): 4f^4 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(35 210 196);
       rootsCASPT2=(35 210 196);
       rootsRASSI=(35 210 196);
       groundL=(13 8 9 5);
       groundLstr=("5I" "5FS" "5G" "5D");
       ActiveElectrons=(4);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "am" ]; then
       # Am(IV): 4f^5 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(21 224 490);
       rootsCASPT2=(21 224 490);
       rootsRASSI=(21 224 490);
       groundL=(11 7 3);
       groundLstr=("6H" "6F" "6P");
       ActiveElectrons=(5);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "cm" ]; then
       # Cm(IV): 4f^6 configuration
       spinMS=(7 5 3 1);
       rootsCASSCF=(7 140 588 490);
       rootsCASPT2=(7 140 588 490);
       rootsRASSI=(7 140 588 490);
       groundL=(7);
       groundLstr=("7F");
       ActiveElectrons=(6);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "bk" ]; then
       # Bk(IV):  4f^7 configuration
       spinMS=(8 6 4 2);
       rootsCASSCF=(1 48 392 784);
       rootsCASPT2=(1 48 392 784);
       rootsRASSI=(1 48 392 784);
       groundL=(1);
       groundLstr=("8S");
       ActiveElectrons=(7);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(8);

   elif [ "$metal" == "cf" ]; then
       # Cf(IV):  4f^8 configuration
       spinMS=(7 5 3 1);
       rootsCASSCF=(7 140 588 490);
       rootsCASPT2=(7 140 588 490);
       rootsRASSI=(7 140 588 490);
       groundL=(7);
       groundLstr=("7F");
       ActiveElectrons=(8);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 1);

   elif [ "$metal" == "es" ]; then
       # Es(IV):  4f^9 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(21 224 490);
       rootsCASPT2=(21 224 490);
       rootsRASSI=(21 224 490);
       groundL=(11 7 3);
       groundLstr=("6H" "6F" "6P");
       ActiveElectrons=(9);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "fm" ]; then
       # Fm(IV):  4f^10 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(35 210 196);
       rootsCASPT2=(35 210 196);
       rootsRASSI=(35 210 196);
       groundL=(13 8 9 5);
       groundLstr=("5I" "5FS" "5G" "5D");
       ActiveElectrons=(10);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 1);

   elif [ "$metal" == "md" ]; then
       # Md(IV):  4f^11 configuration
       spinMS=(4 2);
       rootsCASSCF=(35 112);
       rootsCASPT2=(35 112);
       rootsRASSI=(35 112);
       groundL=(13 8 9 5);
       groundLstr=("4I" "4FS" "4G" "4D");
       ActiveElectrons=(11);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "no" ]; then
       # No(IV):  4f^12 configuration
       spinMS=(3 1);
       rootsCASSCF=(21 28);
       rootsCASPT2=(21 28);
       rootsRASSI=(21 28);
       groundL=(11 7 3);
       groundLstr=("3H 3F 3P");
       ActiveElectrons=(12);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "lr" ]; then
       # Lr(IV):  4f^13 configuration
       spinMS=(2);
       rootsCASSCF=(7);
       rootsCASPT2=(7);
       rootsRASSI=(7);
       groundL=(7);
       groundLstr=("2F");
       ActiveElectrons=(13);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2);

   else
      echo "Undefined METAL for oxidation state = $oxidation_state"
   fi











elif [ "$oxidation_state" == "3" ]; then

   if   [ "$metal" == "sc" ]; then
       # Sc(III):  3d^0 configuration
       spinMS=(1);
       rootsCASSCF=(1);
       rootsCASPT2=(1);
       rootsRASSI=(1);
       groundL=(1);
       groundLstr=("1S");
       ActiveElectrons=(0);
       SS_ActiveOrbitals=(0);
       DS_ActiveOrbitals=(0);
       MLTP_in_ANISO=(1);

   elif  [ "$metal" == "ti" ]; then
       # Ti(III):  3d^1 configuration
       spinMS=(2);
       rootsCASSCF=(5);
       rootsCASPT2=(5);
       rootsRASSI=(5);
       groundL=(5);
       groundLstr=("2D");
       ActiveElectrons=(1);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(2 2 2 2 2);

   elif [ "$metal" == "v" ]; then
       # V(III):  3d^2 configuration
       spinMS=(3 1);
       rootsCASSCF=(10 15);
       rootsCASPT2=(10 15);
       rootsRASSI=(10 15);
       groundL=(7 3);
       groundLstr=("3F" "3P");
       ActiveElectrons=(2);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(3);

   elif [ "$metal" == "cr" ]; then
       # Cr(III):  3d^3 configuration
       spinMS=(4 2);
       rootsCASSCF=(10 40);
       rootsCASPT2=(10 40);
       rootsRASSI=(10 40);
       groundL=(7 3);
       groundLstr=("4F" "4P");
       ActiveElectrons=(3);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "mn" ]; then
       # Mn(III):  3d^4 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(5 45 50);
       rootsCASPT2=(5 45 50);
       rootsRASSI=(5 45 50);
       groundL=(5);
       groundLstr=("5D");
       ActiveElectrons=(4);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(5);

   elif [ "$metal" == "fe" ]; then
       # Fe(III):  3d^5 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(1 24 75);
       rootsCASPT2=(1 24 75);
       rootsRASSI=(1 24 75);
       groundL=(1);
       groundLstr=("6S");
       ActiveElectrons=(5);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(6);

   elif [ "$metal" == "co" ]; then
       # Co(III):  3d^6 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(5 45 50);
       rootsCASPT2=(5 45 50);
       rootsRASSI=(5 45 50);
       groundL=(5);
       groundLstr=("5D");
       ActiveElectrons=(6);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(5);

   elif [ "$metal" == "ni" ]; then
       # Ni(III):  3d^7 configuration
       spinMS=(4 2);
       rootsCASSCF=(10 40);
       rootsCASPT2=(10 40);
       rootsRASSI=(10 40);
       groundL=(7 3);
       groundLstr=("4F" "4P");
       ActiveElectrons=(7);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif  [ "$metal" == "cu" ]; then
       # Cu(III): 3d^8 configuration
       spinMS=(3 1);
       rootsCASSCF=(10 15);
       rootsCASPT2=(10 15);
       rootsRASSI=(10 15);
       groundL=(7 3);
       groundLstr=("3F" "3P");
       ActiveElectrons=(8);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(3);

   elif  [ "$metal" == "zn" ]; then
       # Zn(III): 3d^9 configuration
       spinMS=(2);
       rootsCASSCF=(5);
       rootsCASPT2=(5);
       rootsRASSI=(5);
       groundL=(5);
       groundLstr=("2D");
       ActiveElectrons=(9);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(2 2 2 2 2);


# transition metals 4-th row
   elif [ "$metal" == "ru" ]; then
       # Ru(III):  4d^5 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(1 24 75);
       rootsCASPT2=(1 24 75);
       rootsRASSI=(1 24 75);
       groundL=(1);
       groundLstr=("6S");
       ActiveElectrons=(5);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(6);






#lanthanides +3:
   elif [ "$metal" == "la" ]; then
       # La(III): 4f^0 configuration
       spinMS=(1);
       rootsCASSCF=(1);
       rootsCASPT2=(1);
       rootsRASSI=(1);
       groundL=(1);
       groundLstr=("1S");
       ActiveElectrons=(0);
       SS_ActiveOrbitals=(0);
       DS_ActiveOrbitals=(0);
       MLTP_in_ANISO=(1);

   elif [ "$metal" == "ce" ]; then
       # Ce(III): 4f^1 configuration
       spinMS=(2);
       rootsCASSCF=(7);
       rootsCASPT2=(7);
       rootsRASSI=(7);
       groundL=(7);
       groundLstr=("2F");
       ActiveElectrons=(1);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2);

   elif [ "$metal" == "pr" ]; then
       # Pr(III): 4f^2 configuration
       spinMS=(3 1);
       rootsCASSCF=(21 28);
       rootsCASPT2=(21 28);
       rootsRASSI=(21 28);
       groundL=(11 7 3);
       groundLstr=("3H" "3F" "3P");
       ActiveElectrons=(2);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2);

   elif [ "$metal" == "nd" ]; then
       # Nd(III): 4f^3 configuration
       spinMS=(4 2);
       rootsCASSCF=(35 112);
       rootsCASPT2=(35 112);
       rootsRASSI=(35 112);
       groundL=(13 8 9 5);
       groundLstr=("4I" "4FS" "4G" "4D");
       ActiveElectrons=(3);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "pm" ]; then
       # Pm(III): 4f^4 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(35 210 196);
       rootsCASPT2=(35 210 196);
       rootsRASSI=(35 210 196);
       groundL=(13 8 9 5);
       groundLstr=("5I" "5FS" "5G" "5D");
       ActiveElectrons=(4);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "sm" ]; then
       # Sm(III): 4f^5 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(21 224 490);
       rootsCASPT2=(21 224 490);
       rootsRASSI=(21 224 490);
       groundL=(11 7 3);
       groundLstr=("6H" "6F" "6P");
       ActiveElectrons=(5);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "eu" ]; then
       # Eu(III): 4f^6 configuration
       spinMS=(7 5 3 1);
       rootsCASSCF=(7 140 588 490);
       rootsCASPT2=(7 140 588 490);
       rootsRASSI=(7 140 588 490);
       groundL=(7);
       groundLstr=("7F");
       ActiveElectrons=(6);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "gd" ]; then
       # Gd(III):  4f^7 configuration
       spinMS=(8 6 4 2);
       rootsCASSCF=(1 48 392 784);
       rootsCASPT2=(1 48 392 784);
       rootsRASSI=(1 48 392 784);
       groundL=(1);
       groundLstr=("8S");
       ActiveElectrons=(7);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(8);

   elif [ "$metal" == "tb" ]; then
       # Tb(III):  4f^8 configuration
       spinMS=(7 5 3 1);
       rootsCASSCF=(7 140 588 490);
       rootsCASPT2=(7 140 588 490);
       rootsRASSI=(7 140 588 490);
       groundL=(7);
       groundLstr=("7F");
       ActiveElectrons=(8);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 1);

   elif [ "$metal" == "dy" ]; then
       # Dy(III):  4f^9 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(21 224 490);
       rootsCASPT2=(21 224 490);
       rootsRASSI=(21 224 490);
       groundL=(11 7 3);
       groundLstr=("6H" "6F" "6P");
       ActiveElectrons=(9);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "ho" ]; then
       # Ho(III):  4f^10 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(35 210 196);
       rootsCASPT2=(35 210 196);
       rootsRASSI=(35 210 196);
       groundL=(13 8 9 5);
       groundLstr=("5I" "5FS" "5G" "5D");
       ActiveElectrons=(10);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 1);

   elif [ "$metal" == "er" ]; then
       # Er(III):  4f^11 configuration
       spinMS=(4 2);
       rootsCASSCF=(35 112);
       rootsCASPT2=(35 112);
       rootsRASSI=(35 112);
       groundL=(13 8 9 5);
       groundLstr=("4I" "4FS" "4G" "4D");
       ActiveElectrons=(11);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "tm" ]; then
       # Tm(III):  4f^12 configuration
       spinMS=(3 1);
       rootsCASSCF=(21 28);
       rootsCASPT2=(21 28);
       rootsRASSI=(21 28);
       groundL=(11 7 3);
       groundLstr=("3H 3F 3P");
       ActiveElectrons=(12);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "yb" ]; then
       # Yb(III):  4f^13 configuration
       spinMS=(2);
       rootsCASSCF=(7);
       rootsCASPT2=(7);
       rootsRASSI=(7);
       groundL=(7);
       groundLstr=("2F");
       ActiveElectrons=(13);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2);

   elif [ "$metal" == "lu" ]; then
       # Lu(III):  4f^14 configuration
       spinMS=(1);
       rootsCASSCF=(1);
       rootsCASPT2=(1);
       rootsRASSI=(1);
       groundL=(1);
       groundLstr=("1S");
       ActiveElectrons=(0);
       SS_ActiveOrbitals=(0);
       DS_ActiveOrbitals=(0);
       MLTP_in_ANISO=(1);



#actinides +3:
   elif [ "$metal" == "ac" ]; then
       # Ac(III): 4f^0 configuration
       spinMS=(1);
       rootsCASSCF=(1);
       rootsCASPT2=(1);
       rootsRASSI=(1);
       groundL=(1);
       groundLstr=("1S");
       ActiveElectrons=(0);
       SS_ActiveOrbitals=(0);
       DS_ActiveOrbitals=(0);
       MLTP_in_ANISO=(1);

   elif [ "$metal" == "th" ]; then
       # Th(III): 4f^1 configuration
       spinMS=(2);
       rootsCASSCF=(7);
       rootsCASPT2=(7);
       rootsRASSI=(7);
       groundL=(7);
       groundLstr=("2F");
       ActiveElectrons=(1);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2);

   elif [ "$metal" == "pa" ]; then
       # Pa(III): 4f^2 configuration
       spinMS=(3 1);
       rootsCASSCF=(21 28);
       rootsCASPT2=(21 28);
       rootsRASSI=(21 28);
       groundL=(11 7 3);
       groundLstr=("3H" "3F" "3P");
       ActiveElectrons=(2);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2);

   elif [ "$metal" == "u" ]; then
       # U(III): 4f^3 configuration
       spinMS=(4 2);
       rootsCASSCF=(35 112);
       rootsCASPT2=(35 112);
       rootsRASSI=(35 112);
       groundL=(13 8 9 5);
       groundLstr=("4I" "4FS" "4G" "4D");
       ActiveElectrons=(3);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "np" ]; then
       # Np(III): 4f^4 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(35 210 196);
       rootsCASPT2=(35 210 196);
       rootsRASSI=(35 210 196);
       groundL=(13 8 9 5);
       groundLstr=("5I" "5FS" "5G" "5D");
       ActiveElectrons=(4);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "pu" ]; then
       # Pu(III): 4f^5 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(21 224 490);
       rootsCASPT2=(21 224 490);
       rootsRASSI=(21 224 490);
       groundL=(11 7 3);
       groundLstr=("6H" "6F" "6P");
       ActiveElectrons=(5);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "am" ]; then
       # Am(III): 4f^6 configuration
       spinMS=(7 5 3 1);
       rootsCASSCF=(7 140 588 490);
       rootsCASPT2=(7 140 588 490);
       rootsRASSI=(7 140 588 490);
       groundL=(7);
       groundLstr=("7F");
       ActiveElectrons=(6);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "cm" ]; then
       # Cm(III):  4f^7 configuration
       spinMS=(8 6 4 2);
       rootsCASSCF=(1 48 392 784);
       rootsCASPT2=(1 48 392 784);
       rootsRASSI=(1 48 392 784);
       groundL=(1);
       groundLstr=("8S");
       ActiveElectrons=(7);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(8);

   elif [ "$metal" == "bk" ]; then
       # Bk(III):  4f^8 configuration
       spinMS=(7 5 3 1);
       rootsCASSCF=(7 140 588 490);
       rootsCASPT2=(7 140 588 490);
       rootsRASSI=(7 140 588 490);
       groundL=(7);
       groundLstr=("7F");
       ActiveElectrons=(8);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 1);

   elif [ "$metal" == "cf" ]; then
       # Cf(III):  4f^9 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(21 224 490);
       rootsCASPT2=(21 224 490);
       rootsRASSI=(21 224 490);
       groundL=(11 7 3);
       groundLstr=("6H" "6F" "6P");
       ActiveElectrons=(9);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "es" ]; then
       # Es(III):  4f^10 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(35 210 196);
       rootsCASPT2=(35 210 196);
       rootsRASSI=(35 210 196);
       groundL=(13 8 9 5);
       groundLstr=("5I" "5FS" "5G" "5D");
       ActiveElectrons=(10);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 1);

   elif [ "$metal" == "fm" ]; then
       # Fm(III):  4f^11 configuration
       spinMS=(4 2);
       rootsCASSCF=(35 112);
       rootsCASPT2=(35 112);
       rootsRASSI=(35 112);
       groundL=(13 8 9 5);
       groundLstr=("4I" "4FS" "4G" "4D");
       ActiveElectrons=(11);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "md" ]; then
       # Md(III):  4f^12 configuration
       spinMS=(3 1);
       rootsCASSCF=(21 28);
       rootsCASPT2=(21 28);
       rootsRASSI=(21 28);
       groundL=(11 7 3);
       groundLstr=("3H 3F 3P");
       ActiveElectrons=(12);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14)
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "no" ]; then
       # No(III):  4f^13 configuration
       spinMS=(2);
       rootsCASSCF=(7);
       rootsCASPT2=(7);
       rootsRASSI=(7);
       groundL=(7);
       groundLstr=("2F");
       ActiveElectrons=(13);
       SS_ActiveOrbitals=(7);
       DS_ActiveOrbitals=(14);
       MLTP_in_ANISO=(2 2 2 2 2 2 2);

   elif [ "$metal" == "lr" ]; then
       # Lr(III):  4f^14 configuration
       spinMS=(1);
       rootsCASSCF=(1);
       rootsCASPT2=(1);
       rootsRASSI=(1);
       groundL=(1);
       groundLstr=("1S");
       ActiveElectrons=(0);
       SS_ActiveOrbitals=(0);
       DS_ActiveOrbitals=(0);
       MLTP_in_ANISO=(1);

   else
      echo "Undefined METAL for oxidation state = $oxidation_state"
   fi










# other oxiadation states
elif [ "$oxidation_state" == "2" ]; then
echo inside initialize_system function: metal=$metal oxidation_state=$oxidation_state

   if   [ "$metal" == "sc" ]; then
       # Sc(II):  3d^1 configuration
       spinMS=(2);
       rootsCASSCF=(5);
       rootsCASPT2=(5);
       rootsRASSI=(5);
       groundL=(5);
       groundLstr=("2D");
       ActiveElectrons=(1);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(2 2 2 2 2);

   elif [ "$metal" == "ti" ]; then
       # Ti(II):  3d^2 configuration
       spinMS=(3 1);
       rootsCASSCF=(10 15);
       rootsCASPT2=(10 15);
       rootsRASSI=(10 15);
       groundL=(7 3);
       groundLstr=("3F" "3P");
       ActiveElectrons=(2);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(3);

   elif [ "$metal" == "v" ]; then
       # V(II):  3d^3 configuration
       spinMS=(4 2);
       rootsCASSCF=(10 40);
       rootsCASPT2=(10 40);
       rootsRASSI=(10 40);
       groundL=(7 3);
       groundLstr=("4F" "4P");
       ActiveElectrons=(3);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif [ "$metal" == "cr" ]; then
       # Cr(II):  3d^4 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(5 45 50);
       rootsCASPT2=(5 45 50);
       rootsRASSI=(5 45 50);
       groundL=(5);
       groundLstr=("5D");
       ActiveElectrons=(4);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(5);

   elif [ "$metal" == "mn" ]; then
       # Mn(II):  3d^5 configuration
       spinMS=(6 4 2);
       rootsCASSCF=(1 24 75);
       rootsCASPT2=(1 24 75);
       rootsRASSI=(1 24 75);
       groundL=(1);
       groundLstr=("6S");
       ActiveElectrons=(5);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(6);

   elif [ "$metal" == "fe" ]; then
       # Fe(II):  3d^6 configuration
       spinMS=(5 3 1);
       rootsCASSCF=(5 45 50);
       rootsCASPT2=(5 45 50);
       rootsRASSI=(5 45 50);
       groundL=(5);
       groundLstr=("5D");
       ActiveElectrons=(6);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(5);

   elif [ "$metal" == "co" ]; then
       echo inside initialize_system function: metal=$metal oxidation_state=$oxidation_state
       echo 'we are HERE'
       # Co(II):  3d^7 configuration
       spinMS=(4 2);
       rootsCASSCF=(10 40);
       rootsCASPT2=(10 40);
       rootsRASSI=(10 40);
       groundL=(7 3);
       groundLstr=("4F" "4P");
       ActiveElectrons=(7);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(2 2 2 2 2 2 2 2 2 2);

   elif  [ "$metal" == "ni" ]; then
       # Ni(II): 3d^8 configuration
       spinMS=(3 1);
       rootsCASSCF=(10 15);
       rootsCASPT2=(10 15);
       rootsRASSI=(10 15);
       groundL=(7 3);
       groundLstr=("3F" "3P");
       ActiveElectrons=(8);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(3);

   elif  [ "$metal" == "cu" ]; then
       # Cu(II): 3d^9 configuration
       spinMS=(2);
       rootsCASSCF=(5);
       rootsCASPT2=(5);
       rootsRASSI=(5);
       groundL=(5);
       groundLstr=("2D");
       ActiveElectrons=(9);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10);
       MLTP_in_ANISO=(2 2 2 2 2);

   elif  [ "$metal" == "zn" ]; then
       # Cu(II): 3d^10 configuration
       spinMS=(1);
       rootsCASSCF=(1);
       rootsCASPT2=(1);
       rootsRASSI=(1);
       groundL=(1);
       groundLstr=("1S");
       ActiveElectrons=(0);
       SS_ActiveOrbitals=(0);
       DS_ActiveOrbitals=(0);
       MLTP_in_ANISO=(1);







   else
       echo "Undefined METAL for oxidation state = $oxidation_state"
   fi


elif [ "$oxidation_state" == "1" ]; then

   if   [ "$metal" == "ca" ]; then
       spinMS=(2);
       rootsCASSCF=(5);
       rootsCASPT2=(5);
       rootsRASSI=(5);
       groundL=(5);
       groundLstr=("2D");
       ActiveElectrons=("1");
       ActiveOrbitals=("5");
       DS_ActiveOrbitals=(10)
   elif [ "$metal" == "sc" ]; then
       spinMS=(3 1); rootsCASSCF=(10 15); rootsCASPT2=(10 15); rootsRASSI=(10 15); groundL=(7 3); groundLstr=("3F" "3P"); ActiveElectrons=("2"); ActiveOrbitals=("5");
       DS_ActiveOrbitals=(10)
   elif [ "$metal" == "ti" ]; then
       spinMS=(4 2); rootsCASSCF=(10 40); rootsCASPT2=(10 40); rootsRASSI=(10 40); groundL=(7 3); groundLstr=("4F" "4P"); ActiveElectrons=("3"); ActiveOrbitals=("5");
       DS_ActiveOrbitals=(10)
   elif [ "$metal" == "v" ]; then
       spinMS=(5 3 1); rootsCASSCF=(5 45 50); rootsCASPT2=(5 45 50); rootsRASSI=(5 45 50); groundL=(5); groundLstr=("5D"); ActiveElectrons=("4"); ActiveOrbitals=("5");
       DS_ActiveOrbitals=(10)
   elif [ "$metal" == "cr" ]; then
       spinMS=(6 4 2); rootsCASSCF=(1 24 75); rootsCASPT2=(1 24 75); rootsRASSI=(1 24 75); groundL=(1); groundLstr=("6S"); ActiveElectrons=("5"); ActiveOrbitals=("5");
       DS_ActiveOrbitals=(10)
   elif [ "$metal" == "mn" ]; then
       spinMS=(5 3 1); rootsCASSCF=(5 45 50); rootsCASPT2=(5 45 50); rootsRASSI=(5 45 50); groundL=(5); groundLstr=("5D"); ActiveElectrons=("6"); ActiveOrbitals=("5");
       DS_ActiveOrbitals=(10)
   elif [ "$metal" == "fe" ]; then
       spinMS=(4 2); rootsCASSCF=(10 40); rootsCASPT2=(10 40); rootsRASSI=(10 40); groundL=(7 3); groundLstr=("4F" "4P"); ActiveElectrons=("7"); ActiveOrbitals=("5");
       DS_ActiveOrbitals=(10)
   elif [ "$metal" == "co" ]; then
       spinMS=(3 1); rootsCASSCF=(10 15); rootsCASPT2=(10 15); rootsRASSI=(10 15); groundL=(7 3); groundLstr=("3F" "3P"); ActiveElectrons=("8"); ActiveOrbitals=("5");
       DS_ActiveOrbitals=(10)
   elif [ "$metal" == "ni" ]; then
       spinMS=(2);
       rootsCASSCF=(5);
       rootsCASPT2=(5);
       rootsRASSI=(5);
       groundL=(5);
       groundLstr=("2D");
       ActiveElectrons=(9);
       SS_ActiveOrbitals=(5);
       DS_ActiveOrbitals=(10)
   else
      echo "Undefined METAL for oxidation state = $oxidation_state"
   fi


else
   echo "Undefined oxidation state"
fi # oxidation_state


#----- final setup of other variables:
active_electrons=${ActiveElectrons[0]}
active_orbitals=${SS_ActiveOrbitals[0]}

#------ MOLCAS related variables -------#
# initialize MOLCAS variables
# determine the total number of cores available
n_procs=$( getconf _NPROCESSORS_ONLN )

export MOLCAS_PRINT=3
export MOLCAS_OUTPUT='WORKDIR'
export MOLCAS_TIME='YES'
export MOLCAS_SOURCE=$MOLCAS
export MOLCAS_NPROCS=$n_procs
#if [[ $MOLCAS_MEM -le 10000 ]]; then
#    echo MOLCAS_MEM initialize = $MOLCAS_MEM
#    export MOLCAS_MEM=10000
#fi
export imag_caspt2_shift="0.1"
export ipea_caspt2_shift="0.0"

}


