#!/bin/bash

run_rassi(){

local kind=$1
declare -a spinMults=("${!2}")
declare -a roots_in_RASSI=("${!3}")
#echo ${spinMults[*]}
#echo ${roots_in_RASSI[*]}

#  check_previous_runs
local rassi_old_run=0
if [ -f $CurrDir/"$Project"_rassi_"$kind".out ] && [ -f $FileDir/"$Project"_rassi_"$kind".RunFile ]; then
    rassi_old_run=`grep 'Stop Module:' $CurrDir/"$Project"_rassi_"$kind".out | grep 'rassi ' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
fi

if [ ! -f $CurrDir/"$Project"_rassi_"$kind".out ] || [ "$rassi_old_run" -ne 1 ]; then
# proceed to a new RASSI run:
     delete $FileDir/"$Project"_rassi_"$kind".RunFile
     delete $FileDir/"$Project"_rassi_"$kind".h5
     delete $CurrDir/"$Project"_rassi_"$kind".inp

     cat <<EOF > $CurrDir/"$Project"_rassi_"$kind".inp
&RASSI
OMEG
J-VA
MEES
Prop
6
AngMom  1
AngMom  2
AngMom  3
MLTPL1  1
MLTPL1  2
MLTPL1  3
SpinOrbit
NR  OF  JOBIPHS
${#roots_in_RASSI[*]}  ` printf ' %s' "${roots_in_RASSI[@]}" `
EOF
     for i in ${!spinMults[*]}; do
         echo $(seq ${roots_in_RASSI[$i]})  >> $CurrDir/"$Project"_rassi_"$kind".inp
     done
     echo "EJOB"         >> $CurrDir/"$Project"_rassi_"$kind".inp
     echo "End Of Input" >> $CurrDir/"$Project"_rassi_"$kind".inp
     export WorkDir=$RootDir

# a bit of cleanup
     if [ "$cluster" == "NSCC" ] ; then
         if [ -f $CurrDir/"$Project"_rassi_"$kind".sh ] ; then rm -rf $CurrDir/"$Project"_rassi_"$kind".sh ; fi
         echo "#!/bin/sh"                               > $CurrDir/"$Project"_rassi_"$kind".sh
             echo "#PBS -q $queue"                     >> $CurrDir/"$Project"_rassi_"$kind".sh
         if [ ! -z "$ProjectID" ] ; then
            echo "#PBS -P $ProjectID"                  >> $CurrDir/"$Project"_rassi_"$kind".sh
         else
            echo "#PBS -P Personal"                    >> $CurrDir/"$Project"_rassi_"$kind".sh
         fi
         echo "#PBS -l walltime=$walltime:00:00"       >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "#PBS -l select=$no_of_cores:ncpus=$no_of_ncpus:mpiprocs=$no_of_mpiprocs:mem=$memory" >> $CurrDir/"$Project"_rassi_"$kind".sh
             echo "#PBS -j oe"                         >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "#PBS -N "$Project"_rassi_"$kind""       >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo ""                                       >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "module load python/3.5.1"               >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "module load hdf5/1.8.16/xe_2016/serial" >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo ""                                       >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "export CurrDir=$CurrDir"                >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "export Project=$Project"                >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "export FileDir=$FileDir"                >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "export WorkDir=$WorkDir"                >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "export MOLCAS=$MOLCAS"                  >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "export MOLCAS_MEM=$MOLCAS_MEM"          >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo ""                                       >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "if [ ! -d $WorkDir ]"                   >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "then"                                   >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "   mkdir -p $WorkDir"                   >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "fi"                                     >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "cd $WorkDir"                            >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo ""                                       >> $CurrDir/"$Project"_rassi_"$kind".sh

         for i in ${!roots_in_RASSI[*]}; do
                       ims=${spinMults[$i]}
                       inum=$(( $i + 1 ))
               echo "cp $FileDir/"$Project"_ms"$ims".JobIph  $WorkDir/JOB00"$inum""     >> $CurrDir/"$Project"_rassi_"$kind".sh
               echo "cp $FileDir/"$Project"_ms"$ims".RunFile $WorkDir/$Project.RunFile" >> $CurrDir/"$Project"_rassi_"$kind".sh
         done

         echo ""                                       >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "pymolcas $CurrDir/"$Project"_rassi_"$kind".inp > $CurrDir/"$Project"_rassi_"$kind".out 2> $CurrDir/"$Project"_rassi_"$kind".err " >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo ""                                       >> $CurrDir/"$Project"_rassi_"$kind".sh
         echo "exit $?"                                >> $CurrDir/"$Project"_rassi_"$kind".sh
     else
         delete $WorkDir/JOB001
         delete $WorkDir/JOB002
         delete $WorkDir/JOB003
         delete $WorkDir/JOB004
         delete $WorkDir/JOB005
         delete $WorkDir/JOB006
         delete $WorkDir/JOB007
         delete $WorkDir/JOB008
         delete $WorkDir/JOB009
         delete $WorkDir/$Project.rassi.h5
         delete $WorkDir/$Project.JobIph
         delete $WorkDir/$Project.JobMix

         if [[ $kind == "cas" ]]; then
            for i in ${!roots_in_RASSI[*]}; do
               ims=${spinMults[$i]}
               inum=$(( $i + 1 ))
#link  $FileDir/"$Project"_ms"$ims".JobIph  $WorkDir/JOB00"$inum"
#link  $FileDir/"$Project"_ms"$ims".RunFile $WorkDir/$Project.RunFile
               copy $FileDir/"$Project"_ms"$ims".JobIph  $WorkDir/JOB00"$inum"
               copy $FileDir/"$Project"_ms"$ims".RunFile $WorkDir/$Project.RunFile
            done
         elif [[ $kind == "ss_pt2" ]] || [[ $kind == "ms_pt2" ]] || [[ $kind == "xms_pt2" ]] || [[ $kind == "rcp_pt2" ]] ; then
            echo Performing rassi "$PT2_TYPE"_pt2
            for i in ${!roots_in_RASSI[*]}; do
               ims=${spinMults[$i]}
               inum=$(( $i + 1 ))
               copy $FileDir/"$Project"_"$PT2_TYPE"_ms"$ims".JobMix   $WorkDir/JOB00"$inum"
               copy $FileDir/"$Project"_"$PT2_TYPE"_ms"$ims".RunFile  $WorkDir/$Project.RunFile
#link  $FileDir/"$Project"_"$PT2_TYPE"_ms"$ims".JobMix    $WorkDir/JOB00"$inum"
#link  $FileDir/"$Project"_"$PT2_TYPE"_ms"$ims".RunFile   $WorkDir/$Project.RunFile
            done
         else
            echo "Unknown type of the RASSI run... exitting"
            return 2
         fi

         # run RASSI -----------------
         cd $WorkDir
         pymolcas $CurrDir/"$Project"_rassi_"$kind".inp  > \
                  $CurrDir/"$Project"_rassi_"$kind".out  2> \
                  $CurrDir/"$Project"_rassi_"$kind".err
         # end run RASSI -----------------
         local rassi_return_code=0
         rassi_return_code=`grep 'Stop Module:' $CurrDir/"$Project"_rassi_"$kind".out | grep 'rassi ' | grep 'rc=_RC_ALL_IS_WELL_' |  wc -l`
         echo RASSI_return_code= $rassi_return_code

         if [ $? -eq 0 ] && [ "$rassi_return_code" -eq 1 ] ; then
         # a bit of saving & clean-up
            copy $WorkDir/$Project.rassi.h5  $FileDir/"$Project"_rassi_"$kind".h5
            copy $WorkDir/$Project.RunFile   $FileDir/"$Project"_rassi_"$kind".RunFile
            delete $CurrDir/"$Project"_rassi_"$kind".inp
            delete $CurrDir/"$Project"_rassi_"$kind".xml
            delete $WorkDir/JOB001
            delete $WorkDir/JOB002
            delete $WorkDir/JOB003
            delete $WorkDir/JOB004
            delete $WorkDir/JOB005
            delete $WorkDir/JOB006
            delete $WorkDir/JOB007
            delete $WorkDir/JOB008
            delete $WorkDir/JOB009
            delete $WorkDir/$Project.rassi.h5
            delete $WorkDir/$Project.JobIph
            delete $WorkDir/$Project.JobMix
         else
            echo "RASSI did not finish well. Please check.   "
            echo "No important files (like *.rassi.h5) were saved into $FileDir"
            return $?
         fi
     fi

else
     echo The "$Project"_rassi_"$kind".out is present in the "$CurrDir". Nothing to do.
     echo "If not satisfied, remove the following files or folders and resubmit"
     echo $CurrDir/"$Project"_rassi_"$kind".out
fi

}


