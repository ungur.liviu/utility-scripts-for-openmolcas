This is a collection of scripts setting the environment and automatization of
OpenMolcas calculations for various tasks, like CASSCF/RASSI/SINGLE_ANISO,
CASSCF/CASPT2/RASSI/SINGLE_ANISO as well as the evaluation of the analytic
or numerical vibronic couplings for any molecule is done in a parallel
fashion efficiently, even on a single multi-core node, with limited scratch
space and memory.

Among the most important features are:
 - automated setup of all required inputs
 - parallel execution of CASSCF calculations for different spin states
 - parallel (per-root) execution of the CASPT2
 - parallel evaluation of molecular gradients and non-adiabatic couplings

All these tasks are re-using the common RICD / ERI integrals and other (large) files
inside OpenMolcas $WorkDir as much as possible, without duplication or making redundant copies.

Make sure to use a "serial" version of the OpenMolcas.

How to use the scripts?

1) Clone the repository:
```
git clone https://gitlab.com/ungur.liviu/utility-scripts-for-openmolcas.git    $HOME/<path of your choice>/USOM.git

```

2) Update your $PATH variable to include the "sbin" folder (so that Linux environment can find and execute these functions)
```
export PATH=$HOME/<path of your choice>/USOM.git/sbin:$PATH

```

3) Use (and modify accordingly) the  `run_script.sh` as the main script to prepare a computational project.
Make sure you have the XYZ file of your molecule and you know the total charge.


