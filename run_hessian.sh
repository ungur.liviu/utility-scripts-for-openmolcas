#!/bin/bash


Complex="CoO"                                                    #  define here your complex, the core name for output and data files.
CleanWorkDir="no"                                                #  set it to "yes" in case you wish that $WorkDir to be cleaned-up (i.e. to erase everything)
metal="Co"                                                       #  define here the chemical symbol of the main metal in your computed system (where your active orbitals are localized)
oxidation_state=2                                              #  defines the oxidation state (valence) of the main metal site
total_charge=0                                                 #
frozen_orbitals=0
inactive_orbitals=14                                           #  define the number of inactive orbitals
magnetism="yes"                                                  #  defines if the M=f(H,T) is to be computed for single_aniso
analytical_gradient_calculation="no"
number_of_procs_for_caspt2=8                                   #  defines the numbe of available processors (i.e. cores) to run the calculations in parallel
                                                                 #  if this line is commented, the script will use all available cores
number_of_procs_for_caspt2=3                                   #  defines the numbe of available processors (i.e. cores) to run the calculations in parallel
number_of_procs_for_alaska=5                                   #  defines the numbe of available processors (i.e. cores) to run the calculations in parallel
run_caspt2_on_separate_nodes="no"                                #  "no =  is also when this string is undefined"
run_alaska_on_separate_nodes="no"                                #  "no =  is also when this string is undefined"
numerical_gradient="no"
numerical_hessian="yes"
cluster="local"                                                  #  define here the name of the cluster and generate a new function which will
                                                                 #  produce a simple submitting script (only the PBS commands) for your caspt2 run
                                                                 #  so far only NSCC and HPC are defined
#------------------------------------------------------------------------------#
export CurrDir=`pwd`   # =$PBS_O_WORKDIR                         #  defines the path to the folder where this calculation is submitted from
export FileDir=$CurrDir/trash                                    #  defines the path to the folder where all calculations extra files will be stored
export RootProject=$Complex                                      #  user-defined name for the entire project ...
export ScratchDir=/home/scratch/liviu/$Project                   #  scratch folder where all the claculations will take place
export MOLCAS=$HOME/bin/OpenMolcas_15Jan2025_gcc142_obl_serial   #  use a SERIAL installation of OpenMolcas. Parallel OpenMolcas installation
export USOM=/Users/liviuungur/Programs/utility-scripts-for-openmolcas.git
export PATH=$USOM:$PATH
export MOLCAS_MEM=5000
export MOLCAS_CPUS=1
export MOLCAS_NPROCS=1
#============================================================================================a=
. $USOM/run_openmolcas_util
# initalize default values...
initialize_system $metal $oxidation_state
echo "after initialisation"

# user-defined values for spin multiplicities and  number of states
# the dafalut will include ALL possible states for the given d^N or  f^N configuration of the defined
# metal ion in its oxidation state
export ras1_o=0
export ras3_o=0
export holes_ras1=0
export electrons_ras3=0
export cutoff_dist="3.0"
export PT2_TYPE="xms"                                            # may use the following types:  "ss"= state specific; "ms"=multistate; "xms"=extended multistate; "dw"=dynamically weighted

active_electrons=(7)
active_orbitals=(5)
#spinMS=(8 6 4 2)
#rootsCASSCF=(1 48 129)
#rootsCASPT2=(35) #21 128 130)
#rootsRASSI=( ) #21 128 130)
#groundL=(11 7 3)
active_space="$active_electrons"in"$active_orbitals"

NOMULT="false"
do_caspt2="false"
do_restart="EXPBAS"
#same_multiplet="true"
#supsym="1;  16   29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 "

queue="normal"
walltime=24
no_of_cores=1
no_of_ncpus=12
no_of_mpiprocs=12
memory="40GB"


#=============================================================================================
#                   MAIN PART   define the major loops ove various variables
#--------------------------------------------------------------------------------------------
cd $CurrDir
if [ ! -d "$FileDir" ]; then mkdir $FileDir ; fi
if [ ! -d "$ScratchDir" ]; then  mkdir -p -v "$ScratchDir" ; fi ;

echo 'active electrons  ='$active_electrons
echo 'active orbitals   ='$active_orbitals

if [ "$numerical_hessian" == "yes" ] ; then
    echo "run first a standard molcas single point calculation"
    my_molcas_run $Complex mb  mb  $metal $active_electrons  $active_orbitals &
    wait

    echo "Entering hessian_by_shifting_atoms"
    hessian_by_shifting_atoms $Complex  mb  mb  $metal $active_electrons  $active_orbitals  2
    wait
    exit
fi

wait
#--------------------------------------------------------------------------------------------


# clean-up all error files, in case all calculations finished ok
if [ $? -eq 0 ]; then
   rm -rf $CurrDir/*.err
   rm -rf $CurrDir/*.xml
   rm -rf $CurrDir/*.status
fi
exit 0

